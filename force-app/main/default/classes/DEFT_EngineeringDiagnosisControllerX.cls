/*-------------------------------------------------------------------------------------------------
Author     : Marcel Brimus
             Nikhil Shanbhag
Company    : IBM
Description: Controller Extension for Engineering Diagnosis form
Inputs     : N/A
Test Class : DEFT_EngineeringDiagnosisControllerXTest
History    :      
---------------------------------------------------------------------------------------------------*/ 
public class DEFT_EngineeringDiagnosisControllerX {

    @TestVisible private final HOG_Settings__c hogSettings;
    @TestVisible private final HOG_Engineering_Diagnosis__c eDiagnosis;
    public HOG_Service_Rig_Program__c serviceRigProgram {get;private set;}
    public HOG_EOJ__c endOfJobReport {get;private set;}

    public Boolean displayForm{
      get{
        if(displayForm == null)
          displayForm = true;
        return displayForm;
      }
      private set;
    }

    public DEFT_EngineeringDiagnosisControllerX(ApexPages.StandardController stdController) 
    {   
        this.eDiagnosis = (HOG_Engineering_Diagnosis__c)stdController.getRecord();
        hogSettings = HOG_Settings__c.getInstance(); 
        
        if(eDiagnosis.Service_Rig_Program__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'Engineering Diagnosis can be created only through Service Rig Program.'));
            displayForm = false;
        } else {
            serviceRigProgram = retrieveOneServiceRigProgram(); 
            initializeEndOfJobReport();    
           
            //This sets picklist values for ED picklists only when ED is first created
            //Those two picklists are required in vf page 
            if(endOfJobReport != null && eDiagnosis.Service_Rig_Program__c != null 
				&& eDiagnosis.Diagnosis_General__c == null
				&& eDiagnosis.Diagnosis_Detail__c == null)
            {
                initializeEDPicklists();
                
                /* SPR-EOJ-ED reporting story */
                //populate EOJ lookup on ED
                eDiagnosis.End_Of_Job__c = endOfJobReport.Id;
          }          
        }
    } 
    
    public HOG_Service_Rig_Program__c retrieveOneServiceRigProgram() 
    {                         
        return
          [
              Select 
              Id,
              Well_Location__c,
              Well_Event_Lookup__c,
              Operating_Field_AMU__c,
              Well_Location__r.Route__c,
              Well_Location__r.Well_Type__c,
              Well_Location__r.Well_Orientation__c,
              Current_Well_Status__c,
              H2S__c,
              Notification_Number__c,
              HOG_Service_Request_Notification_Form__c,
              Service_Type__c,
              Service_Rig_Planner__c,
              Priority__c,
              Service_Rig_Request_Reason__c,
              Status__c
              From HOG_Service_Rig_Program__c
              Where id = :eDiagnosis.Service_Rig_Program__c
              Limit 1
           ];                                              
    }
    
    public HOG_EOJ__c retrieveOneEndOfJobReport() 
    {   
        try {
            return
              [
                  Select 
                  Id,
                  Name,
                  Status__c,
                  Previous_Service_Date__c,
                  Service_Started__c,
                  Budget_SRP__c,
                  Service_Completed__c,
                  Final_Cost__c,
                  Service_General__c,
                  Cost_Variance__c,
                  Service_Detail__c,
                  Job_Cost_Comments__c,
                  EOJ_Details__c,
                  Well_Event_PVR_UWI_RAW__c
                  From HOG_EOJ__c 
                  Where Status__c = 'Submitted' And Service_Rig_Program__c = :serviceRigProgram.Id
                  Limit 1	//this is based on fact, that SRP has 1 (ONLY ONE) EOJ
               ];
        } Catch (Exception e) {
            return null;
        }
    }

    public List<Schema.PicklistEntry> retrievePickListValues(String objectName, String fieldName)
    {

      Schema.SObjectType globalDescribe = Schema.getGlobalDescribe().get(objectName);
      if(globalDescribe.getDescribe().fields.getMap().get(fieldName) != null) 
      {
              Schema.DescribeFieldResult fieldResult = globalDescribe.getDescribe().fields.getMap().get(fieldName).getDescribe();
              List<Schema.PicklistEntry> picklistElements = fieldResult.getPicklistValues(); 
              return picklistElements;
      }

        return null;
    }
    
    public boolean generalPicklistsMatch (){
         List<Schema.PicklistEntry> eoj_serviceGeneralPL = retrievePickListValues('HOG_EOJ__c', 'Service_General__c');
         List<Schema.PicklistEntry> engDiagGeneralPL = retrievePickListValues('HOG_Engineering_Diagnosis__c', 'Diagnosis_General__c');
        
         Set<Schema.PicklistEntry> eoj_serviceGeneralPLSet = new Set<Schema.PicklistEntry>(eoj_serviceGeneralPL);
                 Set<Schema.PicklistEntry> engDiagGeneralPLSet = new Set<Schema.PicklistEntry>(engDiagGeneralPL);

                 for(Schema.PicklistEntry o : eoj_serviceGeneralPLSet)
                 {
                    if(!engDiagGeneralPLSet.contains(o))
                    {
                      return false;
                    }
                 }
        
        return true;
    }
    
    public boolean detailPicklistMatch (){
        List<Schema.PicklistEntry> eoj_serviceDetailPL = retrievePickListValues('HOG_EOJ__c', 'Service_Detail__c');
                List<Schema.PicklistEntry> engDiagDetailPL = retrievePickListValues('HOG_Engineering_Diagnosis__c', 'Diagnosis_Detail__c');
        
        Set<Schema.PicklistEntry> eoj_serviceDetailPLSet = new Set<Schema.PicklistEntry>(eoj_serviceDetailPL);
        Set<Schema.PicklistEntry> engDiagDetailPLSet = new Set<Schema.PicklistEntry>(engDiagDetailPL);
        
        for(Schema.PicklistEntry o : eoj_serviceDetailPLSet)
        {
            if(!engDiagDetailPLSet.contains(o))
            {
                return false;
            }
        }
        
        return true;
    }
    
    public void initializeEndOfJobReport(){
        endOfJobReport = retrieveOneEndOfJobReport();
        if(endOfJobReport == null) {
             //In case somebody deletes End of Job report that has been submitted on Aproved Service Rig Program
            //At this moment we dont have any mechanism in place to go back to previous record type thus removing
            //engineering diagnosis related list on layout. So to prevent user from creating Engineering diagnosis
            //we present this message
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,'End of job report of this Service Rig Program is missing.'));
            displayForm = false;
          }
    }
    
    public void initializeEDPicklists(){
         if(generalPicklistsMatch () == true && detailPicklistMatch () == true) 
         {
             eDiagnosis.Diagnosis_General__c = endOfJobReport.Service_General__c;
             eDiagnosis.Diagnosis_Detail__c = endOfJobReport.Service_Detail__c;
             
         }
    }
    
    public PageReference saveED(){
    
        if(eDiagnosis.Status__c.contains('Complete') && eDiagnosis.Diagnosis_Completed_Date__c == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Please fill in the Diagnosis Complete Date '));            
        }
        if(!ApexPages.hasMessages()){
            upsert eDiagnosis;
            PageReference detailPage = new PageReference('/apex/DEFT_EngineeringDiagnosisView?id='+eDiagnosis.Id);
            detailPage.setRedirect(true);
            return detailPage;
        }
        
        return null;
    }

    //This function calls out to the SAP PI service for 
    //PVR Data for the date range Previous 
    public PageReference updatePVRProductionValues() {
      if(endOfJobReport.Previous_Service_Date__c == null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,
          'Previous service date not specified on End of Job report!'));
        return null;
      }

      if(endOfJobReport.Service_Started__c == null) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,
          'Service started not specified on End of Job report!'));
        return null;
      }

      //Create Request Body
      DEFT_Utilities.PVRProductionValuesRequest pvrRequest =  
        new DEFT_Utilities.PVRProductionValuesRequest(endOfJobReport.Previous_Service_Date__c, endOfJobReport.Service_Started__c, 
          endOfJobReport.Well_Event_PVR_UWI_RAW__c);

      //Create Request
      try {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        System.debug(hogSettings.SAP_DPVR_Endpoint__c);
        request.setEndpoint(hogSettings.SAP_DPVR_Endpoint__c);
        request.setClientCertificateName(hogSettings.Client_Certificate_Name__c);

        //Authorization Header (dahodh:Cricket2016)
        Blob headerValue = Blob.valueOf(hogSettings.SAP_DPVR_Username__c + ':' + hogSettings.SAP_DPVR_Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        request.setHeader('Authorization', authorizationHeader);

        request.setMethod('POST');
        request.setBody(JSON.serialize(pvrRequest));
        request.setHeader('Content-Type', 'application/json');
        HttpResponse response = http.send(request);

        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            // Deserialize into response object
            DEFT_Utilities.PVRProductionValuesResponse pvrResponse = (DEFT_Utilities.PVRProductionValuesResponse) JSON.deserialize(response.getBody(), 
              DEFT_Utilities.PVRProductionValuesResponse.class);
            if(pvrResponse != null && pvrResponse.pvrUwiRaw == endOfJobReport.Well_Event_PVR_UWI_RAW__c) {
              this.eDiagnosis.Total_Oil_Production_Between_Services__c = pvrResponse.producedOilBetweenServices;
              this.eDiagnosis.Total_Water_Production_Between_Services__c = pvrResponse.producedWaterBetweenServices;
              this.eDiagnosis.Total_Gas_Production_Between_Services__c = pvrResponse.producedGasBetweenServices;
              this.eDiagnosis.Total_Sand_Production_Between_Services__c = pvrResponse.producedSandBetweenServices;
              this.eDiagnosis.PVR_Runtime__c = pvrResponse.operatingHoursBetweenServices/24;

              //update record
              try {
                update this.eDiagnosis;
              } catch (DmlException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,
                  'Error updating record: ' + ex.getMessage()));
              }
            } else {
                if(String.isBlank(pvrResponse.pvrUwiRaw)) {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 
                    'No records founds for the input data.'));
                } else {
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info, 
                    'Invalid response from PVR Service'));
                }
            }
            
        } else {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 
            'Received following error code from PVR service: ' + response.getStatusCode()));
        }
      } catch (CalloutException ex) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,
          ex.getMessage()));
      }


      return null;
    }
}