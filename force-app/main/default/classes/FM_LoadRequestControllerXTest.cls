@isTest
private class FM_LoadRequestControllerXTest {

	@isTest static void loadRequestControllerX_Facility() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_Field_Operator');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		Id facilityId = [SELECT Id FROM Facility__c LIMIT 1].get(0).Id;
		Id routeId = [SELECT Id FROM Route__c LIMIT 1].get(0).Id;

		PageReference pageRef = Page.FM_AddLoadRequest;
		Test.setCurrentPage(pageRef);
		Test.setCurrentPageReference(pageRef);
		System.currentPageReference().getParameters().put('facilityid', facilityId);
		System.currentPageReference().getParameters().put('routeid', routeId);

		FM_Load_Request__c lr = new FM_Load_Request__c();
		ApexPages.StandardController stdController = new ApexPages.StandardController(lr);

		System.runAs(runningUser) {
			FM_LoadRequestControllerX controller = new FM_LoadRequestControllerX(stdController);
			System.assertEquals(null, controller.selectedLocationId);
			System.assertEquals('facility', controller.sourceType);
			System.assertEquals(facilityId, controller.selectedFacilityId);
			System.assertEquals(routeId, controller.selectedRouteId);
			System.assertEquals(null, controller.selectedLocation);
			System.assertEquals(null, controller.selectedFacility);
			System.assertEquals(null, controller.sourceH2S);
			System.assertEquals(2, controller.sourceTypeOptions.size());
			System.assertEquals(null, controller.locationOptions);
			System.assertEquals(1, controller.facilityOptions.size());
			System.assertEquals(1, controller.routeOptions.size());
			System.assertEquals(null, controller.getSourceData());
			System.assertEquals(null, controller.selectedLocation);
			System.assertNotEquals(null, controller.selectedFacility);
			System.assertEquals(false, controller.sourceH2S);
			System.assertEquals(null, controller.RefreshLocationList());
			System.assertEquals(null, controller.selectedFacilityId);
			System.assertEquals(null, controller.selectedLocationId);
			System.assertEquals(null, controller.NoAction());
		}
	}

	@isTest static void loadRequestControllerX_Static() {
		System.assertEquals(FM_Utilities.RUNSHEET_VALUE_NOT_SELECTED, FM_LoadRequestControllerX.getNotSelected());
	}

	@isTest static void loadRequestControllerX_Location() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_Field_Operator');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		Id locationId = [SELECT Id FROM Location__c LIMIT 1].get(0).Id;
		Id routeId = [SELECT Id FROM Route__c LIMIT 1].get(0).Id;

		PageReference pageRef = Page.FM_AddLoadRequest;
		Test.setCurrentPage(pageRef);
		Test.setCurrentPageReference(pageRef);
		System.currentPageReference().getParameters().put('wellid', locationId);
		System.currentPageReference().getParameters().put('routeid', routeId);

		FM_Load_Request__c lr = new FM_Load_Request__c();
		ApexPages.StandardController stdController = new ApexPages.StandardController(lr);

		System.runAs(runningUser) {
			FM_LoadRequestControllerX controller = new FM_LoadRequestControllerX(stdController);
			System.assertEquals(locationId, controller.selectedLocationId);
			System.assertEquals(null, controller.selectedFacilityId);
			System.assertEquals('location', controller.sourceType);
			System.assertEquals(routeId, controller.selectedRouteId);
			System.assertEquals(null, controller.selectedLocation);
			System.assertEquals(null, controller.selectedFacility);
			System.assertEquals(null, controller.sourceH2S);
			System.assertEquals(2, controller.sourceTypeOptions.size());
			System.assertEquals(1, controller.facilityOptions.size());
			System.assertEquals(1, controller.routeOptions.size());
			System.assertEquals(null, controller.getSourceData());
			System.assertNotEquals(null, controller.selectedLocation);
			System.assertEquals(null, controller.selectedFacility);
			System.assertEquals(false, controller.sourceH2S);
			System.assertEquals(null, controller.RefreshLocationList());
			System.assertEquals(null, controller.selectedFacilityId);
			System.assertEquals(null, controller.selectedLocationId);
			System.assertEquals(1, controller.locationOptions.size());
			System.assertEquals(null, controller.NoAction());
		}
	}

	@isTest static void loadRequestControllerX_null() {
		User runningUser = createUser();
		runningUser = assignPermissionset(runningUser, 'HOG_Field_Operator');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		PageReference pageRef = Page.FM_AddLoadRequest;
		Test.setCurrentPage(pageRef);

		FM_Load_Request__c lr = new FM_Load_Request__c();
		ApexPages.StandardController stdController = new ApexPages.StandardController(lr);

		System.runAs(runningUser) {
			FM_LoadRequestControllerX controller = new FM_LoadRequestControllerX(stdController);
			System.assertEquals(null, controller.selectedLocationId);
			System.assertEquals(null, controller.selectedFacilityId);
			System.assertEquals(null, controller.selectedRouteId);
			System.assertEquals(null, controller.getSourceData());
			System.assertEquals(null, controller.selectedLocation);
			System.assertEquals(null, controller.selectedFacility);
			System.assertEquals(false, controller.sourceH2S);
		}
	}

	@testSetup
	static void testSetup() {
		Account acc = new Account();
		acc.Name = 'MI6 Account';
		insert acc;

		Carrier__c carrier = new Carrier__c();
		carrier.Carrier__c = acc.Id;
		carrier.Carrier_Name_For_Fluid__c = 'James Bond';
		insert carrier;

		Carrier_Unit__c carrierUnit = new Carrier_Unit__c();
		carrierUnit.Carrier__c = carrier.Id;
		carrierUnit.Unit_Email__c = 'bond@jamesbond.com';
		carrierUnit.Name = 'Bond1';
		carrierUnit.Enabled__c = true;
		insert carrierUnit;

		Business_Department__c business = new Business_Department__c();
		business.Name = 'Husky Business';
		insert business;

		Operating_District__c district = new Operating_District__c();
		district.Name = 'District 9';
		district.Business_Department__c = business.Id;
		insert district;

		Field__c field = new Field__c();
		field.Name = 'AMU Field';
		field.Operating_District__c = district.Id;
		insert field;

		Route__c route = new Route__c();
		route.Fluid_Management__c = true;
		route.Name = '007';
		route.Route_Number__c = '007';
		insert route;

		Location__c loc = new Location__c();
		loc.Name = 'London';
		loc.Surface_Location__c = 'London';
		loc.Fluid_Location_Ind__c = false;
		loc.Location_Name_for_Fluid__c = 'London HQ';
		loc.Operating_Field_AMU__c = field.Id;
		loc.Route__c = route.Id;
		loc.Status__c = 'PROD';
		loc.Well_Type__c = 'OIL';
		loc.Functional_Location_Category__c	= 4;
		insert loc;

		Equipment__c eq = EquipmentTestData.createEquipment(loc);
		eq.Equipment_Category__c = 'D';
		eq.Object_Type__c = 'VESS_ATMOS';
		update eq;

		Equipment_Tank__c eqt = new Equipment_Tank__c();
		eqt.Equipment__c = eq.id;
		eqt.Low_Level__c = 10;
		eqt.Tank_Size_m3__c = 30;
		eqt.Tank_Label__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		insert eqt;

		Facility__c fac = new Facility__c();
		fac.Name = 'Brezno';
		fac.Facility_Name_for_Fluid__c = 'Brezno Facilty';
		fac.Fluid_Facility_Ind__c = true;
		fac.Fluid_Facility_Ind_Origin__c = true;
		fac.Operating_Field_AMU__c = field.Id;
		fac.Functional_Location_Category__c	= 3;
		// fac.Plant_Section__c = route.id;
		insert fac;
	}

	private static User createUser() {
		User user = new User();
		Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

		Double random = Math.Random();

		user.Email = 'TestUser' +  random + '@hog.com';
		user.Alias = 'TestUser' ;
		user.EmailEncodingKey = 'UTF-8';
		user.LastName = 'User';
		user.LanguageLocaleKey = 'en_US';
		user.LocaleSidKey = 'en_US';
		user.ProfileId = p.Id;
		user.TimeZoneSidKey = 'America/Los_Angeles';
		user.UserName = 'TestUser' + random + '@hog.com.unittest';

		insert user;
		return user;
	}

	private static User assignPermissionSet(User user, String userPermissionSetName){
		 PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName];
		 PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();

		 permissionSetAssignment.PermissionSetId = permissionSet.Id;
		 permissionSetAssignment.AssigneeId = user.Id;

		 insert permissionSetAssignment;

		 return user;
	 }

	private static User assignProfile (User user, String profileName){
		Profile profile = [SELECT Id FROM Profile WHERE Name =: profileName];
		user.ProfileId = profile.Id;
		update user;
		return user;
	}

}