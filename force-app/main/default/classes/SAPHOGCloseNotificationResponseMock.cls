@isTest
global with sharing class SAPHOGCloseNotificationResponseMock implements WebServiceMock {
  
  global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    
       SAPHOGNotificationServices.CloseNotificationResponse responseElement = new SAPHOGNotificationServices.CloseNotificationResponse();
       responseElement.Message = 'SAPHOGNotificationServices.CloseNotificationResponse Test Message';
       responseElement.Notification = new SAPHOGNotificationServices.Notification();
       responseElement.Notification.NotificationNumber = '7521256';
       responseElement.type_x = True;
       
       response.put('response_x', responseElement);
  }
}