@isTest
global with sharing class SAPHOGRefreshWorkOrdersResponseMock implements WebServiceMock {
  
  global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    
       SAPHOGWorkOrderServices.RefreshSalesforceWorkOrdersResponse responseElement = new SAPHOGWorkOrderServices.RefreshSalesforceWorkOrdersResponse();
       responseElement.Message = 'SAPHOGWorkOrderServices.RefreshSalesforceWorkOrdersResponse Test Message';
       responseElement.type_x = True;
       
       response.put('response_x', responseElement);
  }
}