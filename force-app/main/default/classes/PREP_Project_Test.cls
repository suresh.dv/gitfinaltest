@isTest
private class PREP_Project_Test {

    static testMethod void myUnitTest() 
    {       
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)']; 
        
        User u = new User(Alias = 'standt', Email='PREP@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP@testorg.com');
            
        insert u;
        
        PREP_Discipline__c disp = new PREP_Discipline__c();
        disp.Name = 'Disp 1';
        disp.Active__c = true;
        disp.SCM_Manager__c = u.Id;
        
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c();
        cat.Name = 'Category';
        cat.Active__c = true;
        cat.Discipline__c = disp.Id;
        
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c();
        sub.Name = 'SubCat';
        sub.Category__c = cat.Id;
        sub.Category_Manager__c = u.Id;
        sub.Category_Specialist__c = u.Id;
        sub.Active__c = true;
        
        insert sub;
        
        Id initRecordTypeId = Schema.SObjectType.PREP_Initiative__c.getRecordTypeInfosByName().get('Projects Initiative').getRecordTypeId();

        PREP_Initiative__c ini = new PREP_Initiative__c();
        ini.Initiative_Name__c = 'Test ini';
        ini.Discipline__c = disp.Id;
        ini.TIC__c = 10000;
        ini.PDE_Phase__c = '1';
        ini.Phase_4_Start_Date__c = Date.newInstance(2015, 9, 10);
        ini.Phase_4_Completion_Date__c = Date.newInstance(2015, 9, 10);
        ini.Status__c = 'Active';
        ini.Business_Unit__c = 'Atlantic Region';
        ini.Allocation_Level__c = 'Atlantic Region - Operations';
        ini.Project_Complexity__c = 'High';
        ini.X01_Segmentation_Team_Selection__c = Date.newInstance(2015, 9, 25);
        ini.X02_Business_Requirements__c = Date.newInstance(2015, 9, 26);
        ini.X03_Supplier_Market_Analysis__c = Date.newInstance(2015, 9, 27);
        ini.X04_Sourcing_Options__c = Date.newInstance(2015, 9, 28);
        ini.Strategy_Approved__c = Date.newInstance(2015, 9, 29); 
        ini.Project_End_Date__c = Date.newInstance(2015, 9, 29);
        ini.SCM_Manager__c = u.Id;
        ini.SCM_Team__c = 'HOG';
        ini.RecordTypeId = initRecordTypeId;
        
        insert ini; 
        
        Id projBaselineRecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Projects Baseline').getRecordTypeId();
        
        PREP_Baseline__c baseline = new PREP_Baseline__c();
        baseline.P_Baseline_Spend_Dollar_To_Be_Awarded__c = 10000;
        baseline.MSA_Baseline_Spend_Dollar__c = 10000;
        baseline.Local_Baseline_Spend_Percent__c = 10;
        baseline.Local_Baseline_Savings_Percent__c = 10;
        baseline.Global_Baseline_Savings_Percent__c = 10;
        baseline.X05_Going_To_Market__c = Date.newInstance(2015, 9, 26);
        baseline.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 9, 27);
        baseline.X07_Contract_Start_Up__c = Date.newInstance(2015, 9, 28);
        baseline.RecordTypeId = projBaselineRecordTypeId;
        
        insert baseline;
        
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c();
        MS.Name = 'MS Name';
        MS.Material_Service_Group_Name__c = 'MS';
        MS.Sub_Category__c = sub.Id;
        MS.Category_Manager__c = u.Id;
        MS.SAP_Short_Text_Name__c = 'MS';
        MS.Active__c = true;
        MS.Type__c = 'Material';
        MS.Category_Specialist__c = u.Id;
        MS.Includes__c = 'MS';
        MS.Does_Not_Include__c = 'MS';
        insert MS;
        
        PREP_Material_Service_Group__c MS2 = new PREP_Material_Service_Group__c();
        MS2.Name = 'MS Name2';
        MS2.Material_Service_Group_Name__c = 'MS2';
        MS2.Sub_Category__c = sub.Id;
        MS2.Category_Manager__c = u.Id;
        MS2.SAP_Short_Text_Name__c = 'MS2';
        MS2.Active__c = true;
        MS2.Type__c = 'Material';
        MS2.Category_Specialist__c = u.Id;
        MS2.Includes__c = 'MS2';
        MS2.Does_Not_Include__c = 'MS2';
        insert MS2;
        
        
        Id MSGBARecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Mat Group Baseline Allocation').getRecordTypeId();
        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSGBA = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSGBA.Material_Service_Group__c = MS.Id;
        MSGBA.Global_Sourcing__c = 'Yes';
        MSGBA.Planned_Spend_Allocation_Dollar__c = 10000;
        MSGBA.Planned_Savings_Allocation_Dollar__c = 10000;
        MSGBA.Baseline_Id__c = baseline.Id;
        MSGBA.RecordTypeId = MSGBARecordTypeId;
        
        insert MSGBA;
        
        List<PREP_Mat_Serv_Group_Planned_Schedule__c> msguaList = [SELECT Id FROM PREP_Mat_Serv_Group_Planned_Schedule__c WHERE Mat_Serv_Group_Baseline_Allocation_Id__c = :MSGBA.Id]; 
        
        
        Id scheRecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Planned_Schedule__c.getRecordTypeInfosByName().get('Mat Group Baseline Schedule').getRecordTypeId();

        
        
        PREP_Mat_Serv_Group_Planned_Schedule__c sche = msguaList[0];
        sche.Acquisition_Cycle_Type__c = 'Standard 12';
        sche.R_A_S_Date__c = Date.newInstance(2015, 9, 9);
        sche.Critical_Vendor_Data_Receipt_1_Weeks__c = 1;
        sche.Critical_Vendor_Data_Receipt_2_Weeks__c = 1;
        sche.MFG_Lead_Time_Weeks__c = 1;
        sche.RecordTypeId = scheRecordTypeId;
        
        update sche;
        
        Id updateRecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Mat Group Update').getRecordTypeId();
        
        PREP_Mat_Serv_Group_Update__c matUpdate = new PREP_Mat_Serv_Group_Update__c();
        matUpdate.RecordTypeId = updateRecordTypeId;
        matUpdate.Planned_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);
        matUpdate.Forecast_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);

        matUpdate.Planned_2_Business_Requirements__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_3_Supplier_Market_Analysis__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_4_Sourcing_Options__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_Strategy_Approval__c = Date.newInstance(2015, 2, 3);

        matUpdate.Planned_MRQ_Received__c = Date.newInstance(2015, 2, 3);
        matUpdate.Planned_Bid_Request_Issued_RFQ__c = Date.newInstance(2015, 2, 6);
        matUpdate.Planned_Bids_Received__c = Date.newInstance(2015, 3, 6);
        matUpdate.Planned_Commercial_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        matUpdate.Planned_Technical_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        matUpdate.Planned_Bid_Tab_Finalized__c = Date.newInstance(2015, 3, 29);
        
        
        matUpdate.Planned_Decision_Sum_Issued_Approval__c = Date.newInstance(2015, 4, 5);
        matUpdate.Planned_Decision_Summary_Approved__c = Date.newInstance(2015, 4, 12);
        matUpdate.Planned_MRP_Received__c = Date.newInstance(2015, 4, 19);
        matUpdate.Planned_P_O_Issued__c = Date.newInstance(2015, 4, 28);
        matUpdate.Planned_Vendor_Init_Drawing_Submittal__c = Date.newInstance(2015, 5, 5);
        matUpdate.Planned_Drawings_Approved__c = Date.newInstance(2015, 5, 12);
        matUpdate.Planned_Ship_Date__c = Date.newInstance(2015, 5, 19);
        matUpdate.Planned_ETA__c = Date.newInstance(2015, 5, 26);
        matUpdate.R_A_S_Date__c = Date.newInstance(2015, 9, 9);

        matUpdate.Forecast_Business_Requirements__c = 1;
        matUpdate.Forecast_Supplier_Market_Analysis__c = 1;
        matUpdate.Forecast_Sourcing_Options__c = 1;
        matUpdate.Forecast_Strategy_Approval_Cycle__c = 1;
        matUpdate.Forecast_MRQ_To_Be_Received__c = 1;
        matUpdate.Forecast_RFQ_Prep__c = 1;
        matUpdate.Forecast_Bid_Period__c = 1;
        matUpdate.Forecast_Bid_Evaluation_1__c = 1;
        matUpdate.Forecast_Bid_Evaluation_2__c = 1;
        matUpdate.Forecast_Bid_Evaluation_3__c = 1;
        matUpdate.Forecast_Bid_Evaluation_4__c = 1;
        
        matUpdate.Forecast_Husky_Approval__c = 1;
        matUpdate.Forecast_MRP_Prep__c = 1;
        matUpdate.Forecast_PO_Prep_Issued__c = 1;
        matUpdate.Forecast_Critical_Vendor_Data_Receipt1_W__c = 1;
        matUpdate.Forecast_Critical_Vendor_Data_Receipt2_W__c = 1;
        matUpdate.Forecast_MFG_Lead_Time_Weeks__c = 1;
        matUpdate.Forecast_Shipping_Duration__c = 1;
        
        matUpdate.Actual_1_Segmentation_Team_Selection__c = null;
        matUpdate.Actual_2_Business_Requirements__c = null;
        matUpdate.Actual_3_Supplier_Market_Analysis__c = null;
        matUpdate.Actual_4_Sourcing_Options__c = null;
        matUpdate.Actual_Strategy_Approval__c = null;
        matUpdate.Actual_MRQ_Received__c = null;
  
        insert matUpdate; 
        
        matUpdate.Planned_1_Segmentation_Team_Selection__c = Date.newInstance(2030, 9, 10);
        matUpdate.Actual_1_Segmentation_Team_Selection__c = Date.newInstance(2030, 9, 10);
        matUpdate.Actual_2_Business_Requirements__c = Date.newInstance(2030, 9, 10);
        matUpdate.Actual_3_Supplier_Market_Analysis__c = Date.newInstance(2030, 9, 10);
        matUpdate.Actual_4_Sourcing_Options__c = Date.newInstance(2030, 9, 10);
        matUpdate.Actual_Strategy_Approval__c = Date.newInstance(2030, 9, 10);
        matUpdate.Actual_MRQ_Received__c = Date.newInstance(2030, 9, 10);
        update matUpdate;
        
        
       Id MSGBARecordTypeId2 = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Serv Group Baseline Allocation').getRecordTypeId();

        PREP_Mat_Serv_Group_Baseline_Allocation__c MSGBA2 = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSGBA2.Material_Service_Group__c = MS2.Id;
        MSGBA2.Global_Sourcing__c = 'Yes';
        MSGBA2.Planned_Spend_Allocation_Dollar__c = 10000;
        MSGBA2.Planned_Savings_Allocation_Dollar__c = 10000;
        MSGBA2.Baseline_Id__c = baseline.Id;
        MSGBA2.RecordTypeId = MSGBARecordTypeId2;
        
        insert MSGBA2;
        
        List<PREP_Mat_Serv_Group_Planned_Schedule__c> msguaList2 = [SELECT Id FROM PREP_Mat_Serv_Group_Planned_Schedule__c WHERE Mat_Serv_Group_Baseline_Allocation_Id__c = :MSGBA2.Id]; 
        
        
        Id scheRecordTypeId2 = Schema.SObjectType.PREP_Mat_Serv_Group_Planned_Schedule__c.getRecordTypeInfosByName().get('Serv Group Baseline Schedule').getRecordTypeId();
 
        PREP_Mat_Serv_Group_Planned_Schedule__c sche2 = msguaList2[0];
        sche2.Acquisition_Cycle_Type__c = 'Standard 12';
        sche2.CWP_Completion__c = Date.newInstance(2015, 9, 9);
        sche2.Duration_Of_Works_Weeks__c = 1;
        sche2.RecordTypeId = scheRecordTypeId2;
        
        update sche2;
        
        
        Id updateRecordTypeId2 = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Serv Group Update').getRecordTypeId();
        
        PREP_Mat_Serv_Group_Update__c matUpdate2 = new PREP_Mat_Serv_Group_Update__c();
        matUpdate2.RecordTypeId = updateRecordTypeId2;
        matUpdate2.Planned_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);
        matUpdate2.Forecast_1_Segmentation_Team_Selection__c = Date.newInstance(2015, 2, 3);

        matUpdate2.Planned_2_Business_Requirements__c = Date.newInstance(2015, 2, 3);
        matUpdate2.Planned_3_Supplier_Market_Analysis__c = Date.newInstance(2015, 2, 3);
        matUpdate2.Planned_4_Sourcing_Options__c = Date.newInstance(2015, 2, 3);
        matUpdate2.Planned_Strategy_Approval__c = Date.newInstance(2015, 2, 3);

        matUpdate2.Planned_CWP_Received__c = Date.newInstance(2015, 2, 3);
        matUpdate2.Planned_RFQ_Issued__c = Date.newInstance(2015, 2, 6);
        matUpdate2.Planned_Bids_Received__c = Date.newInstance(2015, 3, 6);
        matUpdate2.Planned_Commercial_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        matUpdate2.Planned_Technical_Evaluations_Received__c = Date.newInstance(2015, 3, 27);
        matUpdate2.Planned_Bid_Tab_Finalized__c = Date.newInstance(2015, 3, 29);
        
        
        matUpdate2.Planned_Decision_Sum_Issued_Approval__c = Date.newInstance(2015, 4, 5);
        matUpdate2.Planned_Decision_Summary_Approved__c = Date.newInstance(2015, 4, 12);
        matUpdate2.Planned_Final_CWP_Received__c = Date.newInstance(2015, 4, 19);
        matUpdate2.Planned_Contract_Executed_LOA_Effective__c = Date.newInstance(2015, 4, 28);
        matUpdate2.Planned_Mobilization_Construction_Start__c = Date.newInstance(2015, 5, 5);
        matUpdate2.Planned_Construction_Completion__c = Date.newInstance(2015, 5, 12);
        matUpdate2.Planned_Demob__c = Date.newInstance(2015, 5, 19);
        
        matUpdate2.CWP_Completion__c = Date.newInstance(2015, 9, 9);

        matUpdate2.Forecast_Business_Requirements__c = 1;
        matUpdate2.Forecast_Supplier_Market_Analysis__c = 1;
        matUpdate2.Forecast_Sourcing_Options__c = 1;
        matUpdate2.Forecast_Strategy_Approval_Cycle__c = 1;
        matUpdate2.Forecast_CWP_To_Be_Received__c = 1;
        matUpdate2.Forecast_RFQ_Prep__c = 1;
        matUpdate2.Forecast_Bid_Period__c = 1;
        matUpdate2.Forecast_Bid_Evaluation_1__c = 1;
        matUpdate2.Forecast_Bid_Evaluation_2__c = 1;
        matUpdate2.Forecast_Bid_Evaluation_3__c = 1;
        matUpdate2.Forecast_Bid_Evaluation_4__c = 1;   
        matUpdate2.Forecast_Husky_Approval__c = 1;
        matUpdate2.Forecast_CWP_Prep__c = 1;
        matUpdate2.Forecast_CT_Prep_Issued__c = 1;
        matUpdate2.Forecast_Kickoff_Mobilization__c = 1;
        matUpdate2.Forecast_Duration_Of_Work_Weeks__c = 1;
        matUpdate2.Forecast_Demob_Cycle__c = 1;
  
        insert matUpdate2; 
        
     }     
}