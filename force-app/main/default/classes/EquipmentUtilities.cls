public class EquipmentUtilities {
    
    public Class UserPermissions {
        
        public Boolean isSystemAdmin {get; private set;}
        public Boolean isHogAdmin {get; private set;}
        public Boolean isSapSupportLead {get; private set;}


        public UserPermissions() {
            this.isSystemAdmin = isUserProfile('System Administrator');
            this.isHogAdmin = isUserPermissionSet('HOG_Administrator');
            this.isSapSupportLead = isUserRole('SAP_Support_Lead');

        }
    }
    
    //profile check
    private static Boolean isUserProfile(String profileName) {
        return (currentUserProfile == null) ? false : currentUserProfile.Name == profileName;
    }
    
    //role check
    private static Boolean isUserRole(String roleName) {
        return (currentUserRole == null) ? false : currentUserRole.DeveloperName == roleName;
    }
    
    //permission set check
    private static Boolean isUserPermissionSet(String permissionSet) {
        for(PermissionSetAssignment psa :currentUserPermissionSetAssignments) {
            if(psa.PermissionSet != null && psa.PermissionSet.Name.equals(permissionSet)) {
                return true;
            }
        }

        return false;
    }
    
    //get current user Profile
    private static Profile currentUserProfile {
        get {
            if(currentUserProfile == null) {
                currentUserProfile = [SELECT Id, Name
                                      FROM Profile
                                      WHERE Id =: UserInfo.getProfileId()];
            }
            return currentUserProfile;
        }
    }
    
    //get current user Role
    private static UserRole currentUserRole{
        get{
            if(currentUserRole == null){
                currentUserRole = [SELECT Id, DeveloperName 
                                   FROM UserRole 
                                   WHERE Id =: UserInfo.getUserRoleId()];
            }
            return currentUserRole;
        }
    }
    
    //get current user Permission sets
    private static List<PermissionSetAssignment> currentUserPermissionSetAssignments {
        get{
            if(currentUserPermissionSetAssignments == null || currentUserPermissionSetAssignments.isEmpty()) {
                currentUserPermissionSetAssignments = [SELECT Id, PermissionSet.Name, AssigneeId
                                                       FROM PermissionSetAssignment
                                                       WHERE AssigneeId =: UserInfo.getUserId()];
            }
            return currentUserPermissionSetAssignments;
        }
    }
}