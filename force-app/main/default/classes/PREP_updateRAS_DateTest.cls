@isTest
private class PREP_updateRAS_DateTest{
public static Id mcycle;
public static Id msaid;
static testmethod void test_trigger(){
 Profile p = [ SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)' ];
   User u = new User( Alias = 'standt', Email='PREP@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing',
                    LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles',
                    UserName='PREP123@testorg.com' ); 
        insert u;
  
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true, SCM_Manager__c = u.Id );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id, Category_Manager__c = u.Id,
                                                                Category_Specialist__c = u.Id, Active__c = true );
        insert sub;
        
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'Yes', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',PDE_Phase__c='1',Phase_4_Start_Date__c=Date.newInstance(2017, 7, 7),Phase_4_Completion_Date__c=Date.newInstance(2018, 7, 7),
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = u.Id, SCM_Team__c='E&C - Equipment',
                                Category_Manager__c = u.Id, Category_Specialist__c = u.Id, RecordTypeId ='01216000001QPTv' );
                                
        insert ini;
        PREP_Initiative__c ini1 = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'no', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',Global_Sourcing_Exception_Reason__c='none',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',PDE_Phase__c='1',Phase_4_Start_Date__c=Date.newInstance(2017, 7, 7),Phase_4_Completion_Date__c=Date.newInstance(2018, 7, 7),
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = u.Id, SCM_Team__c='E&C - Equipment',
                                Category_Manager__c = u.Id, Category_Specialist__c = u.Id, RecordTypeId ='01216000001QPTu' );
                                
        insert ini1;   
        /*PREP_Initiative__c ini1 = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id,Business_Unit__c ='legal',Allocation_Level__c='legal Group !', 
                                 Status__c = 'Active',  X02_Business_Requirements__c=Date.newInstance(2017, 7, 7),PDE_Phase__c='1',Phase_4_Start_Date__c=Date.newInstance(2017, 7, 7),Phase_4_Completion_Date__c=Date.newInstance(2018, 7, 7),
                                 Project_Complexity__c='Low',Project_End_Date__c=Date.newInstance(2018, 7, 7),SCM_Team__c='delivary Plan',SCM_Manager__c = u.Id,Strategy_Approved__c=Date.newInstance(2018, 7, 7),
                                    X01_Segmentation_Team_Selection__c=Date.newInstance(2018, 7, 7),X03_Supplier_Market_Analysis__c=Date.newInstance(2018, 7, 7),X04_Sourcing_Options__c=Date.newInstance(2018, 7, 7),RecordTypeId ='01216000001QPTv' );
         insert ini1;*/
        PREP_Baseline__c baseline = new PREP_Baseline__c();
        baseline.Baseline_Spend_Dollar_Target__c = 10000;
        baseline.Local_Baseline_Spend_Percent__c = 0.10;
        baseline.Local_Baseline_Savings_Percent__c = 0.10;
        baseline.Global_Baseline_Savings_Percent__c = 0.10;
        baseline.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 10);
        baseline.RecordTypeId = '01216000001QPTt';
        baseline.Initiative_Id__c = ini.Id;
        insert baseline;
        PREP_Baseline__c baseline1 = new PREP_Baseline__c();
        baseline1.Baseline_Spend_Dollar_Target__c = 10000;
        baseline1.Local_Baseline_Spend_Percent__c = 0.10;
        baseline1.Local_Baseline_Savings_Percent__c = 0.10;
        baseline1.Global_Baseline_Savings_Percent__c = 0.10;
        baseline1.Validation_Rule_Override__c = true;
        baseline1.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 10);
        baseline1.RecordTypeId = '01216000001QPTt';
        baseline1.Initiative_Id__c = ini1.Id;
        insert baseline1;
        
         
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c();
        MS.Name = 'MS Name';
        MS.Material_Service_Group_Name__c = 'MS';
        MS.Category_Link__c= cat.Id;
        MS.Sub_Category__c = sub.Id;
        MS.Category_Manager__c = u.Id;
        MS.SAP_Short_Text_Name__c = 'MS';
        MS.Active__c = true;
        MS.Type__c = 'Material';
        MS.Category_Specialist__c = u.Id;
        MS.Includes__c = 'MS';
        MS.Does_Not_Include__c = 'MS';
        //MS.PREP_ForecastDates=Date.newInstance(2015, 5, 10);
        insert MS;
         PREP_Mat_Serv_Group_Baseline_Allocation__c MSA = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSA.Baseline_Id__c = baseline.Id;
        MSA.Material_Service_Group__c = MS.Id;
        //MSA.Phase_4_Completion_Date__c=Date.newInstance(2015, 5, 10);
        MSA.RecordTypeId = '01216000001QPTw';
        MSA.Sub_Category__c = sub.Id;
        insert MSA;
        system.debug('++> '+msa.id);
        msaid = MSA.Id;
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSA1 = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSA1.Baseline_Id__c = baseline.Id;
        MSA1.Material_Service_Group__c = MS.Id;
        MSA1.RecordTypeId = '01216000001QPTx';
        MSA1.Sub_Category__c = sub.Id;
        insert MSA1;
        PREP_Acquisition_Cycle__c MatAcqCycle = new PREP_Acquisition_Cycle__c();
            MatAcqCycle.Initiative__c= ini.Id;
            MatAcqCycle.Mat_Serv_Group_Baseline_Allocation__c = MSA.Id;
            MatAcqCycle.RFQ_Prep__c = 3;
            MatAcqCycle.Bid_Period__c =  28;
            MatAcqCycle.Bid_Evaluation_1__c = 21;
            MatAcqCycle.Bid_Evaluation_2__c = 21;
            MatAcqCycle.Bid_Evaluation_3__c = 2;
            MatAcqCycle.Bid_Evaluation_4__c = 7;
            MatAcqCycle.Husky_Approval__c = 7;
            MatAcqCycle.MRP_Prep__c = 7;
            MatAcqCycle.PO_Prep_Issue__c = 9; 
            MatAcqCycle.Shipping_Duration__c = 7;
            MatAcqCycle.Type__c = 'Standard 12';
            MatAcqCycle.RecordTypeId = '01216000001QPTr';
            
            
            insert MatAcqCycle;
            mcycle = MatAcqCycle.Id;
        
            PREP_Acquisition_Cycle__c MatAcqCycle2 = new PREP_Acquisition_Cycle__c();
             MatAcqCycle.Mat_Serv_Group_Baseline_Allocation__c = MSA.Id;
            MatAcqCycle2.Initiative__c= ini.Id;
            MatAcqCycle2.RFQ_Prep__c = 3;
            MatAcqCycle2.Bid_Period__c =  14;
            MatAcqCycle2.Bid_Evaluation_1__c = 7;
            MatAcqCycle2.Bid_Evaluation_2__c = 7;
            MatAcqCycle2.Bid_Evaluation_3__c = 1;
            MatAcqCycle2.Bid_Evaluation_4__c = 1;
            MatAcqCycle2.Husky_Approval__c = 7;
            MatAcqCycle2.MRP_Prep__c = 7;
            MatAcqCycle2.PO_Prep_Issue__c = 2; 
            MatAcqCycle2.Shipping_Duration__c = 7;
            MatAcqCycle2.Type__c = 'Standard 6';
            MatAcqCycle2.RecordTypeId = '01216000001QPTr';

            insert MatAcqCycle2;

        
            PREP_Acquisition_Cycle__c MatAcqCycle3 = new PREP_Acquisition_Cycle__c();
            MatAcqCycle.Mat_Serv_Group_Baseline_Allocation__c = MSA.Id;
            MatAcqCycle3.Initiative__c= ini.Id;
            MatAcqCycle3.RFQ_Prep__c = 1;
            MatAcqCycle3.Bid_Period__c =  5;
            MatAcqCycle3.Bid_Evaluation_1__c = 2;
            MatAcqCycle3.Bid_Evaluation_2__c = 2;
            MatAcqCycle3.Bid_Evaluation_3__c = 2;
            MatAcqCycle3.Bid_Evaluation_4__c = 1;
            MatAcqCycle3.Husky_Approval__c = 1;
            MatAcqCycle3.MRP_Prep__c = 0;
            MatAcqCycle3.PO_Prep_Issue__c = 2; 
            MatAcqCycle3.Shipping_Duration__c = 7;
            MatAcqCycle3.Type__c = 'Standard 2';
            MatAcqCycle3.RecordTypeId = '01216000001QPTr';

            insert MatAcqCycle3;

        
            PREP_Acquisition_Cycle__c ServAcqCycle = new PREP_Acquisition_Cycle__c();
             MatAcqCycle.Mat_Serv_Group_Baseline_Allocation__c = MSA1.Id;
            ServAcqCycle.Initiative__c= ini.Id;
            ServAcqCycle.RFQ_Prep__c = 3;
            ServAcqCycle.Bid_Period__c =  28;
            ServAcqCycle.Bid_Evaluation_1__c = 21;
            ServAcqCycle.Bid_Evaluation_2__c = 21;
            ServAcqCycle.Bid_Evaluation_3__c = 2;
            ServAcqCycle.Bid_Evaluation_4__c = 7;
            ServAcqCycle.Husky_Approval__c = 7;
            ServAcqCycle.CWP_Prep__c = 7;
            ServAcqCycle.CT_Prep_Issue__c = 9;
            ServAcqCycle.Kick_off_Mobilization__c = 5;
            ServAcqCycle.Demob__c = 5;
            ServAcqCycle.Type__c = 'Standard 12';
            ServAcqCycle.RecordTypeId = '01216000001QPTs';
  
            insert ServAcqCycle;
      
        
            PREP_Acquisition_Cycle__c ServAcqCycle2 = new PREP_Acquisition_Cycle__c();
            ServAcqCycle2.Initiative__c= ini.Id;
             MatAcqCycle.Mat_Serv_Group_Baseline_Allocation__c = MSA1.Id;
            ServAcqCycle2.RFQ_Prep__c = 3;
            ServAcqCycle2.Bid_Period__c =  14;
            ServAcqCycle2.Bid_Evaluation_1__c = 7;
            ServAcqCycle2.Bid_Evaluation_2__c = 7;
            ServAcqCycle2.Bid_Evaluation_3__c = 1;
            ServAcqCycle2.Bid_Evaluation_4__c = 1;
            ServAcqCycle2.Husky_Approval__c = 7;
            ServAcqCycle2.CWP_Prep__c = 7;
            ServAcqCycle2.CT_Prep_Issue__c = 2;
            ServAcqCycle2.Kick_off_Mobilization__c = 3;
            ServAcqCycle2.Demob__c = 3;            
            ServAcqCycle2.Type__c = 'Standard 6';
            ServAcqCycle2.RecordTypeId = '01216000001QPTs';
        
            insert ServAcqCycle2;
            
            PREP_Acquisition_Cycle__c ServAcqCycle3 = new PREP_Acquisition_Cycle__c();
             MatAcqCycle.Mat_Serv_Group_Baseline_Allocation__c = MSA1.Id;
            ServAcqCycle3.Initiative__c= ini.Id;
            ServAcqCycle3.RFQ_Prep__c = 1;
            ServAcqCycle3.Bid_Period__c =  5;
            ServAcqCycle3.Bid_Evaluation_1__c = 2;
            ServAcqCycle3.Bid_Evaluation_2__c = 2;
            ServAcqCycle3.Bid_Evaluation_3__c = 2;
            ServAcqCycle3.Bid_Evaluation_4__c = 1;
            ServAcqCycle3.Husky_Approval__c = 1;
            ServAcqCycle3.CWP_Prep__c = 0;
            ServAcqCycle3.CT_Prep_Issue__c = 2;
            ServAcqCycle3.Kick_off_Mobilization__c = 2;
            ServAcqCycle3.Demob__c = 2;            
            ServAcqCycle3.Type__c = 'Standard 2';
            ServAcqCycle3.RecordTypeId = '01216000001QPTs';

            insert ServAcqCycle3;
            PREP_Mat_Serv_Group_Planned_Schedule__c msps = new PREP_Mat_Serv_Group_Planned_Schedule__c();
            msps.Acquisition_Cycle_Type__c='Standard 12';
            msps.Acquisition_Cycle__c=MatAcqCycle.Id;
            msps.Mat_Serv_Group_Baseline_Allocation_Id__c=MSA.Id;
            msps.R_A_S_Date__c=Date.newInstance(2018, 4, 9);
            msps.Phase_4_Completion_Date__c= Date.newInstance(2018, 5, 10);
            msps.Critical_Vendor_Data_Receipt_1_Weeks__c=2;
            msps.Critical_Vendor_Data_Receipt_2_Weeks__c=2;
            msps.MFG_Lead_Time_Weeks__c=3;
            msps.RecordTypeId='01216000001QPTy';
            insert msps;
    PREP_Mat_Serv_Group_Planned_Schedule__c msps1 = new PREP_Mat_Serv_Group_Planned_Schedule__c();
            msps1.Acquisition_Cycle_Type__c='Standard 12';
            msps1.Acquisition_Cycle__c=MatAcqCycle.Id;
            msps1.Mat_Serv_Group_Baseline_Allocation_Id__c=MSA1.Id;
            msps1.R_A_S_Date__c=Date.newInstance(2018, 12, 9);
            msps1.Phase_4_Completion_Date__c= Date.newInstance(2018, 5, 10);
            msps1.Critical_Vendor_Data_Receipt_1_Weeks__c=2;
            msps1.Critical_Vendor_Data_Receipt_2_Weeks__c=2;
            msps1.MFG_Lead_Time_Weeks__c=3;
            msps1.CWP_Completion__c = Date.newInstance(2017, 7, 7);
             msps1.RecordTypeId='01216000001QPTz';
                       insert msps1;
            //system.assertnotequals('ss',msps.id);
      Id matId = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Mat Group Update').getRecordTypeId();
    Id servId = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Serv Group Update').getRecordTypeId();      
     PREP_Mat_Serv_Group_Update__c msgu =   new PREP_Mat_Serv_Group_Update__c();
                msgu.Mat_Serv_Group_Planned_Schedule_Id__c =msps.Id;
                msgu.R_A_S_Date__c =Date.newInstance(2017, 7, 7)  ;
                msgu.Planned_Bid_Request_Issued_RFQ__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_MRQ_Received__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Bids_Received__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Commercial_Evaluations_Received__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Technical_Evaluations_Received__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Bid_Tab_Finalized__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Decision_Sum_Issued_Approval__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Decision_Summary_Approved__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_MRP_Received__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_P_O_Issued__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Vendor_Init_Drawing_Submittal__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Drawings_Approved__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Ship_Date__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_ETA__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_Business_Requirements__c =2;
                msgu.Planned_1_Segmentation_Team_Selection__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_1_Segmentation_Team_Selection__c= Date.newInstance(2017, 7, 7);
                msgu.Planned_2_Business_Requirements__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_3_Supplier_Market_Analysis__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_4_Sourcing_Options__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Strategy_Approval__c = Date.newInstance(2017, 7, 7);
                msgu.Planned_Strategy_Approval__c =Date.newInstance(2017, 7, 7);
                msgu.Forecast_Supplier_Market_Analysis__c=1;
                msgu.Forecast_Sourcing_Options__c=3;
                msgu.Forecast_Strategy_Approval_Cycle__c=3;
                msgu.Forecast_Strategy_Approval__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_MRQ_To_Be_Received__c=3;
                msgu.Forecast_Bid_Period__c=3;
                msgu.Forecast_RFQ_Prep__c=2;
                msgu.Actual_MRQ_Received__c =Date.newInstance(2017, 7, 7);
                msgu.Actual_Bid_Request_Issued_RFQ__c =Date.newInstance(2017, 7, 7);
                msgu.Forecast_Bid_Evaluation_1__c=1;
                msgu.Forecast_Bid_Evaluation_2__c=3;
                msgu.Actual_Bids_Received__c = Date.newInstance(2017, 7, 7);
                msgu.Actual_Comm_Eval_Received__c =Date.newInstance(2017, 7, 7);
                msgu.Actual_Tech_Eval_Received__c =Date.newInstance(2017, 7, 7);
                msgu.Forecast_Comm_Eval_Received_Mat__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_Bid_Evaluation_3__c=3;
                msgu.Actual_Comm_Eval_Received__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_Bid_Evaluation_4__c=2;
                msgu.Forecast_Husky_Approval__c=4;
                msgu.Forecast_MRP_Prep__c=2;
                msgu.Forecast_PO_Prep_Issued__c=3;
                msgu.Forecast_Critical_Vendor_Data_Receipt1_W__c=4;
                msgu.Forecast_Critical_Vendor_Data_Receipt2_W__c=1;
                msgu.Forecast_MFG_Lead_Time_Weeks__c=1;
                msgu.Forecast_Shipping_Duration__c=1;
                msgu.Actual_1_Segmentation_Team_Selection__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_2_Business_Requirements__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_Business_Requirements__c = 1;
                msgu.Actual_2_Business_Requirements__c = Date.newInstance(2017, 7, 7);
                msgu.Actual_3_Supplier_Market_Analysis__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_4_Sourcing_Options__c = Date.newInstance(2017, 7, 7);
                msgu.Actual_Strategy_Approval__c = Date.newInstance(2017, 7, 7);
                msgu.Forecast_Bids_Received_Mat__c = Date.newInstance(2017, 7, 7);
                msgu.RecordTypeId = matId;
            insert msgu;
            //system.assertnotequals('ss',msgu.id);
   PREP_Mat_Serv_Group_Update__c msgu1 =   new PREP_Mat_Serv_Group_Update__c();
            msgu1.Mat_Serv_Group_Planned_Schedule_Id__c =msps1.Id;
            //msgu1.R_A_S_Date__c = Date.newInstance(2018, 1,12);           
            msgu1.RecordTypeId = servId;
            msgu1.Planned_CWP_Received__c = Date.newInstance(2018, 1,12);
            msgu1.Planned_RFQ_Issued__c =Date.newInstance(2018, 1,12);
                msgu1.Planned_Bids_Received__c =Date.newInstance(2018, 1,12);
                msgu1.Planned_Commercial_Evaluations_Received__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Technical_Evaluations_Received__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Bid_Tab_Finalized__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Decision_Sum_Issued_Approval__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Decision_Summary_Approved__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Final_CWP_Received__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Contract_Executed_LOA_Effective__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Mobilization_Construction_Start__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Construction_Completion__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Demob__c = Date.newInstance(2018, 1,12);
                msgu1.CWP_Completion__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_1_Segmentation_Team_Selection__c = Date.newInstance(2018, 1,12);
                msgu1.Forecast_1_Segmentation_Team_Selection__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_2_Business_Requirements__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_3_Supplier_Market_Analysis__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_4_Sourcing_Options__c = Date.newInstance(2018, 1,12);
                msgu1.Planned_Strategy_Approval__c = Date.newInstance(2018, 1,12);
                //msgu1.Actual_1_Segmentation_Team_Selection__c = Date.newInstance(2018, 1,12);
                msgu1.Forecast_Business_Requirements__c = 1;  
                msgu1.Forecast_Supplier_Market_Analysis__c=1;  
                msgu1.Actual_3_Supplier_Market_Analysis__c = Date.newInstance(2017, 29,11);
                 //msgu1Actual_2_Business_Requirements__c=Date.newInstance(2017, 30,11);
                 msgu1.Actual_3_Supplier_Market_Analysis__c=Date.newInstance(2018, 1,12);
                 msgu1.Forecast_Sourcing_Options__c=1;
                 
                 insert msgu1;
        
        
            msgu1.Completed__c = true;
            update msgu1;
            update msgu;
        
            msps.Acquisition_Cycle_Type__c='Standard 6';
            msps1.Acquisition_Cycle_Type__c='Standard 6';
            update msps;
            update msps1;
             list<PREP_Mat_Serv_Group_Planned_Schedule__c> mslist =[select Id,R_A_S_Date__c from PREP_Mat_Serv_Group_Planned_Schedule__c  where Id = : msps.Id];
             list<PREP_Mat_Serv_Group_Update__c> mulist = [select Id from PREP_Mat_Serv_Group_Update__c where Mat_Serv_Group_Planned_Schedule_Id__c = :mslist[0].Id];
             list<PREP_Mat_Serv_Group_Update__c> msg= new list<PREP_Mat_Serv_Group_Update__c>();
             for(PREP_Mat_Serv_Group_Planned_Schedule__c msl : mslist){
             for( PREP_Mat_Serv_Group_Update__c mu : mulist)
             {
             mu.R_A_S_Date__c = msl.R_A_S_Date__c;
             msg.add(mu);
             }
                 }   
                 //insert msg;
             //update msg;
     }
     }