/*************************************************************************************************\
Author:         ???
Company:        Husky Energy
Description:    Load Request Utility class containing general methods and constants for Load Request
Test Class:     FM_LoadRequest_UtilitiesTest
History:        jschn 06.05.2018 - added several methods for US within E-Ticketing Project Release 5
				ssd   07.12.2018 - added logic to only return true on isVendor if not other permission
								   sets
**************************************************************************************************/
global class FM_LoadRequest_Utilities {

	/******************
	****EXPORT*STUF****
	******************/
	public static final String LOADREQUEST_EXPORT_LINK_BASE = '/apex/FM_LoadRequestExportCSVv2?lrs=';
	public static final String EXPORT_LOADREQUESTS_QUERY = 'SELECT Id'
														+ ', Axle__c'
														+ ', Unit_Configuration__c'
														+ ', Carrier__c'
									                    + ', Carrier__r.Name'
									                    + ', Carrier__r.Carrier_Name_For_Fluid__c'
														+ ', Carrier_Name__c'
														+ ', Standing_Comments__c'
														+ ', Create_Reason__c'
														+ ', Destination_Facility__c'
														+ ', Destination_Location__c'
														+ ', Source_Location__c'
														+ ', Source_Location__r.Name'
														+ ', Source_Location__r.Surface_Location__c'
														+ ', Source_Location__r.Route__c'
														+ ', Source_Location__r.Route__r.Name'
														+ ', Source_Facility__c'
														+ ', Source_Facility__r.Name'
														+ ', Source_Facility__r.Plant_Section__c'
														+ ', Source_Facility__r.Plant_Section__r.Name'
														+ ', CreatedDate'
														+ ', Name'
														+ ', Tank_Size__c'
														+ ', Tank__c'
														+ ', Status__c'
														+ ', Shift__c'
														+ ', Product__c'
														+ ', Sour__c'
														+ ', Tank_Low_Level__c'
														+ ', Act_Tank_Level__c'
														+ ', Act_Flow_Rate__c'
														+ ', Load_Weight__c'
														+ ' FROM FM_Load_Request__c';

	public static String getSelectedRoutesForQuery(List<String> routes) {
		String queryString = '';
        for (String item : routes) {
            if (queryString != '') queryString += ',';
            queryString += '\'' + item +'\'';
        }
        return queryString;
	}

	public static Boolean getIsVendor() {
		Boolean isInternal = false;
		Boolean isExternal = false;
		List<PermissionSetAssignment> pss = [SELECT Id, PermissionSet.Name
											FROM PermissionSetAssignment
											WHERE AssigneeId =: UserInfo.getUserId()];
		for (PermissionSetAssignment ps : pss) {
			if (ps.PermissionSet.Name == 'HOG_FM_Dispatcher_Vendor') 
				isExternal = true;
			
			if (ps.PermissionSet.Name == 'HOG_FM_Dispatcher' || 
				ps.PermissionSet.Name == 'HOG_Fluid_Management_Admin' ||
				ps.PermissionSet.Name == 'HOG_Administrator') {
				isInternal = true;
			}
		}

		return isExternal && !isInternal;
	}

	public static final String TANK_SETTINGS_HIDDEN = 'Hidden - Hide this Tank from the selectable list.';
	public static final String STANDARD_TANK_DELIMITER = '###';

	public static final String SOURCE_LOCATION = 'location';
	public static final String SOURCE_FACILITY = 'facility';

	////////////////////////////
	// LOAD REQUEST STATUSES  //
	////////////////////////////
	public static final String LOADREQUEST_STATUS_NEW 			= 'New';
	public static final String LOADREQUEST_STATUS_SUBMITTED 	= 'Submitted';
	public static final String LOADREQUEST_STATUS_BOOKED 		= 'Booked';
	public static final String LOADREQUEST_STATUS_EXPORTED		= 'Exported';
	public static final String LOADREQUEST_STATUS_DISPATCHED	= 'Dispatched';
	public static final String LOADREQUEST_STATUS_HAULED		= 'Hauled';
	public static final String LOADREQUEST_STATUS_CANCELLED 	= 'Cancelled';

	//////////////////////////////
	// LOAD REQUEST LOAD WEIGHT //
	//////////////////////////////
	public static final String LOADREQUEST_LOADWEIGHT_PRIMARY 	= 'Primary';
	public static final String LOADREQUEST_LOADWEIGHT_SECONDARY	= 'Secondary';
	public static final String LOADREQUEST_LOADWEIGHT_75	 	= '75%';

	////////////////////////////
	// LOAD REQUEST SHIFTS    //
	////////////////////////////
	public static final String LOADREQUEST_SHIFT_DAY = 'Day';
	public static final String LOADREQUEST_SHIFT_NIGHT = 'Night';
	public static final String LOADREQUEST_DAYSHIFT_YESTERDAY = 'Yesterday';
	public static final String LOADREQUEST_DAYSHIFT_TODAY = 'Today';
	public static final String LOADREQUEST_DAYSHIFT_TOMORROW = 'Tomorrow';

	////////////////////////////
	// LOAD REQUEST PRODUCTS  //
	////////////////////////////
	public static final String LOADREQUEST_PRODUCT_OIL = 'O';
	public static final String LOADREQUEST_PRODUCT_WATER = 'W';

	////////////////////////////////
	// LOAD REQUEST LOAD Types    //
	////////////////////////////////
	public static final String LOADREQUEST_LOADTYPE_STANDARD = 'Standard Load';
	//add as needed

	//////////////////////////////////
	// LOAD REQUEST SOURCE TYPES    //
	//////////////////////////////////
    public static final Map<String, String> LOADREQUEST_SOURCE_TYPE =
    	new Map<String, String>{'location' => 'Well Location', 'facility' => 'Facility'};

	public static Map<String, String> defaultAxles = new Map<String,String> {
		'5'		=>	'T5X',
		'6'		=>	'T6X',
		'8'		=>	'T8X',
		'BJ'	=>	'B3X',
		'BJP'	=>	'B6X'
	};

	public static FM_Load_Request__c getLoadRequestById(Id loadRequestId) {
		List<FM_Load_Request__c> loadRequests = [SELECT Id
													, Act_Flow_Rate__c
													, Act_Tank_Level__c
													, Axle__c
													, Cancel_Reason__c
													, Cancel_Comments__c
													, Carrier__c
													, Create_Reason__c
													, Create_Comments__c
													, Equipment_Tank__c
													, Flowline_Volume__c
													, Load_Type__c 
													, Load_Weight__c
													, Load_Weight_m3__c
													, Name
													, Product__c
													, Route__c 
													, Run_Sheet_Lookup__c
													, Shift_Day__c
													, Standing_Comments__c
													, Source_Facility__c
													, Source_Location__c
													, Tank__c
													, Tank_Label__c
													, Tank_Low_Level__c
													, Tank_Size__c
													, Unit_Configuration__c
													, Shift__c
													, Sour__c
													, Status__c
												FROM FM_Load_Request__c
												WHERE Id =: loadRequestId];
		if(loadRequests != null 
		&& loadRequests.size() > 0)
			return loadRequests.get(0);
		return null;
	}

	//////////////////////
	// Trigger Handlers //
	//////////////////////
	public static void TriggerBeforeInsertLoadRequest(List<FM_Load_Request__c> loadRequests) {
		assignNameToLoadRequests(loadRequests);
	}

	public static void TriggerAfterInsertLoadRequest(List<FM_Load_Request__c> loadRequests) {
		createTruckTripsForSubmittedLoadRequests(loadRequests, null);
	}

	public static void TriggerBeforeUpdateLoadRequest(List<FM_Load_Request__c> loadRequests,
		Map<Id, FM_Load_Request__c> oldMap) {
		cannotCancelLoadRequestsWithDispatchedTruckTrips(loadRequests, oldMap);
	}

	public static void TriggerAfterUpdateLoadRequest(List<FM_Load_Request__c> loadRequests,
		Map<Id, FM_Load_Request__c> oldMap) {
		createTruckTripsForSubmittedLoadRequests(loadRequests, oldMap);
		cancelTruckTripsForCancelledLoadRequests(loadRequests, oldMap);
	}

	public static void TriggerAfterInsertLoadConfirmation(List<FM_Load_Confirmation__c> loadConfirmations) {
		completeTrucktripForLoadConfirmations(loadConfirmations);
	}

	public static void TriggerAfterUpdateLoadConfirmation(List<FM_Load_Confirmation__c> loadConfirmations, Map<Id, FM_Load_Confirmation__c> oldMap) {
		updateLoadRequestInfo(loadConfirmations, oldMap);
	}

	public static void assignNameToLoadRequests(List<FM_Load_Request__c> loadRequests) {
		Set<Id> sourceIds = new Set<Id>();
		Map<Id, Location__c> locationMap = new Map<Id, Location__c>();
		Map<Id, Facility__c> facilityMap = new Map<Id, Facility__c>();

		for(FM_Load_Request__c lr : loadRequests) {
			if(lr.Source_Location__c <> null) sourceIds.add(lr.Source_Location__c);
			else if (lr.Source_Facility__c <> null) sourceIds.add(lr.Source_Facility__c);
		}

		locationMap =  new Map<Id,Location__c>([Select Id, Name
												From Location__c
												Where Id In :sourceIds]);
		facilityMap =  new Map<Id,Facility__c>([Select Id, Name
												From Facility__c
												Where Id In :sourceIds]);
		System.debug('loadRequests: ' + loadRequests);
		System.debug('locationMap: ' + locationMap);
		System.debug('facilityMap: ' + facilityMap);
		for(FM_Load_Request__c lr : loadRequests) {
			lr.Name = (lr.Source_Location__c <> null) ? locationMap.get(lr.Source_Location__c).Name : facilityMap.get(lr.Source_Facility__c).Name;
			lr.Name += ' / ' + lr.Tank_Label__c + ' - ' + Date.today().format();
		}

	}

	private static void createTruckTripsForSubmittedLoadRequests(
		List<FM_Load_Request__c> loadRequests, Map<Id, FM_Load_Request__c> oldMap) {
		List<FM_Truck_Trip__c> truckTrips = new List<FM_Truck_Trip__c>();
		List<FM_Load_Request__c> actionableLoadRequests = new List<FM_Load_Request__c>();

		for(FM_Load_Request__c lr : loadRequests) {
			//Only create truck trip if inserted "Submitted" or changed from "New" to "Submitted"
			if(lr.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED &&
				(oldMap == null || oldMap.get(lr.Id).Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW)) {
				actionableLoadRequests.add(lr);
				truckTrips.add(new FM_Truck_Trip__c(Load_Request__c	= lr.id,
					Load_Type__c = lr.Load_Type__c == LOADREQUEST_LOADTYPE_STANDARD ?
						null : lr.Load_Type__c,
					Product__c = lr.Product__c,
					Shift__c = lr.Shift__c,
					Shift_Day__c = (String.isNotBlank(lr.Shift_Day__c) 
									? lr.Shift_Day__c 
									: (lr.Shift__c == 'Night') ? 'Today' : 'Tomorrow'), 
					Carrier__c = lr.Carrier__c,
					Axle__c = FM_Utilities.RUNSHEET_UNITCONFIG_MAP.get(lr.Axle__c),
			  		Standing_Comments__c = lr.Standing_Comments__c,
			  		Run_Sheet_Date__c = Date.today()
				));
			}
		}

		Database.SaveResult[] saveResults = Database.insert(truckTrips, false);
		for(Integer i=0; i < saveResults.size(); i++) {
			Database.SaveResult result = saveResults[i];
			if(!result.isSuccess()) {
				String errorMessage = '';
				for(Database.Error error : result.getErrors()) {
					errorMessage += error.getMessage() + '\n';
				}
				actionableLoadRequests[i].addError(errorMessage);
			}
		}
	}

	private static void cannotCancelLoadRequestsWithDispatchedTruckTrips(List<FM_Load_Request__c> loadRequests,
		Map<Id, FM_Load_Request__c> oldMap) {
		Set<Id> loadRequestIds = new Set<Id>();
		Map<Id,FM_Load_Request__c> actionableLoadRequestMap = new Map<Id,FM_Load_Request__c>();

		for(FM_Load_Request__c lr : loadRequests) {
			if(lr.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED &&
				(oldMap.get(lr.Id) == null ||
				oldMap.get(lr.Id).Status__c <> FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED)) {
				loadRequestIds.add(lr.Id);
				actionableLoadRequestMap.put(lr.Id, lr);
			}
		}

		for(FM_Truck_Trip__c truckTrip : [Select Id, Name, Truck_Trip_Status__c, Load_Request__c
										  From FM_Truck_Trip__c
										  Where Load_Request__c In :loadRequestIds
										  And Truck_Trip_Status__c =: FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED]) {
			FM_Load_Request__c lr = actionableLoadRequestMap.get(truckTrip.Load_Request__c);
			lr.addError('Cannot cancel load request ' + lr.Name + '. Since it has a dispatched truck trip assigned to it. Need to cancel the dispatched truck trip from Truck Lists tab to cancel this Load Request.');
		}
	}

	private static void cancelTruckTripsForCancelledLoadRequests(
		List<FM_Load_Request__c> loadRequests, Map<Id, FM_Load_Request__c> oldMap) {
		Set<Id> loadRequestIds = new Set<Id>();
		List<FM_Truck_Trip__c> truckTripsToCancel = new List<FM_Truck_Trip__c>();
		Map<Id,FM_Load_Request__c> actionableLoadRequestMap = new Map<Id,FM_Load_Request__c>();

		for(FM_Load_Request__c lr : loadRequests) {
			if(lr.Status__c == FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED &&
				(oldMap.get(lr.Id) == null ||
				oldMap.get(lr.Id).Status__c <> FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED)) {
				loadRequestIds.add(lr.Id);
				actionableLoadRequestMap.put(lr.Id, lr);
			}
		}

		for(FM_Truck_Trip__c truckTrip : [Select Id, Name, Truck_Trip_Status__c, Load_Request__c, Load_Request__r.Cancel_Reason__c
												, Load_Request__r.Cancel_Comments__c
										  From FM_Truck_Trip__c
										  Where Load_Request__c In :loadRequestIds]) {
			if(truckTrip.Truck_Trip_Status__c == FM_Utilities.TRUCKTRIP_STATUS_GIVENBACK)
				continue; //ignore rebooked trucktrips

			truckTrip.Load_Request_Cancel_Reason__c = truckTrip.Load_Request__r.Cancel_Reason__c;
			truckTrip.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CANCELLED;
			if(String.isNotBlank(truckTrip.Load_Request__r.Cancel_Comments__c))
				truckTrip.Load_Request_Cancel_Comments__c = truckTrip.Load_Request__r.Cancel_Comments__c;
				
			truckTripsToCancel.add(truckTrip);
		}

		//Commit to Database
		update truckTripsToCancel;
	}

	private static void completeTrucktripForLoadConfirmations(List<FM_Load_Confirmation__c> loadConfirmationList) {
		Set<Id> truckTripIdSet = new Set<Id>();

		//Accumulate Truck Trip IDs
		for(FM_Load_Confirmation__c loadConfirmation : loadConfirmationList) {
			truckTripIdSet.add(loadConfirmation.Truck_Trip__c);
		}

		//Get Trucktrips
		List<FM_Truck_Trip__c> truckTripList = [Select Id, Name, Truck_Trip_Status__c
											 From FM_Truck_Trip__c
											 Where Id In :truckTripIdSet];

		//Update Trucktrips Status' to 'Complete'
		for(FM_Truck_Trip__c truckTrip : truckTripList) {
			truckTrip.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_COMPLETED;
		}

		Database.SaveResult[] saveResults = Database.update(truckTripList, false);
		for(Integer i=0; i < saveResults.size(); i++) {
			Database.SaveResult result = saveResults[i];
			if(!result.isSuccess()) {
				String errorMessage = '';
				for(Database.Error error : result.getErrors()) {
					errorMessage += error.getMessage() + '\n';
				}
				loadConfirmationList[i].addError(errorMessage);
			}
		}
	}

	private static void updateLoadRequestInfo(List<FM_Load_Confirmation__c> loadConfirmationList, Map<Id, FM_Load_Confirmation__c> oldMap) {
		Map<Id, Id> loadConfirmationToLoadRequestIdMap = new Map<Id, Id>();
		Map<Id, FM_Load_Request__c> loadRequestMap = new Map<Id, FM_Load_Request__c>();

		//Accumulate truck trip Ids
		for(FM_Load_Confirmation__c loadConfirmation : loadConfirmationList) {
			if(loadConfirmation.Ticket_Number__c != oldMap.get(loadConfirmation.Id).Ticket_Number__c) {
				loadConfirmationToLoadRequestIdMap.put(loadConfirmation.Id, loadConfirmation.LoadRequestId__c);
			}
		}

		//Accumulate load requests 
		loadRequestMap = new Map<Id, FM_Load_Request__c>([Select Id, Name, Ticket_Number__c
														  From FM_Load_Request__c
														  Where Id In :loadConfirmationToLoadRequestIdMap.values()]);

		//Update ticket number on Load Request
		List<FM_Load_Request__c> loadRequestsToUpdate = new List<FM_Load_Request__c>();
		for(FM_Load_Confirmation__c loadConfirmation : loadConfirmationList) {
			FM_Load_Request__c lr = loadRequestMap.get(loadConfirmationToLoadRequestIdMap.get(loadConfirmation.Id));
			if (lr != null) {
				lr.Ticket_Number__c = loadConfirmation.Ticket_Number__c;
				loadRequestsToUpdate.add(lr);
			}
		}

		update loadRequestsToUpdate;
	}

	public static FM_Load_Request__c createLoadRequest(String runSheetName,
														String loadType,
														String product,
														String shift,
														String dayShift,
														String carrier,
														String axle,
														String loadWeight,
														String comments,
														String createReason,
														String sourceLocation,
														String sourceFacility,
														String status,
														String tankId,
														String tankLabel,
														Decimal tankLevel,
														Decimal tankLevelLow,
														Decimal tankSize,
														Decimal flowRate,
														Decimal flowlineVolume) {
		FM_Load_Request__c lr = createLoadRequest(runSheetName,
												loadType,
												product,
												shift,
												carrier,
												axle,
												loadWeight,
												comments,
												createReason,
												sourceLocation,
												sourceFacility,
												status,
												tankId,
												tankLabel,
												tankLevel,
												tankLevelLow,
												tankSize,
												flowRate,
												flowlineVolume);
		lr.Shift_Day__c = dayShift;
		return lr;
	}

	public static FM_Load_Request__c createLoadRequest (	String runSheetName,
															String loadType,
															String product,
															String shift,
															String carrier,
															String axle,
															String loadWeight,
															String comments,
															String createReason,
															String sourceLocation,
															String sourceFacility,
															String status,
															String tankId,
															String tankLabel,
															Decimal tankLevel,
															Decimal tankLevelLow,
															Decimal tankSize,
															Decimal flowRate,
															Decimal flowlineVolume) {



		return new FM_Load_Request__c(	//Run_Sheet_Lookup__c = runSheetId,
										Run_Sheet_Lookup__r = new FM_Run_Sheet__c(Name = runSheetName),
										Load_Type__c = (loadType == null) ?  'Standard Load' : loadType,
									 	Product__c = product,
									  	Shift__c = shift,
									  	Carrier__c = carrier,
									  	Axle__c = axle,
									  	Load_Weight__c = loadWeight,
									  	Unit_Configuration__c = defaultAxles.get(axle),
									  	Standing_Comments__c = comments,
									  	Create_Reason__c = createReason,
										Source_Facility__c = sourceFacility,
										Source_Location__c =sourceLocation,
										Status__c = status,
										Equipment_Tank__c = tankId,
										Tank__c = tankLabel,
										Act_Tank_Level__c = tankLevel,
										Tank_Low_Level__c = tankLevelLow,
										Tank_Size__c = tankSize,
										Act_Flow_Rate__c = flowRate,
										Flowline_Volume__c = flowlineVolume
									);

	}

	public static List<SelectOption> GetLocationOptionsForLoadRequests(String routeId) {
		List<SelectOption> locationOptions = new List<SelectOption>();
		if(String.isNotBlank(routeId)) {
			List<Equipment_Tank__c> lTankLocations = (routeId != FM_Utilities.RUNSHEET_VALUE_NOT_SELECTED) ?
				FM_Utilities.GetLocationTankList(routeId) : new List<Equipment_Tank__c>();
			Map<Id, Location__c> mapLocationsWithNoTanks = FM_Utilities.GetLocationMapNotExistingTanks(routeId,FM_Utilities.GetLocationMap(routeId));
			Set<string> locationsIncludedSet = new Set<String>(); //to prevent two options for the same location

			//Include locations with real tanks
			for(Equipment_Tank__c tank : lTankLocations) {
		        if(!locationsIncludedSet.contains(tank.Equipment__r.Location__r.Id)){
		            SelectOption option = new SelectOption(tank.Equipment__r.Location__r.Id, String.format('{0}~{1}',
		            	new String[]{tank.Equipment__r.Location__r.Status__c, tank.Equipment__r.Location__r.Name}));
		            locationsIncludedSet.add(tank.Equipment__r.Location__r.Id);
		            locationOptions.add(option);
		        }
		    }

		    for(Location__c location: mapLocationsWithNoTanks.values()) {
		        if(!locationsIncludedSet.contains(location.Id)) {
		            SelectOption option = new SelectOption(location.Id, String.format('{0}~{1}', new String[]{location.Status__c, location.Name}));
					locationsIncludedSet.add(location.Id);
		            locationOptions.add(option);
		        }
		    }
		}
        return locationOptions;
    }
	public static List<SelectOption> getCancelReasons() {
        List<SelectOption> cancelReasonOptions = new List<SelectOption>();
        for (Schema.PicklistEntry ple : FM_Load_Request__c.Cancel_Reason__c.getDescribe().getPickListValues()) {
            cancelReasonOptions.add(new SelectOption(ple.getLabel(), ple.getValue()));
        }
        return cancelReasonOptions;
    }

    //Added 01.06.2018
    /**
     * [getShiftString description]
     * Business requirement is to have this value in separated fields(some users are restricted 
     * to see Shift Day value).
     * @param  loadRequest [description]
     * @return             [description]
     */
    public static String getShiftString(FM_Load_Request__c loadRequest) {
    	String shiftString = '';
		if(loadRequest != null) {
			shiftString = (String.isNotBlank(loadRequest.Shift__c) 
								? loadRequest.Shift__c 
								: '')
        				+ ' - '
        				+ (String.isNotBlank(loadRequest.Shift_Day__c) 
        						? loadRequest.Shift_Day__c 
        						: getDefaultShiftString(loadRequest.Shift__c));
		}
		return shiftString;
    }

    private static String getDefaultShiftString(String shift) {
		if(String.isNotBlank(shift)) 
			return (shift.equals(LOADREQUEST_SHIFT_NIGHT) 
				? LOADREQUEST_DAYSHIFT_TODAY 
				: LOADREQUEST_DAYSHIFT_TOMORROW);
		return '';		
    }

    /**
     * [setShift description]
     * Business requirement is to have this value in separated fields(some users are restricted 
     * to see Shift Day value).
     * @param  loadRequest [description]
     * @param  value       [description]
     * @return             [description]
     */
    public static FM_Load_Request__c setShift(FM_Load_Request__c loadRequest, String value) {
    	String[] shiftSplitted = value.split(' - ');
		if(shiftSplitted != null && shiftSplitted.size() > 0) {

			loadRequest.Shift__c = shiftSplitted.get(0);

			if(shiftSplitted.size() == 2)
				loadRequest.Shift_Day__c = shiftSplitted.get(1);
		}
		return loadRequest;
    }

    /**
     * [getShiftOptions description]
     * Business requirement is to have just this 4 combinations.
     * Business requirement is to have this value in separated fields(some users are restricted 
     * to see Shift Day value).
     * @return [description]
     */
    public static List<SelectOption> getShiftOptions() {
    	List<SelectOption> options = new List<SelectOption>();
    	String dayToday 		= LOADREQUEST_SHIFT_DAY   + ' - ' + LOADREQUEST_DAYSHIFT_TODAY;
    	String nightToday 		= LOADREQUEST_SHIFT_NIGHT + ' - ' + LOADREQUEST_DAYSHIFT_TODAY;
    	String dayTomorrow 		= LOADREQUEST_SHIFT_DAY   + ' - ' + LOADREQUEST_DAYSHIFT_TOMORROW;
    	String nightYesterday 	= LOADREQUEST_SHIFT_NIGHT + ' - ' + LOADREQUEST_DAYSHIFT_YESTERDAY;
    	options.add(new SelectOption(dayToday, dayToday));
    	options.add(new SelectOption(nightToday, nightToday));
    	options.add(new SelectOption(dayTomorrow, dayTomorrow));
    	//if(!getIsVendor())
    	//	options.add(new SelectOption(nightYesterday, nightYesterday));
    	return options;
    }

    public static List<SelectOption> getCreateReasonOptionsWithoutDefault() {
    	List<SelectOption> createReasonOptions = new List<SelectOption>();
		for (Schema.PicklistEntry ple : FM_Load_Request__c.Create_Reason__c.getDescribe().getPickListValues())
			if(!ple.isDefaultValue()) 
				createReasonOptions.add(new SelectOption(ple.getLabel(), ple.getValue()));
		return createReasonOptions;
	}

	public static List<SelectOption> getStandardTanks(FM_Load_Request__c loadRequest) {
		List<SelectOption> standardTanks = new List<SelectOption>();
		if(loadRequest != null
		&& loadRequest.Source_Location__c != null) {
			
			for(Equipment_Tank__c equipmentTank : getEquipmentTanksOnLocation(loadRequest.Source_Location__c)) {
				
				if(String.isNotBlank(equipmentTank.Tank_Label__c)) {

					String idLabel = equipmentTank.Id + STANDARD_TANK_DELIMITER + equipmentTank.Tank_Label__c;
					standardTanks.add(new SelectOption(idLabel , equipmentTank.Tank_Label__c));

				}
			}
		}
		return standardTanks;
	}

	public static List<Equipment_Tank__c> getEquipmentTanksOnLocation(Id locationId) {
		return [SELECT Id
					, Tank_Label__c
               FROM Equipment_Tank__c
               WHERE Equipment__r.Location__c =: locationId
               AND Tank_Settings__c <> :TANK_SETTINGS_HIDDEN];
	}

	public static List<SelectOption> getExtraTanks(FM_Load_Request__c loadRequest, List<SelectOption> standardTanks) {
		List<SelectOption> extraTanks = new List<SelectOption>();
		if(loadRequest!= null) {
			String sourceType = (String.isNotBlank(loadRequest.Source_Location__c) 
							? SOURCE_LOCATION 
							: SOURCE_FACILITY);
			extraTanks = FM_Utilities.getTankLabels(sourceType);
			if(sourceType.equals(SOURCE_LOCATION))
				extraTanks = removeUsedLabels(extraTanks, standardTanks);
		}
		return extraTanks;
	}

	public static List<SelectOption> removeUsedLabels(List<SelectOption> extraTanks, List<SelectOption> standardTanks) {
		List<SelectOption> returnOptions = new List<SelectOption>();
        for (SelectOption option : extraTanks) {
    		Boolean found = false;
        	for (SelectOption tank : standardTanks) {
				if(option.getLabel() == tank.getLabel())
	        		found = true;
            }
            if(!found) returnOptions.add(option);
        }
        return returnOptions;
	}

	public static Map<String, Equipment_Tank__c> getEquipmentTankMap(FM_Load_Request__c loadRequest) {
		String sourceId = String.isNotBlank(loadRequest.Source_Location__c) 
						? loadRequest.Source_Location__c 
						: loadRequest.Source_Facility__c;
		return new Map<String, Equipment_Tank__c>([SELECT Id, SCADA_Tank_Level__c, Latest_Tank_Reading_Date__c
														, Low_Level__c, Tank_Size_m3__c, Flow_Rate__c, Tank_Label__c
											   	   FROM Equipment_Tank__c
											       WHERE ((Equipment__r.Location__c =: sourceId 
											       			AND Equipment__r.Location__c != null)
											       		OR (Equipment__r.Facility__c =: sourceId 
											       			AND Equipment__r.Facility__c != null))
											       	AND Tank_Settings__c <> :TANK_SETTINGS_HIDDEN]);
	}

}