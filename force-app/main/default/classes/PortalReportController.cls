/// controller classs for reports displayed in customer portals
public with sharing class PortalReportController {

/// Gets the corect base URL regardless if the page is called from platform 
/// or from a Force.com site

public string ReportName {get;set;}
public string ReportDescription {get;set;}
Public String ReportID {get;set;}

public string baseURL {
    get{
        return Site.getCurrentSiteUrl() == null ? URL.getSalesforceBaseUrl().toExternalForm() +'/' : Site.getCurrentSiteUrl();
    }
}

public string ReportURL {
    get{
        return baseURL  +  ReportID ;
    }
}


    public PageReference RefreshData()
    {
        retrieveReportInfo();       
        return null;
    }   


    public void retrieveReportInfo() {
        ReportID = System.currentPagereference().getParameters().get('ReportID');
        
        LIST<Report> AgR;

        AgR = [select Name, Description from Report where id = :ReportID]; 

        // Loop through the list and update the Name field
        for(Report s : AgR){
            ReportName = String.valueOf(s.get('Name'));
            ReportDescription = String.valueOf(s.get('Description'));
         }
        
    }




}