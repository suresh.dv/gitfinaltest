/*-------------------------------------------------------------------------------------------------
Author     : Marcel Brimus
Company    : Husky Energy
Description: This class schedules the batch job to change status on Work Order Activities
Inputs     : N/A
Test Class : VTT_RevertActivityStatusBatchTest
---------------------------------------------------------------------------------------------------*/
global class VTT_UpdateActivitySchedulable implements Schedulable{
	public static final String UPDATE_ACTIVITY_STATUS_CRON_STRING = '0 0 7,8,9,10,11,12,13,14,15,16,17,18 ? * MON-FRI';
	public static final String UPDATE_ACTIVITY_STATUS_JOB_NAME = 'VTT Update Activity Status Batch' + (Test.isRunningTest() ? ' Test':'');

	public static String REVERT_ACTIVITY_CRON_STRING = '0 0 1 ? * MON-FRI';
	public static final String REVERT_ACTIVITY_JOB_NAME = 'VTT Revert Activity Status Batch' + (Test.isRunningTest() ? ' Test':'');
	public static final String REVERT_ACTIVITY_JOB_NAME_RETRY = 'Retry VTT Revert Activity Status Batch' + (Test.isRunningTest() ? ' Test':'');

	public String jobType {get; private set;}

	public VTT_UpdateActivitySchedulable(String jobType) {
		this.jobType = jobType;
	}

	global void execute(SchedulableContext sc) {
		if(jobType == VTT_RevertActivityStatusBatch.JOBTYPE_REVERT_ACTIVITY_STATUS)
			Database.executeBatch(new VTT_RevertActivityStatusBatch(), 20);
		else if (jobType == VTT_UpdateActivityStatusBatch.JOBTYPE_RETRY_UPDATE_INTEGRATION_ERRORS)
			Database.executeBatch(new VTT_UpdateActivityStatusBatch(VTT_UpdateActivityStatusBatch.JOBTYPE_RETRY_UPDATE_INTEGRATION_ERRORS));
	}

	global static void scheduleJob() {
		System.schedule(REVERT_ACTIVITY_JOB_NAME, REVERT_ACTIVITY_CRON_STRING, 
				new VTT_UpdateActivitySchedulable(VTT_RevertActivityStatusBatch.JOBTYPE_REVERT_ACTIVITY_STATUS));

		System.schedule(UPDATE_ACTIVITY_STATUS_JOB_NAME, UPDATE_ACTIVITY_STATUS_CRON_STRING, 
			new VTT_UpdateActivitySchedulable(VTT_UpdateActivityStatusBatch.JOBTYPE_RETRY_UPDATE_INTEGRATION_ERRORS));
	}
}