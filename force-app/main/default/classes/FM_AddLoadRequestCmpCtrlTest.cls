@isTest
private class FM_AddLoadRequestCmpCtrlTest {

	@isTest
	static void testAsRunSheetDetail() {
		User runningUser = createUser();
        runningUser = assignPermissionSet(runningUser, 'HOG_Field_Operator');
        runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		FM_AddLoadRequestCmpCtrl controller = setController(true);
		System.runAs(runningUser) {
	        Test.startTest();
			Test.setCurrentPage(Page.FM_AddLoadRequest);
	        ApexPages.currentPage().getParameters().put('index', '0');

			checkInitialSettings(controller);

			controller.getData();

			System.assert(controller.getIsAction());
			System.assert(!controller.isDispatcher);
			System.assert(!controller.getIsLoadRequestsToSave());
			System.assert(!controller.getIsHaluedLoadRequest());

			System.assertEquals(4, controller.loadRequestWrappers.size());

			controller.addLoadRequest();
			System.assert(controller.loadRequestWrappers.get(0).isNew);
			controller.loadRequestWrappers.get(0).tank = (controller.tanks != null && controller.tanks.size() > 0) ?
														controller.tanks.get(0).getValue() : controller.extraTanks.get(0).getValue();
			controller.updateLoadRequestTankInfo();//replicate the page updates due to tank changes
			Equipment_Tank__c eqTank = [Select Id, SCADA_Tank_Level__c, Low_Level__c, Tank_Size_m3__c
										From Equipment_Tank__c Limit 1];
			System.assertEquals(controller.loadRequestWrappers.get(0).loadRequest.Act_Tank_Level__c, Math.round(eqTank.SCADA_Tank_Level__c));
			System.assertEquals(controller.loadRequestWrappers.get(0).loadRequest.Tank_Low_Level__c, eqTank.Low_Level__c);
			System.assertEquals(controller.loadRequestWrappers.get(0).loadRequest.Tank_Size__c, eqTank.Tank_Size_m3__c);

			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();
			controller.loadRequestWrappers.get(0).loadRequest.Act_Tank_Level__c = 100;
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Low_Level__c = 60;
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Size__c = 160;

			System.assertEquals(5, controller.loadRequestWrappers.size());
			System.assert(controller.getIsLoadRequestsToSave());
			System.assert(controller.getIsAction());
			controller.addLoadRequest();
			controller.loadRequestWrappers.get(0).tank = controller.extraTanks.get(0).getValue();
			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();

			//Check Errors
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Act_Tank_Level__c = 100;
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Low_Level__c = 60;
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Size__c = 160;
			controller.saveNewLoadRequests();

			// + coverage
			controller.getIsTankInTankLabels();
            
			for(FM_AddLoadRequestCmpCtrl.LoadRequestWrapper lrw : controller.loadRequestWrappers)
				System.assert(!lrw.isNew);
			System.assertEquals(6, controller.loadRequestWrappers.size());

			controller.addLoadRequest();
			controller.loadRequestWrappers.get(0).tank = (controller.tanks != null && controller.tanks.size() > 0) ?
														controller.tanks.get(0).getValue() : controller.extraTanks.get(0).getValue();
			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();
			System.assertEquals(7, controller.loadRequestWrappers.size());
			ApexPages.currentPage().getParameters().put('index', '0');
			controller.removeLoadRequestByIndex();
			System.assertEquals(6, controller.loadRequestWrappers.size());

			Test.stopTest();
		}
	}

	@isTest
	static void testAsNonRunSheetDetail() {
		User runningUser = createUser();
        runningUser = assignPermissionSet(runningUser, 'HOG_FM_Dispatcher');
        runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		FM_AddLoadRequestCmpCtrl controller = setController(false);
		System.runAs(runningUser) {
			Test.startTest();
			Test.setCurrentPage(Page.FM_AddLoadRequest);
	        ApexPages.currentPage().getParameters().put('index', '0');

			checkInitialSettings(controller);

			controller.getData();
			System.assert(controller.getIsAction());
			System.assert(controller.isDispatcher);
			System.assert(!controller.getIsLoadRequestsToSave());
			System.assert(!controller.getIsHaluedLoadRequest());

			System.assertEquals(2, controller.loadRequestWrappers.size());
            
            controller.addLoadRequest();
            System.assert(controller.loadRequestWrappers.get(0).isNew);
            controller.loadRequestWrappers.get(0).tank = (controller.tanks != null && controller.tanks.size() > 0) ?
														controller.tanks.get(0).getValue() : controller.extraTanks.get(0).getValue();
            
			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();
			controller.loadRequestWrappers.get(0).loadRequest.Act_Tank_Level__c = 100;
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Low_Level__c = 60;
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Size__c = 160;

			System.assertEquals(3, controller.loadRequestWrappers.size());
			System.assert(controller.getIsLoadRequestsToSave());
			System.assert(controller.getIsAction());
			controller.addLoadRequest();
			controller.loadRequestWrappers.get(0).tank = controller.extraTanks.get(0).getValue();
			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();

			//Check Errors
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Act_Tank_Level__c = 100;
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Low_Level__c = 60;
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Size__c = 160;
			controller.saveNewLoadRequests();

			for(FM_AddLoadRequestCmpCtrl.LoadRequestWrapper lrw : controller.loadRequestWrappers)
				System.assert(!lrw.isNew);
			System.assertEquals(4, controller.loadRequestWrappers.size());

			controller.addLoadRequest();
			controller.loadRequestWrappers.get(0).tank = (controller.tanks != null && controller.tanks.size() > 0) ?
														controller.tanks.get(0).getValue() : controller.extraTanks.get(0).getValue();
			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();
			ApexPages.currentPage().getParameters().put('index', '0');
			controller.removeLoadRequestByIndex();

			Test.stopTest();
		}
	}

	@isTest
	static void testWithoutParameters() {
		User runningUser = createUser();
		runningUser = assignPermissionSet(runningUser, 'HOG_FM_Dispatcher');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		FM_AddLoadRequestCmpCtrl controller = setController(false);
		controller.sourceType = '';
		System.runAs(runningUser) {
			Test.startTest();
			Test.setCurrentPage(Page.FM_AddLoadRequest);
	        ApexPages.currentPage().getParameters().put('index', '0');

			controller.getData();
			System.assert(ApexPages.hasMessages());
			Test.stopTest();
		}
	}

	@isTest
	static void testWithFacility() {
		User runningUser = createUser();
		runningUser = assignPermissionSet(runningUser, 'HOG_FM_Dispatcher');
		runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		FM_AddLoadRequestCmpCtrl controller = setControllerfacility();
		System.runAs(runningUser) {
			Test.startTest();
			Test.setCurrentPage(Page.FM_AddLoadRequest);
	        ApexPages.currentPage().getParameters().put('index', '0');

			checkInitialSettings(controller);
			controller.getData();

			System.assert(controller.getIsAction());
			System.assert(controller.isDispatcher);
			System.assert(!controller.getIsLoadRequestsToSave());
			System.assert(!controller.getIsHaluedLoadRequest());

			System.assertEquals(2, controller.loadRequestWrappers.size());

			controller.addLoadRequest();
			System.assert(controller.loadRequestWrappers.get(0).isNew);
			controller.loadRequestWrappers.get(0).tank = (controller.tanks != null && controller.tanks.size() > 0) ?
														controller.tanks.get(0).getValue() : controller.extraTanks.get(0).getValue();

			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();
			controller.loadRequestWrappers.get(0).loadRequest.Act_Tank_Level__c = 100;
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Low_Level__c = 60;
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Size__c = 160;

			System.assertEquals(3, controller.loadRequestWrappers.size());
			System.assert(controller.getIsLoadRequestsToSave());
			System.assert(controller.getIsAction());
			controller.addLoadRequest();
			controller.loadRequestWrappers.get(0).tank = controller.extraTanks.get(0).getValue();
			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();

			//Check Errors
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Act_Tank_Level__c = 100;
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Low_Level__c = 60;
			controller.saveNewLoadRequests();
			verifyPageMessages();
			controller.loadRequestWrappers.get(0).loadRequest.Tank_Size__c = 160;
			controller.saveNewLoadRequests();
			
			for(FM_AddLoadRequestCmpCtrl.LoadRequestWrapper lrw : controller.loadRequestWrappers)
				System.assert(!lrw.isNew);
			System.assertEquals(4, controller.loadRequestWrappers.size());

			controller.addLoadRequest();
			controller.loadRequestWrappers.get(0).tank = (controller.tanks != null && controller.tanks.size() > 0) ?
														controller.tanks.get(0).getValue() : controller.extraTanks.get(0).getValue();
			controller.loadRequestWrappers.get(0).loadRequest.Create_Reason__c = FM_Load_Request__c.Create_Reason__c.getDescribe().getPicklistValues().get(0).getLabel();
			ApexPages.currentPage().getParameters().put('index', '0');
			controller.removeLoadRequestByIndex();

			Test.stopTest();
		}
	}

	@isTest
	static void testCancelLoadRequest() {
		User runningUser = createUser();
        runningUser = assignPermissionSet(runningUser, 'HOG_Field_Operator');
        runningUser = assignProfile(runningUser, 'Standard HOG - General User');
		System.runAs(runningUser) {
	        Test.startTest();
			
			Test.setCurrentPage(Page.FM_AddLoadRequest);
	        ApexPages.currentPage().getParameters().put('index', '3'); //For Cancelling

	        FM_AddLoadRequestCmpCtrl controller = setController(true);
			checkInitialSettings(controller);
			controller.getData();

			System.assert(controller.getIsAction());
			System.assert(!controller.isDispatcher);
			System.assert(!controller.getIsLoadRequestsToSave());
			System.assert(!controller.getIsHaluedLoadRequest());

			System.assertEquals(4, controller.loadRequestWrappers.size());

			controller.cancelLoadRequestPopupOpen();
			System.assertEquals(controller.cancelLoadRequest.Id, controller.loadRequestWrappers.get(3).loadRequest.Id);
			System.assertEquals(controller.cancelLoadRequest.Cancel_Reason__c, FM_Load_Request__c.Cancel_Reason__c.getDescribe().getPicklistValues().get(0).getValue());
			System.assertEquals(controller.showCancelPopup, true);

			controller.cancelLoadRequestPopupCancel();
			System.assertEquals(controller.cancelLoadRequest, null);
			System.assertEquals(controller.cancelLROldStatus, null);
			System.assertEquals(controller.showCancelPopup, false);

			controller.cancelLoadRequestPopupOpen();
			controller.cancelLoadRequestPopupConfirm();
			//System.assertEquals(controller.cancelLROldStatus, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED);
			//System.assertEquals(controller.cancelLoadRequest.Status__c, FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED);

			Test.stopTest();
		}

	}

	private static void checkInitialSettings(FM_AddLoadRequestCmpCtrl controller) {
		System.assert(controller.loadRequestWrappers == null);
		System.assert(controller.tanks == null);
		System.assert(controller.extraTanks == null);

		//testing status getters
		System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED, FM_AddLoadRequestCmpCtrl.getLoadRequestCancelledStatus());
		System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, FM_AddLoadRequestCmpCtrl.getLoadRequestSubmittedStatus());
		System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_STATUS_HAULED, FM_AddLoadRequestCmpCtrl.getLoadRequestHauledStatus());
		System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_STATUS_BOOKED, FM_AddLoadRequestCmpCtrl.getLoadRequestBookedStatus());
		System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW, FM_AddLoadRequestCmpCtrl.getLoadRequestNewStatus());
	}

	private static void verifyPageMessages() {
		for(Integer i=0; i < ApexPages.getMessages().size(); i++) {
			String message = (i==0) ? 'Please enter the tank level.' : 
				(i==1) ? 'Please enter the tank low level.' :
				(i==2) ? 'Please enter the tank size.' : null;
			System.assertEquals(message, ApexPages.getMessages().get(i).getSummary());
			System.assertEquals(ApexPages.Severity.FATAL, ApexPages.getMessages().get(i).getSeverity()); 
		}
	}

	private static FM_AddLoadRequestCmpCtrl setController(Boolean runAsRunSheet) {
		FM_AddLoadRequestCmpCtrl ctrl = new FM_AddLoadRequestCmpCtrl();
		ctrl.sourceType = 'location'; //TODO create also facility
		ctrl.sourceId = String.valueOf([SELECT id FROM Location__c].get(runAsRunSheet ? 0 : 1).id);
		return ctrl;
	}

	private static FM_AddLoadRequestCmpCtrl setControllerfacility() {
		FM_AddLoadRequestCmpCtrl ctrl = new FM_AddLoadRequestCmpCtrl();
		ctrl.sourceType = 'facility'; //TODO create also facility
		ctrl.sourceId = String.valueOf([SELECT id FROM Facility__c].get(0).id);
		return ctrl;
	}

	@testSetup
	static void createTestData() {
		Account acc = new Account();
		acc.Name = 'MI6 Account';
		insert acc;

		Carrier__c carrier = new Carrier__c();
        carrier.Carrier__c = acc.Id;
        carrier.Carrier_Name_For_Fluid__c = 'James Bond';
        insert carrier;

        Carrier_Unit__c carrierUnit = new Carrier_Unit__c();
        carrierUnit.Carrier__c = carrier.Id;
        carrierUnit.Unit_Email__c = 'bond@jamesbond.com';
        insert carrierUnit;

		Business_Department__c business = new Business_Department__c();
        business.Name = 'Husky Business';
        insert business;

        Operating_District__c district = new Operating_District__c();
        district.Name = 'District 9';
        district.Business_Department__c = business.Id;
        insert district;

        Field__c field = new Field__c();
        field.Name = 'AMU Field';
        field.Operating_District__c = district.Id;
        insert field;

		Route__c route = new Route__c();
        route.Fluid_Management__c = true;
        route.Name = '007';
        route.Route_Number__c = '007';
        insert route;

		Location__c loc = new Location__c();
        loc.Name = 'London';
        loc.Fluid_Location_Ind__c = true;
        loc.Location_Name_for_Fluid__c = 'London HQ';
        loc.Operating_Field_AMU__c = field.Id;
        loc.Route__c = route.Id;
		loc.Functional_Location_Category__c	= 4;
        insert loc;

		Equipment__c eq = EquipmentTestData.createEquipment(loc);
		eq.Equipment_Category__c = 'D';
		eq.Object_Type__c = 'VESS_ATMOS';
		update eq;

		Equipment_Tank__c eqt = new Equipment_Tank__c();
		eqt.Equipment__c = eq.id;
		eqt.SCADA_Tank_Level__c = 87.432;
		eqt.Low_Level__c = 50;
 		eqt.Tank_Size_m3__c = 160;
		eqt.Tank_Label__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		insert eqt;

		//create run sheet for runSheet test
		FM_Run_Sheet__c rs = new FM_Run_Sheet__c();
        rs.Date__c = Date.today();
        rs.Well__c = loc.Id;
        rs.Act_Tank_Level__c = 40;
		rs.Standing_Comments__c = 'Test comment';
		rs.Axle__c = '6';
		rs.Load_Weight__c = 'Primary';
		rs.Act_Flow_Rate__c = 5;
		rs.Flowline_Volume__c = 10;
        rs.Tomorrow_Oil__c = 1;
        rs.Tomorrow_Water__c = 1;
        rs.Tonight_Oil__c = 1;
        rs.Tonight_Water__c = 1;
        insert rs;

		List<FM_Load_Request__c> lrList = new List<FM_Load_Request__c>();

		FM_Load_Request__c lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt.Id;
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt.Id;
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt.Id;
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Run_Sheet_Lookup__c = rs.id;
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt.Id;
		lrList.add(lr);

		Location__c loc2 = new Location__c();
        loc2.Name = 'Brezno';
        loc2.Fluid_Location_Ind__c = true;
        loc2.Location_Name_for_Fluid__c = 'Brezno HQ';
        loc2.Operating_Field_AMU__c = field.Id;
        loc2.Route__c = route.Id;
		loc2.Functional_Location_Category__c = 4;
        insert loc2;

		Equipment__c eq2 = EquipmentTestData.createEquipment(loc2);
		eq2.Equipment_Category__c = 'D';
		eq2.Object_Type__c = 'VESS_ATMOS';
		update eq2;

		Equipment_Tank__c eqt2 = new Equipment_Tank__c();
		eqt2.Equipment__c = eq2.id;
		eqt2.SCADA_Tank_Level__c = 87.432;
		eqt2.Low_Level__c = 50;
 		eqt2.Tank_Size_m3__c = 160;
		eqt2.Tank_Label__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		insert eqt2;


		//Create load request for nonRunSheet test
		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt2.Id;
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt2.Id;
		lrList.add(lr);

		Facility__c fac = new Facility__c();
		fac.Name = 'Test fac';
		fac.Facility_Name_for_Fluid__c = 'Tst Facilty';
		fac.Fluid_Facility_Ind__c = true;
		fac.Fluid_Facility_Ind_Origin__c = true;
        fac.Operating_Field_AMU__c = field.Id;
		fac.Functional_Location_Category__c	= 3;
		fac.Plant_Section__c = route.id;

		insert fac;

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt2.Id;
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Equipment_Tank__c = eqt2.Id;
		lrList.add(lr);

		insert lrList;
	}

	private static User createUser() {
		User user = new User();
        Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

        Double random = Math.Random();

        user.Email = 'TestUser' +  random + '@hog.com';
        user.Alias = 'TestUser' ;
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'User';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.ProfileId = p.Id;
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.UserName = 'TestUser' + random + '@hog.com.unittest';

        insert user;
        return user;
	}

	private static User assignPermissionSet(User user, String userPermissionSetName){
         PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName];
         PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();

         permissionSetAssignment.PermissionSetId = permissionSet.Id;
         permissionSetAssignment.AssigneeId = user.Id;

         insert permissionSetAssignment;

         return user;
     }

    private static User assignProfile (User user, String profileName){
        Profile profile = [SELECT Id FROM Profile WHERE Name =: profileName];
        user.ProfileId = profile.Id;
        update user;
        return user;
    }

}