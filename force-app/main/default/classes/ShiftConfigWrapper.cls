global class ShiftConfigWrapper implements Comparable{

	public Shift_Configuration__c shiftConfig;
	
	// Constructor
	public ShiftConfigWrapper(Shift_Configuration__c shiftConfig) {
		this.shiftConfig = shiftConfig;
	}
	
	global Integer compareTo(Object compareTo) {
		
		ShiftConfigWrapper compareToShiftConfig = (ShiftConfigWrapper)compareTo;
		
		Integer returnValue = 0;
		if(Integer.valueOf(shiftConfig.Start_Time__c) > Integer.valueOf(compareToShiftConfig.shiftConfig.Start_Time__c)) {
			returnValue = 1;
		} else if(Integer.valueOf(shiftConfig.Start_Time__c) > Integer.valueOf(compareToShiftConfig.shiftConfig.Start_Time__c)) {
			returnValue = -1;
		}
		
		return returnValue;
	}
}