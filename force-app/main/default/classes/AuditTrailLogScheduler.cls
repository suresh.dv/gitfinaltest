/*--------------------------------------------------------------------------------------
Adapted from AuditTrailDownloadManager apex class
This class implements Schedulable now
SetupAuditTrail is accessible and can run soql on it

To schedule this job, run below Apex code in developer console:
    System.schedule('Audit Trail Log Job 1', '0 5 * * * ?', new AuditTrailLogScheduler());
    System.schedule('Audit Trail Log Job 2', '0 15 * * * ?', new AuditTrailLogScheduler());
    System.schedule('Audit Trail Log Job 3', '0 25 * * * ?', new AuditTrailLogScheduler());
    System.schedule('Audit Trail Log Job 4', '0 35 * * * ?', new AuditTrailLogScheduler());
    System.schedule('Audit Trail Log Job 5', '0 45 * * * ?', new AuditTrailLogScheduler());
    System.schedule('Audit Trail Log Job 6', '0 55 * * * ?', new AuditTrailLogScheduler());    
----------------------------------------------------------------------------------------*/ 

global class AuditTrailLogScheduler implements Schedulable {

    global void execute(SchedulableContext SC) { 
        generateLogCsv();
    }
    
    @future
    private static void generateLogCsv(){
        String csvFileString = '';
        String fileRow = '';
        Document auditTrail = null;
        
        List<SetupAuditTrail> audit = [SELECT CreatedDate, createdBy.username, Action, Section,  DelegateUser FROM SetupAuditTrail WHERE CreatedDate = LAST_N_DAYS:90];
        
        for(SetupAuditTrail a : audit){
            fileRow =   a.CreatedDate + ',' + 
                        a.createdBy.username + ',' +
                        a.Action.replaceAll(',', '') + ',' + 
                        a.Section + ',' + 
                        a.DelegateUser 
                        + '\n';
                        
            csvFileString = csvFileString + fileRow;
        }
        
        
        Folder fileFolder = [SELECT Id FROM Folder WHERE Name = 'HR Audit Trail'];
        
        List<Document> existingAuditTrail = [SELECT Id, Body, Name, FolderId, Type, Description, ContentType 
                                                FROM Document WHERE Name = 'HR_AuditTrail'];      
                                                
        if (existingAuditTrail.size() == 0)
        {
            auditTrail = new Document();
        }
        else
            auditTrail =  existingAuditTrail[0];
            
        auditTrail.Name = 'HR_AuditTrail';
        auditTrail.Body = Blob.valueOf(csvFileString);  
        auditTrail.FolderId = fileFolder.Id;
        auditTrail.Type = 'csv';
        auditTrail.Description = 'this Salesforce audit trail is downloaded at ' + datetime.now().format();
        auditTrail.ContentType = 'application/vnd.ms-excel';   
        
        try
        {  
            upsert auditTrail;
            System.debug('******** Audit Trail Download Successful **********');
        }
        catch(Exception e)
        {
            System.debug('*********** Audit Trail Download Errors ' + e.getMessage() + ' ***********');
        }          

    }

        
}