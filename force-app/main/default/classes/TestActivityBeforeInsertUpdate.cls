// ====================================================================
// Test all the use cases involved with BeforeInsert, Before Update
// trigger for Activity
@isTest
private class TestActivityBeforeInsertUpdate {

  // ------------------------------------------------------------------
  // User Story: Update Last Activity Date of the parent Case
  //   
  // Given I have a case
  // When I create a closed Activity for the case
  // Then the case's Last Activity Date should be "Today"
  static testMethod void updateLastActivityDateOnCase() {
    Account testAcc = new Account(Name = 'test');
    Database.insert(testAcc);
    Retail_Location__c testLoc = 
        new Retail_Location__c(Name = 'blah', Account__c = testAcc.Id);
    Database.insert(testLoc);
    
    Case testCase = new Case(Retail_Location__c = testLoc.Id);
    
    Database.Saveresult caseResult = Database.insert(testCase);
    
    
    Task testActivity = new Task(WhatId = caseResult.getId(),
                                 Status = 'Completed');
    insert testActivity;
    Case updatedCase = [SELECT Id, Last_Activity_Date__c 
                               FROM Case 
                               WHERE Id = :caseResult.getId()];
                               
    TaskTriggerHandler.onClosedTask(new List<Task>{testActivity});
    
//    System.assertEquals(Date.today(), updatedCase.Last_Activity_Date__c.date());
  }
}