global class WatchDogScadaUpload implements Messaging.InboundEmailHandler 
{
    private string csvAsString;
    private String[] csvFileLines = new String[]{};
    private Blob csvFileBody;
    private List<GRD_Well_Production_Summary__c> wellData;
    private Map<String, Location__c> m;
    
    private GRD_Well_Production_Summary__c histWellProduction;
    private string currDownTimeReason;

    /**
     * Method: handleInboundEmail
     * @param: email - Instance of Messaging.InboundEmail to get email description
     *         envelope - Instance of Messaging.InboundEnvelope to get receiver or sender email address
     * @return: Messaging.InboundEmailResult - Instance of Messaging.InboundEmailResult
     * @Desc: Method to upload data
    **/
    global Messaging.InboundEmailResult handleInboundEmail( Messaging.InboundEmail email, Messaging.InboundEnvelope envelope )
    {
        // To store Messaging.InboundEmailResult
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        if( email.subject.contains( 'Volume Report' ) && email.binaryAttachments != null )
        {
            // DML statement is in for loop because there will always be a single attachment in an email
            for( Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments )
            {
                //system.assert( false, bAttachment );
                csvFileBody = bAttachment.body;
                csvAsString = csvFileBody.toString();
                csvFileLines = csvAsString.split('\n');         
     
                /*Upsert csv file and display success message*/
                importRecords();
            }
        }
                            
        // Set success and message
        result.success = true;
        result.message = 'Records successfully saved in Salesforce';
        return result;
    }
    
    private void importRecords(){
        List<GRD_Well_Production_Summary__c> wellList = new List<GRD_Well_Production_Summary__c>();
        List<GRD_Well_Production__c> wellVolList = new List<GRD_Well_Production__c>();
        Date firstProdDate;      
        
        /*Read the header records*/
        string[] headerRecord = csvFileLines[0].split(',');  
        String[] scadaDate = headerRecord[12].split('-');
        
        firstProdDate = Date.newInstance(Integer.valueOf(scadaDate[0].trim()),
                                                Integer.valueOf(scadaDate[1].trim()),
                                                Integer.valueOf(scadaDate[2].trim()));             
        

        /* Get summary objects with downtime reason and comments before they are potentially erased - to be stored in vol object */
        List<GRD_Well_Production_Summary__c> prodCommentsDownTimeList  = new List<GRD_Well_Production_Summary__c>([select Well_Name__c, Down_Time_Reasons__c, Comments__c 
                                                                                                             from GRD_Well_Production_Summary__c where Down_Time_Reasons__c != null]);      

        Map<String, Id> bMap = getBottomHoleMap();
        
        /*Iterate through all lines of csv file and create GRD_Well_Production__c object */
        for(Integer i=1; i < csvFileLines.size(); i++){
            GRD_Well_Production_Summary__c well = new GRD_Well_Production_Summary__c();
            GRD_Well_Production__c wellVol = new GRD_Well_Production__c();
            
            string[] csvRecordData = csvFileLines[i].split(',');
            
            well.Well_Name__c = csvRecordData[0];
            well.name = csvRecordData[0];
            well.source_system_id__c = csvRecordData[0];
            //well.UWI__c = m.get(csvRecordData[1].trim()) == null ? null : m.get(csvRecordData[1].trim()).Id;
            well.UWI_Name__c = csvRecordData[1];
            well.GRD_Borehole__c = bMap.get(csvRecordData[1]);
            well.Run_Name__c = csvRecordData[2];
            well.Area_Supervisor__c = csvRecordData[3];                    
            well.Foreman_Name__c = csvRecordData[4];
            well.Drop_Below_LCL__c = csvRecordData[5].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[5]);
            well.Min_30_Day__c = csvRecordData[6].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[6]);
            well.Max_30_Day__c = csvRecordData[7].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[7]);
            well.Average__c = csvRecordData[8].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[8]);
            well.Standard_Deviation__c = csvRecordData[9].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[9]);
            well.Lower_Control_Limit__c = csvRecordData[10].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[10]);
            well.Upper_Control_Limit__c = csvRecordData[11].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[11]);
            well.Drop_Below_Previous_Day__c = (csvRecordData[13].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[13])) - 
                                                (csvRecordData[12].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[12])); 
            well.Last_Production_date__c = firstProdDate;
            well.Production_Volume__c = csvRecordData[12].trim() != '' ?  Double.valueOf(csvRecordData[12]) : Double.valueOf('0.0');                                             

           /* VALIDATE WHEN TO CLEAR COMMENTS/DOWNTIME REASON */           
            if (well.Drop_Below_LCL__c == 0)
            {                
                well.Down_Time_Reasons__c = '';
                well.Comments__c = '';
            }            
            
            well.Today_1__c = csvRecordData[13].trim() != '' ?  Double.valueOf(csvRecordData[13]) : Double.valueOf('0.0'); 
            well.Today_2__c = csvRecordData[14].trim() != '' ?  Double.valueOf(csvRecordData[14]) : Double.valueOf('0.0'); 
            well.Today_3__c = csvRecordData[15].trim() != '' ?  Double.valueOf(csvRecordData[15]) : Double.valueOf('0.0');
            
            well.Upload_Date__c = System.today(); 
              
            wellList.add(well); 

            
            /*Upsert Individual well records*/     
            wellVol.Well_Name__c = csvRecordData[0];
            wellVol.Name = csvRecordData[0];
            wellVol.Production_Date__c = firstProdDate;
            wellVol.Production_Volume__c =  csvRecordData[12].trim() != '' ?  Double.valueOf(csvRecordData[12]) : Double.valueOf('0.0'); 
            wellVol.source_system_id__c =  csvRecordData[0] + scadaDate[0] + scadaDate[1] + scadaDate[2];
            
            /* Store current data in Vol object for historical purposes */
            wellVol.Drop_Below_LCL__c = csvRecordData[5].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[5]);
            wellVol.Min_30_Day__c = csvRecordData[6].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[6]);
            wellVol.Max_30_Day__c = csvRecordData[7].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[7]);
            wellVol.Average__c = csvRecordData[8].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[8]);
            wellVol.Standard_Deviation__c = csvRecordData[9].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[9]);
            wellVol.Lower_Control_Limit__c = csvRecordData[10].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[10]);
            wellVol.Upper_Control_Limit__c = csvRecordData[11].trim() == '' ? Double.valueOf('0') : Double.valueOf(csvRecordData[11]); 
            wellVol.GRD_Borehole__c = bMap.get(csvRecordData[1]);
                        
            wellVolList.add(wellVol);                  
      
        }
        
        //upsert wellList;
        Database.upsert(wellList,GRD_Well_Production_Summary__c.source_system_id__c,false);        
        
        for(GRD_Well_Production__c p : wellVolList){
            for(GRD_Well_Production_Summary__c s : wellList){
                if (p.Well_Name__c == s.Well_Name__c){
                    p.GRD_Well_Production_Summary__c = s.Id;
                }
            }
        }
        
        wellVolList = addCommentsAndDownTimeReasons(wellVolList, prodCommentsDownTimeList);        

        Database.upsert(wellVolList,GRD_Well_Production__c.source_system_id__c,false);
                
    }  
    
    /*Pull data for the admin view*/
    public List<GRD_Well_Production_Summary__c> getWellData(){
        wellData = [select id, Well_Name__c, UWI__c, UWI_Name__c, Run_Name__c, Drop_Below_LCL__c, Drop_Below_Previous_Day__c, Down_Time_Reasons__c, Comments__c, LastModifiedDate from GRD_Well_Production_Summary__c ORDER By Well_Name__c limit 500];
        
        return wellData;
    }  
    
    private List<GRD_Well_Production__c> addCommentsAndDownTimeReasons(List<GRD_Well_Production__c> wellVolList, List<GRD_Well_Production_Summary__c> prodCommentsDownTimeList)
    {        
        for(GRD_Well_Production_Summary__c s : prodCommentsDownTimeList){
            for(GRD_Well_Production__c p : wellVolList){
                if (p.Well_Name__c == s.Well_Name__c){
                    p.Comments__c = s.Comments__c;
                    p.Down_Time_Reasons__c = s.Down_Time_Reasons__c;
                }
            }            
        }     
        
        return wellVolList;
    }   
    
    private Map<String, Id> getBottomHoleMap(){
        Map<String, Id> bMap = new Map<String, Id>(); 
        for (GRD_Borehole__c b : [select Id, UWI_Location__r.Name from GRD_Borehole__c WHERE UWI_Location__c != null]) {
            bMap.put(b.UWI_Location__r.Name, b.Id);
        }
        
        return bMap;
    }
}