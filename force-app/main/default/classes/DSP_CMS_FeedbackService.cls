/*
 * Called from javascript in the Downstream Portal, this calls a future method to send a "feedback" email message to Portal admins.
 */
global with sharing class DSP_CMS_FeedbackService implements cms.ServiceInterface
{
    private String action;
    private System.JSONGenerator response;

    public System.Type getType() {
        return DSP_CMS_FeedbackService.class;
    }
    
    global String executeRequest(Map<String, String> p)
    {
        action = p.get('action');
        
        String toAddress = p.get('toAddress');
        
        String feedbackMessage = p.get('feedbackMessage');
        
        response = System.JSON.createGenerator(false);
        
        response.writeStartObject();
        
        DSP_CMS_Email.sendEmail(toAddress, feedbackMessage);
        
        response.close();
        return (response.getAsString());
    }
}