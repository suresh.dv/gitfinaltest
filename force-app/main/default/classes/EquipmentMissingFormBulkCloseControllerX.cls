/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentMissingFormBulkClose page to close Status of multiple Add Equipment Form records

Test Class:    EquipmentMissingFormTest

History:       26-Mar-15	Gangha Kaliyan	To include facilities in Equipment Inventory
               28-Feb-17    Miro Zelina - included Well Events in Equipment Inventory
               03-Jul-17    Miro Zelina - included System & Sub-System in Equipment Inventory
               27-Jul-17    Miro Zelina - added role check for SAP Support Lead
               04-Aug-17    Miro Zelina - added profile & PS check for process button
------------------------------------------------------------*/

public with sharing class EquipmentMissingFormBulkCloseControllerX {
    
    public List<Equipment_Missing_Form__c> selectedList {get; private set;}
    public Integer CurrentIndex {get; private set;}
    public Equipment_Missing_Form__c CurrentForm {get; private set;}
    public EquipmentUtilities.UserPermissions userPermissions;
    private String retUrlID;

    public EquipmentMissingFormBulkCloseControllerX(ApexPages.StandardSetController controller)
    {
       userPermissions = new EquipmentUtilities.UserPermissions();
        
       if (!Test.isRunningTest())
       {
            controller.addFields(new List<String>{'Name', 'Status__c', 'Equipment_Status__c', 'Comments__c', 'P_ID__c','Description__c', 'Part_of_Package__c',
                        'Manufacturer__c', 'Model_Number__c', 'Manufacturer_Serial_No__c', 'Tag_Number__c', 
                        'Location__c', 'Serial_plate_of_asset__c', 'Attachment__c',
                        'Location__r.Route__c', 'Location__r.Planner_Group__c',
                        'Well_Event__c', 'Well_Event__r.Route__c', 'Well_Event__r.Planner_Group__c',
                        'System__c', 'System__r.Route__c', 'System__r.Planner_Group__c',
                        'Sub_System__c', 'Sub_System__r.Route__c', 'Sub_System__r.Planner_Group__c',
                        'Functional_Equipment_Level__c', 'Functional_Equipment_Level__r.Route__r.Route_Number__c','Functional_Equipment_Level__r.Planner_Group__c',
                        'Yard__c', 'Yard__r.Route__r.Route_Number__c','Yard__r.Planner_Group__c',
                        'CreatedById', 'CreatedDate','LastModifiedDate', 'LastModifiedById', 'Facility__c',
                        'Facility__r.Plant_Section__c', 'Facility__r.Planner_Group__c'});
       }  
      
      // This will set ID from first equipment missing form for when users wants go back
      // they will be redirected to correct FLOC  
      figureOutReturnURL(controller);

      if (!userPermissions.isSystemAdmin && !userPermissions.isSapSupportLead && !userPermissions.isHogAdmin){
           
          selectedList = new List<Equipment_Missing_Form__c>();
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
          return;
      }
      
      else if(controller.getSelected().Size()==0)
      {
          selectedList = new List<Equipment_Missing_Form__c>();
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Add Equipment Form'));
          return;
      }
      
      
      selectedList = controller.getSelected();
      CurrentIndex = 1;       
      CurrentForm = selectedList[CurrentIndex-1];
    }

    public Attachment attachment {
        get{
            if (attachment == null)
            {
                List<Attachment> att = [select Id, Name, Body, ContentType from Attachment 
                                where ParentId =: CurrentForm.Id and Id =: CurrentForm.Attachment__c];
                if (att.size() > 0)
                   attachment = att[0];
               
            }
            return attachment;
        }
        set;
    }
    public Attachment serialPlate
    {
        get{
            if (serialPlate == null)
            {
                List<Attachment> att = [select Id, Name, Body, ContentType from Attachment 
                                where ParentId =: CurrentForm.Id and Id =: CurrentForm.Serial_plate_of_asset__c];
                if (att.size() > 0)
                   serialPlate = att[0];
               
            }
            return serialPlate;
        }
        set;
    }
    public boolean getHasPreviousForm()
    {
        return (CurrentIndex > 1);
    }   
   
    public boolean getHasNextForm()
    {
        return (CurrentIndex < selectedList.size());
    }
    
    public PageReference NextForm()
    {
        if(getHasNextForm())
        { 
            CurrentIndex = CurrentIndex + 1 ;
            CurrentForm = selectedList[CurrentIndex-1];   
            attachment = null;
            serialPlate = null;
        }
       return null; 
    }
   
    public PageReference PreviousForm()
    { 
       if(getHasPreviousForm())
       { 
           CurrentIndex = CurrentIndex - 1 ;
           CurrentForm = selectedList[CurrentIndex-1];      
           attachment = null;
           serialPlate = null; 
       }
       return null;
    }    
    
    public PageReference CloseForm()
    {
       Equipment_Missing_Form__c obj = CurrentForm;  

       if(obj.Status__c != 'Closed')
       {
           try
           {
            obj.Status__c = 'Closed';
            update obj;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully CLOSED.'));
            return null;
            
           }
           catch(Exception ex)    
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
               return null;
           }               
       }   
  
       return null;   
   
    }

    public void figureOutReturnURL(ApexPages.StandardSetController controller){
      if(controller.getSelected().Size() > 0){
        Equipment_Missing_Form__c eq = (Equipment_Missing_Form__c) controller.getSelected()[0];

        if(eq.Well_Event__c != null){
          retUrlID = eq.Well_Event__c;
        } else if (eq.Yard__c != null){
          retUrlID = eq.Yard__c;
        } else if (eq.Functional_Equipment_Level__c != null){
          retUrlID = eq.Functional_Equipment_Level__c;
        } else if (eq.Sub_System__c != null){
          retUrlID = eq.Sub_System__c;
        } else if (eq.System__c != null){
          retUrlID = eq.System__c;
        } else if (eq.Location__c != null){
          retUrlID = eq.Location__c;
        } else if (eq.Facility__c != null){
          retUrlID = eq.Facility__c;
        } else {
          retUrlID = '';
        }
      }  else {
        retUrlID = '';
      }
    }

    public PageReference Exit() {
        
        PageReference pageRef;

        System.debug('retUrlID ' + retUrlID);
        pageRef = new PageReference('/'+retUrlID);
        return pageRef;  
    }
   
}