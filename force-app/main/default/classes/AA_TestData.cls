/**
 * Test utilities for Agile Initiative related 
 * apex classes
 * 
 * Created: 2018/04/16
 * Author: MBrimus
 *suresh filaltest git testP*/
public without sharing class AA_TestData {
	
	public static User createAgileInitiativeUser() {
        ID ProfileID = [ Select id from Profile where name = 'Standard HOG - General User'].id;
                
        User user = new User();
        Double random = Math.Random();
        user.email = 'test-user' + random + '@fakeemail.com';
        user.ProfileId = ProfileID;
        user.UserName = 'test-user' + random + '@fakeemail.com';
        user.alias = 'tu' ;
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1'; 
        user.LanguageLocaleKey='en_US';
        user.FirstName = 'Test';
        user.LastName = 'User';
        insert user;
        
       	PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Label = 'Agile Accelerator Initiatives'];              
		PermissionSetAssignment permissionSetAssignement = new PermissionSetAssignment();        
		permissionSetAssignement.PermissionSetId = permissionSet.Id;
        permissionSetAssignement.AssigneeId = user.Id;            
        
        insert permissionSetAssignement;

        return user;
    }

    public static AA_Initiative__c createInitiative(User user){
    	AA_Initiative__c record = new AA_Initiative__c();
    	record.Name = 'test name';
    	record.Point_of_Contact__c = user.id;
    	record.Submitted_Date__c = Date.today();
    	record.Current_Year_Savings__c = '$0 - $500K';

    	insert record;
    	return record;
    }    
}