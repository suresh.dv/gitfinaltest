/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PREP_MSGSchedule_Test {

    static testMethod void myUnitTest() 
    {       
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)']; 
        
        User u = new User(Alias = 'standt', Email='PREP@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP@testorg.com');
            
        insert u;
        
        PREP_Discipline__c disp = new PREP_Discipline__c();
        disp.Name = 'Disp 1';
        disp.Active__c = true;
        disp.SCM_Manager__c = u.Id;
        
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c();
        cat.Name = 'Category';
        cat.Active__c = true;
        cat.Discipline__c = disp.Id;
        
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c();
        sub.Name = 'SubCat';
        sub.Category__c = cat.Id;
        sub.Category_Manager__c = u.Id;
        sub.Category_Specialist__c = u.Id;
        sub.Active__c = true;
        
        insert sub;
        
        Id iniRecordTypeId = Schema.SObjectType.PREP_Initiative__c.getRecordTypeInfosByName().get('Projects Initiative').getRecordTypeId();

        PREP_Initiative__c ini = new PREP_Initiative__c();
        ini.Initiative_Name__c = 'Test ini';
        ini.Discipline__c = disp.Id;
        ini.Category__c = cat.Id;
        ini.Sub_Category__c = sub.Id;
        ini.Status__c = 'Active';
        ini.Type__c = 'Category';
        ini.Risk_Level__c = 'High';
        ini.HSSM_Package__c = 'Full';
        ini.Workbook__c = 'Yes';
        ini.SRPM_Package__c = 'As Needed';
        ini.Aboriginal__c = 'No';
        ini.Global_Sourcing__c = 'Yes';
        ini.OEM__c = 'Yes';
        ini.Rate_Validation__c = 'Yes';
        ini.Reverse_Auction__c = 'No';
        ini.Rebate__c = 'Multiple Area Award';
        ini.Rebate_Frequency__c = 'Annually';
        ini.SCM_Manager__c = u.Id;
        ini.Category_Manager__c = u.Id;
        ini.Category_Specialist__c = u.Id;
        ini.RecordTypeId = iniRecordTypeId;
        
        insert ini;
        
        Id baseRecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Projects Baseline').getRecordTypeId();
        
        PREP_Baseline__c baseline = new PREP_Baseline__c();
        baseline.P_Baseline_Spend_Dollar_To_Be_Awarded__c = 1000000000;
        baseline.P_Global_Baseline_Spend_Dollar__c = 10000;
        baseline.MSA_Baseline_Spend_Dollar__c = 10000;
        baseline.Global_Baseline_Negotiated_Sav_Dollar__c = 10000;
        baseline.Local_Baseline_Negotiated_Savings_Dollar__c = 10000;
        baseline.X05_Going_To_Market__c = Date.newInstance(2015, 5, 10);
        baseline.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 10);
        baseline.X07_Contract_Start_Up__c = Date.newInstance(2015, 5, 10);
        baseline.RecordTypeId = baseRecordTypeId;
        
        insert baseline;
        
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c();
        MS.Name = 'MS Name';
        MS.Material_Service_Group_Name__c = 'MS';
        MS.Sub_Category__c = sub.Id;
        MS.Category_Manager__c = u.Id;
        MS.SAP_Short_Text_Name__c = 'MS';
        MS.Active__c = true;
        MS.Type__c = 'Material';
        MS.Category_Specialist__c = u.Id;
        MS.Includes__c = 'MS';
        MS.Does_Not_Include__c = 'MS';
        
        insert MS;
        
        PREP_Material_Service_Group__c MS2 = new PREP_Material_Service_Group__c();
        MS2.Name = 'MS Name';
        MS2.Material_Service_Group_Name__c = 'MS';
        MS2.Sub_Category__c = sub.Id;
        MS2.Category_Manager__c = u.Id;
        MS2.SAP_Short_Text_Name__c = 'MS';
        MS2.Active__c = true;
        MS2.Type__c = 'Material';
        MS2.Category_Specialist__c = u.Id;
        MS2.Includes__c = 'MS';
        MS2.Does_Not_Include__c = 'MS';
        
        insert MS2;
        
        Id MSARecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Mat Group Baseline Allocation').getRecordTypeId();
        Id MSARecordTypeId2 = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Serv Group Baseline Allocation').getRecordTypeId();

        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSA = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSA.Baseline_Id__c = baseline.Id;
        MSA.Material_Service_Group__c = MS.Id;
        MSA.RecordTypeId = MSARecordTypeId;
        MSA.Planned_Spend_Allocation_Dollar__c = 1000000000;
        MSA.Planned_Savings_Allocation_Dollar__c = 100000;
        MSA.Global_Sourcing__c = 'Yes';
        
        insert MSA;
        
        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSA2 = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSA2.Baseline_Id__c = baseline.Id;
        MSA2.Material_Service_Group__c = MS.Id;
        MSA.Planned_Spend_Allocation_Dollar__c = 1000000000;
        MSA.Planned_Savings_Allocation_Dollar__c = 100000;
        MSA2.RecordTypeId = MSARecordTypeId2;
        
        insert MSA2;
        
        
    }
}