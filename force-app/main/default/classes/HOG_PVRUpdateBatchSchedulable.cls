/*-------------------------------------------------------------------------------------------------
Author     : Shreyas Dhond
Company    : Husky Energy
Description: This class schedules the batch job to update PVR average volumes.
Inputs     : N/A
Test Class : HOG_PVRUpdateTest
---------------------------------------------------------------------------------------------------*/
global class HOG_PVRUpdateBatchSchedulable implements Schedulable {
	public static String cronString = '0 30 7 ? * MON-SUN';

	public HOG_PVRUpdateBatchSchedulable() {

	}

	global void execute(SchedulableContext sc) {
		HOG_PVRUpdateBatch pvrUpdateBatch = new HOG_PVRUpdateBatch();
		Database.executeBatch(pvrUpdateBatch, 200);
	}

	global static String scheduleJob() {
		System.debug('Scheduling HOG_PVRUpdateBatch ...');
		HOG_PVRUpdateBatchSchedulable pvrUpdateBatch = new HOG_PVRUpdateBatchSchedulable();
		String jobId = System.schedule(((Test.isRunningTest()) ? 'Test ' : '')
			+ 'HOG - PVR Average Volume Update Batch', cronString, pvrUpdateBatch);
		return jobId;
	}
}