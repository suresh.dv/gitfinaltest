/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_InvoiceControllerExtensionTest{
    static string invoiceid;
    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {


            setup();                
            USCP_Invoice__c invoice = [select id, name, Account__r.Customer_Number__c from USCP_Invoice__c where id = :invoiceid];
            
            Test.startTest();

            //ApexPages.currentPage().getParameters().put('id',invoice.id);
            System.AssertNotEquals(invoice.Account__r.Customer_Number__c, null);
            System.AssertNotEquals(invoice.Name, null);            
            
            ApexPages.StandardController stdinvoice = new ApexPages.StandardController(invoice);
            USCP_InvoiceControllerExtension invExtController  = new USCP_InvoiceControllerExtension(stdinvoice);

            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new USCP_HEI_MockHttpResponseGenerator());

            System.AssertNotEquals(invExtController.DownloadPdf(), null);
            
            System.AssertNotEquals(invExtController.pdfDocumentID, null);
            
            Test.stopTest();    
        }
    }

    public static void setup() {
            //setup custom settings for DTN call
            USCP_Settings__c newUserVal =  new USCP_Settings__c();
            newUserVal.HEI_WS_Endpoint__c = 'https://somewhere.com'; 
            //newUserVal.HEI_WS_CertificateName__c = 'TestCertificate';              

            newUserVal.Customer_Support_Email__c =  'test@gmail.com';  
            newUserVal.Customer_Support_Phone__c = '111-111-1111';   
            newUserVal.Customer_Support_Message__c = 'something wrong';            
            insert newUserVal; 
    
    
    
            Account account = AccountTestData.createAccount('100', Null);
            account.Customer_Number__c = '100';
            insert account;
            Date invdate =  Date.today();
            //create test invoice and link it to the second eft
            USCP_Invoice__c invoice = USCP_TestData.createInvoice(account.Id, 'INV100',  null,  invdate , invdate , invdate , 1000.0,  true);
            invoiceid =  invoice.id;
            
    }    
}