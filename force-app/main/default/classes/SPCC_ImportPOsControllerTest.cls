@isTest
private class SPCC_ImportPOsControllerTest {
	
    //Test FindCollumnIndexes() method
    @isTest static void FindCollumnIndexesTest() {

        //Test find column index with template header values
        String headerLine = 'PO Number,LI Number, EWR,AFE,GL Code,Cost Code,Vendor,Vendor ID,Line Item Value,Line Item Short Text,Description';
        SPCC_ImportPOsController.CheckCollumnIndexes(headerLine);
        System.assertEquals(0, SPCC_ImportPOsController.COLUMN_INDEX_PO_NUMBER);
        System.assertEquals(1, SPCC_ImportPOsController.COLUMN_INDEX_LI_NUMBER);
        System.assertEquals(2, SPCC_ImportPOsController.COLUMN_INDEX_EWR);
        System.assertEquals(3, SPCC_ImportPOsController.COLUMN_INDEX_AFE);
        System.assertEquals(4, SPCC_ImportPOsController.COLUMN_INDEX_GL_CODE);
        System.assertEquals(5, SPCC_ImportPOsController.COLUMN_INDEX_COST_CODE);
        System.assertEquals(6, SPCC_ImportPOsController.COLUMN_INDEX_VENDOR);
        System.assertEquals(7, SPCC_ImportPOsController.COLUMN_INDEX_VENDOR_ID);
        System.assertEquals(8, SPCC_ImportPOsController.COLUMN_INDEX_LI_VALUE);
        System.assertEquals(9, SPCC_ImportPOsController.COLUMN_INDEX_LI_SHORT_TEXT);
        System.assertEquals(10, SPCC_ImportPOsController.COLUMN_INDEX_DESCRITPION);
    }


    //String & Decimal Parse Test 
    @isTest 
    static void GetValueMethodsTest() {
        String[] csvRecordData = new String[]{'TEST1', '5.5'};
        System.assertEquals(csvRecordData[0], SPCC_ImportPOsController.GetStringValue(csvRecordData, 0));     
        System.assertEquals(5.5, SPCC_ImportPOsController.GetDecimalValue(csvRecordData, 1));               
    }
    
    
    //Test Header checks
    @isTest 
    static void headerCheckTest() {
        
        CreateTestData();

    	//Start Test
    	Test.startTest();
        User pcTestUser = GetPCUser();

        System.runAs(pcTestUser)
        {
            PageReference pageRef = Page.SPCC_ImportPOs;//Observe how we set the Page here
            Test.setCurrentPage(pageRef);//Applying page context here
        	SPCC_ImportPOsController ctrl = new SPCC_ImportPOsController();
        	
        	 //test header field count (number of columns in ideal case)
        	 String[] csvLines = new String[]{
            'PO Number,Line Item Number,EWR,AFE,GL Code,Cost Code,Vendor,Vendor ID,Line Item Value,Line Item Short Text', //missing PO Description
            '0123456789,0010,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,5000,updated li text,Test Update' 
             };

            string strCSV =  String.join( csvLines, '\r\n' );
            Blob fileBody = Blob.valueof(strCSV);

            ctrl.csvFileBody = fileBody;
            ctrl.importCSVFile();
            
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean containsError = false;
            
            for (Apexpages.Message msg:msgs){
                    
                    if (msg.getDetail().contains('Import file needs to have 11 columns')){ 
                        containsError = true;
                    }
            }
            system.assert(containsError);//Assert the Page Error Message was Properly Displayed
        	
        	
            //test header names
        	String[] csvLines2 = new String[]{
            'PO,1Line Item Number,2EWR,3AFE,4GL Code,5Cost Code,6Vendor,7Vendor ID,8Line Item Value,9Line Item Short Text,10PO Description', //all header fields with corrupted names
            '0123456789,0010,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,5000,updated li text,Test Update' 
             };

            string strCSV2 =  String.join( csvLines2, '\r\n' );
            Blob fileBody2 = Blob.valueof(strCSV2);
            
            ctrl.csvFileBody = fileBody2;
            ctrl.importCSVFile();
            
            List<Apexpages.Message> msgs2 = ApexPages.getMessages();//there will be more error messages - for each header name
            boolean containsError2 = false;
            
            for (Apexpages.Message msg  :msgs2){
                    
                    if (msg.getDetail().contains('<b> Import file error: </b> 1st column\'s header has to be " <b> PO Number </b> " (case sensitive). ')){
                        containsError2 = true;
                    }
            }
            system.assert(containsError2);//Assert the Page Error Message was Properly Displayed
        }
        Test.stopTest();  
    }


    //Test Controller
    @isTest 
    static void ControllerTest() {

        CreateTestData();

    	//Start Test
    	Test.startTest();

        List<SPCC_Purchase_Order__c> testPoList = GetPOData();
        User pcTestUser = GetPCUser();

        System.runAs(pcTestUser)
        {
        	SPCC_ImportPOsController ctrl = new SPCC_ImportPOsController();

        	String[] csvLines = new String[]{
            'PO Number,Line Item Number,EWR,AFE,GL Code,Cost Code,Vendor,Vendor ID,Line Item Value,Line Item Short Text,PO Description',
            '0123456789,0010,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,5000,updated li text,Test Update', //exsiting PO - test of update PO and LI  [0]
            '0123456789,0030,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,50,new li,Test Update', //exsiting PO - adding new LI  [1]
            '0000000001,0010,00001,22-123A56-12-AB-12,1234590,8888,SPCC Account,55555,899,asdf,Error Vendor', //existing PO - trying to update vendor - ERROR [2]
            '0000000002,0040,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,299,new LI,Test Insert new PO with LI', //new PO - test insertion + new LI creation [3]
            ',,,,,,,,,,Missing Data Test', //test of required fields missing [4]
            '0000000003,0010,54321,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,10,asd,Wrong EWR', //not existing EWR [5]
            '0000000002,0040,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,299,new LI duplicate,duplicate PO entry', //duplicate PO entry - ERROR [6]
            '0000000004,0010,12345,12-123A56-12-AB-12,123456789,9999,SPCC Account,55555,10,asd,wrong GL Number format', //wrong GL Number format' [7]
            '0000000005,0010,12345,12-123A56-12-AB-12,1111111,9999,SPCC Account,55555,10,asd,custom GL number', //custom GL number [8]
            '0000000007,0010,12345,22-123A56-12-AB-12,1234567,9999,SPCC Account,55555,30,asd,Wrong EWR/AFE cpmbination', //not existing EWR/AFE combination [9]
            '0000000008,0010,12345,12-123A56-12-AB-12,1234567,,Not Existing Account,44444,40,asd,Unable to find Vendor', //Unable to find Vendor[10]
            '0000000002,0050,12345,12-123A56-12-AB-12,1234567,9999,Random Acc,33333,299,new LI,different key field - Vendor', //PO entry with different key field [11]
            '0000000009,0010,12345,77-000000-22-BC-88,1234567,9999,SPCC Account,55555,20,asdf,Wrong AFE', //not existing AFE [12]
            '0000000011,0010,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account 2,66666,200,asdf,New VA', //new VAA [13]
            '12,0040,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,299,new LI,new PO with shorter po number length', //new po with short po number [14]
            '0000000013,40,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,299,new LI,new PO with shorter po li number length', //new po with short li number [15]
            '0000000014,44,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,299,new LI,new PO with not valid li number', //new po with no valid li number [16]
            '0000000015,0010,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account55555,,-7000,bcd,negative Li value', //error - negative li value [17]
            '0000000016,0010,12345,12-123A56-12-AB-12,1234567,9999,SPCC Account,55555,7000,"bcd, hello","negative, Li value"', //double quotes handling test [18]
            '0000000017,0010,12345,12-123A56-12-AB-12,,9999,SPCC Account,55555,7000,bcd,asdf',  //Cost codes are not in the system yet. Please enter GL Code [19]
            '0000000005,0020,12345,12-123A56-12-AB-12,1111111,9999,SPCC Account,55555,10,asd,custom GL number' //custom GL number presented second time [8] - [20]
             };


            string strCSV =  String.join( csvLines, '\r\n' );
            Blob fileBody = Blob.valueof(strCSV);

            ctrl.csvFileBody = fileBody;

            ctrl.importCSVFile();
            System.assertEquals(21, ctrl.poList.size());
            System.assertEquals(1, [SELECT Id FROM SPCC_Vendor_Account_Assignment__c WHERE Engineering_Work_Request__c =: ctrl.poList[13].po.Engineering_Work_Request__c].Size());
            System.assertEquals(2, [SELECT Id FROM SPCC_Line_Item__c WHERE Purchase_Order__r.Name = '0123456789'].Size());
            System.assertEquals(0, [SELECT Id FROM SPCC_Cost_Element__c WHERE Category__c = 'Miscellaneous'].Size());
            System.assertEquals(2, [SELECT Id FROM SPCC_Purchase_Order__c].Size());
            
            System.assertEquals(true, ctrl.poList[13].isInfo);
            System.assert(ctrl.poList[13].errorMessage.contains('will be assigned'));
            System.assertEquals(true, ctrl.poList[17].isError);
            System.assert(ctrl.poList[17].errorMessage.contains('Line Item Value can not be negative.'));


            ctrl.importPOList();
            System.assertEquals(false, ctrl.poList[0].isError);
            System.assertEquals(true, ctrl.poList[0].existingPO);
            System.assertEquals(false, ctrl.poList[1].isError);
            System.assertEquals(true, ctrl.poList[1].existingPO);
            System.Debug('debug_ctrl.poList[2].errorMessage ' + ctrl.poList[2].errorMessage);
            System.assertEquals(true, ctrl.poList[2].isError);
            System.assert(ctrl.poList[2].errorMessage.contains('Some of the key fields are different for the PO to be imported'));
            System.assertEquals(false, ctrl.poList[3].existingPO);
            System.assertEquals(false, ctrl.poList[3].isError);
 			System.assertEquals(false, ctrl.poList[4].existingPO);
            System.assertEquals(true, ctrl.poList[4].isError);
            System.assert(ctrl.poList[4].errorMessage.contains('PO Number is required'));
            System.assert(ctrl.poList[4].errorMessage.contains('EWR is required'));
            System.assert(ctrl.poList[4].errorMessage.contains('AFE is required'));
            System.assert(ctrl.poList[4].errorMessage.contains('Either GL Code or Cost Code is required'));
            System.assert(ctrl.poList[4].errorMessage.contains('Vendor ID is required'));
            System.assert(ctrl.poList[4].errorMessage.contains('Line Item Number is required'));
            System.assert(ctrl.poList[4].errorMessage.contains('Line Item Value is required'));
            System.assert(ctrl.poList[4].errorMessage.contains('Line Item Short Text is required'));
            System.assertEquals(true, ctrl.poList[5].isError);
            System.assert(ctrl.poList[5].errorMessage.contains('Unable to find EWR'));
            System.assertEquals(true, ctrl.poList[6].isError);
            System.assert(ctrl.poList[6].errorMessage.contains('Line Item for this PO is already presented in import file'));
            System.assert(ctrl.poList[6].errorMessage.contains('PO with the same LI Number, EWR Number, AFE Number and Vendor'));
            System.assertEquals(true, ctrl.poList[7].isError);
            System.assert(ctrl.poList[7].errorMessage.contains('Wrong format for G/L Account'));
            System.assertEquals(false, ctrl.poList[8].isError);
            System.assertEquals(false, ctrl.poList[8].existingPO);
            System.assertEquals(true, ctrl.poList[9].isError);
            System.assert(ctrl.poList[9].errorMessage.contains('No such combination of AFE / EWR in the system'));
            System.assertEquals(true, ctrl.poList[10].isError);
            System.assert(ctrl.poList[10].errorMessage.contains('Unable to find Vendor'));
            System.assertEquals(true, ctrl.poList[11].isError);
            System.assert(ctrl.poList[11].errorMessage.contains('Some of the key fields on this PO does not match with previous PO entry key fields in import field'));
            System.assertEquals(true, ctrl.poList[12].isError);
            System.assert(ctrl.poList[12].errorMessage.contains('Unable to find AFE'));
            System.assertEquals(false, ctrl.poList[14].isError);
            System.assertEquals(false, ctrl.poList[14].existingPO);
            System.assertEquals(false, ctrl.poList[15].isError);
            System.assertEquals(false, ctrl.poList[15].existingPO);
            System.assertEquals(true, ctrl.poList[16].isError);
            System.assert(ctrl.poList[16].errorMessage.contains('Line Item value does not match line item picklist value in salesforce'));

            //Test creation of new records after import
            System.assertEquals(2, [SELECT Id 
                                    FROM SPCC_Vendor_Account_Assignment__c 
                                    WHERE Engineering_Work_Request__c =: ctrl.poList[13].po.Engineering_Work_Request__c].Size());
                                    
            System.assertEquals('1111111', [SELECT Purchase_Order__r.Purchase_Order_Number__c, Cost_Element__r.GL_Number__c 
                                            FROM SPCC_Line_Item__c 
                                            WHERE Purchase_Order__r.Purchase_Order_Number__c = '0000000005' 
                                            AND Item__c = '0010'].Cost_Element__r.GL_Number__c);
                                            
            System.assertEquals('1111111', [SELECT Purchase_Order__r.Purchase_Order_Number__c, Cost_Element__r.GL_Number__c 
                                            FROM SPCC_Line_Item__c 
                                            WHERE Purchase_Order__r.Purchase_Order_Number__c = '0000000005' 
                                            AND Item__c = '0020'].Cost_Element__r.GL_Number__c);
                                            
            System.assertEquals(1, [SELECT Id 
                                    FROM SPCC_Cost_Element__c 
                                    WHERE Category__c = 'Miscellaneous'].Size());
                                    
            System.assertEquals(8, [SELECT Id 
                                    FROM SPCC_Purchase_Order__c].Size());
                                    
            System.assertEquals('0000000012', [SELECT Id,Purchase_Order_Number__c 
                                               FROM SPCC_Purchase_Order__c 
                                               WHERE Id =: ctrl.poList[14].po.Id].Purchase_Order_Number__c);
                                               
            System.assertEquals('0040', [SELECT Id, Item__c 
                                         FROM SPCC_Line_Item__c 
                                         WHERE Purchase_Order__c =: ctrl.poList[15].po.Id].Item__c);
            
            //Test field values (double quotes handling for PO Description and LI Short Text) after import
            System.assertEquals('negative, Li value', [SELECT Id,Description__c 
                                                       FROM SPCC_Purchase_Order__c 
                                                       WHERE Purchase_Order_Number__c = '0000000016'].Description__c);
                                                       
            System.assertEquals('bcd, hello', [SELECT Id,Short_Text__c 
                                               FROM SPCC_Line_Item__c 
                                               WHERE Purchase_Order__c =: ctrl.poList[18].po.Id].Short_Text__c);
        }
        Test.stopTest();  
    }


    //Create Test Data for test methods
    private static void CreateTestData() {

		
		//Create SPCC Users
		User pcUser = SPCC_TestData.createPCUser();
		User spccUser = SPCC_TestData.createSPCCUser();
		User plUser = SPCC_TestData.createPLUser();
		User epcUser = SPCC_TestData.createEPCUser();

		System.runAs(pcUser){

			//Create a SPCC Data
			Account spccAccount = SPCC_TestData.createSPCCVendor('SPCC Account');
			spccAccount.SAP_ID__c = '55555';
			insert spccAccount;

            Account spccAccount2 = SPCC_TestData.createSPCCVendor('SPCC Account 2');
            spccAccount2.SAP_ID__c = '66666';
            insert spccAccount2;

            Account epcAccount = SPCC_TestData.createEPCVendor('EPC Vendor');
            insert epcAccount;

            Contact epcContact = SPCC_TestData.createContact(epcAccount.Id, 'EPC', 'Acc', epcUser.Id);
            insert epcContact;

			SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('Test Project', '12345', '12345','Other');
			SPCC_Engineering_Work_Request__c ewr1 = SPCC_TestData.createEWR('Test Project 2', '00001', '00001','Other');
			insert ewr;
			insert ewr1;

			SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('12-123A56-12-AB-12', '12-123A56-12-AB-12', 'Test', ewr.Id);
			SPCC_Authorization_for_Expenditure__c afe1 = SPCC_TestData.createAFE('22-123A56-12-AB-12', '22-123A56-12-AB-12', 'Test AFE 2', ewr1.Id);
			insert afe;
			insert afe1;

			SPCC_Cost_Element__c glCode = SPCC_TestData.createCostElement(afe.Id, null, 'Test GL Code', '1234567', 'Service', 30, 200);
			SPCC_Cost_Element__c glCode1 = SPCC_TestData.createCostElement(afe1.Id, null, 'Test GL Code 2', '1234590', 'Service', 20, 100);
			insert glCode;
			insert glCode1;

			SPCC_Cost_Code__c costCode = SPCC_TestData.createCostCode(glCode.Id, '9999', 'Test Codt Code', 300);
			SPCC_Cost_Code__c costCode1 = SPCC_TestData.createCostCode(glCode1.Id, '8888', 'Test Codt Code 2', 500);
			insert costCode;
			insert costCode1;

			SPCC_Vendor_Account_Assignment__c vaa = SPCC_TestData.createVAA(ewr.Id, spccAccount.Id);
			SPCC_Vendor_Account_Assignment__c vaa1 = SPCC_TestData.createVAA(ewr1.Id, spccAccount.Id);
			insert vaa;
			insert vaa1;

			SPCC_Project_Lead_Assignment__c pla = SPCC_TestData.createPLA(ewr.Id, plUser.Id);
			SPCC_Project_Lead_Assignment__c pla1 = SPCC_TestData.createPLA(ewr1.Id, plUser.Id);
			insert pla;
			insert pla1;

			SPCC_Purchase_Order__c po = SPCC_TestData.createPO('0123456789', ewr.ID, afe.Id, spccAccount.Id, 
														       '0123456789', 250, 'test PO', 'In Progress');
			insert po;

            SPCC_Purchase_Order__c po2 = SPCC_TestData.createPO('0000000001', ewr1.ID, afe1.Id, spccAccount2.Id, 
                                                                '0000000001', 250, 'test EPC PO', 'In Progress');
            insert po2;

            SPCC_Line_Item__c li1_po =  SPCC_TestData.createLineItem(po.Id, glCode.Id, 100, '0010', 'some text li 1');
            insert li1_po;

            SPCC_Line_Item__c li2_po =  SPCC_TestData.createLineItem(po.Id, glCode.Id, 100, '0020', 'some text li 2');
            insert li2_po;
		}
	}


    //DATA RETRIEVAL FUNCTIONS FOR TEST DATA
    private static List<SPCC_Purchase_Order__c> GetPOData(){

    	return [SELECT Name, Engineering_Work_Request__c, Authorization_for_Expenditure__c, Vendor_Account__c,
    				   Cost_Code__c, Purchase_Order_Number__c, Description__c, Status__c
    			FROM SPCC_Purchase_Order__c];
    }


    private static User GetPCUser(){

    	return [SELECT UserName, Email 
    			FROM User 
    			WHERE LastName = 'User_PC' LIMIT 1];
	}
}