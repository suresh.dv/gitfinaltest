/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class StandardPaginationControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {

            Integer numAccounts = 200;
            setup(numAccounts);
    
            ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(Database.getQueryLocator([select Id, Name, CreatedDate from Account]));
            StandardPaginationController pController = new StandardPaginationController(standardSetController);
            Integer pageCount = pController.getPageCount();
            //test account search method for autocomplete
            System.AssertEquals(10,pageCount);
            
            StandardPaginationController pc = pController.getPaginationController();
            System.AssertNotEquals(pc, Null);  
        }
    }

    public static void setup(Integer numAccounts) {
        // create numAccounts Account records
        List<Account> accounts = new List<Account>();
        for (Integer i=0; i<numAccounts; i++) {
            Account account = new Account(Name = 'TEST ' + i);
            accounts.add(account);
        }
        if (!accounts.isEmpty()) insert accounts;
    }    
}