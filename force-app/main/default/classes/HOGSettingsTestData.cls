/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Settings__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class HOGSettingsTestData
{
    public static HOG_Settings__c createHOGSettings()
    {                
		HOG_Settings__c results = new HOG_Settings__c
			(
				Recurring_Service_Max_Daily_Work_Orders__c = 99,
				Recurring_Service_Can_Create_Prior_Dates__c = false,
				SAP_Create_Work_Order_WSR_Work_Centre__c = 'OPER',
				SAP_Create_Work_Order_Activity_Code__c = '0010',
				SAP_Create_Work_Order_Control_Key__c = 'INT1'
			);

        return results;
    }
    
}