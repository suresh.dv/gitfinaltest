@isTest(SeeAllData = True)
private class opportunityProductTest {

    static testMethod void OpportunityProductMultilineItemTest(){
        // Create Products        
        List<Product2> productList = ProductTestData.createProducts(101);
        insert productList;
        
        // Create Price Book Entries
        /*List<PriceBookEntry> priceBookEntryList = PriceBookEntryTestData.createPriceBookEntry(productList);
        insert priceBookEntryList;*/
        
        Map<String, Id> recordTypeMap = new Map<String, Id>();

        for(RecordType recType : [SELECT Id, DeveloperName FROM RecordType 
                                  WHERE DeveloperName in ('ATS_Asphalt_Product_Category', 'ATS_Emulsion_Product_Category', 'ATS_Residual_Product_Category') AND 
                                  SObjectType = 'Opportunity']) {
            recordTypeMap.put(recType.DeveloperName, recType.Id);            
        }
        
        ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];
                
        List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
        
        Opportunity opp = OpportunityTestData.createOpportunity(recordTypeMap.get('ATS_Asphalt_Product_Category'));
        opp.Opportunity_ATS_Product_Category__c = tender.Id;
        insert opp;
        opp.Pricebook2Id = standardPriceBook[0].Id;
        opp.priceBook2 = standardPriceBook[0];
        update opp;
        
        List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: standardPriceBook[0].Id];
        priceBookEntryList[0].IsActive = true;
        update priceBookEntryList[0];
        OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
        insert oli;
        
        Opportunity theOpp = [select Id, Pricebook2Id, PriceBook2.Name, RecordTypeId from Opportunity where Id = :opp.Id limit 1];
        System.assertEquals(theOpp.Pricebook2Id, standardPriceBook[0].Id);
        System.assertNotEquals(theOpp.Pricebook2, Null);
        System.assertNotEquals(theOpp.PriceBook2.Name, Null);
        ////////////////////////////////////////
        //  test OpportunityProductMultilineItem
        ////////////////////////////////////////
        
        // load the page       
        PageReference pageRef = Page.OpportunityProductMultilineItem;
        pageRef.getParameters().put('Id',opp.Id);
        Test.setCurrentPageReference(pageRef);
        
        // load the extension
        OpportunityProductMultilineItem oPMLI = new OpportunityProductMultilineItem(new ApexPages.StandardController(opp));
        
        // We know that there is at least one line item, so we confirm
        Integer startCount = oPMLI.ShoppingCart.size();
        system.assert(startCount>0);

        //test search functionality without finding anything
        oPMLI.searchString = '**********????????';
        oPMLI.updateAvailableList();
        system.assert(oPMLI.AvailableProducts.size()==0);
        
        //test remove from shopping cart
        oPMLI.toUnselect = oli.PricebookEntryId;
        oPMLI.removeFromShoppingCart();
        system.assert(oPMLI.shoppingCart.size() == startCount-1);
        
        //test save and reload extension
        oPMLI.onSave();
        oPMLI = new OpportunityProductMultilineItem(new ApexPages.StandardController(opp));
        system.assert(oPMLI.shoppingCart.size()==startCount-1);
        
        // test search again, this time we will find something
        oPMLI.searchString = oli.PricebookEntry.Name;
        oPMLI.updateAvailableList();
        system.assert(oPMLI.AvailableProducts.size()>0);       

        // test add to Shopping Cart function
        oPMLI.toSelect = oPMLI.AvailableProducts[0].Id;
        oPMLI.addToShoppingCart();
        system.assert(oPMLI.shoppingCart.size()==startCount);
                
        // test save method - WITHOUT quanitities and amounts entered and confirm that error message is displayed
        oPMLI.onSave();
        system.assert(ApexPages.getMessages().size()>0);
        
        // add required info and try save again
        for(OpportunityLineItem o : oPMLI.ShoppingCart){
            o.quantity = 5;            
        }
        oPMLI.onSave();
        
        // query line items to confirm that the save worked
        opportunityLineItem[] oli2 = [select Id from opportunityLineItem where OpportunityId = :oli.OpportunityId];
        system.assert(oli2.size()==startCount);
        
        System.assert(oPMLI.changePricebook() != Null);
        System.assert(oPMLI.onCancel() != Null);
     
    }
    
    static testMethod void OpportunityProductViewAndEditTest_Asphalt(){
        // Create Products        
        List<Product2> productList = ProductTestData.createProducts(101, Product2.SObjectType.getDescribe().getRecordTypeInfosByName().get('Asphalt').getRecordTypeId());
        insert productList;
        
        Map<String, Schema.Recordtypeinfo> recordTypeMap = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();

        
        ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];
        
        List<PriceBook2> asphaltPriceBook = [SELECT Id FROM PriceBook2 WHERE name =: 'Asphalt Price Book'];
        
        //Opportunity opp = OpportunityTestData.createOpportunity(recordTypeMap.get('ATS: Asphalt Product Category').getRecordTypeId());
        Account acct = AccountTestData.createAccount('Test Account', null);
        insert acct;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.AccountId = acct.Id;
        opp.CloseDate = Date.today();
        opp.StageName = 'Opportunity Initiation - Marketer';
        opp.RecordTypeId = recordTypeMap.get('ATS: Asphalt Product Category').getRecordTypeId();
        opp.Opportunity_ATS_Product_Category__c = tender.Id;
        insert opp;
        System.debug('Opportunity.RecordType: ' + opp.RecordTypeId);
        opp.Pricebook2Id = asphaltPriceBook[0].Id;
        opp.priceBook2 = asphaltPriceBook[0];
        update opp;

        Opportunity theOpp = [select Id, Pricebook2Id, PriceBook2.Name, RecordTypeId from Opportunity where Id = :opp.Id limit 1];
        System.assertEquals(theOpp.Pricebook2Id, asphaltPriceBook[0].Id);
        System.assertNotEquals(theOpp.Pricebook2, Null);
        System.assertNotEquals(theOpp.PriceBook2.Name, Null);

        //freight
        ATS_Freight__c  freight = new ATS_Freight__c();
        freight.Husky_Supplier_1__c= 'Husky_Supplier_1';
        freight.Husky_Supplier_2__c= 'Husky_Supplier_2';          
        freight.Competitor_Supplier_1__c= 'Competitor_Supplier_1';
        freight.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
        freight.ATS_Freight__c = opp.Id;
        freight.Husky_Supplier_1_Selected__c = true;
        freight.Prices_F_O_B__c = 'Origin + Freight';                       
        insert freight;
        opp.ATS_Freight__c = freight.Id;
        update opp;

        List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: asphaltPriceBook[0].Id And Name = 'Product Test Name 0'];
        priceBookEntryList[0].IsActive = true;
        update priceBookEntryList[0];
        OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
        insert oli;
        System.debug('Product2Id: ' + oli.Product2Id);
        System.debug('oli.Opportunity.RecordTypeId: ' + oli.Opportunity.RecordTypeId);
        
        // load the page       
        PageReference pageRef = Page.OpportunityProductView;
        pageRef.getParameters().put('id',oli.Id);
        pageRef.getParameters().put('sfdc.override','1');
        Test.setCurrentPageReference(pageRef);

        
        // load the extension
        OpportunityProduct op = new OpportunityProduct(new ApexPages.StandardController(oli));
        
        // load the page       
        pageRef = Page.OpportunityProductEdit_Asphalt;
        pageRef.getParameters().put('id',oli.Id);
        Test.setCurrentPageReference(pageRef);
        
        //Test Functionality
        List<SelectOption> supplierWinners = op.getSupplierWinnerList();
        System.assertNotEquals(supplierWinners, null);
        System.assertNotEquals(op.save(), null);
        oli.Currency__c = 'US';
        oli.Exchange_Rate_to_CAD__c = 0;
        update oli;
        System.assertEquals(op.save(), null);
        System.assertNotEquals(op.redirect(), null);
        op.recalculateRefineryNetback();
        op.axle5PriceManuallySet();
        op.axle6PriceManuallySet();
        op.axle7PriceManuallySet();
        op.axle8PriceManuallySet();
        op.recalculateCalculatedAxlePrices();
        op.recalculateAllNetbacks();
        op.recalculateAllEmulsion();
    }

    static testMethod void OpportunityProductViewAndEditTest_Emulsion() {
        // Create Products        
        List<Product2> productList = ProductTestData.createProducts(101, Product2.SObjectType.getDescribe().getRecordTypeInfosByName().get('Emulsion').getRecordTypeId());
        insert productList;
        
        Map<String, Schema.Recordtypeinfo> recordTypeMap = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();

        
        ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];
        
        List<PriceBook2> emulsionPriceBook = [SELECT Id FROM PriceBook2 WHERE name =: 'Emulsion Price Book'];
        
        //Opportunity opp = OpportunityTestData.createOpportunity(recordTypeMap.get('ATS: Asphalt Product Category').getRecordTypeId());
        Account acct = AccountTestData.createAccount('Test Account', null);
        insert acct;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.AccountId = acct.Id;
        opp.CloseDate = Date.today();
        opp.StageName = 'Opportunity Initiation - Marketer';
        opp.RecordTypeId = recordTypeMap.get('ATS: Emulsion Product Category').getRecordTypeId();
        opp.Opportunity_ATS_Product_Category__c = tender.Id;
        insert opp;
        System.debug('Opportunity.RecordType: ' + opp.RecordTypeId);
        opp.Pricebook2Id = emulsionPriceBook[0].Id;
        opp.priceBook2 = emulsionPriceBook[0];
        update opp;

        Opportunity theOpp = [select Id, Pricebook2Id, PriceBook2.Name, RecordTypeId from Opportunity where Id = :opp.Id limit 1];
        System.assertEquals(theOpp.Pricebook2Id, emulsionPriceBook[0].Id);
        System.assertNotEquals(theOpp.Pricebook2, Null);
        System.assertNotEquals(theOpp.PriceBook2.Name, Null);

        //freight
        ATS_Freight__c  freight = new ATS_Freight__c();
        freight.Husky_Supplier_1__c= 'Husky_Supplier_1';
        freight.Husky_Supplier_2__c= 'Husky_Supplier_2';          
        freight.Competitor_Supplier_1__c= 'Competitor_Supplier_1';
        freight.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
        freight.ATS_Freight__c = opp.Id;
        freight.Husky_Supplier_1_Selected__c = true;
        freight.Prices_F_O_B__c = 'Origin + Freight';                       
        insert freight;
        opp.ATS_Freight__c = freight.Id;
        update opp;

        List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: emulsionPriceBook[0].Id And Name = 'Product Test Name 0'];
        priceBookEntryList[0].IsActive = true;
        update priceBookEntryList[0];
        OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
        insert oli;
        System.debug('Product2Id: ' + oli.Product2Id);
        System.debug('oli.Opportunity.RecordTypeId: ' + oli.Opportunity.RecordTypeId);
        OpportunityLineItem oli2 = [Select Id, Product2Id, Opportunity.ATS_Freight__c, Opportunity.RecordTypeId, 
                                    Axle_5_Price_Set_Manually__c,
                                    Axle_6_Price_Set_Manually__c,
                                    Axle_7_Price_Set_Manually__c,
                                    Axle_8_Price_Set_Manually__c,
                                    Competitor_1__c,
                                    Competitor_2__c,
                                    Competitor_3__c,
                                    Competitor_4__c,
                                    Competitor_5__c,
                                    Currency__c,
                                    Exchange_Rate_to_CAD__c,
                                    Refinery_Netback_CAD__c,
                                    Terminal_Netback_CAD__c,
                                    Axle_5_Price__c,
                                    Axle_6_Price__c,
                                    Axle_7_Price__c,
                                    Axle_8_Price__c,
                                    Competitor_1_Refinery_Netback_CAD__c,
                                    Competitor_1_Terminal_Netback_CAD__c,
                                    Competitor_2_Refinery_Netback_CAD__c,
                                    Competitor_2_Terminal_Netback_CAD__c,
                                    Competitor_3_Refinery_Netback_CAD__c,
                                    Competitor_3_Terminal_Netback_CAD__c,
                                    Competitor_4_Refinery_Netback_CAD__c,
                                    Competitor_4_Terminal_Netback_CAD__c,
                                    Competitor_1_Emulsion_Margin_CAD__c,
                                    Competitor_2_Emulsion_Margin_CAD__c,
                                    Competitor_3_Emulsion_Margin_CAD__c,
                                    Competitor_4_Emulsion_Margin_CAD__c
                                    From OpportunityLineItem Where Id =: oli.Id ];
        System.debug('oli2.Product2Id: ' + oli2.Product2Id);
        System.debug('oli2.Opportunity.RecordTypeId: ' + oli2.Opportunity.RecordTypeId);

        // load the page       
        PageReference pageRef = Page.OpportunityProductView;
        pageRef.getParameters().put('id',oli.Id);
        pageRef.getParameters().put('sfdc.override','1');
        Test.setCurrentPageReference(pageRef);

        
        // load the extension
        OpportunityProduct op = new OpportunityProduct(new ApexPages.StandardController(oli2));
        
        // load the page       
        pageRef = Page.OpportunityProductEdit_Asphalt;
        pageRef.getParameters().put('id',oli.Id);
        Test.setCurrentPageReference(pageRef);
        
        //Test Functionality
        List<SelectOption> supplierWinners = op.getSupplierWinnerList();
        System.assertNotEquals(supplierWinners, null);
        System.assertNotEquals(op.save(), null);
        System.assertNotEquals(op.redirect(), null);
        op.recalculateRefineryNetback();
        op.axle5PriceManuallySet();
        op.axle6PriceManuallySet();
        op.axle7PriceManuallySet();
        op.axle8PriceManuallySet();
        op.recalculateCalculatedAxlePrices();
        op.recalculateAllNetbacks();
        op.recalculateAllEmulsion();
    }

    static testMethod void testCheckDuplicateCompetitor()
    {
        // Create Products        
        List<Product2> productList = ProductTestData.createProducts(101);
        insert productList;
        
        // Create Price Book Entries
        /*List<PriceBookEntry> priceBookEntryList = PriceBookEntryTestData.createPriceBookEntry(productList);
        insert priceBookEntryList;*/
        
        Map<String, Schema.Recordtypeinfo> recordTypeMap = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();

        
        ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];
        
        List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
        
        Opportunity opp = OpportunityTestData.createOpportunity(recordTypeMap.get('ATS: Asphalt Product Category').getRecordTypeId());
        opp.Opportunity_ATS_Product_Category__c = tender.Id;
        insert opp;
        opp.Pricebook2Id = standardPriceBook[0].Id;
        opp.priceBook2 = standardPriceBook[0];
        update opp;
        
        List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: standardPriceBook[0].Id];
        priceBookEntryList[0].IsActive = true;
        update priceBookEntryList[0];
        OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
        insert oli;
        try
        {
            oli.Competitor_1__c = 'Cenex-Grand Forks';
            oli.Competitor_2__c = 'Cenex-Grand Forks';
            oli.Competitor_3__c = 'Cenex-Grand Forks';
            oli.Competitor_4__c = 'Cenex-Grand Forks';
            oli.Competitor_5__c = 'Cenex-Grand Forks';
            update oli;
            System.assert(true, 'You cannot choose the same Competitor more than once (Competitors 1, 2, 3, 4, 5)');
           
        }
        catch (Exception e){}
        
        try
        {
            oli.Competitor_1__c = 'Unknown';
            oli.Competitor_2__c = 'Cenex-Grand Forks';
            oli.Competitor_3__c = 'Cenex-Grand Forks';
            oli.Competitor_4__c = 'Cenex-Grand Forks';
            oli.Competitor_5__c = 'Cenex-Grand Forks';
            update oli;
            System.assert(true, 'You cannot choose the same Competitor more than once (Competitors 2, 3, 4, 5)');
        }
        catch (Exception e){}
        try
        {    
            oli.Competitor_1__c = 'Unknown';
            oli.Competitor_2__c = 'Unknown';
            oli.Competitor_3__c = 'Cenex-Grand Forks';
            oli.Competitor_4__c = 'Cenex-Grand Forks';
            oli.Competitor_5__c = 'Cenex-Grand Forks';
            update oli;
            System.assert(true, 'You cannot choose the same Competitor more than once (Competitors 3, 4, 5)');
        }
        catch (Exception e){}
        try
        {
            oli.Competitor_1__c = 'Unknown';
            oli.Competitor_2__c = 'Unknown';
            oli.Competitor_3__c = 'Unknown';
            oli.Competitor_4__c = 'Cenex-Grand Forks';
            oli.Competitor_5__c = 'Cenex-Grand Forks';
            update oli;
            System.assert(true, 'You cannot choose the same Competitor more than once (Competitors 4, 5)');
        }
        catch (Exception e)
        {
            
        }
    }
    
}