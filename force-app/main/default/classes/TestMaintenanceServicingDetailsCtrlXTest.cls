@isTest
private class TestMaintenanceServicingDetailsCtrlXTest {
	
	@isTest
	static void MaintenanceServicingActivityDetailsCtrlX_Test() {
        User supervisorUser = VTT_TestData.createVendorSupervisorUser();
        User runningUser = VTT_TestData.createVTTAdminUser();   
            
        System.runAs(runningUser) {
            //Setup Data
            Account vendor = VTT_TestData.createVendorAccount('Vendor1');
            Contact tradesman = VTT_TestData.createTradesmanContact('Test', 'Tradesman', vendor.Id);
            Contact supervisor = VTT_TestData.createTradesmanContact('Test', 'Supervisor',  vendor.Id, supervisorUser.Id);

            MaintenanceServicingUtilities.executeTriggerCode = false;       
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest();       
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);

            workOrder.Order_Type__c = 'WP01';
            workOrder.MAT_Code__c = 'TXN';
            update workOrder;

            //Create and assign work order activity
            List<Work_Order_Activity__c> activityList = VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder.Id, tradesman.Id, 1);
            activityList[0].Scheduled_Start_Date__c = Date.today();
            activityList[0].Assigned_Vendor__c = vendor.id;
            activityList[0].Vendor_Supervisor__c = supervisor.id;
            update activityList;

            //Test with dummy entries
            List<Work_Order_Activity_Log_Entry__c> testLogEntries = new List<Work_Order_Activity_Log_Entry__c>();
            testLogEntries.add(VTT_Utilities.CreateLogEntry(tradesman, activityList[0], VTT_Utilities.LOGENTRY_STARTJOB, 0, 0, 'Test Start Job', null));
            testLogEntries.add(VTT_Utilities.CreateLogEntry(tradesman, activityList[0], VTT_Utilities.LOGENTRY_STARTATSITE, 0, 0, 'Test Start At Site', null));
            insert testLogEntries;


            MaintenanceServicingActivityDetailsCtrlX controller = new MaintenanceServicingActivityDetailsCtrlX(new ApexPages.StandardController(workOrder));
            List<Work_Order_Activity__c> testActivityList = controller.getActivityList();
            List<Work_Order_Activity_Log__c> testActivityLogList = controller.getActivityLogMap().get(testActivityList[0].Id);
            List<Work_Order_Activity_Log_Entry__c> testActivityLogEntryList = controller.getActivityLogEntryMap().get(testActivityLogList[0].Id);

            System.assertEquals(testActivityList.size(), 1);
            System.assertEquals(testActivityLogList.size(), 1);
            System.assertEquals(testActivityLogEntryList.size(), 2);
        }
	}
}