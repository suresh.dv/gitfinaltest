public class PTBillingRateCheckCacheQuery {
	public static Map<id,Utility_Billing__c> uBillingMap = new Map<id,Utility_Billing__c>([Select id, name,billing_date__c,service_id__c,service_id__r.name,service_rate__c,utility_rate__c,site_id__c,billing_checked__c,service_id__r.business_unit__c,service_id__r.operating_district__c,service_id__r.amu_id__r.name,service_id__r.lsd_id__c from utility_billing__c order by billing_date__C DESC limit 25000]);
	public static boolean infoQueried = true;
    
    public static Map<id,Utility_Billing__c> queryInfo(){
        if(infoQueried == false){
            uBillingMap =new Map<id,Utility_Billing__c>([Select id, name,billing_date__c,service_id__c,service_id__r.name,service_rate__c,utility_rate__c,site_id__c,billing_checked__c,service_id__r.business_unit__c,service_id__r.operating_district__c,service_id__r.amu_id__r.name,service_id__r.lsd_id__c from utility_billing__c order by billing_date__C DESC limit 25000]);  
        }
        infoQueried = true;
        return uBillingMap;
    }
    
}