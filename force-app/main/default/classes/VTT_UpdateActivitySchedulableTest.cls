@isTest
private class VTT_UpdateActivitySchedulableTest {
	@isTest static void Test_Schedule() {
    
	    Test.startTest();
	      List<String> jobNames = new List<String> {
	        'VTT Revert Activity Status Batch Test', 'VTT Update Activity Status Batch Test'};

	      VTT_UpdateActivitySchedulable.scheduleJob();

	      List<CronTrigger> cts = [SELECT Id, CronJobDetail.Name
	                        FROM CronTrigger
	                        Where CronJobDetail.Name In :jobNames];
	      System.assertEquals(jobNames.size(), cts.size());
	    Test.stopTest();
  	}
	
}