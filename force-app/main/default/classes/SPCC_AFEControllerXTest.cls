@isTest
private class SPCC_AFEControllerXTest {
    static testMethod void testAFEControllerCreate() {
		
		User projectControllerUser = SPCC_TestData.createPCUser();
		
		System.runAs(projectControllerUser){

			SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('Test Project', '12345', '12345','Other');
			insert ewr;

			SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('12-123A56-12-AB-12', '12-123A56-12-AB-12', 'Test', ewr.Id);           
            PageReference pageRef = Page.SPCC_AFECreate;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters();
            
            ApexPages.StandardController sController = new ApexPages.StandardController(afe);
            SPCC_AFEControllerX afeController = new SPCC_AFEControllerX(sController);
            System.assertEquals(null,afeController.showGLBreakdown());

            //Create Miscellaneous GL Codes
            System.assertEquals(null, afeController.addMiscellaneousGLCode());
            System.assertEquals(null, afeController.addMiscellaneousGLCode());
            System.assertEquals(null, afeController.addMiscellaneousGLCode());
            System.assertEquals(3, afeController.miscGlCodes.size());
            afeController.miscGlCodes[0].costElement.GL_number__c = '6000001';
            afeController.miscGlCodes[0].costElement.Label__c = 'Test Misc GL Code 1';
            afeController.miscGlCodes[0].costElement.Original_Auth_Amt__c = 100.00;
            afeController.miscGlCodes[0].costElement.Trend__c = -50.00;
            afeController.miscGlCodes[1].costElement.GL_number__c = '6000002';
            afeController.miscGlCodes[1].costElement.Label__c = 'Test Misc GL Code 2';
            afeController.miscGlCodes[1].costElement.Original_Auth_Amt__c = 100.00;
            afeController.miscGlCodes[1].costElement.Trend__c = -50.00;
            afeController.miscGlCodes[2].costElement.GL_number__c = '6160056'; //Using standard GL Code to check the duplicate error
            afeController.miscGlCodes[2].costElement.Label__c = 'Test Misc GL Code 3';
            afeController.miscGlCodes[2].costElement.Original_Auth_Amt__c = 100.00;
            afeController.miscGlCodes[2].costElement.Trend__c = -50.00;

            //Check Math and Subtotal for Miscellaneous
            System.assertEquals(null, afeController.refreshApprovedLtdGross());
            System.assertEquals(50, afeController.miscGlCodes[0].currentAuthAmount);
            System.assertEquals(50, afeController.miscGlCodes[1].currentAuthAmount);
            System.assertEquals(50, afeController.miscGlCodes[2].currentAuthAmount);
            System.assertEquals(150, afeController.subTotalPerCategory.get('Miscellaneous').currentAuthAmtTotal);


            //Check Duplicate Error
            System.debug('testAFEControllerCreate->costElementCategoryMap: ' + afeController.costElementCategoryMap);
            System.assertEquals(null, afeController.save());
            Boolean errorFound = false;
            for(Apexpages.Message msg : Apexpages.getMessages()) {
                if(msg.getSummary().contains('duplicated!')) errorFound = true;
            }
            System.assert(errorFound);

            //Check GL Number Validation Error
            afeController.miscGlCodes[2].costElement.GL_number__c = '60000030';
            System.assertEquals(null, afeController.save());
            errorFound = false;
            for(Apexpages.Message msg : Apexpages.getMessages()) {
                if(msg.getSummary().contains('GL Number should be 7 digits long and numeric.')) errorFound = true;
            }
            System.assert(errorFound);

            //Correct Error and re save
            afeController.miscGlCodes[2].costElement.GL_number__c = '6000003';
            afeController.save();
            
            SPCC_Authorization_for_Expenditure__c afeRetrieved = [SELECT Id FROM SPCC_Authorization_for_Expenditure__c Where Name = :afe.Name];
 			System.assertEquals(afe.Id, afeRetrieved.Id);
            
            ///////////////////////////////
            //**Testing GL Metadata map**//
            //////////////////////////////
            
            Schema.DescribeSObjectResult descobj = SPCC_GL_Code_Entry__mdt.SObjectType.getDescribe();
            Map<String, Schema.SObjectField> objectFields = descobj.fields.getMap();
			List<SPCC_GL_Code_Entry__mdt> gls = [SELECT Category__c, GL_number__c, Label 
                                                 FROM SPCC_GL_Code_Entry__mdt];
        	Map<String, String> glMap = new Map<String, String>();
            for(SPCC_GL_Code_Entry__mdt glMD : gls) {
                glMap.put(glMD.GL_number__c, glMD.GL_number__c + '-' + glMD.Category__c + '-' + glMD.Label);
            }
            List<SPCC_Cost_Element__c> costEls = [SELECT Category__c, GL_Number__c, Label__c 
                                                 FROM SPCC_Cost_Element__c 
                                                 WHERE AFE__c =: afe.Id
                                                 And Category__c <> :SPCC_Utilities.GL_CODE_CATEGORY_OTHER];
            Map<String, String> costElMap = new Map<String, String>();
            for(SPCC_Cost_Element__c costEl : costEls ) {
                costElMap.put(costEl.GL_number__c, costEl.GL_number__c + '-' + costEl.Category__c + '-' + costEl.Label__c);  
            }
            for(String glNumber : costElMap.keySet() ) {
                System.assert(glMap.containsKey(glNumber));
            }
            for(String glKey : costElMap.values()){
                System.assertEquals(costElMap.get(glKey), glMap.get(glKey));
            }

            //Test MiscGl Codes
            List<SPCC_Cost_Element__c> miscGlCodes = [SELECT Category__c, GL_Number__c, Label__c 
                                                      FROM SPCC_Cost_Element__c 
                                                      WHERE AFE__c =: afe.Id
                                                      And Category__c = :SPCC_Utilities.GL_CODE_CATEGORY_OTHER];
            System.assertEquals('6000001', miscGlCodes[0].GL_number__c);
            System.assertEquals('Test Misc GL Code 1', miscGlCodes[0].Label__c);
            System.assertEquals('6000002', miscGlCodes[1].GL_number__c);
            System.assertEquals('Test Misc GL Code 2', miscGlCodes[1].Label__c);
            System.assertEquals('6000003', miscGlCodes[2].GL_number__c);
            System.assertEquals('Test Misc GL Code 3', miscGlCodes[2].Label__c);
            
        }
	}
    
    static testMethod void testAFEControllerEdit() {
		
		User projectControllerUser = SPCC_TestData.createPCUser();
		
		System.runAs(projectControllerUser){

			SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('Test Project', '12345', '12345','Other');
			insert ewr;

			SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('12-123A56-12-AB-12', '12-123A56-12-AB-12', 'Test', ewr.Id);
			insert afe;
            
            PageReference pageRef = Page.SPCC_AFEEdit;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('id', afe.Id);
            
            ApexPages.StandardController sController = new ApexPages.StandardController(afe);
            SPCC_AFEControllerX afeController = new SPCC_AFEControllerX(sController);

            SPCC_Cost_Element__c glCode = SPCC_TestData.createCostElement(afe.Id, null, 'Test GL Code', '0123456', 'Service', 40, 200);
			insert glCode;
            afeController.edit();
            afeController.save();
		}
	}
	
}