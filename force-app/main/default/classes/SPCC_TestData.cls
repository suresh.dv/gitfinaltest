@isTest
public class SPCC_TestData {

	/****************************** USER Related Data ******************************/ 

	  //Create SPCC User
	 private static User createUser(){

        User user = new User();
       
        Profile p = [SELECT Id FROM Profile WHERE Name =: SPCC_Utilities.SPCC_USER_PROFILE];

        Double random = Math.Random();

        user.email = 'SPCCUser' +  random + '@spcc.com';
        user.Alias = 'SPCCUser' ;
        user.EmailEncodingKey = 'UTF-8';
        user.LastName = 'User';
        user.LanguageLocaleKey = 'en_US'; 
        user.LocaleSidKey = 'en_US'; 
        user.ProfileId = p.Id;
        user.TimeZoneSidKey = 'America/Los_Angeles'; 
        user.UserName = 'SPCCUser' + random + '@SPCC.com.unittest';
        
        insert user;
        
        return user;    
    }


    //Permission Set Assignemnt
    private static User assignPermissionSet(User user, String userPermissionSetName){
    
        PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName]; 
        PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
        
        permissionSetAssignment.PermissionSetId = permissionSet.Id;
        permissionSetAssignment.AssigneeId = user.Id; 
        
        insert permissionSetAssignment;
            
        return user;
    }


    //Role Assignment
    private static User assignRole(User user, String userDeveloperRoleName) {
        UserRole role = [Select Id From UserRole Where DeveloperName =: userDeveloperRoleName];
        user.UserRoleId = role.Id;
        update user;
        return user;
    }


    //Profile Assignment
    private static User assignProfile(User user, String profileName) {
        Profile profile = [Select Id From Profile Where Name =: profileName];
        user.ProfileId = profile.Id;
        update user;
        return user;
    }

    public static User createEPCUser() {
        User user = createUser();
        user.LastName = user.LastName + '_EPC';
        user = assignProfile(user,SPCC_Utilities.SPCC_USER_PROFILE);
        user = assignPermissionSet(user,SPCC_Utilities.SPCC_EPC);
        user = assignRole(user,SPCC_Utilities.SPCC_EPC_ROLE);
        return user;
    }

    public static User createSPCCUser() {
    	User user = createUser();
    	return user;
    }
    
    public static User createCSRUser() {
        User user = createUser();
        user.LastName = user.LastName + '_CSR';
        user = assignProfile(user,SPCC_Utilities.SPCC_USER_PROFILE);
        user = assignPermissionSet(user,SPCC_Utilities.SPCC_CSR);
        user = assignRole(user,SPCC_Utilities.SPCC_CSR_ROLE);
        return user;
    }

    public static User createPCUser() {
        User user = createUser();
        user.LastName = user.LastName + '_PC';
        user = assignProfile(user,SPCC_Utilities.SPCC_USER_PROFILE);
        user = assignPermissionSet(user,SPCC_Utilities.SPCC_PROJECT_CONTROLLER);
        user = assignRole(user,SPCC_Utilities.SPCC_PC_ROLE);
        return user;
    }

    public static User createPLUser() {
        User user = createUser();
        user.LastName = user.LastName + '_PL';
        user = assignProfile(user,SPCC_Utilities.SPCC_USER_PROFILE);
        user = assignPermissionSet(user,SPCC_Utilities.SPCC_PROJECT_LEAD);
        user = assignRole(user,SPCC_Utilities.SPCC_PL_ROLE);
        return user;
    }


/****************************** SPCC Related Functions ******************************/   
	
	//Create EWR (Project)
	public static SPCC_Engineering_Work_Request__c createEWR(String projectName, 
															 String name, 
															 String ewrNumber,
															 String projectType){

		SPCC_Engineering_Work_Request__c ewr = new SPCC_Engineering_Work_Request__c(Project_Name__c = projectName,
																					Name = name,
																					EWR_Number__c = ewrNumber,
																				    Project_Type__c = projectType);
		return ewr;
	}


	//Create AFE
	public static SPCC_Authorization_for_Expenditure__c createAFE(String name,
																  String afeNumber,
																  String des,
																  Id ewrId){

		SPCC_Authorization_for_Expenditure__c afe = new SPCC_Authorization_for_Expenditure__c(Name = name,
																							  AFE_Number__c = afeNumber,
																							  Description__c = des,
																							  Engineering_Work_Request__c = ewrId);
		return afe;
	}


	//Create project Lead Assignemnt
	public static SPCC_Project_Lead_Assignment__c createPLA(Id ewrId, Id projectLeadId){

		SPCC_Project_Lead_Assignment__c pla = new SPCC_Project_Lead_Assignment__c(Engineering_Work_Request__c = ewrId,
																				  Project_Lead__c = projectLeadId);
		return pla;
	}


	//Create Vendor Account Assignment
	public static SPCC_Vendor_Account_Assignment__c createVAA(Id ewrId, Id vendorId){

		SPCC_Vendor_Account_Assignment__c vaa = new SPCC_Vendor_Account_Assignment__c(Engineering_Work_Request__c =  ewrId,
																					  Vendor_Account__c = vendorId);	
		return vaa;
	}

	//Create EPC Vendor Assignment
	public static SPCC_EPC_Assignment__c createEPCAssignment(Id ewrId, Id vendorId){
		SPCC_EPC_Assignment__c epcAssignment = new SPCC_EPC_Assignment__c(
			Engineering_Work_Request__c = ewrId,
			EPC_Vendor__c = vendorId
		);
		return epcAssignment;
	}

	//Create CSR Assignment
	public static SPCC_CSR_Assignment__c createCSRAssignment(Id ewrId, Id csrId) {
		SPCC_CSR_Assignment__c csrA = new SPCC_CSR_Assignment__c(
			Engineering_Work_Request__c = ewrId,
			Construction_Site_Representative__c = csrId
		);
		return csrA;
	}


	//Create Cost Element (G/L Account)
	public static SPCC_Cost_Element__c createCostElement(Id afeId, 
														 String category, 
														 String des, 
														 String glNumber,
														 String label,
														 Decimal origAuthAmt,
														 Decimal trend){

		SPCC_Cost_Element__c glCode = new SPCC_Cost_Element__c(AFE__c = afeId,
															   Category__c = category,
															   Description__c = des,
															   GL_Number__c = glNumber,
															   Label__c = label,
															   Original_Auth_Amt__c = origAuthAmt,
															   Trend__c = trend);
		return glCode;
	}


	//Create Cost Code
	public static SPCC_Cost_Code__c createCostCode(Id glId, String costCodeNumber ,String des, Decimal incurredCost){

		SPCC_Cost_Code__c costCode = new SPCC_Cost_Code__c(GL_Account__c = glId,
														   Cost_Code_Number__c = costCodeNumber,
														   Description__c = des,
														   Incurred_Cost__c = incurredCost);
		return costCode;
	}


	//Create Purchase Order
	public static SPCC_Purchase_Order__c createPO(String name,
												  Id ewrId,
												  Id afeId,
												  Id vendorId,
												  String poNumber,
												  Decimal poValue,
												  String des,
												  String status){

		SPCC_Purchase_Order__c po = new SPCC_Purchase_Order__c(Name = name,
															   Engineering_Work_Request__c = ewrId,
															   Authorization_for_Expenditure__c = afeId,
															   Vendor_Account__c = vendorId,
															   Purchase_Order_Number__c = poNumber,
															   Description__c = des,
															   Status__c = status);
		return po;
	}

	//Create Line Items
	public static SPCC_Line_Item__c createLineItem(Id purchaseOrderId, Id costElementId, 
		Decimal committedAmt, String itemNumber, String shortText) {
		SPCC_Line_Item__c lineItem = new SPCC_Line_Item__c(
			Purchase_Order__c = purchaseOrderId,
			Cost_Element__c = costElementId,
			Committed_Amount__c = committedAmt,
			Item__c = itemNumber,
			Short_Text__c = shortText
		);
		return lineItem;
	}

	//Create Service Entry Sheet
	public static SPCC_Service_Entry_Sheet__c createSES(Id poId, Id lineItem, Decimal amount, Date dateEntered, String ticketNumber){

		SPCC_Service_Entry_Sheet__c ses = new SPCC_Service_Entry_Sheet__c(
			Purchase_Order__c = poId,
			Line_Item__c = lineItem,
			Amount__c = amount,
			Date_Entered__c = dateEntered,
			Ticket_Number__c = ticketNumber
		);

		return ses;
	}
	
	//Create SPCC Vendor
   	public static Account createSPCCVendor(String name){
   		Integer n = crypto.getRandomInteger();
		String s = string.valueOf(n).right(10);

		RecordType spccRecordType = [Select Id, Name From RecordType Where DeveloperName = 'SPCC_Vendor'];
   		Account account = new Account(Name= name, RecordTypeId = spccRecordType.Id);
   		account.SAP_ID__c = s;
   		return account; 
   	}

   	//Create EPC Vendor
   	public static Account createEPCVendor(String name) {
   		Integer n = crypto.getRandomInteger();
		String s = string.valueOf(n).right(10);

		RecordType epcRecordType = [Select Id, Name From RecordType Where DeveloperName = 'EPC_Vendor'];
   		Account account = new Account(Name= name, RecordTypeId = epcRecordType.Id);
   		account.SAP_ID__c = s;
   		return account; 
   	}


   	//Create Contact
   	public static Contact createContact(Id accountId,
    									String firstName,
    									String lastName,
    									Id userId){

   		Contact contact = new Contact(AccountId = accountId,
   									  FirstName = firstName,
   									  LastName = lastName,
   									  User__c = userId);

   		return contact; 
   	}
   	
   	//Create EPC Contact
   	public static Contact createEPCContact(Id accountId,
    									String firstName,
    									String lastName,
    									Id userId){
        RecordType epcRecordType = [Select Id, Name From RecordType Where DeveloperName = 'SPCC_EPC'];
   		Contact contact = new Contact(AccountId = accountId,
   									  FirstName = firstName,
   									  LastName = lastName,
   									  User__c = userId,
   									  RecordTypeId = epcRecordType.Id);

   		return contact; 
   	}
	
}