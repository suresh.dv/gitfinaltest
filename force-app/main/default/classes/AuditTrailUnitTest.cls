@isTest 
public class AuditTrailUnitTest {
    
    static testMethod void testAuditTrailDownloadScheduler() {
        Test.StartTest();
        HR_Audit_Trail_Config__c c = new HR_Audit_Trail_Config__c(name='name',
                                                                      username__c = 'usergroup@danpeter.com',
                                                                      password__c = 'password',
                                                                      token__c = 'token');
        insert c;
            
        Map<String,String> configMap = AuditTrailUtils.getConfig();
        System.AssertEquals(configMap.get('username'), 'usergroup@danpeter.com');
        System.AssertEquals(configMap.get('password'), 'password');
        System.AssertEquals(configMap.get('token'), 'token');
      
        System.AssertEquals(AuditTrailUtils.getSessionId(), '00Di0000000fnjR!ARAAQHLABD_pDEsAVmS6vPUORiIThiH9B4GE7cJHE5htToV9UOshfxPUIfVcBYRxKzincGp_6uIPoiHqhTzPRSULo0h7mv1l');    
        
        
        Datetime d = Datetime.now().addMinutes(1);
        String sch = d.second()+' '+d.minute()+' '+d.hour()+' '+d.day()+' '+d.month()+' ? '+d.year();   
        AuditTrailDownloadScheduler a = new AuditTrailDownloadScheduler();
        String jobId = System.Schedule('Download Audi Trail Job TEST', sch, a);  
        
        

        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                            FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(sch,  ct.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        Test.StopTest();  
          
    }

}