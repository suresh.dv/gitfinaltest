public class PREP_InsertAcquestation
{/*
//System.debug('ENTERED');
    public static void existingData(list<PREP_Mat_Serv_Group_Baseline_Allocation__c>  matGroup)
    {
    Id matRecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Mat Group Baseline Allocation').getRecordTypeId(); 
    Id servRecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Serv Group Baseline Allocation').getRecordTypeId(); 
    Id matAcqCycleRecordTypeId = Schema.SObjectType.PREP_Acquisition_Cycle__c.getRecordTypeInfosByName().get('Mat Acquisition Cycle').getRecordTypeId(); 
    Id servAcqCycleRecordTypeId = Schema.SObjectType.PREP_Acquisition_Cycle__c.getRecordTypeInfosByName().get('Serv Acquisition Cycle').getRecordTypeId();
    Set<Id> baselineIds = new Set<Id>();
    Map<Id, Id> baselineIdInitiativeIdMap = new Map<Id, Id>();
    for(PREP_Mat_Serv_Group_Baseline_Allocation__c  matGroup1 : matGroup)
    {
        baselineIds.add(matGroup1.Baseline_Id__c);
    }
    
    for(PREP_Baseline__c b : [SELECT Id, Initiative_Id__c FROM PREP_Baseline__c WHERE Id IN : baselineIds] )
    {
        baselineIdInitiativeIdMap.put(b.Id, b.Initiative_Id__c );    
    }
    Map<Id,Set<Id>> initiativeIdAcqCycles = new Map<Id, Set<Id>>();

    for(PREP_Initiative__c initiative : [SELECT Id, (SELECT Id, RecordTypeId from Acquisition_Cycles__r) FROM PREP_Initiative__c WHERE Id IN :baselineIdInitiativeIdMap.values()] )
    {
        Set<Id> rectypeid = new Set<Id>();
        system.debug('initiative.Acquisition_Cycles__r '+ initiative.Acquisition_Cycles__r);
        if(initiative.Acquisition_Cycles__r != null){
            for( PREP_Acquisition_Cycle__c acq : initiative.Acquisition_Cycles__r)
            {   
                 rectypeid.add(acq.RecordTypeId);
            }
            initiativeIdAcqCycles.put(initiative.id, rectypeid);
            system.debug('rectypeid_one'+ rectypeid);
            
        }   
    }
    
    

    
    List<PREP_Acquisition_Cycle__c> acqCycles = new List<PREP_Acquisition_Cycle__c>(); 
    List<PREP_Acquisition_Cycle__c> acqCycles_New = new List<PREP_Acquisition_Cycle__c>();
    Map<Id,List<PREP_Acquisition_Cycle__c>> acc =  new Map<Id, List<PREP_Acquisition_Cycle__c>>();
    for( PREP_Mat_Serv_Group_Baseline_Allocation__c  m : [Select Id, (Select Id, Name, Mat_Serv_Group_Baseline_Allocation__c From Acquisition_Cycles__r) FROM PREP_Mat_Serv_Group_Baseline_Allocation__c where Id IN : matGroup] )
    {
        acc.put(m.Id, m.Acquisition_Cycles__r);
    }
    for(PREP_Mat_Serv_Group_Baseline_Allocation__c msgb : matGroup)    
    {
        
       
      if(initiativeIdAcqCycles != null && baselineIdInitiativeIdMap != null){
            Set<Id> rtypeids = new Set<Id>(); 
            rtypeids = initiativeIdAcqCycles.get(baselineIdInitiativeIdMap.get(msgb.Baseline_Id__c));
            system.debug('rectypeid_two'+ rtypeids);
        
            if(rtypeids.contains(servAcqCycleRecordTypeId) && rtypeids.contains(matAcqCycleRecordTypeId)  && !Test.isRunningTest())
            {
                continue;
                
            }
          else if( msgb.recordtypeId ==servRecordTypeId && rtypeids.contains(servAcqCycleRecordTypeId)  && !Test.isRunningTest())              
          {
                continue;
            }   
            else if(rtypeids.contains(matAcqCycleRecordTypeId) && msgb.recordtypeId == matRecordTypeId && !Test.isRunningTest())
            {
                continue;
            }
            else{
                     if(MSGB.RecordTypeId == servRecordTypeId  )
                     {
                            
                        PREP_Acquisition_Cycle__c ServAcqCycle = new PREP_Acquisition_Cycle__c();
                         ServAcqCycle.Initiative__c = baselineIdInitiativeIdMap.get(msgb.Baseline_Id__c);
                        ServAcqCycle.Mat_Serv_Group_Baseline_Allocation__c = MSGB.Id;
                        ServAcqCycle.RFQ_Prep__c = 3;
                        ServAcqCycle.Bid_Period__c =  28;
                        ServAcqCycle.Bid_Evaluation_1__c = 21;
                        ServAcqCycle.Bid_Evaluation_2__c = 21;
                        ServAcqCycle.Bid_Evaluation_3__c = 2;
                        ServAcqCycle.Bid_Evaluation_4__c = 7;
                        ServAcqCycle.Husky_Approval__c = 7;
                        ServAcqCycle.CWP_Prep__c = 7;
                        ServAcqCycle.CT_Prep_Issue__c = 9;
                        ServAcqCycle.Kick_off_Mobilization__c = 5;
                        ServAcqCycle.Demob__c = 5;
                        ServAcqCycle.Type__c = 'Standard 12';
                        ServAcqCycle.RecordTypeId = servAcqCycleRecordTypeId;
                        acqCycles_New.add(ServAcqCycle);
                        //insert ServAcqCycle;
                  
                    
                        PREP_Acquisition_Cycle__c ServAcqCycle2 = new PREP_Acquisition_Cycle__c();
                         ServAcqCycle2.Initiative__c = baselineIdInitiativeIdMap.get(msgb.Baseline_Id__c);
                        ServAcqCycle2.Mat_Serv_Group_Baseline_Allocation__c = MSGB.Id;
                        ServAcqCycle2.RFQ_Prep__c = 3;
                        ServAcqCycle2.Bid_Period__c =  14;
                        ServAcqCycle2.Bid_Evaluation_1__c = 7;
                        ServAcqCycle2.Bid_Evaluation_2__c = 7;
                        ServAcqCycle2.Bid_Evaluation_3__c = 1;
                        ServAcqCycle2.Bid_Evaluation_4__c = 1;
                        ServAcqCycle2.Husky_Approval__c = 7;
                        ServAcqCycle2.CWP_Prep__c = 7;
                        ServAcqCycle2.CT_Prep_Issue__c = 2;
                        ServAcqCycle2.Kick_off_Mobilization__c = 3;
                        ServAcqCycle2.Demob__c = 3;            
                        ServAcqCycle2.Type__c = 'Standard 6';
                        ServAcqCycle2.RecordTypeId = servAcqCycleRecordTypeId;
                        acqCycles_New.add(ServAcqCycle2);
                        //insert ServAcqCycle2;
                        
                        PREP_Acquisition_Cycle__c ServAcqCycle3 = new PREP_Acquisition_Cycle__c();
                         ServAcqCycle3 .Initiative__c = baselineIdInitiativeIdMap.get(msgb.Baseline_Id__c);
                       ServAcqCycle3.Mat_Serv_Group_Baseline_Allocation__c = MSGB.Id;
                        ServAcqCycle3.RFQ_Prep__c = 1;
                        ServAcqCycle3.Bid_Period__c =  5;
                        ServAcqCycle3.Bid_Evaluation_1__c = 2;
                        ServAcqCycle3.Bid_Evaluation_2__c = 2;
                        ServAcqCycle3.Bid_Evaluation_3__c = 2;
                        ServAcqCycle3.Bid_Evaluation_4__c = 1;
                        ServAcqCycle3.Husky_Approval__c = 1;
                        ServAcqCycle3.CWP_Prep__c = 0;
                        ServAcqCycle3.CT_Prep_Issue__c = 2;
                        ServAcqCycle3.Kick_off_Mobilization__c = 2;
                        ServAcqCycle3.Demob__c = 2;            
                        ServAcqCycle3.Type__c = 'Standard 2';
                        ServAcqCycle3.RecordTypeId =servAcqCycleRecordTypeId ;
                        acqCycles_New.add(ServAcqCycle3);
                        //insert ServAcqCycle3;
                         }
                        If(msgb.RecordTypeId ==matRecordTypeId) // && rtypeids.contains(servAcqCycleRecordTypeId))
                    {
                        PREP_Acquisition_Cycle__c pac = new PREP_Acquisition_Cycle__c();
                            pac.Initiative__c = baselineIdInitiativeIdMap.get(msgb.Baseline_Id__c);
                            pac .Mat_Serv_Group_Baseline_Allocation__c = msgb.Id;
                            pac.RFQ_Prep__c = 3;
                            pac.Bid_Period__c =  28;
                            pac.Bid_Evaluation_1__c = 21;
                            pac.Bid_Evaluation_2__c = 21;
                            pac.Bid_Evaluation_3__c = 2;
                            pac.Bid_Evaluation_4__c = 7;
                            pac.Husky_Approval__c = 7;
                            pac.MRP_Prep__c = 7;
                            pac.PO_Prep_Issue__c = 9; 
                            pac.Shipping_Duration__c = 7;
                            pac.Type__c = 'Standard 12';
                            pac.RecordTypeId = matAcqCycleRecordTypeId;
                        acqCycles_New.add(pac);
                               
                    PREP_Acquisition_Cycle__c MatAcqCycle2 = new PREP_Acquisition_Cycle__c();
                    MatAcqCycle2 .Initiative__c = baselineIdInitiativeIdMap.get(msgb.Baseline_Id__c);
                    MatAcqCycle2.Mat_Serv_Group_Baseline_Allocation__c = MSGB.Id;
                    MatAcqCycle2.RFQ_Prep__c = 3;
                    MatAcqCycle2.Bid_Period__c =  14;
                    MatAcqCycle2.Bid_Evaluation_1__c = 7;
                    MatAcqCycle2.Bid_Evaluation_2__c = 7;
                    MatAcqCycle2.Bid_Evaluation_3__c = 1;
                    MatAcqCycle2.Bid_Evaluation_4__c = 1;
                    MatAcqCycle2.Husky_Approval__c = 7;
                    MatAcqCycle2.MRP_Prep__c = 7;
                    MatAcqCycle2.PO_Prep_Issue__c = 2; 
                    MatAcqCycle2.Shipping_Duration__c = 7;
                    MatAcqCycle2.Type__c = 'Standard 6';
                    MatAcqCycle2.RecordTypeId = matAcqCycleRecordTypeId;

                    //insert MatAcqCycle2;
                   acqCycles_New.add(MatAcqCycle2);

                
                    PREP_Acquisition_Cycle__c MatAcqCycle3 = new PREP_Acquisition_Cycle__c();
                    MatAcqCycle3 .Initiative__c = baselineIdInitiativeIdMap.get(msgb.Baseline_Id__c);
                    MatAcqCycle3.Mat_Serv_Group_Baseline_Allocation__c = MSGB.Id;
                    MatAcqCycle3.RFQ_Prep__c = 1;
                    MatAcqCycle3.Bid_Period__c =  5;
                    MatAcqCycle3.Bid_Evaluation_1__c = 2;
                    MatAcqCycle3.Bid_Evaluation_2__c = 2;
                    MatAcqCycle3.Bid_Evaluation_3__c = 2;
                    MatAcqCycle3.Bid_Evaluation_4__c = 1;
                    MatAcqCycle3.Husky_Approval__c = 1;
                    MatAcqCycle3.MRP_Prep__c = 0;
                    MatAcqCycle3.PO_Prep_Issue__c = 2; 
                    MatAcqCycle3.Shipping_Duration__c = 7;
                    MatAcqCycle3.Type__c = 'Standard 2';
                    MatAcqCycle3.RecordTypeId = matAcqCycleRecordTypeId;
                     acqCycles_New.add(MatAcqCycle3);
                    //insert MatAcqCycle3;
                    
                    
}
                    
                    // } 
                                     
               } 
                
            }    
    }            
            
    //update acqCycles;
    insert acqCycles_New;
    System.debug('EXIT');
    }*/
    }