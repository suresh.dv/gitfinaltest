public class FM_UserSettingsController
{
	// *************************************************************************************** //
	// ** THIS CONTROLLER EXTENSION IS USED FOR THE USER SETTINGS & CONFIGURATION FOR FLUID ** //
	// *************************************************************************************** //

	private FM_CustomWellOrder__c oCustomWellOrderData;

	public SelectOption[] aSelectedWells {get; set;}
	public SelectOption[] aAllWells {get; set;}
	public String sSavedSelectedWells {get; set;}

	public Route__c oFoundRoute {get; private set;}
	public String sRouteNum {get; private set;}

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public FM_UserSettingsController()
	{
		InitializeData();
		VerifyRoute();

		if(this.oFoundRoute != null)
		{
			GenerateLocationData();
		}
	}

	//////////////////////////////////////////////
	// * INITIALIZE CONFIGURATION & MISC DATA * //
	//////////////////////////////////////////////

	private void InitializeData()
	{
		this.aSelectedWells = new SelectOption[]{};
		this.aAllWells = new SelectOption[]{};
		this.sSavedSelectedWells = '';
	}

	///////////////////////////////////////////
	// * VERIFY ROUTE -> FLUID ROUTES ONLY * //
	///////////////////////////////////////////

	private void VerifyRoute()
	{
		this.sRouteNum = ApexPages.currentPage().getParameters().get('route');
		List<Route__c> lRoutes = [SELECT Id, Name FROM Route__c WHERE Name = :this.sRouteNum AND Fluid_Management__c = true LIMIT :FM_Utilities.RUNSHEET_SINGLE_RESULT];

		this.oFoundRoute = (lRoutes.size() == FM_Utilities.RUNSHEET_SINGLE_RESULT) ? lRoutes[0] : null;
	}

	//////////////////////////////////////////////////////
	// * GENERATE THE LOCATION DATA -> FROM & TO LIST * //
	//////////////////////////////////////////////////////

	private void GenerateLocationData()
	{
		List<FM_CustomWellOrder__c> lFluidSettings = [SELECT WellIDList__c FROM FM_CustomWellOrder__c WHERE Route__c = :this.oFoundRoute.Id AND User__c = :UserInfo.getUserId() LIMIT :FM_Utilities.RUNSHEET_SINGLE_RESULT];

		this.oCustomWellOrderData = (lFluidSettings.size() == FM_Utilities.RUNSHEET_SINGLE_RESULT) ? lFluidSettings[0] : new FM_CustomWellOrder__c(Route__c = this.oFoundRoute.Id, User__c = UserInfo.getUserId());
		this.sSavedSelectedWells = (this.oCustomWellOrderData.WellIDList__c == null) ? '' : this.oCustomWellOrderData.WellIDList__c;

		Map<String, List<SelectOption>> mapLocationData = FM_Utilities.BuildLocationMaps(
				(this.sSavedSelectedWells == '') ? new List<String>() : new List<String>(this.sSavedSelectedWells.split(',')), 
				(this.sSavedSelectedWells == '') ? new Set<String>() : new Set<String>(this.sSavedSelectedWells.split(',')), 
				this.oFoundRoute.Id);
		System.debug('FM_UserSettingsController->GenerateLocationData->mapLocationData: ' + mapLocationData);
		this.aAllWells = RebuildLocationList(mapLocationData.get(FM_Utilities.LOCATION_INDEX_ALL_WELLS));
		this.aSelectedWells = RebuildLocationList(mapLocationData.get(FM_Utilities.LOCATION_INDEX_SELECTED_WELLS));
	}

	//////////////////////////////
	// * SAVE WELL ORDER DATA * //
	//////////////////////////////

	private void SaveWellOrderData()
	{
		this.oCustomWellOrderData.WellIDList__c = this.sSavedSelectedWells;
		upsert this.oCustomWellOrderData;
	}

	///////////////////////////
	// * UPDATE WELL ORDER * //
	///////////////////////////

	public PageReference UpdateWellOrder()
	{
		SelectOption oOneOption;
		String sWellList = '';

		for(Integer iCount = 0; iCount < aSelectedWells.size(); iCount++)
		{
			oOneOption = aSelectedWells[iCount];

			sWellList += (iCount > 0) ? ',' : '';
			sWellList += oOneOption.getValue();
		}

		this.sSavedSelectedWells = sWellList;
		SaveWellOrderData();

		return ExitWellOrderForm();
	}

	////////////////////////////////////////////////////////////
	// * EXIT THE FORM -> REDIRECT TO THE NEW RUNSHEET PAGE * //
	////////////////////////////////////////////////////////////

	public PageReference ExitWellOrderForm()
	{
		PageReference oPage = Page.FM_AddRunSheet;
		return oPage;
	}

	//////////////////////////////////////////////////////
	// * REBUILD THE SELECT OPTION LIST WITH SECTIONS * //
	//////////////////////////////////////////////////////

	public List<SelectOption> RebuildLocationList(List<SelectOption> lRebuildList)
	{
		List<SelectOption> lFinalList = new List<SelectOption>();
		SelectOption oInsertOption;

		for(SelectOption oOneOption : lRebuildList)
		{
			oInsertOption = new SelectOption(oOneOption.getValue(), oOneOption.getLabel().substringAfter('~'));
			lFinalList.add(oInsertOption);
		}

		return lFinalList;
	}
}