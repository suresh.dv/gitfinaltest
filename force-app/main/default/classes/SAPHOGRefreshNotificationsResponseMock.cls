@isTest
global with sharing class SAPHOGRefreshNotificationsResponseMock implements WebServiceMock {
  
  global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
    
       SAPHOGNotificationServices.RefreshSalesforceNotificationsResponse responseElement = new SAPHOGNotificationServices.RefreshSalesforceNotificationsResponse();
       responseElement.Message = 'SAPHOGNotificationServices.RefreshSalesforceNotificationsResponse Test Message';
       responseElement.type_x = True;
       
       response.put('response_x', responseElement);
  }
}