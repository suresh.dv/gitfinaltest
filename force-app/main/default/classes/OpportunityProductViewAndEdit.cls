public class OpportunityProductViewAndEdit {
    
    // visualforce page per Role
    private static String roleName = OpportunityUtilities.getUserRoleName();    
    public Boolean isCardlockFleetRole {get; set;}
    public Boolean isWholesaleRole {get; set;}
    public Boolean isDefaultUserRole {get; set;}
    
    public OpportunityProductViewAndEdit(ApexPages.StandardController controller) {

        // visualforce page per Role
        if( OpportunityUtilities.isCardlockFleetRole(roleName)){
            isCardlockFleetRole = true;
        }else if( OpportunityUtilities.isWholesaleRole(roleName)){
            isWholesaleRole = true;
        }else{
            isDefaultUserRole = true;
        }
    }        
}