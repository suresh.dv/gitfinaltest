public class reCAPTCHA_TestFull {
 
    //private final Account AccInq;
    public List<Account> AccID = new List <Account>();
    public string AccName;
    public Account AccInq { get; set; }
 
    /*public reCAPTCHA_TestFull(ApexPages.StandardController controller)
    {
    this.AccInq = (Account)controller.getRecord();
    system.debug ('Current Record -'+ this.AccInq);
    }*/
 
    public reCAPTCHA_TestFull() {
        this.verified = false;
        AccInq = new Account();}
 
    /* Configuration */
    SelfRegistrationCodes__c medinfos = SelfRegistrationCodes__c.getInstance('Welding Tracker Community User');
    //MedInfo__c medinfos = MedInfo__c.getInstance('reCaptcha');  //Custom Setting 
 
    // The API endpoint for the reCAPTCHA service
    private static String baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
 
    // The keys you get by signing up for reCAPTCHA for your domain
    public String publicKey {
        get { return medinfos.CAPTCHA_Public_Key__c;}} 
 
    public String response
    {
     get {
      return ApexPages.currentPage().getParameters().get('g-recaptcha-response');}
    }
 
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; public set; }
 
    // this method is called on click of Submit button
    public PageReference verify() {
        System.debug('Entered Verify Function from VF Page to verify reCaptcha');
 
        String privateKeys = medinfos.CAPTCHA_Private_Key__c;
        System.debug (privatekeys);
        
        system.debug('Private Key==='+privateKeys);
            system.debug('base URL==='+baseUrl);
            system.debug('Response==='+response);
            system.debug('Remote Host==='+remoteHost);
        // call goes to to API endpoint
        HttpResponse r = makeRequest(baseUrl,
            'secret=' + privateKeys +
            '&response='  + response +
            '&remoteip='  + remoteHost //+
           // '&challenge=' + challenge +
        );
 
        if ( r!= null )
        {
 
         JSONParser parser = JSON.createParser(r.getBody());
         //system.assert( false, r.getBody()+'::::::::'+parser+':::::::::::'+JSONToken.FIELD_NAME );
         while (parser.nextToken() != null)
         {
          system.debug('====current Token===='+parser.getCurrentToken());
                    system.debug('====JSONToken.FIELD_NAME===='+JSONToken.FIELD_NAME);
                    system.debug('====parser.getText()===='+parser.getText());
          if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'success'))
          {
           parser.nextToken();
           system.debug('====parser.getBooleanValue()===='+parser.getBooleanValue());
           if (parser.getBooleanValue()==true)
           {this.verified =true;}
           else{this.verified = false;}
          }
         }
        }
        
         if(!this.verified) {
            system.assert( false, this.verified );
            return null;
        }
 
        if(this.verified) {
            insert AccInq;
            system.debug ('Inserted - '+AccInq);
 
            AccID = [Select ID, Name from Account where ID = :AccInq.id];
            AccName = AccID[0].Name;
 
            // To display the confirmation page to the user
            PageReference pg = new PageReference('/'+AccInq.id);
            //pg.getParameters().put('str', AccName);
 
            pg.setRedirect(false);
            return pg;
 
          //   return null;
        }
        else {
            // stay on page to re-try reCAPTCHA
            return null;
        }
    }
 
    /* Private helper methods */
 
    public static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http = new Http();
            response = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }   
 
    private String remoteHost {
        get {
            String ret = '127.0.0.1';
            // also could use x-original-remote-host
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
}