/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_InvoiceListDownloadControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);

        PageReference pageRef = Page.USCP_InvoiceListDownload;
        Test.setCurrentPageReference(pageRef);
        
        System.runAs(runningUser) {
        

        
            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            account.Customer_Number__c = 'CN1';
            insert account;
            System.AssertNotEquals(account.Id, Null);          
        
            //create some invoices
            Date invoicedate =  Date.today()-30;        
            USCP_Invoice__c inv1 = USCP_TestData.createInvoice(account.Id, 'INV200', null, invoicedate , invoicedate , invoicedate , 1000.0,  true);
            USCP_Invoice__c inv2 = USCP_TestData.createInvoice(account.Id, 'INV201', null, invoicedate , invoicedate , invoicedate , 2000.0,  true);            
            USCP_Invoice__c inv3 = USCP_TestData.createInvoice(account.Id, 'INV202', null, invoicedate , invoicedate , invoicedate , 3000.0,  true);            
                    
            String idlist = inv1.id + '-' + inv2.id + '-' +  inv3.id;
            String InvoiceNumbers = 'INV200-INV201-INV202-';
            
            pageRef.getParameters().put('idlist', idlist);
                
            Test.startTest();
                Boolean flag = false;
                USCP_InvoiceListDownloadController sc = new USCP_InvoiceListDownloadController();
                System.AssertEquals(InvoiceNumbers,sc.InvoiceNumbers); 
                System.AssertEquals('CN1',sc.CustomerNumber); 

                //not initialized, redirect to null
                System.AssertEquals(null,sc.RedirectToDocument());                                             

                USCP_Settings__c  mc = new USCP_Settings__c();
                mc.HEI_WS_Endpoint_MI__c = 'google.ca'; 
                insert mc;
                 
                sc.DownloadDocument();
                System.Assert(sc.initialised);
                System.AssertNotEquals(null,sc.DocumentID); 
                System.AssertNotEquals(null,sc.docUrl); 
                
                //initialized, redirect should not be null
                System.AssertNotEquals(null,sc.RedirectToDocument());     
                
                idlist = inv1.id + '-' + inv2.id;
                InvoiceNumbers = 'INV200-INV201-';
                pageRef.getParameters().put('idlist', idlist);
                sc = new USCP_InvoiceListDownloadController(); 
                System.AssertEquals(InvoiceNumbers,sc.InvoiceNumbers);                                                            
                sc.DownloadDocument();
                System.Assert(sc.initialised);
                System.AssertNotEquals(null,sc.DocumentID); 
                System.AssertNotEquals(null,sc.docUrl);                 
                                
            Test.stopTest(); 
        }
    }
}