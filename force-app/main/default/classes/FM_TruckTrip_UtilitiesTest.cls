/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for FM_TruckTrip_Utilities
History:        
**************************************************************************************************/
@isTest private class FM_TruckTrip_UtilitiesTest {
	
	@isTest static void testTruckTripHistoryRenew() {
		FM_Truck_Trip__c dispatchedTruckTrip = [SELECT Id, Truck_Trip_Status__c, Unit__c, Well__c, Facility_Lookup__c, Location_Lookup__c, Facility_Type__c 
												FROM FM_Truck_Trip__c
												WHERE Shift_Day__c = 'Today'
												LIMIT 1];
		FM_Truck_Trip__c newTruckTrip = [SELECT Id, Truck_Trip_Status__c, Unit__c, Well__c, Facility_Lookup__c, Location_Lookup__c, Facility_Type__c 
										FROM FM_Truck_Trip__c
										WHERE Shift_Day__c = 'Tomorrow'
										LIMIT 1];

		List<String> newTruckTripHistoryFields = generateNewTruckTripHistoryFields();
		List<String> newTruckTripHistoryValues = generateNewTruckTripHistoryValues();
		List<String> dispatchedTruckTripHistoryFields = generateDispatchedTruckTripHistoryFields();
		List<String> dispatchedTruckTripHistoryValues = generateDispatchedTruckTripHistoryValues();

		Test.startTest();
		//Test history data retrieve methods
		System.assertEquals(null, FM_TruckTrip_Utilities.getTruckTripHistoryData(null));
		List<FM_Truck_Trip__History> newTTHistory = FM_TruckTrip_Utilities.getTruckTripHistoryData(newTruckTrip.Id);
		List<FM_Truck_Trip__History> dispatchedTTHistory = FM_TruckTrip_Utilities.getTruckTripHistoryData(dispatchedTruckTrip.Id);
		List<FM_Truck_Trip__History> exportedTTHistory = FM_TruckTrip_Utilities.getTruckTripHistoryData(dispatchedTruckTrip.Id);
		//NOTE: in test there is no data history stored.
		System.assert(newTTHistory.size() == 0);
		System.assert(dispatchedTTHistory.size() == 0);
		System.assert(exportedTTHistory.size() == 0);
		FM_TruckTrip_Utilities.skipInTest = false;
		System.assert(!FM_TruckTrip_Utilities.isTruckTripConfirmable(newTTHistory, false));
		System.assert(!FM_TruckTrip_Utilities.isTruckTripConfirmable(newTTHistory, true));
		System.assert(!FM_TruckTrip_Utilities.isTruckTripRenewable(dispatchedTTHistory, false));
		System.assert(!FM_TruckTrip_Utilities.isTruckTripRenewable(dispatchedTTHistory, true));
		System.assert(!FM_TruckTrip_Utilities.wasTruckTripExported(exportedTTHistory));
		//test value renewal methods
		//new Truck Trip
		FM_Truck_Trip__c newTTClone = newTruckTrip.clone(true, true, true, true);

		for(Integer i = 0; i < newTruckTripHistoryFields.size(); i++) 
			newTTClone = FM_TruckTrip_Utilities.setValueFromHistoryItem(newTTClone
																	, newTruckTripHistoryFields.get(i)
																	, newTruckTripHistoryValues.get(i)
																	, FM_TruckTrip_Utilities.UNCANCEL_MODE_RENEW);

		System.assertNotEquals(newTruckTrip, newTTClone);
		System.assertEquals(FM_TruckTrip_Utilities.STATUS_BOOKED, newTTClone.get(FM_TruckTrip_Utilities.FIELD_STATUS));
		System.assertEquals([SELECT Id FROM Carrier_Unit__c LIMIT 1][0].Id, newTTClone.get(FM_TruckTrip_Utilities.FIELD_UNIT));
		System.assertEquals([SELECT Id FROM Location__c LIMIT 1][0].Id, newTTClone.get(FM_TruckTrip_Utilities.FIELD_WELL));
		//dispatched Truck Trip
		FM_Truck_Trip__c dispatchedTTClone = dispatchedTruckTrip.clone(true, true, true, true);

		for(Integer i = 0; i < dispatchedTruckTripHistoryFields.size(); i++) 
			dispatchedTTClone = FM_TruckTrip_Utilities.setValueFromHistoryItem(dispatchedTTClone
																			, dispatchedTruckTripHistoryFields.get(i)
																			, dispatchedTruckTripHistoryValues.get(i)
																			,  FM_TruckTrip_Utilities.UNCANCEL_MODE_CONFIRM);

		System.assertNotEquals(dispatchedTruckTrip, dispatchedTTClone);
		//Status field is not in field history renew for Confirm mode.
		System.assertEquals(FM_TruckTrip_Utilities.STATUS_CANCELLED, dispatchedTTClone.get(FM_TruckTrip_Utilities.FIELD_STATUS));
		System.assertEquals(FM_TruckTrip_Utilities.FACILITY_TYPE_LOCATION, dispatchedTTClone.get(FM_TruckTrip_Utilities.FIELD_FACILITY_TYPE));
		System.assertEquals([SELECT Id FROM Carrier_Unit__c LIMIT 1][0].Id, dispatchedTTClone.get(FM_TruckTrip_Utilities.FIELD_UNIT));
		System.assertEquals([SELECT Id FROM Location__c LIMIT 1][0].Id, dispatchedTTClone.get(FM_TruckTrip_Utilities.FIELD_LOCATION));
		Test.stopTest();
	}

	private static List<String> generateNewTruckTripHistoryFields() {
		List<String> truckTripHistoryFields = new List<String>();
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_STATUS);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_UNIT);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_WELL);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_WELL);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_STATUS);
		return truckTripHistoryFields;
	}

	private static List<String> generateNewTruckTripHistoryValues() {
		List<String> truckTripHistoryFields = new List<String>();
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.STATUS_BOOKED);
		truckTripHistoryFields.add([SELECT Id FROM Carrier_Unit__c LIMIT 1][0].Id);
		truckTripHistoryFields.add('Well Fluid Name');
		truckTripHistoryFields.add([SELECT Id FROM Location__c LIMIT 1][0].Id);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.STATUS_NEW);
		return truckTripHistoryFields;

	}

	private static List<String> generateDispatchedTruckTripHistoryFields() {
		List<String> truckTripHistoryFields = new List<String>();
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_STATUS);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_STATUS);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_UNIT);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_LOCATION);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_FACILITY_TYPE);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_LOCATION);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FIELD_STATUS);
		return truckTripHistoryFields;
	}

	private static List<String> generateDispatchedTruckTripHistoryValues() {
		List<String> truckTripHistoryFields = new List<String>();
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.STATUS_DISPATCHED);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.STATUS_BOOKED);
		truckTripHistoryFields.add([SELECT Id FROM Carrier_Unit__c LIMIT 1][0].Id);
		truckTripHistoryFields.add('Well Fluid Name');
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.FACILITY_TYPE_LOCATION);
		truckTripHistoryFields.add([SELECT Id FROM Location__c LIMIT 1][0].Id);
		truckTripHistoryFields.add(FM_TruckTrip_Utilities.STATUS_NEW);
		return truckTripHistoryFields;
	}

	@testSetup static void prepareData() {
		Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);

        Field__c field = HOG_TestDataFactory.createAMU(true);

        Carrier__c carrier = FM_TestDataFactory.createCarrier(acc.Id, true);
        Carrier_Unit__c unit = FM_TestDataFactory.createCarrierUnit(carrier.Id, true);

        Route__c route = HOG_TestDataFactory.createRoute(true);

        Location__c location = HOG_TestDataFactory.createLocation(route.Id, field.Id, true);

        Facility__c facility = HOG_TestDataFactory.createFacility(route.Id, field.Id, true);

        List<FM_Load_Request__c> loadRequestList = new List<FM_Load_Request__c>();
        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                                                                        facility.Id,
                                                                        'O', null,
                                                                        'Night',
                                                                        location.Id,
                                                                        FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                                                                        false));

        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id,  null, 
                                                                        facility.Id,
                                                                        'W', null, 
                                                                        'Night', 
                                                                        location.Id, 
                                                                        FM_LoadRequest_Utilities.LOADREQUEST_STATUS_NEW, 
                                                                        false)); 


        insert loadRequestList;

        List<FM_Truck_Trip__c> truckTrips = new List<FM_Truck_Trip__c>();
        truckTrips.add(FM_TestDataFactory.createTruckTrip(loadRequestList.get(0), 
	                                                    'Today', 
	                                                    FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED, 
	                                                    unit.Id, 
	                                                    false));

        truckTrips.add(FM_TestDataFactory.createTruckTrip(loadRequestList.get(1), 
	                                                    'Tomorrow', 
	                                                    FM_Utilities.TRUCKTRIP_STATUS_NEW, 
	                                                    unit.Id, 
	                                                    false));

        insert truckTrips;

        truckTrips.get(0).Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CANCELLED;
        truckTrips.get(0).Load_Request_Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
        truckTrips.get(1).Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CANCELLED;
        truckTrips.get(1).Load_Request_Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());

        update truckTrips;
	}
	
}