/*-------------------------------------------------------------------
Author      : Gangha Kaliyan * MODIFIED BY GUI MANDERS ON 2015-06-09 *
Company     : Husky
Description : Enables users to view/edit dispatcher comments for truck lists
Test Class  : FM_TruckList_Test
Date        : 2-Mar-2015
----------------------------------------------------------------------*/

public class FM_TruckListCommentsControllerX 
{
    
    public FM_Truck_List_Comments__c truckListComments {get; set;}
    public FM_Truck_List_Comments__c dummyCommentsRecord {get; set;}

    public List<FM_Truck_List_Comments__c> selectedDateComments {get; set;}
	public List<SelectOption> lGroupList {get; private set;}

    public String formattedDate {get; set;}
	public String sGroupSelection {get; set;}

    public FM_TruckListCommentsControllerX(ApexPages.StandardController stdCon)
    {
        dummyCommentsRecord = new FM_Truck_List_Comments__c();
        dummyCommentsRecord.Run_Sheet_Date__c = date.today();

		BuildGroupingList();
        getCommentsForSelectedDate();
    }

	private void BuildGroupingList()
	{
		this.lGroupList = new List<SelectOption>();
		SelectOption oNewOption;

		Schema.DescribeFieldResult oResult = Schema.sObjectType.Dispatch_Desk__c.fields.Grouping__c;
		List<Schema.PicklistEntry> lEntries = oResult.getPicklistValues();

		this.sGroupSelection = (lEntries.size() > 0) ? lEntries[0].getValue() : '';

		for(Schema.PicklistEntry oOneEntry : lEntries)
		{
			this.lGroupList.add(new SelectOption(oOneEntry.getValue(), oOneEntry.getLabel()));
		}
	}

    public void getCommentsForSelectedDate()
    {
        truckListComments = new FM_Truck_List_Comments__c();
        truckListComments.Run_Sheet_Date__c = dummyCommentsRecord.Run_Sheet_Date__c;
        selectedDateComments = [SELECT Id, Name, Run_Sheet_Date__c, Dispatch_Comments__c, Dispatch_Desk__c 
									FROM FM_Truck_List_Comments__c 
									WHERE Run_Sheet_Date__c = :dummyCommentsRecord.Run_Sheet_Date__c 
									AND (Dispatch_Desk__c = :this.sGroupSelection OR Dispatch_Desk__c = null) 
									ORDER BY Dispatch_Desk__c DESC NULLS LAST 
									LIMIT 1];

        if(selectedDateComments.size() == 1)
        {
            truckListComments = selectedDateComments[0].clone(true, false);
			ApexPages.getMessages().clear();
        }
        else
        {
        	// KK 2015-06-29:	only go back by one day.
        	// GM 2015-08-14:	changing to most recent comment
        	// GM 2015-11-10:	adding WHERE clause for DESK
            List<FM_Truck_List_Comments__c> recentComments = [SELECT Id, Name, Run_Sheet_Date__c, Dispatch_Desk__c, Dispatch_Comments__c 
                                                                    FROM FM_Truck_List_Comments__c 
                                                                    WHERE Run_Sheet_Date__c <: dummyCommentsRecord.Run_Sheet_Date__c 
                                                                    AND (Dispatch_Desk__c = :this.sGroupSelection OR Dispatch_Desk__c = null)
																	ORDER BY Run_Sheet_Date__c DESC, Dispatch_Desk__c DESC NULLS LAST 
                                                                    LIMIT 1];

            //truck list comments exist (previous)
            if(recentComments.size() > 0)
            {
                //format date to show in info message
                formattedDate = recentComments[0].Run_Sheet_Date__c.day() + '/' + recentComments[0].Run_Sheet_Date__c.month() + '/' + recentComments[0].Run_Sheet_Date__c.year();
                String copiedRecentCommentsStr = 'Copied from the most recent comments (' + formattedDate + ')' + ((recentComments[0].Dispatch_Desk__c == null) ? ' NO GROUPING FOUND (HISTORICAL COMMENTS)' : '');
                ApexPages.Message copiedRecentCommentsMsg = new ApexPages.Message(ApexPages.Severity.INFO, copiedRecentCommentsStr);
                ApexPages.addMessage(copiedRecentCommentsMsg);
                truckListComments.Dispatch_Comments__c = recentComments[0].Dispatch_Comments__c;
            }
        }
    }

    public void save()
    {
        if(truckListComments != null)
        {
			truckListComments.Dispatch_Desk__c = this.sGroupSelection;
            upsert truckListComments;
        }
    }

}