global with sharing class WTDCLoginController {
    global String username {get; set;}
    global String password {get; set;}
    global WTDCLoginController () {}
    global PageReference forwardToCustomAuthPage() {
        return new PageReference( '/WTDCLoginPage');
    }
    global PageReference login() {
        return Site.login(username, password, null);
    }
}