@isTest (SeeAllData = false)
public class VendorPortalUtilityTest {

    static testMethod void testAddingUserToGroup() {
    	User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);

        System.runAs(runningUser) {
        	//Get Data
            Profile p = [SELECT Id, Name FROM Profile WHERE Name =: VendorPortalUtility.VENDOR_PORTAL_PROFILE];
            
            //Create Account
            Account portalAccount = new Account();
			      portalAccount.Name = 'PortalAccount';
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;
            
            //Create Contact
            Contact portalContact = new Contact(AccountId = portalAccount.id, 
                                                FirstName='firstname', 
                                                LastName = 'lastname',
                                                Email = 'email@email.com');
            insert portalContact;
            
            //Create Tradesmen User
            User u = new User();
            u.ContactId = portalContact.id;
            u.username = portalContact.Email + '.vendor.com';
            u.Email = portalContact.Email;
            u.FirstName = portalContact.FirstName;
            u.LastName = portalContact.LastName;
            u.Alias = string.valueof(portalContact.FirstName.substring(0,1) + portalContact.LastName.substring(0,1));
            u.ProfileId = p.id;
            u.emailencodingkey='UTF-8';
            u.languagelocalekey='en_US';
            u.localesidkey='en_CA';
            u.timezonesidkey='America/Denver';
            u.Vendor_Portal_User_Type__c = 'HOG_Vendor_Community_VTT_User';
            insert u;
            
          	//Check if User has correct permission  
            PermissionSet permissionSet = [SELECT Id, Name 
                                           FROM PermissionSet 
                                           WHERE Name =: u.Vendor_Portal_User_Type__c LIMIT 1];
            PermissionSetAssignment psa = [SELECT Id, PermissionSetId, AssigneeId 
                                           FROM PermissionSetAssignment 
                                           WHERE PermissionSetId =: permissionSet.Id 
                                           AND PermissionSetId IN (SELECT Id FROM PermissionSet WHERE IsOwnedByProfile = false) LIMIT 1];
            System.assertEquals(permissionSet.Id, psa.PermissionSetId);
            
            //Check if User is assigned to correct group
            Group g=[SELECT Id 
                     FROM Group 
                     WHERE DeveloperName=:VendorPortalUtility.GROUP_NAME];
            GroupMember gm = [SELECT Id 
                              FROM GroupMember 
                              WHERE GroupId=:g.id AND UserOrGroupId=: u.id ];
            System.assertNotEquals(gm, null);
            
            
            //Reactivate user and change his permission set
      			u.IsActive = false;
      			update u;
            u.IsActive = true;
            u.Vendor_Portal_User_Type__c = 'HOG_Vendor_Community_VTT_Vendor_Supervisor';
            VendorPortalUtility.createOrReactivatePortalUser(u.ContactId, u.Email, u.FirstName, u.LastName, u.ProfileId, u.Vendor_Portal_User_Type__c);
            
            //Check if user has correct permissions set
			      PermissionSet ps = [SELECT Id, Name FROM PermissionSet WHERE Name = 'HOG_Vendor_Community_VTT_Vendor_Supervisor'];
            PermissionSetAssignment newPSA = [SELECT Id, PermissionSetId, AssigneeId 
                                           FROM PermissionSetAssignment 
                                           WHERE AssigneeId =: u.id
                                           AND PermissionSetId IN (SELECT Id FROM PermissionSet WHERE IsOwnedByProfile = false) LIMIT 1];
            System.assertEquals(ps.Id, newPSA.PermissionSetId);
            

          //Create another contact and user to test the createOrReactivatePortalUser create method
          //Create Contact
            Contact portalContactSecond = new Contact(AccountId = portalAccount.id, 
                                                FirstName='firstnameSecond', 
                                                LastName = 'lastnameSecond',
                                                Email = 'emailSecond@email.com');
            insert portalContactSecond;
            
            //Create VTT User
            User userSecond = new User();
            userSecond.ContactId = portalContactSecond.id;
            userSecond.username = portalContactSecond.Email + '.vendor.com';
            userSecond.Email = portalContactSecond.Email;
            userSecond.FirstName = portalContactSecond.FirstName;
            userSecond.LastName = portalContactSecond.LastName;
            userSecond.Alias = string.valueof(portalContactSecond.FirstName.substring(0,1) + portalContactSecond.LastName.substring(0,1));
            userSecond.ProfileId = p.id;
            userSecond.emailencodingkey='UTF-8';
            userSecond.languagelocalekey='en_US';
            userSecond.localesidkey='en_CA';
            userSecond.timezonesidkey='America/Denver';
            userSecond.Vendor_Portal_User_Type__c = 'HOG_Vendor_Community_VTT_User';
            insert userSecond;
            VendorPortalUtility.createOrReactivatePortalUser(userSecond.ContactId, userSecond.Email, userSecond.FirstName, userSecond.LastName, userSecond.ProfileId, userSecond.Vendor_Portal_User_Type__c);
            System.assertEquals(userSecond.IsActive, false);
        }
   	}  
}