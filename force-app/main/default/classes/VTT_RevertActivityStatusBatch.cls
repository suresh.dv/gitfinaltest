/*-------------------------------------------------------------------------------------------------
Author:         Marcel Brimus
Company:        Husky Energy
Description:    A batch job that runs at the end of the day and reverts THERMAL activities 
                from 2SCH to 1RTS in SF and in SAP

Test Class:     VTT_RevertActivityStatusBatchTest 
History:        mbrimus 10.5.2018 - Created.
---------------------------------------------------------------------------------------------------*/ 
global class VTT_RevertActivityStatusBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    public static String TAG = 'VTT_RevertActivityStatusBatch';
    // HOG Settings
    public static HOG_Settings__c settings = HOG_Settings__c.getInstance();
    public static String LAST_N_DAYS = settings.Revert_Status_For_Activities_X_DaysOld__c == null ? '3' : settings.Revert_Status_For_Activities_X_DaysOld__c;
    public static String REVERT_ACTIVITY_STATUS_UNTIL = settings.Revert_Status_Until_Hour__c == null ? '2' : settings.Revert_Status_Until_Hour__c;

    public static final String JOBTYPE_REVERT_ACTIVITY_STATUS = 'REVERT_ACTIVITY_STATUS';
    public static final String QUERY = 'SELECT Id, Name, Operating_Field_AMU__c, Operating_Field_AMU__r.Is_Thermal__c, '
                    + ' Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c, Maintenance_Work_Order__r.Planner_Group__c, ' 
                    + ' Maintenance_Work_Order__r.Order_Type__c, Work_Center__c, Assigned_Text__c, Assigned_Vendor__c, '
                    + ' Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c, '
                    + ' Operation_Number__c, Sub_Operation_Number__c, User_Status__c, Description__c, Maintenance_Work_Order__r.Work_Order_Number__c'
                    + ' FROM Work_Order_Activity__c '
                    + ' WHERE ((Operating_Field_AMU__c != null AND (Operating_Field_AMU__r.Is_Thermal__c = true AND (User_Status__c LIKE \'2SCH %FIX%\'))) ' 
                    + ' OR (Operating_Field_AMU__c = null AND Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c != null' 
                    + ' AND (Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = true' 
                    + ' AND (User_Status__c LIKE \'2SCH %FIX%\')))) ' 
                    + ' AND Scheduled_Start_Date__c < LAST_N_DAYS:' + LAST_N_DAYS
                    + ' AND SAP_Deleted__c = FALSE'
                    + ' AND Status__c =\'' + VTT_Utilities.ACTIVITY_STATUS_SCHEDULED + '\''
                    + ' Order By Scheduled_Start_Date__c Desc';

    // Indicator is callout retry is required
    @TestVisible public Boolean retryRequired = false;
    
    // Current hour when finish method is runned
    @TestVisible public Integer currentHour {get; set;}
    
    // List of failed updates on activites, for email
    @TestVisible public List<Work_Order_Activity__c> failedActivities = new List<Work_Order_Activity__c>();

    global VTT_RevertActivityStatusBatch() {
        System.debug('VTT_RevertActivityStatusBatch initiating with query: ' + QUERY);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(QUERY);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<Work_Order_Activity__c> woas = (List<Work_Order_Activity__c>) scope;
        // Activities that will have to be updated
        System.debug(TAG + ' workOrderActivities that are going to be updated' + woas);
        System.debug(TAG + ' workOrderActivities size ' + woas.size());

        // Since this is batch that is runned multiple times we only want to set this variable to true only once
        // So that its not set to true on next round, but we still want to run method to update activities
        List<Work_Order_Activity__c> failedUpdates = VTT_Utilities.updateActityStatusesAndRetrieveFailedUpdates(woas);

        if(!failedUpdates.isEmpty()){
            retryRequired = true;
            failedActivities.addAll(failedUpdates);
        }
        
        System.debug(TAG + ' retryRequired execute ' + retryRequired);
        System.debug(TAG + ' failedActivities ' + failedActivities);
    }
    
    /**
     * This is called at the end once every batch is processed.
     * Logic here makes sure that we try for atleas 1hour to process failed records
     * by rescheduling job every 10 minutes, and also aborting old jobs.
     * If this is not successfull by the end of 2AM we send an email
     * @param  BC [context]
     */
    global void finish(Database.BatchableContext BC) {
        System.debug(TAG + ' executing finish');
		if(!Test.isRunningTest()) currentHour = System.now().hour();
        
        // Cleanup of retry job before scheduling new one
        HOG_ScheduleUtilities.abortCompletedJobsEqualTo(VTT_UpdateActivitySchedulable.REVERT_ACTIVITY_JOB_NAME_RETRY);
        
        // This should try only until certain time
        if(retryRequired && currentHour < Integer.valueOf(REVERT_ACTIVITY_STATUS_UNTIL)){
            // We need to schedule another run of this batch again in x minutes
            System.schedule(
                VTT_UpdateActivitySchedulable.REVERT_ACTIVITY_JOB_NAME_RETRY,
                getRetryCroneString(), 
                new VTT_UpdateActivitySchedulable(VTT_RevertActivityStatusBatch.JOBTYPE_REVERT_ACTIVITY_STATUS));

        } else if(retryRequired && currentHour >= Integer.valueOf(REVERT_ACTIVITY_STATUS_UNTIL)){
        	VTT_EmailNotifications emailNotifications = new VTT_EmailNotifications();
        	Map<String, Set<String>> roleEmailMap = new Map<String, Set<String>>();
        	roleEmailMap = emailNotifications.findNotificationContactsFromActivitiesForPlannerGroup(failedActivities);
        	List<String> schedulerEmails = new List<String>();

        	if(roleEmailMap.containsKey(VTT_EmailNotifications.ROLE_FIELDSCHEDULER)){
        		schedulerEmails.addAll(roleEmailMap.get(VTT_EmailNotifications.ROLE_FIELDSCHEDULER));
        	}

        	if(!schedulerEmails.isEmpty()){
        		// Send an email that retryies were not successfull
	            String message = VTT_UpdateActivitySchedulable.REVERT_ACTIVITY_JOB_NAME + ' failed to revert status back to 1RTS, '
	                +' another run is scheduled on 1AM next day... please investigate';
	            message += generateTable(failedActivities);

	            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	            mail.setToAddresses(schedulerEmails);
	            mail.setSaveAsActivity(false);
	            mail.setSubject(VTT_UpdateActivitySchedulable.REVERT_ACTIVITY_JOB_NAME);
	            mail.setHtmlBody(message);
	            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        	}
		}

        System.debug(TAG + ' currentHour ' + currentHour);
        System.debug(TAG + ' retryRequired ' + retryRequired);
    }

    /**
     * Generates table from failed activities
     * @param  activities [failed activities]
     */
    private String generateTable(List<Work_Order_Activity__c> activities){
        Integer maxNumberOfDisplayedActivities = 20;
        String table = '<div>Number of failed activities: ' + activities.size() + '</div>'
        				+ '<br></br>'
                        + '<table style="width:100%; border: 1px solid black; text-align:center; border-collapse: collapse; page-break-inside: avoid;">'
                        + '<tr><th style="border: 1px solid black; background-color: #CCCCCC; text-align: center; padding: 1px">Work Order Activity ID</th>'
                        + '<th style="border: 1px solid black; background-color: #CCCCCC; text-align: center; padding: 1px">Work Order Name</th>';

        if(activities.size() > maxNumberOfDisplayedActivities){
            // Only display max amount of activities
            for(Integer i = 0; i < maxNumberOfDisplayedActivities; i++){
                table  += '<tr><td style="border: 1px solid black; background-color: #CCCCCC; text-align: center; padding: 1px"><a target="vtt_activvity" href="https://'+ System.URL.getSalesforceBaseUrl().getHost() + '/' + activities.get(i).Id + '">View Activity</a></td>'
                    + '<td style="border: 1px solid black; background-color: #CCCCCC; text-align: center; padding: 1px">' +  activities.get(i).Name + '<td/></tr>';
            }
        } else {
            for (Work_Order_Activity__c activity: activities){
                table  += '<tr><td style="border: 1px solid black; background-color: #CCCCCC; text-align: center; padding: 1px"><a target="vtt_activvity" href="https://'+ System.URL.getSalesforceBaseUrl().getHost() + '/' + activity.Id + '">View Activity</a></td>'
                    + '<td style="border: 1px solid black; background-color: #CCCCCC; text-align: center; padding: 1px">' +  activity.Name + '<td/></tr>';                  
            }
        }
        
        table += '</table>';    
        return table;
    }

    /**
    * Gets crone String which targets run for 10 mins later.
    */
    private String getRetryCroneString() {
        Datetime t = Datetime.now().addMinutes(10);
        return '0 ' 
            + String.valueOf(t.minute()) 
            + ' ' 
            + String.valueOf(t.hour()) 
            + ' ' 
            + String.valueOf(t.day()) 
            + ' ' 
            + String.valueOf(t.month()) 
            + ' ? ' 
            + String.valueOf(t.year());
    }
}