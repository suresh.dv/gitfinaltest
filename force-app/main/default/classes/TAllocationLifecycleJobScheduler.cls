public class TAllocationLifecycleJobScheduler {
    public static final String JOB_NAME = 'Team Allocation Lifecycle Job';
    
    // Added By Sanchivan Sivadasan
    public static final String JOB_NAME_HALFHOUR = 'Team Allocation Lifecycle Job Half Hour';
    
    public static final String JOB_TYPE = 'TeamAllocationLifecycle';
    public static final String CRON_EXPRESSION = '0 0 * * * ? *';
    
    // Added By Sanchivan Sivadasan
    public static final String CRON_EXPRESSION_HALFHOUR = '0 30 * * * ?';
    
    /***********************************************
     * Start or stop the job depending on whether 
     * there are active shifts or not.
     **********************************************/
    public static void enableOrDisableJob() {
        Integer enabledShiftsCount = [SELECT COUNT() from Shift_Configuration__c WHERE Enabled__c = True];
        if (enabledShiftsCount > 0) {
            start();
        } else { 
            stop();
        }
    }
    
    /*********************************************** 
     * Start the Job if it's not currently scheduled.
     **********************************************/
    public static void start() {
		Set<String> jobNames = new Set<String>();
		jobNames.add(JOB_NAME);
		jobNames.add(JOB_NAME_HALFHOUR);
		
		Map<String, Scheduled_Job__c> scheduledJobMap = new Map<String, Scheduled_Job__c>();
		
		for(Scheduled_Job__c scheduledJob : [SELECT Id, Name, Job_Param__c, Is_Active__c, Job_SID__c, Job_Type__c FROM Scheduled_Job__c WHERE Name = :jobNames]) {
			scheduledJobMap.put(scheduledJob.Name, scheduledJob);	
		}
		
		startHelper(scheduledJobMap, JOB_NAME, CRON_EXPRESSION);
		startHelper(scheduledJobMap, JOB_NAME_HALFHOUR, CRON_EXPRESSION_HALFHOUR);
    }
    
    public static void startHelper(Map<String, Scheduled_Job__c> scheduledJobMap, String jobName, String cronExpression) {
    	if (scheduledJobMap.get(jobName) == null) {
            String jobSid = System.schedule(jobName, cronExpression, new TeamAllocationLifecycleJob());
            
            Scheduled_Job__c newJob = new Scheduled_Job__c();
            newJob.Name = jobName;
            newJob.Job_Type__c = JOB_TYPE;
            newJob.Job_Param__c = cronExpression;
            newJob.Is_Active__c = True;
            newJob.Job_SID__c = jobSid;
            
            insert newJob;
        } else {
            Scheduled_Job__c targetJob = scheduledJobMap.get(jobName);
            
            // Confirm that the job is not currently scheduled.
            List<CronTrigger> cts = [
                SELECT 
                    Id, 
                    CronExpression, 
                    TimesTriggered, 
                    NextFireTime
                FROM CronTrigger
                WHERE Id = :targetJob.Job_SID__c];
            if (cts == null || cts.size() < 1) {
                String jobSid = System.schedule(jobName, cronExpression, new TeamAllocationLifecycleJob());
                
                targetJob.Is_Active__c = True;
                targetJob.Job_SID__c = jobSid;
                
                update targetJob;
            }
        }
    }
    
    /***********************************************
     * Stop the Job if it's currently scheduled.
     **********************************************/
    public static void stop() {
    	Set<String> jobNames = new Set<String>();
		jobNames.add(JOB_NAME);
		jobNames.add(JOB_NAME_HALFHOUR);
		
		List<Scheduled_Job__c> scheduledJobsToUpdate = new List<Scheduled_Job__c>();
		
		Map<String, Scheduled_Job__c> scheduledJobMap = new Map<String, Scheduled_Job__c>();
		
		for(Scheduled_Job__c scheduledJob : [SELECT Id, Name, Job_Param__c, Is_Active__c, Job_SID__c, Job_Type__c FROM Scheduled_Job__c WHERE Name = :jobNames]) {
			scheduledJobMap.put(scheduledJob.Name, scheduledJob);	
		}
		
		for(String jobName : jobNames) {
			if (scheduledJobMap.get(jobName) != null) {
	            Scheduled_Job__c targetJob = scheduledJobMap.get(jobName);
	            
	            if (targetJob.Is_Active__c && !String.isEmpty(targetJob.Job_SID__c)) {
	                // Make sure the system recognizes this job.
	                List<CronTrigger> cts = [
	                    SELECT 
	                        Id, 
	                        CronExpression, 
	                        TimesTriggered, 
	                        NextFireTime
	                    FROM CronTrigger
	                    WHERE Id = :targetJob.Job_SID__c];
	                
	                if (cts != null && cts.size() > 0) {
	                    System.abortJob(targetJob.Job_SID__c);
	                }
	                
	                targetJob.Is_Active__c = False;
	                targetJob.Job_SID__c = '';
	                
	                scheduledJobsToUpdate.add(targetJob);
	            }
	        }	
		}
		
		if(scheduledJobsToUpdate.size() > 0)
			update scheduledJobsToUpdate;
    }
}