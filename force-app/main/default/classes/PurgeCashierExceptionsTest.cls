@isTest
public class PurgeCashierExceptionsTest{
    //schedule this job by Septemper 19, 2020
    public static String CRON_EXP = '0 0 0 17 9 ? 2020';

    static testmethod void testPurge(){
        User adminUser = UserTestData.createTestsysAdminUser();
        insert adminUser;
        System.AssertNotEquals(adminUser.Id, Null);
        
        System.runAs(adminUser) {
           //set up an account
            Account acct = AccountTestData.createAccount('1234', Null);
            insert acct;
            System.AssertNotEquals(acct.Id, Null);
            
            //set up a retail location
            Retail_Location__c retailLocation = RetailLocationTestData.createRetailLocation(acct.Id, 'Test Retail Location 1');
            insert retailLocation;
            System.AssertNotEquals(retailLocation.Id, Null);   
            
            List<LOM_Cashier_Exception__c > exceptionList = new List<LOM_Cashier_Exception__c>();
            for (Integer i = 1; i <= 50; i++)
            {
                LOM_Cashier_Exception__c exp = new LOM_Cashier_Exception__c();
                exp.Retail_Location__c = retailLocation.Id; 
                exp.Date_Time__c = System.Now();
                exp.Employee__c = '';
                exp.Exception__c = '';
                exp.Exception_Name__c = '';
                exp.Items_Count__c = 5;
                exp.Reference__c = '';
                exp.Trans_Number__c = 'TN-' + i;  
                
                exceptionList.add(exp);         
            }
            
            
            for (Integer i = 1; i <= 50; i++)
            {
                LOM_Cashier_Exception__c exp = new LOM_Cashier_Exception__c();
                exp.Retail_Location__c = retailLocation.Id; 
                exp.Date_Time__c = System.today().addDays(-100);
                exp.Employee__c = '';
                exp.Exception__c = '';
                exp.Exception_Name__c = '';
                exp.Items_Count__c = 5;
                exp.Reference__c = '';
                exp.Trans_Number__c = 'TN-' + i;  
                
                exceptionList.add(exp);         
            }            
            insert exceptionList;
            System.assertEquals(100, exceptionList.size());
            
                        
            Test.StartTest();
            
            String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP, new PurgeCashierExceptions());
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);
            System.assertEquals('2020-09-17 00:00:00',String.valueOf(ct.NextFireTime));
                                                

            Test.stopTest();
            
            //System.assertEquals(50, exceptionList.size());                                    
            
        }
        
        
        
    }
}