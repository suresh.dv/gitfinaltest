/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class LOM_RLocationDashboardControllerTest {

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.LOM_Retail_Location_Dashboard;
            Test.setCurrentPageReference(pageRef);
        
            // Create a Parent Account with the name 100
            Account account100 = AccountTestData.createAccount('100', Null);
            insert account100;
            System.AssertNotEquals(account100.Id, Null);
            
            // Create a Child Account for the Account 100 with the name 100.1
            Account account101 = AccountTestData.createAccount('100.1', account100.Id);
            insert account101;
            System.AssertNotEquals(account101.Id, Null);
            
            Retail_Location__c retailLocation = RetailLocationTestData.createRetailLocation(account101.Id, 'Test Retail Location 1');
            insert retailLocation;
            System.AssertNotEquals(retailLocation.Id, Null);
            
            
            Date dt = Date.Today();
            LOM_Daily_Fuel_Sale__c testFuelSale = LOM_TestData.createDailyFuelSale(retailLocation.Id, dt, '89',500.00 );
            insert testFuelSale;            
            System.AssertNotEquals(testFuelSale.Id, Null);

            LOM_Department_Sale__c testDepSale = LOM_TestData.createDepartmentSale(retailLocation.Id, dt, '5', 100.00 );
            insert testDepSale;            
            System.AssertNotEquals(testDepSale.Id, Null);

            LOM_Invoice__c testInvoice =  LOM_TestData.createInvoice(retailLocation.Id, dt, 1500.00, NULL);
            insert testInvoice;  
            System.AssertNotEquals(testInvoice.Id, Null);

            //create standard controller for Retail Location
            ApexPages.StandardController sc = new ApexPages.standardController(retailLocation);

            //Create Dashboard Controller, set dates   
            LOM_Retail_Location_DashboardController controller = new LOM_Retail_Location_DashboardController(sc);
            controller.dateFrom = dt;
            controller.dateTo = dt;            

            //Check if we get our invoice returned by getUnpaidStatementsByDealer
            System.AssertEquals(1, controller.getUnpaidStatementsByDealer().size());
            
            //Check if we get FuelSales and DepartmentSales methods working
            controller.RefreshData();
            System.AssertEquals(True, controller.DepartmentSalesHasData);
            System.AssertEquals(True, controller.FuelSalesHasData);            
            
            
            //create dep sale record with code 100 (Motor Oil)
            //Motor Oil should be excluded from calculation
            testDepSale = LOM_TestData.createDepartmentSale(retailLocation.Id, dt, '100', 200.00 );
            insert testDepSale;            
            System.AssertNotEquals(testDepSale.Id, Null);
            //it should be still one record
            controller.RefreshDepartmentSalesData();               
            System.AssertEquals(1, controller.DepartmentSalesData.size());            

            //create second fuel sale record
            testFuelSale = LOM_TestData.createDailyFuelSale(retailLocation.Id, dt,'91', 1000.00 );
            insert testFuelSale;  
            controller.FuelSalesType='grade';
            controller.RefreshFuelSalesData();             
            System.AssertNotEquals(testFuelSale.Id, Null);
            //Now it fuelsales records should be groupped by grade and we suppose to get 2 records
            System.AssertEquals(2, controller.FuelSalesData.size());  
            
            System.AssertEquals(null, controller.refreshDashBoardByDate());
            
            LOM_Customer_Activity__c ca = new LOM_Customer_Activity__c();
            ca.Retail_Location__c = retailLocation.Id;
            ca.Date__c = dt;
            ca.Hour__c = '12:00';
            ca.Sales_amount__c = 1000;
            ca.Customers_Count__c = 10;
            insert ca;
            System.AssertNotEquals(ca.Id, Null);   
            
            controller.retrieveCustomerActivityData();
            System.AssertEquals(1, controller.CustomerActivityData.size());  
            
            System.AssertEquals(controller.dateToStr, dt.format());
            System.AssertEquals(controller.dateFromStr, dt.format());  
            
            dt = Date.newInstance(2014 ,10, 12);
            controller.dateToStr = '12/10/2014';
            System.AssertEquals(controller.dateTo, dt);                    
        }
    }

}