public class ScaffoldInspectionClone {
    public PageReference createClone() {
        
        String scaffoldId = ApexPages.currentPage().getParameters().get('sId');
        
        List<Scaffold_Inspection__c> inspections = [SELECT 
							Toe_board_surface__c, Toe_board_point_load__c, Toe_board_above_platform__c, Tags_updated__c, Tags_accessess__c, Stairs_wide__c, Stairs_vertical__c, 
							Stairs_height__c, Stairs_depth__c, Stairs_angle__c, Sills_Secured_from_movement__c, Sills_Load_Distribution__c, Sills_Hard_Surfaces__c, 
							Sills_Base_plates_upright__c, Sills_Base_plates_mudsills__c, Safe_Distance_walkway__c, Safe_Distance_impeed_acess__c, Safe_Distance_conductors__c, 
							Ramps_resistance__c, Plum_And_Level_height__c, Plum_And_Level_distance__c, Platforms_span__c, Platforms_painted__c, Platforms_mud_sills__c, 
							Platforms_movement__c, Platforms_minimum_wide__c, Platforms_light_duty__c, Platforms_Scafford_plank__c, Platforms_Planks__c, Platforms_No_gaps__c, 
							Ladders_deck_height__c, Ladders_cage_from_ladder__c, Ladders_cage_extends__c, Ladders_cage_begin__c, Ladders_access_point__c, 
							Ladder_rest_platform__c, Ladder_clear_span__c, Ladder_cage_verticals__c, Id, Hangers_main_clamp__c, Hangers_joiners__c, Guardrails_vertical_members__c, 
							Guardrails_point_load__c, Guardrails_over_height__c, Guardrails_grade_lumber__c, Guardrails_Top_Guard_Rails__c, Guardrails_Midway__c, Guardrails_Gap__c, 
							Engineering__c, Connections_wedges__c, Connections_clamps__c, Connections_Transoms__c, Connections_Node__c, Connections_Ledgers__c, Connections_Joints__c, 
							Connections_Bottom__c, Bracing_wind_force__c, Bracing_head_room__c, Bracing_guy_wired__c, Bracing_free_standing__c, Bracing_csa__c, Access_scafford__c, 
							Access_opening__c, isLocked__c, Date__c, isCertified__c, Scaffold__c
						FROM 
							Scaffold_Inspection__c
						WHERE 
							isCertified__c = 'Approved' AND
                            isLocked__c = True AND
							Scaffold__c =: scaffoldId
						ORDER BY
                            Date__c DESC
						LIMIT 1];
        
        if(inspections != null && inspections.size() > 0) {
            Scaffold_Inspection__c inspection = inspections[0].clone(false, true, false, false);
            inspection.isLocked__c = False;
            inspection.Date__c = Date.today();
            
            insert inspection;
            PageReference ref = new PageReference('/' + inspection.Id);
            ref.setRedirect(true);
            return ref;
        }
        else {
        	PageReference ref = new PageReference('/' + scaffoldId);
            ref.setRedirect(true);
            return ref;
        }
    }
}