/**********************************************************************************
  CreatedBy :   Accenture
  Organization: Accenture
  Purpose   :  Handler class For  Trigger RaM_EquipmentTrigger
  Test Class:  RaM_EquipmentTriggerHandlerTest
  Version:1.0.0.1
**********************************************************************************/
public with Sharing Class RaM_EquipmentTriggerHandler {
   // Private member variable
   //@TestVisible private static final String NS_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/';
   //@TestVisible private static final String NS_SF = 'urn:partner.soap.sforce.com';
   @TestVisible private static final String REST_URL = 'callout:RaM_Named_Credential/services/data/v39.0/limits';
   @TestVisible private static final String ENDPOINT_URL = 'callout:RaM_Named_Credential/services/Soap/m/38.0';
   @TestVisible private static final String SESSION_ID = '{!$Credential.OAuthToken}';
  /**************************************
    CreatedBy:      Accenture
    Method name:    equipmentTypeClassNewValueCheck
    Purpose:        Equipment Trigger handler class main method for insert new picklist value through Metadata API
    Param:          List<RaM_Equipment__c>, Map < id, RaM_Equipment__c >, boolean
    Return:         VOid
    *************************************/  
  public static void equipmentTypeClassNewValueCheck(List < RaM_Equipment__c > equipmentList, Map < id, RaM_Equipment__c > oldMap, boolean isInsert) {
  
    set < string > equipmentTypeList = new set < string > ();
    set < string > equipmentClassList = new set < string > ();
    
    set < string > newEquipmentTypeList = new set < string > ();
    set < string > newEquipmentClassList = new set < string > ();
    
    set < string > newEquipmentTypeVAList = new set < string > ();
    set < string > newEquipmentClassVAList = new set < string > ();
    
    // to get equipment object instance
    SObject sObj= SObjectReturn(RaM_ConstantUtility.EQUIPMENT_OBJECT);

    // to get Vendor Assignment object instance
    SObject sVAObj= SObjectReturn(RaM_ConstantUtility.VENDOR_ASSIGNMENT_OBJECT);
    
    try {
      // get all picklist value 
      for (RaM_Equipment__c equipRecord: equipmentList) {
 
            equipmentTypeList.add(equipRecord.Equipment_Type__c);
            equipmentClassList.add(equipRecord.Equipment_Class__c);
      }
      // for equipment object
      newEquipmentClassList = searchPicklistValue(sObj, RaM_ConstantUtility.EQUIPMENT_CLASS, equipmentClassList);
      newEquipmentTypeList = searchPicklistValue(sObj,RaM_ConstantUtility.EQUIPMENT_TYPE, equipmentTypeList);
      
      //for Vendor Assignment object
      newEquipmentClassVAList = searchPicklistValue(sVAObj, RaM_ConstantUtility.EQUIPMENT_CLASS_VA, equipmentClassList);
      newEquipmentTypeVAList = searchPicklistValue(sVAObj,RaM_ConstantUtility.EQUIPMENT_TYPE_VA, equipmentTypeList);
      
      system.debug('----newEquipmentClassList-----'+newEquipmentClassList );
      system.debug('----newEquipmentTypeList -----'+newEquipmentTypeList );
      
      system.debug('----newEquipmentClassVAList-----'+newEquipmentClassVAList );
      system.debug('----newEquipmentTypeVAList -----'+newEquipmentTypeVAList );

        if(!newEquipmentClassList.isEmpty())
        updateCaseFieldsMetaDataAPI(RaM_ConstantUtility.METADATA_API_EQUIPMENT_CLASS,RaM_ConstantUtility.EQUIPMENT_CLASS_LABEL,RaM_ConstantUtility.PICKLIST_TEXT,newEquipmentClassList);
        
        
        if(!newEquipmentTypeList.isEmpty())
        updateCaseFieldsMetaDataAPI(RaM_ConstantUtility.METADATA_API_EQUIPMENT_TYPE,RaM_ConstantUtility.EQUIPMENT_TYPE_LABEL,RaM_ConstantUtility.PICKLIST_TEXT,newEquipmentTypeList);
        
        
        if(!newEquipmentClassVAList.isEmpty())
        updateCaseFieldsMetaDataAPI(RaM_ConstantUtility.METADATA_API_EQUIPMENT_CLASS_VA,RaM_ConstantUtility.EQUIPMENT_CLASS_LABEL,RaM_ConstantUtility.PICKLIST_TEXT,newEquipmentClassVAList);
        
        
        if(!newEquipmentTypeVAList.isEmpty())
        updateCaseFieldsMetaDataAPI(RaM_ConstantUtility.METADATA_API_EQUIPMENT_TYPE_VA,RaM_ConstantUtility.EQUIPMENT_TYPE_LABEL,RaM_ConstantUtility.PICKLIST_TEXT,newEquipmentTypeVAList);
        
        
    }
    catch(DmlException qExcp) {
        System.debug('An exception occurred: ' + qExcp.getMessage());
        //Error message getTypeName
        System.debug('An exception occurred: ' + qExcp.getMessage());
        RaM_UtilityHolder.errormethod(RaM_ConstantUtility.EQUIPMENT_OBJECT,RaM_ConstantUtility.CLASS_NAME_EQUIPMENT_TRIGGER ,RaM_ConstantUtility.METHOD_NAME_EQUIPMENT_CHECK_NEW_VAL , RaM_ConstantUtility.FATAL,qExcp.getMessage());
    }
  }
  /**************************************
    CreatedBy:      Accenture
    Method name:    SObjectReturn
    Purpose:        to get SObject instance for given object
    Param:          string objectName
    Return:         SObject
    *************************************/
   public static SObject SObjectReturn(string objectName)
  {
        // to get equipment object instance
        String sObjectName = objectName;
        Schema.SObjectType sType = Schema.getGlobalDescribe().get(sObjectName);
        SObject sObj = sType.newSObject();
        return sObj;
  }
    /**************************************
    CreatedBy:      Accenture
    Method name:    updateLocationNumberLookup
    Purpose:        update Functional location field / before insert case
    Param:          list< RaM_Equipment__c >
    Return:         void
    *************************************/
  public static void updateLocationNumberLookup(list< RaM_Equipment__c > equipmentList)
  {
  
    set < string > locationNumberSet = new set < string > ();
    try{
        for (RaM_Equipment__c equipRecord: equipmentList) {
                system.debug('-----FunctionalLocation__c------'+equipRecord.FunctionalLocation__c);
                locationNumberSet.add(equipRecord.FunctionalLocation__c);
        }
        map<String, RaM_Location__c> locationDetail=new map<String, RaM_Location__c>();
        
        for(RaM_Location__c locationList:[select id,SAPFuncLOC__c,name from RaM_Location__c where SAPFuncLOC__c IN :locationNumberSet])
        {
            locationDetail.put(locationList.SAPFuncLOC__c, locationList); 
        }
        system.debug('----locationDetail-----'+locationDetail );
        for(RaM_Equipment__c equipIns: equipmentList)
        {
            RaM_Location__c locationId=locationDetail.get(equipIns.FunctionalLocation__c);
            system.debug('----locationId-----'+locationId );
            if(locationId!=null){
                equipIns.LocationNumber__c=locationId.id;
                equipIns.Location_Number__c=locationId.Name;
            }
            else if(!string.isBlank(equipIns.LocationNumber__c))
            equipIns.addError(RaM_ConstantUtility.LOCATION_ERROR_MESSAGE);
        }
    } catch(ListException qExcp) {
        //Error message getTypeName
        System.debug('An exception occurred: ' + qExcp.getMessage());
        RaM_UtilityHolder.errormethod(RaM_ConstantUtility.EQUIPMENT_OBJECT,RaM_ConstantUtility.CLASS_NAME_EQUIPMENT_TRIGGER ,RaM_ConstantUtility.METHOD_NAME_UPDATE_LOCATION_NUM_LOOKUP , RaM_ConstantUtility.FATAL,qExcp.getMessage());
    }            
    
  }
  /**************************************
    CreatedBy:      Accenture
    Method name:    updateCaseFieldsMetaDataAPI
    Purpose:        Future method to do callout for insert new picklist value in Equipment object fields
    Param:          string, string, string, set < string >
    Return:         void
    *************************************/
  @future (callout=true)
  public static void updateCaseFieldsMetaDataAPI(string fullName, string label, string type_x,set < string > newEquipmentValue)
    {
        
                String restUrl = REST_URL; 
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint(restUrl);
                req.setMethod('GET');
                if (!Test.isRunningTest()) {
                    HttpResponse res = h.send(req);
                }
           
        //Create metadata connection
        MetadataService.MetadataPort service = createService();
         // Read Custom Field
        MetadataService.CustomField customField = new MetadataService.CustomField();
        
        customField.fullName = fullName;
        customField.label = label;
        customField.type_x = type_x;
        
        // add new picklist value
        list<metadataservice.PicklistValue> PicklistValueList=new list<metadataservice.PicklistValue>();
        metadataservice.Picklist pt = new metadataservice.Picklist();
        pt.sorted= false;
        for(string pickValue:newEquipmentValue )
        {
            metadataservice.PicklistValue PickListValueIns = new metadataservice.PicklistValue();
            PickListValueIns.fullName= pickValue;
            PickListValueIns.default_x=false;
            PicklistValueList.add(PickListValueIns);
        }
        pt.picklistValues = PicklistValueList;

        customField.picklist = pt ;

         //call update metadata to update picklist value in object's field
        if (!Test.isRunningTest()) {
            List<MetadataService.SaveResult> results = service.updateMetadata( new MetadataService.Metadata[] { customField });
                system.debug('-----results------'+ results);
        }
    }
    /**************************************
    CreatedBy:      Accenture
    Method name:    createService
    Purpose:        Create Metadata service connection
    Param:          none
    Return:         MetadataService.MetadataPort
    *************************************/
    public static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.endpoint_x = ENDPOINT_URL; 
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = SESSION_ID;
        return service;
    }
    /**************************************
    CreatedBy:      Accenture
    Method name:    searchPicklistValue
    Purpose:        Compare picklist values with existing value and return the set of new picklist values
    Param:          Sobject, String, set < string > 
    Return:         set < string >
    *************************************/
    public static set < string > searchPicklistValue(Sobject object_name, String field_name, set < string > equipmentTypeClassList) {
        set < string > options = new set < string > (); //new list for holding all of the picklist options

        Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map < String, Schema.SObjectField > field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List < Schema.PicklistEntry > pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a: pick_list_values) { //for all values in the picklist list  

          options.add(a.getValue()); //add the value and label to our final list
        }

        for (string equipValue: options) {
          if (equipmentTypeClassList.contains(equipValue))
            equipmentTypeClassList.remove(equipValue);
        }

        return equipmentTypeClassList;
  }
}