// 2013-09-09, we decided for now we are not going to use the scheduler to schedule the batch job.  All scheduler related code 
// are now commented out
//global class VolumeTransactionSummarizeBatchSchedule implements Database.Batchable<AggregateResult>, Schedulable, Database.Stateful
global class VolumeTransactionSummarizeBatchSchedule implements Database.Batchable<AggregateResult>, Database.Stateful
{   
/*******************************************************************************************************
*******************************************************************************************************
*******************************************************************************************************
NOTE: 
UPDATED 26-Sep-2013 for now the emails are sent to Chris Sembaluk
SELECT Email FROM User WHERE Profile.Name = 'System Administrator' And IsActive = true AND FirstName = 'Chris' AND Lastname = 'Sembaluk'
Need to make sure that clause is not there when rolled into production.

Also, when a batch process needs to be retried in 24 hours time, I have this line of code below
                Datetime nextBatch = Datetime.now().addDays(1);
but when doing testing, I didn't want to wait for one full day, so I decreased the retry time to 30 seconds.
                //Datetime nextBatch = Datetime.now().addSeconds(30);
Again, need to make sure we are not rolling the 30-second-retry to production.  It needs to be a one-day-retry.
*******************************************************************************************************
*******************************************************************************************************
*******************************************************************************************************/

    global Integer ErrorCode = 0;
	@TestVisible private static Integer errCode = 0;
	@TestVIsible private static Boolean errCode1EmailSent = false;
	@TestVIsible private static Boolean errCode2EmailSent = false;
	
    global Boolean isManualRun;  // 2013-09-09 for now all runs are manual runs.  Scheduled runs are not in scope for the time being.
    
    // this variable is if the batch is called manually, providing a set of volume weekly data that is not the most updated one 
    // (such as if the person missed a week or two worth of data).
    global Date batchEndDate;
    global final Date aRandomSundayDate = Date.NewInstance(1990, 1, 7); // 7th Jan 1990 is a Sunday.
    global Date lastSundayDate;
    global Boolean isEMailSent = false;
    
// 2013-09-09, we decided for now we are not going to use the scheduler to schedule the batch job.  All scheduler related code 
// are now commented out
/*******************************************************************************************************
This execute method is for Schedulable.
The following schedules the job at 6am every day.
    String sch = '0 0 6 * * ?';
1: second, 2: minute, 3: hour, 4: day of month, 5: month, 6: day of week, and optional 7: year
The following schedules the job at midnight every Monday night.
    String sch = '0 0 0 ? * 2';
? = no specified value, * = ALL

So to call this,
    VolumeTransactionSummarizeBatchSchedule p = new VolumeTransactionSummarizeBatchSchedule();
    String sch = '0 0 0 ? * 2';
    system.schedule('Schedule Volume Transaction Summarize ' + System.Now().format('yyyyMMdd'), sch, p);
    
*******************************************************************************************************/

/*******************************************************************************************************
This is how to run the batch job once, without using the scheduler:
    VolumeTransactionSummarizeBatchSchedule a = new VolumeTransactionSummarizeBatchSchedule();
    system.schedulebatch(a, 'test job', 1);  // to run it one minute after
    or
    Database.executeBatch(new VolumeTransactionSummarizeBatchSchedule());
*******************************************************************************************************/

// 2013-09-09, we decided for now we are not going to use the scheduler to schedule the batch job.  All scheduler related code 
// are now commented out
/*
    global void execute(SchedulableContext SC)
    {
        Cardlock_Settings__c cs = Cardlock_Settings__c.getOrgDefaults();
		cs.Volume_Transaction_Scheduled_Job_Id__c = SC.getTriggerId	();
		update cs;        
    	
        VolumeTransactionSummarizeBatchSchedule b = new VolumeTransactionSummarizeBatchSchedule();
        Database.executeBatch(b);
    }
*/
    
	global VolumeTransactionSummarizeBatchSchedule()
    {
        this(null);
    }
    
    global VolumeTransactionSummarizeBatchSchedule(Date pdWeeklyEndDate)
    {
        // We want to find the Sunday before today (or when this batch job is being carried out).
        // The way we find is to see how many days today is away from Sunday.  Say today and Sunday is 3 days
        // apart, so to get to last SUnday, I just subtract 3 from today's date.
        // NOTE we need the lastSundaydate instance variable in the execute method, regardless of whether the batch is run manually or via 
        // schedule.  So in this routine even though it is only used if pdWeeklyEndDate, the setting of lastSundayDate must not be done
        // inside the if statement.
        Date todayDate = System.Today();
        this.lastSundayDate = todayDate.addDays(-1 * Math.abs(Math.mod(todayDate.daysBetween(this.aRandomSundayDate), 7)));

    	if (pdWeeklyEndDate == null)
    	{
    		//this.isManualRun = false;
    		this.isManualRun = true;  // 2013-09-09 for now all runs are manual runs.  Scheduled runs are not in scope for the time being.
			this.batchEndDate = this.lastSundayDate;
    	}
    	else
    	{
    		this.isManualRun = true;
        	this.batchEndDate = pdWeeklyEndDate;
    	}
    }
    
    global class VolumeTransactionAggregateResultIterable implements Iterable<AggregateResult>
    {
        VolumeTransactionSummarizeBatchSchedule vtsbs;  
        global VolumeTransactionAggregateResultIterable(VolumeTransactionSummarizeBatchSchedule pvtsbs)
        {
            vtsbs = pvtsbs;
        }
        global Iterator<AggregateResult> Iterator()
        {
            return new VolumeTransactionAggregateResultIterator(vtsbs);
        }
    }
    
    global class VolumeTransactionAggregateResultIterator implements Iterator<AggregateResult>
    {
        VolumeTransactionSummarizeBatchSchedule vtsbs;  
        AggregateResult [] results {get;set;}
        Integer index {get; set;}
        
        global VolumeTransactionAggregateResultIterator(VolumeTransactionSummarizeBatchSchedule pvtsbs)
        {
            vtsbs = pvtsbs;
            
            index = 0;

        	// first check if the provided date is a Sunday.  If it is not a Sunday, need to send emails to the system admin.
        	if (Math.mod(vtsbs.batchEndDate.daysBetween(vtsbs.aRandomSundayDate), 7) != 0)
        	{
        		vtsbs.ErrorCode = 1;
        		VolumeTransactionSummarizeBatchSchedule.errCode = vtsbs.ErrorCode;
        		results = null;
        	}
        	else
        	{
				// Check to see if the data can be found in the volume weekly.  If data cannot be found in the volume weekly,
				// 1. if the batch is a manual run, then set ErrorCode to 2.
				// 2. if the batch is a scheduled run, then set ErrorCode to 3.
				// The reason to distinguish the two is so that if the batch is a manual run, the system admin should know immediately, 
				// i.e. you manually specified you want to run a batch for this date, and yet no data exists.  So you need to check 
				// immediately. On the other hand, if the batch is a scheduled run, then retry it for 2 more days before sending an email.

				// 2013-09-26: instead of doing an actual count, just select Id and set to a limit of 1 and see if the list contains
				// anything at all, is enough.  Do NOT do the count because each record in the recordset takes up one queried rowcount and
				// you can only have 50000 rows in a transaction.  In other words, if you have 49999 rows in that record set, you only
				// have one more row querying capability before hitting the governor limit.  Compare these two:
				// List<AggregateResult> VTWeeklyCountAResult = [SELECT Count(Id) RecordCount FROM Volume_Transaction__c
				//   WHERE Transaction_Week_Ending_Date__c = :vtsbs.batchEndDate];
				List<Volume_Transaction__c> VTWeeklyCountList = [SELECT Id FROM Volume_Transaction__c
					WHERE Transaction_Week_Ending_Date__c = :vtsbs.batchEndDate LIMIT 1];
					
				// if ((Integer)VTWeeklyCountAResult[0].get('RecordCount') == 0)
				if (VTWeeklyCountList.size() == 0)
				{
					vtsbs.ErrorCode = (vtsbs.isManualRun ? 2 : 3);
					VolumeTransactionSummarizeBatchSchedule.errCode = vtsbs.ErrorCode;
					results = null;
				}
				else
				{
                	// Check to see if the data has already been summarized (i.e. if the date is found in the volume transaction summary
                	// object, if so, then we can just skip because there's nothing to do.
                	List<AggregateResult> VTSummaryCountAResult = [SELECT Count(Id) RecordCount FROM Volume_Transaction_Summary__c
                		WHERE Transaction_Week_Ending_Date__c = :vtsbs.batchEndDate];
                	if ((Integer)VTSummaryCountAResult[0].get('RecordCount') > 0)
                	{
                		vtsbs.ErrorCode = 4;
                		VolumeTransactionSummarizeBatchSchedule.errCode = vtsbs.ErrorCode;
                		results = null;
                	}
                	else
                	{
                		Date batchEndDate = vtsbs.batchEndDate;
                		
	                    String query = ' SELECT Account__c, ';
	                    query += ' Transaction_Week_Ending_Date__c, ';
	                    query += ' Sum(Volume_Transaction__c) Sum_Volume, ';
	                    query += ' Sum(Total_Profit__c) Sum_Total_Profit, ';
	                    query += ' Sum(YTD_Volume__c) Sum_YTD_Volume, ';
	                    query += ' Sum(Total_YTD_Profit__c) Sum_Total_YTD_Profit, ';
	                    query += ' Sum(PYTD_Volume__c) Sum_PYTD_Volume, ';
	                    query += ' Sum(Total_PYTD_Profit__c) Sum_Total_PYTD_Profit ';
	                    query += ' FROM ';
	                    query += ' Volume_Transaction__c ';
	                    query += ' WHERE ';
	                    query += ' Card_Type__c = \'RC\' AND ';
	                    query += ' Transaction_Week_Ending_Date__c = :batchEndDate ';
	                    query += ' GROUP BY ';
	                    query += ' Account__c, Transaction_Week_Ending_Date__c ';
	                    results = Database.query(query);   
                	}
                }
        	}
        }
        
        global boolean hasNext()
        {
            return results != null && !results.isEmpty() && index < results.size();
        }
        
        global AggregateResult next()
        {
            return results[index++];
        }
    }
    
    global Iterable<AggregateResult> start(Database.BatchableContext BC)
    {
        // just instantiate the new iterable here and return
        return new VolumeTransactionAggregateResultIterable(this);
    }
 
    global void execute(Database.BatchableContext BC, List<Sobject> scope)
    {
    	// There are two things to do.  Populate the data into the summary object, and potentially populate the data into the Account object.
    	// Populate the data into account job if and only if the transaction end date is the Sunday that just passed.
        List<Volume_Transaction_Summary__c> vtsummary = new List<Volume_Transaction_Summary__c>();
        List<Account> vtaccounts = new List<Account>();
        
        for (Sobject singleObject : scope)
        {
            Volume_Transaction_Summary__c vtsingle = new Volume_Transaction_Summary__c(
                Account__c = (Id)singleObject.get('Account__c'),
                Total_Profit__c = (Decimal)singleObject.get('Sum_Total_Profit'),
                Total_Profit_PYTD__c = (Decimal)singleObject.get('Sum_Total_PYTD_Profit'),
                Total_Profit_YTD__c = (Decimal)singleObject.get('Sum_Total_YTD_Profit'),
                Volume__c = (Decimal)singleObject.get('Sum_Volume'),
                Volume_PYTD__c = (Decimal)singleObject.get('Sum_PYTD_Volume'),
                Volume_YTD__c = (Decimal)singleObject.get('Sum_YTD_Volume'),
                Transaction_Week_Ending_Date__c = (Date)singleObject.get('Transaction_Week_Ending_Date__c')
                );
            vtsummary.add(vtsingle);
            
            if (this.batchEndDate == this.lastSundayDate)
            {    
	            Account vtaccount = new Account(
	            	Id = (Id)singleObject.get('Account__c'),
	                Profit_Current_Week__c = (Decimal)singleObject.get('Sum_Total_Profit'),
	                Profit_PYTD__c = (Decimal)singleObject.get('Sum_Total_PYTD_Profit'),
	                Profit_YTD__c = (Decimal)singleObject.get('Sum_Total_YTD_Profit'),
	                Volume_Current_Week__c = (Decimal)singleObject.get('Sum_Volume'),
	                Volume_PYTD__c = (Decimal)singleObject.get('Sum_PYTD_Volume'),
	                Volume_YTD__c = (Decimal)singleObject.get('Sum_YTD_Volume'),
	                Transaction_Week_Ending_Date__c = (Date)singleObject.get('Transaction_Week_Ending_Date__c')
	            	);
	            vtaccounts.add(vtaccount);
            }
        }
        if (vtsummary.size() > 0)
        {
	        insert vtsummary;
        }
        if (this.batchEndDate == this.lastSundayDate)
        {
	        if (vtaccounts.size() > 0)
	        {
	        	update vtaccounts;
	        }
        }
    }
 
    global void sendErrorEmail(Integer piErrorCode)
    {
    	String errorMessage  = '';
    	if (piErrorCode == 1)
    	{
	        errorMessage += 'Dear Sir/Madam\n\r';
	        errorMessage += 'The weekly volume file for the week ending ' + Datetime.newInstance(this.batchEndDate, time.newinstance(0,0,0,0)).format('yyyy-MM-dd') + ' is not a Sunday.\n\r';
	        errorMessage += 'Please check your date and make sure it is entered in year, month, day order, and try again.\n\r';
	        errorMessage += 'Thank you very much.\n\r';
	        errorMessage += 'Husky Retail Team';
    	}
    	else if ((piErrorCode == 2) || (piErrorCode == 3))
    	{
	        errorMessage += 'Dear Sir/Madam\n\r';
	        errorMessage += 'The weekly volume file for the week ending ' + Datetime.newInstance(this.batchEndDate, time.newinstance(0,0,0,0)).format('yyyy-MM-dd') + ' does not exist.\n\r';
	        errorMessage += 'Please upload it and run the Summary batch job manually at your earliest convenience.\n\r';
	        errorMessage += 'Thank you very much.\n\r';
	        errorMessage += 'Husky Retail Team';
    	}
    	         
        List<String> systemAdmins = new List<String>();
        for (List<User> users : [ SELECT Email FROM User WHERE Profile.Name = 'System Administrator' And IsActive = true AND FirstName = 'Chris' AND Lastname = 'Sembaluk'])
        {
            for (User user: users)
            {
                systemAdmins.add(user.Email);
            }
        }
        if (systemAdmins.size() > 0)
        {
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
	        mail.setToAddresses(systemAdmins);
	        mail.setSenderDisplayName('Husky Retail Team');
	        mail.setSubject('Errors found summarizing weekly Volume Transaction file');
	        mail.setPlainTextBody(errorMessage);
	        mail.setBccSender(false);
	        mail.setSaveAsActivity(false);
	        mail.setUseSignature(false);
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	        this.isEMailSent = true;
	        if (piErrorCode == 1)
	        {
	        	VolumeTransactionSummarizeBatchSchedule.errCode1EmailSent = true;
	        }
	        else if (piErrorCode == 2)
	        {
	        	VolumeTransactionSummarizeBatchSchedule.errCode2EmailSent = true;
	        }
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        // the ErrorCode will be one of 0 or 1 or 2.
        // If value is 0 then there are no errors.
        // If value is 1 that means the date is not a Sunday, need to email system admin immediately.
        // If value is 2 or 3 that means the object doesn't contain the expected data.  If manual (2), then we cannot find any data
        // for the date that the user specified.  If scheduled (3), then we cannot find any data for last Sunday.  If manual need to
        // notify system admin immediately.
        // If value is 4 that means the batch has already been done for records ending that date.  Nothing to do at all.

        Cardlock_Settings__c cs = Cardlock_Settings__c.getOrgDefaults();
        
        // If there is an error, send out an email.
        if (this.ErrorCode != 0)
        {
        	// If error code is 4, then nothing to do.
        	if (this.ErrorCode != 4)
        	{
        		if (this.ErrorCode == 1)
        		{
        			sendErrorEmail(this.ErrorCode);
        		}
        		else if (this.ErrorCode == 2)
        		{
        			sendErrorEmail(this.ErrorCode);
        		}
    			// 2013-09-09 for now all runs are manual runs.  Scheduled runs are not in scope for the time being.
    			// Therefore ErrorCode == 3 will not happen - ErrorCode is set to 3 only for scheduled runs.
    			// For now, comment out all the codes related to ErrorCode 3.
    			/*
        		else if (this.ErrorCode == 3)  // this is for the scheduled job, when the weekly data is not found.
        		{
		            if (cs.Num_Errors__c == 2) // already erred twice and error again, need to send email
		            {
		                if (cs.Volume_Transaction_Scheduled_Job_Id__c != '0')
		                {
		                    system.abortJob(cs.Volume_Transaction_Scheduled_Job_Id__c);
		                }
		                cs.Volume_Transaction_Scheduled_Job_Id__c = '0';
		                cs.Num_Errors__c = 0;
		                update cs;
		                sendErrorEmail(this.ErrorCode);
		            }
		            else
		            { 
		                if (cs.Volume_Transaction_Scheduled_Job_Id__c != '0')
		                {
		                    system.abortJob(cs.Volume_Transaction_Scheduled_Job_Id__c);
		                }
		
						// Make sure the re-try should be one day, and not thirty seconds.  Use thirty seconds for testing
						// but need to make sure to change that back to one day for production.
		                Datetime nextBatch = Datetime.now().addDays(1);
		                //Datetime nextBatch = Datetime.now().addSeconds(30);
		
		                String cronSchedule = '';
		                cronSchedule += nextBatch.second() + ' ' + nextBatch.minute() + ' ' + nextBatch.hour() + ' ';
		                cronSchedule += nextBatch.day() + ' ' + nextBatch.month() + ' ? ' + nextBatch.year();
		                cs.Num_Errors__c++;
		                // Need to give the job a different name, otherwise you may get 
		                // First error: The Apex job named "VolumeTransactionSummarizeBatchSchedule" is already scheduled for execution.
		                //cs.Volume_Transaction_Scheduled_Job_Id__c = System.Schedule('VolumeTransactionSummarizeBatchSchedule Retry ' + cs.Num_Errors__c, cronSchedule, new VolumeTransactionSummarizeBatchSchedule());
		                cs.Volume_Transaction_Scheduled_Job_Id__c = System.Schedule('VolumeTransactionSummarizeBatchSchedule', cronSchedule, new VolumeTransactionSummarizeBatchSchedule());
		                update cs;
		            }
        		}
	            */
        	}
        }
        else
        {
        	// 2013-09-09, we decided for now we are not going to use the scheduler to schedule the batch job.  
        	// As a result, Volume_Transaction_Scheduled_Job_Id__c and Num_Errors__c in the Custom Settings
        	// are not used.  We'll therefore comment out any code here that is related to these custom settings. 
        	// At the end of the day, the ONLY line of code that is now run is the Database.executeBatch line
        	// that runs another batch job against the standard Account object.
        	
        	// 2013-09-06: decision was made to not run bullet 2, i.e. do not purge the weekly volume file.
            // If everything is fine then 
            // 1) abort any future job, 
            // 2) purge the weekly volume file, and then 
            // 3) start the batch to update account information.
            
        	// 2013-09-09, we decided for now we are not going to use the scheduler to schedule the batch job.  
        	// As a result, Volume_Transaction_Scheduled_Job_Id__c and Num_Errors__c in the Custom Settings
        	// are not used.  We'll therefore comment out any code here that is related to these custom settings. 
            // 1) abort any future job
            /*
            if (cs.Volume_Transaction_Scheduled_Job_Id__c != '0')
            {
                system.abortJob(cs.Volume_Transaction_Scheduled_Job_Id__c);
            }
            cs.Volume_Transaction_Scheduled_Job_Id__c = '0';
            cs.Num_Errors__c = 0;
            update cs;
            */
            
        	// 2013-09-06: decision was made to not run bullet 2, i.e. do not purge the weekly volume file.
            // 2) purge the weekly volume file
            // Database.executeBatch(new PurgeVolumeWeeklyBatchSchedule(this.batchEndDate));
            
            // 3) start the batch to update account information.
            Database.executeBatch(new VolumeTransactionAccountBatch(), 100);
        }
    }  
}