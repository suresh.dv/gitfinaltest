/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   a test class created to validate the EquipmentCorrectionBulkClose,
                EquipmentCorrectionFormEdit, and EquipmentCorrectionFormView
History: 	   09-Mar-17 Miro Zelina included test for Well Event
               06-Jul-17 Miro Zelina included test for System & Sub-System
               28-Jul-17 Miro Zelina updated test for SAP Support User for bulk close
------------------------------------------------------------*/
@isTest
private class EquipmentCorrectionFormTest { 

    static Field__c field;
    static Route__c route; 
    
    static void createSampleData()
    {
        Business_Unit__c businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

        Business_Department__c  businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

        Operating_District__c operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;
        
        Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;
        
        field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

        route = RouteTestData.createRoute('999');
        insert route; 
    }
    //test new/edit/view Equipment Data Update
    static testMethod void testEquipmentDataUpdateLocation() {
        
        createSampleData();
        Location__c wellLocation = LocationTestData.createLocation('Location for testing', route.Id, field.Id);
        insert wellLocation;
        
        Equipment__c equip = EquipmentTestData.createEquipment(wellLocation);
        
        
        //test create a new Data Update Form
        PageReference pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('equipId', equip.Id);
        Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        correctionForm = correctionFormCtrl.form;
        
        System.assertEquals(correctionForm.Description_old__c, equip.Description_of_Equipment__c);
        System.assertEquals(correctionForm.Manufacturer_old__c, equip.Manufacturer__c);
        System.assertEquals(correctionForm.Model_Number_old__c, equip.Model_Number__c);
        System.assertEquals(correctionForm.Manufacturer_Serial_No_old__c, equip.Manufacturer_Serial_No__c);
        System.assertEquals(correctionForm.Tag_Number_old__c, equip.Tag_Number__c);
        System.assert(correctionFormCtrl.photo.Id  == null);
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'You must enter a value');
        }
        correctionForm.Equipment_Status__c = 'Idle';
        
        try
        {
            correctionForm.Equipment__c = null;
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please select an Equipment and click the "Equipment Data Update" button on Equipment detail page.');
        }
        
        correctionForm.Equipment__c = equip.Id;
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please enter new equipment data for correction.');
        }
        
        
        correctionFormCtrl.photo.Name = 'upload attachment';
        correctionFormCtrl.photo.Body = Blob.valueOf('testing testing');
        correctionForm.Description_new__c = 'update new Description';
        correctionFormCtrl.save();
        //the record is save successfull
        System.assert(stdCtrl.getId() != null);
        
        //test edit Data Update form
        correctionForm = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                    Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                    Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                    from Equipment_Correction_Form__c where Id =: stdCtrl.getId()];
                    
        pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', correctionForm.Id);
        pageRef.getParameters().put('retURL', equip.Id);
        
        stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormEditCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        
        System.assertEquals(correctionFormEditCtrl.form.Description_new__c, 'update new Description');
        System.assert(correctionFormEditCtrl.photo.Id != null);
        
        correctionFormEditCtrl.DeletePhoto();
        correctionFormEditCtrl.form.Comments__c = 'delete attachment';
        correctionFormEditCtrl.save();
        
        //close the Request
        correctionFormEditCtrl.CloseRequest();
        
        correctionForm = [select id, Status__c from Equipment_Correction_Form__c where Id =: correctionForm.Id];
        System.assertEquals(correctionForm.Status__c, 'Closed');
        
    }
    
        static testMethod void testEquipmentDataUpdateEvent() {
        
        createSampleData();
        Location__c wellLocation = LocationTestData.createLocation('Location for testing', route.Id, field.Id);
        insert wellLocation;
        
        Well_Event__c wellEvent = LocationTestData.createEvent('Test Well Event', wellLocation.Id);    
        insert wellEvent;
        
        Equipment__c equip = EquipmentTestData.createEquipment(wellEvent);
        
        
        //test create a new Data Update Form
        PageReference pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('equipId', equip.Id);
        Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        correctionForm = correctionFormCtrl.form;
        
        System.assertEquals(correctionForm.Description_old__c, equip.Description_of_Equipment__c);
        System.assertEquals(correctionForm.Manufacturer_old__c, equip.Manufacturer__c);
        System.assertEquals(correctionForm.Model_Number_old__c, equip.Model_Number__c);
        System.assertEquals(correctionForm.Manufacturer_Serial_No_old__c, equip.Manufacturer_Serial_No__c);
        System.assertEquals(correctionForm.Tag_Number_old__c, equip.Tag_Number__c);
        System.assert(correctionFormCtrl.photo.Id  == null);
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'You must enter a value');
        }
        correctionForm.Equipment_Status__c = 'Idle';
        
        try
        {
            correctionForm.Equipment__c = null;
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please select an Equipment and click the "Equipment Data Update" button on Equipment detail page.');
        }
        
        correctionForm.Equipment__c = equip.Id;
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please enter new equipment data for correction.');
        }
        
        
        correctionFormCtrl.photo.Name = 'upload attachment';
        correctionFormCtrl.photo.Body = Blob.valueOf('testing testing');
        correctionForm.Description_new__c = 'update new Description';
        correctionFormCtrl.save();
        //the record is save successfull
        System.assert(stdCtrl.getId() != null);
        
        //test edit Data Update form
        correctionForm = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                    Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                    Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                    from Equipment_Correction_Form__c where Id =: stdCtrl.getId()];
                    
        pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', correctionForm.Id);
        pageRef.getParameters().put('retURL', equip.Id);
        
        stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormEditCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        
        System.assertEquals(correctionFormEditCtrl.form.Description_new__c, 'update new Description');
        System.assert(correctionFormEditCtrl.photo.Id != null);
        
        correctionFormEditCtrl.DeletePhoto();
        correctionFormEditCtrl.form.Comments__c = 'delete attachment';
        correctionFormEditCtrl.save();
        
        //close the Request
        correctionFormEditCtrl.CloseRequest();
        
        correctionForm = [select id, Status__c from Equipment_Correction_Form__c where Id =: correctionForm.Id];
        System.assertEquals(correctionForm.Status__c, 'Closed');
        
    }
    
    static testMethod void testEquipmentDataUpdateSystem() {
        
        createSampleData();
        
        System__c sys = LocationTestData.createSystem('Test System');   
        
        Equipment__c equip = EquipmentTestData.createEquipment(sys);
        
        
        //test create a new Data Update Form
        PageReference pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('equipId', equip.Id);
        Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        correctionForm = correctionFormCtrl.form;
        
        System.assertEquals(correctionForm.Description_old__c, equip.Description_of_Equipment__c);
        System.assertEquals(correctionForm.Manufacturer_old__c, equip.Manufacturer__c);
        System.assertEquals(correctionForm.Model_Number_old__c, equip.Model_Number__c);
        System.assertEquals(correctionForm.Manufacturer_Serial_No_old__c, equip.Manufacturer_Serial_No__c);
        System.assertEquals(correctionForm.Tag_Number_old__c, equip.Tag_Number__c);
        System.assert(correctionFormCtrl.photo.Id  == null);
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'You must enter a value');
        }
        correctionForm.Equipment_Status__c = 'Idle';
        
        try
        {
            correctionForm.Equipment__c = null;
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please select an Equipment and click the "Equipment Data Update" button on Equipment detail page.');
        }
        
        correctionForm.Equipment__c = equip.Id;
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please enter new equipment data for correction.');
        }
        
        
        correctionFormCtrl.photo.Name = 'upload attachment';
        correctionFormCtrl.photo.Body = Blob.valueOf('testing testing');
        correctionForm.Description_new__c = 'update new Description';
        correctionFormCtrl.save();
        //the record is save successfull
        System.assert(stdCtrl.getId() != null);
        
        //test edit Data Update form
        correctionForm = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                    Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                    Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                    from Equipment_Correction_Form__c where Id =: stdCtrl.getId()];
                    
        pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', correctionForm.Id);
        pageRef.getParameters().put('retURL', equip.Id);
        
        stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormEditCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        
        System.assertEquals(correctionFormEditCtrl.form.Description_new__c, 'update new Description');
        System.assert(correctionFormEditCtrl.photo.Id != null);
        
        correctionFormEditCtrl.DeletePhoto();
        correctionFormEditCtrl.form.Comments__c = 'delete attachment';
        correctionFormEditCtrl.save();
        
        //close the Request
        correctionFormEditCtrl.CloseRequest();
        
        correctionForm = [select id, Status__c from Equipment_Correction_Form__c where Id =: correctionForm.Id];
        System.assertEquals(correctionForm.Status__c, 'Closed');
        
    }
    
    static testMethod void testEquipmentDataUpdateSubSystem() {
        
        Sub_System__c subSys = LocationTestData.createSubSystem('Test Sub-System');    
        
        Equipment__c equip = EquipmentTestData.createEquipment(subSys);
        
        
        //test create a new Data Update Form
        PageReference pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('equipId', equip.Id);
        Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        correctionForm = correctionFormCtrl.form;
        
        System.assertEquals(correctionForm.Description_old__c, equip.Description_of_Equipment__c);
        System.assertEquals(correctionForm.Manufacturer_old__c, equip.Manufacturer__c);
        System.assertEquals(correctionForm.Model_Number_old__c, equip.Model_Number__c);
        System.assertEquals(correctionForm.Manufacturer_Serial_No_old__c, equip.Manufacturer_Serial_No__c);
        System.assertEquals(correctionForm.Tag_Number_old__c, equip.Tag_Number__c);
        System.assert(correctionFormCtrl.photo.Id  == null);
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'You must enter a value');
        }
        correctionForm.Equipment_Status__c = 'Idle';
        
        try
        {
            correctionForm.Equipment__c = null;
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please select an Equipment and click the "Equipment Data Update" button on Equipment detail page.');
        }
        
        correctionForm.Equipment__c = equip.Id;
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please enter new equipment data for correction.');
        }
        
        
        correctionFormCtrl.photo.Name = 'upload attachment';
        correctionFormCtrl.photo.Body = Blob.valueOf('testing testing');
        correctionForm.Description_new__c = 'update new Description';
        correctionFormCtrl.save();
        //the record is save successfull
        System.assert(stdCtrl.getId() != null);
        
        //test edit Data Update form
        correctionForm = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                    Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                    Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                    from Equipment_Correction_Form__c where Id =: stdCtrl.getId()];
                    
        pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', correctionForm.Id);
        pageRef.getParameters().put('retURL', equip.Id);
        
        stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormEditCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        
        System.assertEquals(correctionFormEditCtrl.form.Description_new__c, 'update new Description');
        System.assert(correctionFormEditCtrl.photo.Id != null);
        
        correctionFormEditCtrl.DeletePhoto();
        correctionFormEditCtrl.form.Comments__c = 'delete attachment';
        correctionFormEditCtrl.save();
        
        //close the Request
        correctionFormEditCtrl.CloseRequest();
        
        correctionForm = [select id, Status__c from Equipment_Correction_Form__c where Id =: correctionForm.Id];
        System.assertEquals(correctionForm.Status__c, 'Closed');
        
    }

    static testMethod void testEquipmentDataUpdateFEL() {
        
        Functional_Equipment_Level__c fel = LocationTestData.createFEL('Test FEL');    
        
        Equipment__c equip = EquipmentTestData.createEquipment(fel);
        
        
        //test create a new Data Update Form
        PageReference pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('equipId', equip.Id);
        Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        correctionForm = correctionFormCtrl.form;
        
        System.assertEquals(correctionForm.Description_old__c, equip.Description_of_Equipment__c);
        System.assertEquals(correctionForm.Manufacturer_old__c, equip.Manufacturer__c);
        System.assertEquals(correctionForm.Model_Number_old__c, equip.Model_Number__c);
        System.assertEquals(correctionForm.Manufacturer_Serial_No_old__c, equip.Manufacturer_Serial_No__c);
        System.assertEquals(correctionForm.Tag_Number_old__c, equip.Tag_Number__c);
        System.assert(correctionFormCtrl.photo.Id  == null);
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'You must enter a value');
        }
        correctionForm.Equipment_Status__c = 'Idle';
        
        try
        {
            correctionForm.Equipment__c = null;
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please select an Equipment and click the "Equipment Data Update" button on Equipment detail page.');
        }
        
        correctionForm.Equipment__c = equip.Id;
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please enter new equipment data for correction.');
        }
        
        
        correctionFormCtrl.photo.Name = 'upload attachment';
        correctionFormCtrl.photo.Body = Blob.valueOf('testing testing');
        correctionForm.Description_new__c = 'update new Description';
        correctionFormCtrl.save();
        //the record is save successfull
        System.assert(stdCtrl.getId() != null);
        
        //test edit Data Update form
        correctionForm = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                    Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                    Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                    from Equipment_Correction_Form__c where Id =: stdCtrl.getId()];
                    
        pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', correctionForm.Id);
        pageRef.getParameters().put('retURL', equip.Id);
        
        stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormEditCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        
        System.assertEquals(correctionFormEditCtrl.form.Description_new__c, 'update new Description');
        System.assert(correctionFormEditCtrl.photo.Id != null);
        
        correctionFormEditCtrl.DeletePhoto();
        correctionFormEditCtrl.form.Comments__c = 'delete attachment';
        correctionFormEditCtrl.save();
        
        //close the Request
        correctionFormEditCtrl.CloseRequest();
        
        correctionForm = [select id, Status__c from Equipment_Correction_Form__c where Id =: correctionForm.Id];
        System.assertEquals(correctionForm.Status__c, 'Closed');
        
    }

    static testMethod void testEquipmentDataUpdateYARD() {
        
        Yard__c yard = LocationTestData.createYard('Test Yard');    
        
        Equipment__c equip = EquipmentTestData.createEquipment(yard);
        
        
        //test create a new Data Update Form
        PageReference pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('equipId', equip.Id);
        Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
        ApexPages.Standardcontroller stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        correctionForm = correctionFormCtrl.form;
        
        System.assertEquals(correctionForm.Description_old__c, equip.Description_of_Equipment__c);
        System.assertEquals(correctionForm.Manufacturer_old__c, equip.Manufacturer__c);
        System.assertEquals(correctionForm.Model_Number_old__c, equip.Model_Number__c);
        System.assertEquals(correctionForm.Manufacturer_Serial_No_old__c, equip.Manufacturer_Serial_No__c);
        System.assertEquals(correctionForm.Tag_Number_old__c, equip.Tag_Number__c);
        System.assert(correctionFormCtrl.photo.Id  == null);
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'You must enter a value');
        }
        correctionForm.Equipment_Status__c = 'Idle';
        
        try
        {
            correctionForm.Equipment__c = null;
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please select an Equipment and click the "Equipment Data Update" button on Equipment detail page.');
        }
        
        correctionForm.Equipment__c = equip.Id;
        try
        {
            correctionFormCtrl.save();
        }
        catch(Exception e)
        {
            System.assert(false, 'Please enter new equipment data for correction.');
        }
        
        
        correctionFormCtrl.photo.Name = 'upload attachment';
        correctionFormCtrl.photo.Body = Blob.valueOf('testing testing');
        correctionForm.Description_new__c = 'update new Description';
        correctionFormCtrl.save();
        //the record is save successfull
        System.assert(stdCtrl.getId() != null);
        
        //test edit Data Update form
        correctionForm = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                    Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                    Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                    from Equipment_Correction_Form__c where Id =: stdCtrl.getId()];
                    
        pageRef = Page.EquipmentCorrectionFormEdit;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id', correctionForm.Id);
        pageRef.getParameters().put('retURL', equip.Id);
        
        stdCtrl = new ApexPages.Standardcontroller(correctionForm);
        EquipmentCorrectionFormControllerX correctionFormEditCtrl = new EquipmentCorrectionFormControllerX(stdCtrl);
        
        System.assertEquals(correctionFormEditCtrl.form.Description_new__c, 'update new Description');
        System.assert(correctionFormEditCtrl.photo.Id != null);
        
        correctionFormEditCtrl.DeletePhoto();
        correctionFormEditCtrl.form.Comments__c = 'delete attachment';
        correctionFormEditCtrl.save();
        
        //close the Request
        correctionFormEditCtrl.CloseRequest();
        
        correctionForm = [select id, Status__c from Equipment_Correction_Form__c where Id =: correctionForm.Id];
        System.assertEquals(correctionForm.Status__c, 'Closed');
        
    }
    
    
    //test bulk close Equipment Data Update
    static testMethod void testEquipmentDataUpdateBulkClose()
    {
        
        User sapSupportLeadUser = EquipmentTestData.createUser('SAP Support - Lead','Standard HOG - General User','SAP_Support_LeadUser@test.com'); 

        System.runAs(sapSupportLeadUser)
        {
            createSampleData();
            Location__c wellLocation = LocationTestData.createLocation('Location for testing', route.Id, field.Id);
            insert wellLocation;
            
            Equipment__c equip = EquipmentTestData.createEquipment(wellLocation);
            //create bulk of "Open" Equipment Data Update form
            List<Equipment_Correction_Form__c> formList = new List<Equipment_Correction_Form__c>();
            for(integer i=0; i< 3; i++)
            {
                Equipment_Correction_Form__c form = new Equipment_Correction_Form__c();
                form.Equipment__c = equip.Id;
                form.Status__c = 'Open';
                form.Description_new__c = 'Changing new Description';
               
                formList.add(form);
            }
            insert formList;
            
            formList = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                        Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                        Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                        from Equipment_Correction_Form__c where Equipment__c =: equip.Id];
            
                        
            ApexPages.StandardSetController stdcon = new ApexPages.StandardSetController(new List<Equipment_Correction_Form__c>());
            stdcon.setSelected(new List<Equipment_Correction_Form__c>());
            EquipmentCorrectionBulkCloseControllerX setCtrl;
            //display error when no Form is selected
            try
            {
                setCtrl = new EquipmentCorrectionBulkCloseControllerX(stdcon);
            }
            catch(Exception e)
            {
                System.assert(false, 'Please select at least one Equipment Correction Form');
            }
            System.debug(stdcon.getSelected().size() == 0);
            
            stdcon = new ApexPages.StandardSetController(formList);
            stdcon.setSelected(formList);
            setCtrl = new EquipmentCorrectionBulkCloseControllerX(stdcon);
            
            system.assertEquals(1, setCtrl.CurrentIndex); 
            system.assertNotEquals(null, setCtrl.CurrentForm); 
            system.assertEquals(3, setCtrl.selectedList.Size()); 
                
                
            Equipment_Correction_Form__c CurrentForm = setCtrl.CurrentForm;
            Attachment att = new Attachment();
            att.Name ='test attachment';
            att.body = Blob.valueOf('file content');
            att.ParentId = CurrentForm.Id;
            insert att;
            
            System.assert(setCtrl.photo.Id  != null);
            
            Equipment_Correction_Form__c Form2 = setCtrl.selectedList[1];
            Equipment_Correction_Form__c Form3 = setCtrl.selectedList[2];
                
            system.assertEquals(false, setCtrl.getHasPreviousForm()); 
            system.assertEquals(true, setCtrl.getHasNextForm());            
    
            setCtrl.NextForm();
            setCtrl.photo = null;
            System.assert(setCtrl.photo.Id  == null);
            system.assertEquals(2, setCtrl.CurrentIndex); 
            system.assertEquals(Form2 , setCtrl.CurrentForm); 
            system.assertEquals(true, setCtrl.getHasPreviousForm()); 
            system.assertEquals(true, setCtrl.getHasNextForm()); 
    
            setCtrl.NextForm();
            system.assertEquals(3, setCtrl.CurrentIndex);             
            system.assertEquals(Form3 , setCtrl.CurrentForm); 
            system.assertEquals(true, setCtrl.getHasPreviousForm()); 
            system.assertEquals(false, setCtrl.getHasNextForm());                        
    
            //lets try to go over the last form
            setCtrl.NextForm();
            system.assertEquals(3, setCtrl.CurrentIndex); //still 3            
            system.assertEquals(Form3 , setCtrl.CurrentForm); 
            system.assertEquals(true, setCtrl.getHasPreviousForm()); 
            system.assertEquals(false, setCtrl.getHasNextForm());  
    
            setCtrl.PreviousForm();
            system.assertEquals(2, setCtrl.CurrentIndex); 
            system.assertEquals(Form2 , setCtrl.CurrentForm); 
            system.assertEquals(true, setCtrl.getHasPreviousForm()); 
            system.assertEquals(true, setCtrl.getHasNextForm()); 
                
            setCtrl.CloseForm();
            Form2 = [select Id, Status__c from Equipment_Correction_Form__c where Id=: Form2.id];
            System.assertEquals(Form2.Status__c, 'Closed');
        }
    }
    
        //test bulk close Equipment Data Update
    static testMethod void testEquipmentDataUpdateBulkCloseNonSAPSupportuser()
    {
        
        //user is not SAP Support Lead - will fail
        User HOGUser = EquipmentTestData.createUser('Heavy Oil & Gas','Standard HOG - General User','SAP_Support_LeadUser@test.com'); 

        System.runAs(HOGUser)
        {
            createSampleData();
            Location__c wellLocation = LocationTestData.createLocation('Location for testing', route.Id, field.Id);
            insert wellLocation;
            
            Equipment__c equip = EquipmentTestData.createEquipment(wellLocation);
            //create bulk of "Open" Equipment Data Update form
            List<Equipment_Correction_Form__c> formList = new List<Equipment_Correction_Form__c>();
            for(integer i=0; i< 3; i++)
            {
                Equipment_Correction_Form__c form = new Equipment_Correction_Form__c();
                form.Equipment__c = equip.Id;
                form.Status__c = 'Open';
                form.Description_new__c = 'Changing new Description';
            
                formList.add(form);
            }
            insert formList;
            
            formList = [select Comments__c, Description_new__c, Description_old__c, Equipment__c, Equipment_Status__c, Id,  
                        Manufacturer_new__c, Manufacturer_old__c, Manufacturer_Serial_No_new__c, Manufacturer_Serial_No_old__c, 
                        Model_Number_new__c, Model_Number_old__c, Name, Status__c, Tag_Number_new__c, Tag_Number_old__c 
                        from Equipment_Correction_Form__c where Equipment__c =: equip.Id];
            
                        
            ApexPages.StandardSetController stdcon = new ApexPages.StandardSetController(new List<Equipment_Correction_Form__c>());
            stdcon.setSelected(new List<Equipment_Correction_Form__c>());
            EquipmentCorrectionBulkCloseControllerX setCtrl;
            //display error when no Form is selected
            try
            {
                setCtrl = new EquipmentCorrectionBulkCloseControllerX(stdcon);
            }
            catch(Exception e)
            {
                System.assert(false, 'Please select at least one Equipment Correction Form');
            }

            stdcon = new ApexPages.StandardSetController(formList);
            stdcon.setSelected(formList);
            setCtrl = new EquipmentCorrectionBulkCloseControllerX(stdcon);
            
            
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            
            for (Apexpages.Message msg : msgs){
                 
                if (msg.getDetail().contains('Only SAP Support Lead, HOG - Administrator or System Administrator can process.')){ 
                    
                    b = true;
                }
            }
            
            system.assert(b);   //Assert the Page Message was Properly Displayed
        }
    }
   
}