public class HMSC_EmailManagementControllerX{
    private Apexpages.StandardController controller;
    private String originalSubject;
    private String originalBody;
    public String currentTemplate {get; set;}
    public Email_Object__c email {get; private set;}
    public Id terminalId {private get; set;}
    public String selectedTerminal {get; set;} 


    public HMSC_EmailManagementControllerX(ApexPages.StandardController controller) {
        //Check for permission set
        List<PermissionSetAssignment> p = [
            SELECT Assignee.Name, PermissionSet.Id, PermissionSet.isOwnedByProfile, PermissionSet.Profile.Name, PermissionSet.Label
            FROM PermissionSetAssignment
            WHERE Assignee.Name =: UserInfo.getName()
                AND PermissionSetId IN (
                SELECT Id FROM PermissionSet WHERE Label = 'HMSC Email User'
        )];

        if(p.size() == 0){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error : Permissionset')); 
            PageReference page = new PageReference('/');
            page.setRedirect(true);
        }

        email = (Email_Object__c)controller.getRecord();             
    }

    //get the list of available template names
    public List<SelectOption> getTemplateNames(){
        List<SelectOption> emailTemplates = new List<SelectOption>{};
        emailTemplates.add(new SelectOption('-1', '--None--')); 
        
        try{
            for (Email_Object__c e : [SELECT id, name, Subject__c FROM Email_Object__c WHERE Status__c = 'Active' ORDER BY name]) {  
                emailTemplates.add(new SelectOption(e.id, e.Name));  
            } 
                    
        } catch(Exception exp) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error : '));            
        } 
        
        return emailTemplates;
    }   
    
    //get the email object for a selected email template
    public void EmailSubject(){
        List<Email_Object__c> e = null;
        
        if(currentTemplate != '-1'){
            try{
                e = [SELECT Name, Subject__c, Body__c FROM Email_Object__c WHERE Id =:currentTemplate limit 1];
                
            } catch(Exception exp) {
                email = new Email_Object__c();
                originalSubject = email.Subject__c;
                originalBody = email.Body__c;                   
                ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error : Unable to retrieve email template record '));
            }                
            
            email = e[0];  
            originalSubject = email.Subject__c;
            originalBody = email.Body__c;
            
        } else {
            email = new Email_Object__c();
            originalSubject = email.Subject__c;
            originalBody = email.Body__c;            
        }

    }
    
    public PageReference send(){
        List<Id> accountsList = new List<Id>();
        String[] emailList = new List<String>();
        
        
        //String terminalId = ApexPages.currentPage().getParameters().get('TerminalID');
        String[] terminalIdList = selectedTerminal.split(';');
        System.debug('terminalId List ' + terminalIdList );
        
        if(terminalIdList.size() == 0){
            return new PageReference('/');
        }
        
        try{
            for(String terminalId : terminalIdList){
                System.debug('Now emailing termianl ' + terminalId);
                
                //remove leading and trailing square braces
                terminalId = terminalId.replace('[','');
                terminalId = terminalId.replace(']','');
                
                /*iterate Terminal_Account_Association__c to find all associated accounts of given terminal */
                List<Terminal_Account_Association__c> terminals = [SELECT Id, Account__c FROM Terminal_Account_Association__c WHERE Terminal__c = :terminalId]; 

                for(Terminal_Account_Association__c t : terminals){
                    accountsList.add(t.Account__c);
                }

                List<Contact> contactList = [SELECT id,Email from Contact WHERE HasOptedOutOfEmail = false AND AccountId IN :accountsList];
                for(Contact c : contactList){
                    emailList.add(c.Email);
                }

                email.Last_Body__c = email.Body__c;
                email.Last_Subject__c = email.Subject__c ;
                email.Last_Recipient__c = String.join(emailList, ',');
                email.Body__c = originalBody;
                email.Subject__c = originalSubject;
                            
                update email;  
           
                sendEmail(email.Last_Subject__c, email.Last_Body__c, emailList);
            }            
            
        } catch(Exception exp) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error updating email object. Please contact platform support team.'));            
        }
        
        return new PageReference('/');
    }
    
    public List<SelectOption> getTerminals(){
        List<SelectOption> optns = new List<Selectoption>();
        // before getting here you must populate your queryResult list with required fields
        for(USCP_Terminal__c t : [SELECT Id,Name from USCP_Terminal__c ORDER By Name]){
            optns.add(new selectOption(t.Id, t.Name));
        }            
        
        return optns;
    }    
    
    /*this will return the uri for Husky image logo*/
    private String getImageUri(){
        List<Document> lstDocument = [Select Id,Name,LastModifiedById from Document where Name = 'Husky Logo' limit 1];
        String strOrgId = UserInfo.getOrganizationId();
        String orgInst = URL.getSalesforceBaseUrl().getHost();

        orgInst = orgInst.replace('.visual.','.content.');
        return URL.getSalesforceBaseUrl().getProtocol() + '://' + orgInst + '/servlet/servlet.ImageServer?id=' + lstDocument[0].Id + '&oid=' + strOrgId;
    }
    
    /*this method sends a html email with bcc addresses*/
    private void sendEmail(String subject, String body, String[] emailList){
        string strDocUrl = getImageUri();         
          
        //body += '<p/><p/><p/><p/>Thanks,<p/><p/><p/><p/><apex:image styleClass="photo" id="huskyLogo" value="'+ strDocUrl + '" />';  
        //body += '<img alt="Husky Logo" height="70" width="215" src="https://huskyenergy--dev17--c.cs21.content.force.com/servlet/servlet.ImageServer?id=015q00000000WPeAAM' + '&oid=' + '00Dq0000000BAu9EAG" />';        
        body += '<img alt="Husky Logo" height="70" width="215" src="' + strDocUrl + '" />';        
        body +='</html></body>'; 
        System.debug('messageBody' + body); 
        System.debug('emailList from sendEmail ' + emailList);   
        
        try{
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
            email.setCharset('UTF-8');
            email.setToAddresses(new String[] {UserInfo.getUserEmail()});
            email.setBCcAddresses(emailList);
            email.setSaveAsActivity(false);
            email.setHtmlBody(body);        
            email.setSubject(subject);

            //Email has the correct URI for the image however, Salesforce change "&" to "&amp;" at somepoint after this. Email doesn't have the Husky logo embdded
            if(!Test.isRunningTest()){
                Messaging.sendEmail(new Messaging.SingleEmailmessage[] {email});
            }
              
            
        }  catch(Exception exp) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error updating email object. Please contact platform support team.'));            
        }
    }
    

}