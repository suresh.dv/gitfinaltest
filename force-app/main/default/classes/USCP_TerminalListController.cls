public class USCP_TerminalListController extends DynamicListController {

    public Boolean IsAdmin {get;private set;}
    public boolean Active{get;set;}
    public String City{get;set;}
    public String Terminal{get;set;}
    public String State{get;set;}  
    public String Product{get;set;}      
    public String OPISMarket{get;set;} 
    
    String locationType ='Terminal';
    String LocationFilter =  'locationType__c =\'' + locationType + '\'';

    public USCP_TerminalListController () {
        // TODO: call the parent class constructor with a base query
        super('select Id, Name, isActive__c, Address__c, State__c, City__c, ZipCode__c, Geolocation__Latitude__s,Geolocation__Longitude__s, DisplayAddress__c, OPIS_Market__c from USCP_Terminal__c ');
        sortColumn = 'Name';
        sortAsc = true;
                
        pageSize = 50;
        IsAdmin = USCP_Utils.IsAdminUser();   
        
        ClearFilter();
    }

    // cast the resultset
    public List<USCP_Terminal__c> getTerminalRecords() {
         return (List<USCP_Terminal__c>) getRecords();
    }
    
    // cast the resultset
    public List<USCP_Terminal__c> getAllTerminalRecords() {
         return (List<USCP_Terminal__c>) getAllRecords();
    }

    public List<SelectOption> Cities{
    get {
      if (Cities == null) {
 
        Cities  = new List<SelectOption>();
        Cities.add(new SelectOption('','')); 
        
        AggregateResult[] results = [
            select city__c from USCP_Terminal__c where  city__c!= null and locationType__c= :locationType and IsActive__c = true  group by city__c order by City__c
        ];
        for (AggregateResult ar : results) {
            String city = (String) ar.get('City__c');
            Cities.add(new SelectOption(city ,city ));
        }
       
        }
        return Cities;           
     }        
    set;
    }
    
    public List<SelectOption> States{
    get {
      if (States == null) {
 
        States = new List<SelectOption>();
        States.add(new SelectOption('','')); 
        
        AggregateResult[] results = [
            select State__c from USCP_Terminal__c where State__c!= null and  locationType__c= :locationType and IsActive__c = true group by State__c order by State__c
        ];
        for (AggregateResult ar : results) {
            String strState= (String) ar.get('State__c');
            States.add(new SelectOption(strState,strState));
        }
       
        }
        return States;           
     }        
    set;
    }
    
    public List<SelectOption> OPISMarkets{
    get {
      if (OPISMarkets== null) {
 
        OPISMarkets= new List<SelectOption>();
        OPISMarkets.add(new SelectOption('','')); 
        
        AggregateResult[] results = [
            select OPIS_Market__c from USCP_Terminal__c where OPIS_Market__c!= null and locationType__c= :locationType and IsActive__c = true group by OPIS_Market__c order by OPIS_Market__c
        ];
        for (AggregateResult ar : results) {
            String market= (String) ar.get('OPIS_Market__c');
            OPISMarkets.add(new SelectOption(market,market));
        }
       
        }
        return OPISMarkets;           
     }        
    set;
    }

    public List<SelectOption> Products{
    get {
      if (Products== null) {
 
        Products= new List<SelectOption>();
        Products.add(new SelectOption('','')); 
        
        AggregateResult[] results = [
            select product__r.name from USCP_terminalproductassociation__c where product__c != null group by product__r.name order by product__r.name
        ];
        for (AggregateResult ar : results) {
            String product = (String) ar.get('Name');
            Products.add(new SelectOption(product ,product ));
        }
       
        }
        return Products;           
     }        
    set;
    }

    
    
    public PageReference ClearFilter() {
        Terminal = '';
        City = '';
        State= '';
        Product= '';
        OPISMarket='';           
        Active = true;
        
        this.WhereClause  = LocationFilter  + ' and IsActive__c = true ';
        
        query();
    
        return null;
    }    
    public PageReference RefreshData() {  
        String FilteredSOQL = ' Name!=null and ' + LocationFilter  ; // always true
            
        try {
            if(Active)
                {
                  filteredSOQL  = filteredSOQL + ' and IsActive__c = true' ;
                }
        
            if(Terminal!='' && Terminal!= null )
                {
                filteredSOQL  = filteredSOQL + ' and Name like \'%' +  String.escapeSingleQuotes(Terminal) + '%\'';
                }        
            if(City !='' && City != null )
                {
                filteredSOQL  = filteredSOQL + ' and City__c like \'%' +  String.escapeSingleQuotes(City) + '%\'';
                }
               
            if(State!='' && State!= null )
                {
                filteredSOQL  = filteredSOQL + ' and State__c =\'' +  String.escapeSingleQuotes(State) + '\'';
                }
                
            if(Product!='' && Product!= null )
                {
                filteredSOQL  = filteredSOQL + ' and id in (select terminal__c from USCP_terminalproductassociation__c where product__r.name like \'%' +  String.escapeSingleQuotes(Product) + '%\')';
                } 
            if(OPISMarket!='' && OPISMarket!= null )
                {
                filteredSOQL  = filteredSOQL + ' and OPIS_Market__c like \'%' +  String.escapeSingleQuotes(OPISMarket) + '%\'';
                }                                


         this.WhereClause  = FilteredSOQL;


              query();
        } catch (Exception e) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    
         return null;
    }    
/*    
  public PageReference SaveToExcel() 
   {
        return page.USCP_InvoicesExcel;
   }    
*/   
}