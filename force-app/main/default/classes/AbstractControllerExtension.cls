public abstract class AbstractControllerExtension {
    private static final String ID_PARAM_NAME = 'id';
    public static final String NO_SELECTION_OPTION_VALUE = '-9999999';
    
	public String getPageParamValue(String paramName) {
    	return ApexPages.currentPage().getParameters().get(paramName);
    }
    
    public String getIdParamValue() {
    	return getPageParamValue(ID_PARAM_NAME);
    }
    
    public Boolean isNoSelectionOptionValue(String val) {
    	return (val == NO_SELECTION_OPTION_VALUE);
    }
}