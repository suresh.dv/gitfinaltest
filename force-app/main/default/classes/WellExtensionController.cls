public with sharing class WellExtensionController {
    
    //================================
    //    WCPWells VF page
    //================================

    // Role to show. Optional, and if not present it'll show for all of WCP.
    public String roleLabel {get;set;}
    public Id     roleId    {get;set;}
    // Schemas to show. The schema names == the record type developer names, so this is also the record types to show.
    public Set<String> schemas {get;set;}
    public Boolean schemaError {get;set;}
    // Those schemas converted into record type ids
    public Set<Id> recordTypesToShow {get;set;}
    
    // What were showing. This is either a leaf node like "GRD" or "Swift Current" or a higher node like "Northwest" or "Western Canada Production"
    public String showing {get;set;}
    
    // List view defaults
    public Integer wellsPerPage {get;set;}
    
    // The "view" for the list view of wells.
    public String  wellView {get;set;}
    
    //List view for Operating district drop down
    public String  operatingDistrict {get;set;}
    
    // The field the user wants to sort by, and the previous field user sorted by
    public String sortField         {get;set;}
    public String previousSortField {get;set;}
    public String sortOrder         {get;set;}
    
    public List<SelectOption> getWellsPerPageOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('10','10'));
        options.add(new SelectOption('20','20'));
        options.add(new SelectOption('50','50'));
        return options;
    }
    
    public List<SelectOption> getListviewoptions() {
        List<SelectOption> listViewOptions = new List<SelectOption>();
        listViewOptions.add(new SelectOption('all','All'));
        listViewOptions.add(new SelectOption('wellsimfollowing','Favourites'));
        return listViewOptions;
    }
    
    public List<SelectOption> getListOperatingDistrict(){
        List<SelectOption> os = new List<SelectOption>();
        
        Set<String> opsDist = WCPRoles.roleId2schemaNames(UserInfo.getUserRoleId());
        for(String s : opsDist){
           os.add(new SelectOption(s,WCPRoles.schemaName2RoleName(s))); 
        }
        
        //operatingDistrict = new List<String> (opsDist).get(0);
        
        return os;
    }
    
    public Map<String,Integer> targetFormations {get;set;}
    public Map<String,Integer> budgetYears      {get;set;}
    
    public String targetFormationFilter {get;set;}
    public String budgetYearFilter      {get;set;}
    
    public String getWellsPerPage() {
        return String.valueOf(wellsPerPage);
    }
        
    public void setWellsPerPage(String wpp) {
        this.wellsPerPage = integer.valueof(wpp);
    }
    
    public String searchString {get;set;}
    
    public transient List<GRD_Borehole__c> boreholes {get;set;}
    
    // Returns the wells shown in the list view
    public List<GRD_Borehole__c> getWells()
    {
        return (List<GRD_Borehole__c>) mySetController.getRecords();
    }
    
    //================================
    //    GRD Dashboard page
    //================================
    
    public Boolean showProductionTab {get;set;}
    
    //================================
    //    Both VF pages
    //================================
    
    // The standard controller
    public ApexPages.StandardSetController mySetController {Get;set;}
    
    //================================
    //    WCPWells VF page
    //================================
    
    /*
     * Constructor.
     */
    public WellExtensionController() {
        
        showing = '';
        
        // What role are we showing? This is one of the divisions in WCP, either a District or a Business Department.
        // Possible values are:
        //   <null>            <show current user's wells>
        //   WCP               Shows all wells
        //     GRD             Gas Resource Development
        //     RL              Rainbow Lake
        //     WCC             Western Canada Conventional
        //       NW            North West
        //         GP          Grande Prairie
        //         RMHRD       Rocky Mountain House/Red Deer
        //         SLA         Slave Lake/Athabasca
        //       SE            South East
        //         P           Provost
        //         SC          Swift Current
        //         TH          Taber/Hussar
        schemaError = false;
        if(ApexPages.currentPage().getParameters().containsKey('show'))
        {
            // Get the requested roles to show
            roleLabel = ApexPages.currentPage().getParameters().get('show');
            showing = roleLabel;
system.debug('mikep: requested roleLabel is '+roleLabel);
            // Convert that into a list of schemas; this'll identify the record types to show
            schemas = WCPRoles.roleLabel2schemaNames(roleLabel);
system.debug('mikep: requested schemas are '+schemas);
            if(schemas == null)
            {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'Requested to show wells from "'+roleLabel+'" which is an unknown set of wells.');
                ApexPages.addMessage(error);
                schemaError = true;
                return;
            }
        }
        else
        {
            // No specific set of wells requested, so we'll show only this user's wells.
            // Get user's role
            Id roleId = UserInfo.getUserRoleId();
system.debug('mikep: default role id is '+roleId);
            // Convert that into a schema name(s), this'll identify what record types we're pulling
            schemas = WCPRoles.roleId2schemaNames(roleId);
system.debug('mikep: default schemas are '+schemas);
            
            if(schemas == null)
            {
                ApexPages.Message error = new ApexPages.Message(ApexPages.Severity.ERROR,'Requested to show wells from the current user\'s role. No wells associated with that role.');
                ApexPages.addMessage(error);
                schemaError = true;
                return;
            }
            
            // Get the role name
            UserRole ur = [SELECT Name FROM UserRole WHERE Id =: roleId];
            showing = ur.Name;
            showing = showing.replace(' - Director','');
            showing = showing.replace(' - Approver','');
            showing = showing.replace(' - Manager','');
            showing = showing.replace(' - User','');
        }
        
        // Fetch all record type names and ids
        transient  Map<Id, RecordType> recordTypes = new Map<Id,RecordType>([SELECT Id,
                                                                         Name,
                                                                         DeveloperName
                                                                  FROM RecordType
                                                                  WHERE DeveloperName =: schemas AND
                                                                        sObjectType = 'GRD_Borehole__c']);
        
        recordTypesToShow = recordTypes.keySet();
        
system.debug('mikep recordTypesToShow '+recordTypesToShow);
        
        // By default, we sort wells in page block table by Name.
        sortField = '';
        
        // Set default view to wells followed on chatter
        wellView = 'all';
        
        targetFormations = new Map<String,Integer>();
        budgetYears      = new Map<String,Integer>();
        
        wellsPerPage = 20;
        
        if(operatingDistrict == null) {
            //set the first district as default
            Set<String> opsDist = WCPRoles.roleId2schemaNames(UserInfo.getUserRoleId());
            operatingDistrict = new List<String> (opsDist).get(0);  
        }                  
        
        // Fetch the wells
        viewWells();
    }
    
    
    // Resets the previous sort field. This should be called whenever 
    public PageReference homeSearchWells()
    {
        previousSortField = null;
        targetFormationFilter = null;
        budgetYearFilter = null;
        return sortWells();
    }
    
    
    // Resets the previous sort field. This should be called whenever 
    public PageReference viewWells()
    {
        previousSortField = null;
        targetFormationFilter = null;
        budgetYearFilter = null;
        searchString = null;
        return sortWells();
    }
    
    public PageReference sortWells()
    {
        //========================================================
        // Now run the query to fetch the wells for the list view
        //========================================================
        System.debug('operatingDistrict ' + operatingDistrict );
        Boolean hasWHERE = false;
        
        String order = 'asc NULLS FIRST';
        
        sortOrder = 'ASC';
        
        // If we`re sorting the same field as last time, then we want to switch the order from ASC to DESC
        if(previousSortField == sortField)
        {
            order = 'desc NULLS LAST';
            previousSortField = null;
            sortOrder = 'DESC';
        }
        else
        {
            previousSortField = sortField;
        }
        
        transient String query = 'SELECT Id,RecordTypeId,Name,Scheduled_Future_Date__c,RTD_Initiated__c,License_Status__c,Geologist__c,Well__r.Surface_Location__r.Operating_Field_AMU__r.Name,Well__r.Surface_Location__r.Name,Well_Class__c,Well_Orientation__c,UWI_Location__r.Coordinate__Latitude__s,UWI_Location__r.Coordinate__Longitude__s,UWI_Location__r.Status__c,Well_License_No__c,Final_UWI__c,Drilling_Project__r.AFE_Number__c,Budget_Year_Drilling__c,Budget_Year_Completion__c,Budget_Year_Tie_In__c,Budget_Year_Facility_Equipment__c,Target_Formation__c,Spud_Date__c,Budget_Years_For_Sorting__c,UWI_Location__r.Name, UWI_Location__r.Operating_Field_AMU__r.Name,UWI_Location__r.Operating_Field_AMU__r.Operating_District__r.Name,Well_State__c,Development_Year__c  FROM GRD_Borehole__c ';
        
        hasWHERE = false;
        
        if(wellView == 'wellsimfollowing')
        {
            List<EntitySubscription> followingOnChatter = [SELECT Id,
                                                                  ParentId
                                                           FROM EntitySubscription 
                                                           WHERE SubscriberId =: UserInfo.getUserId() AND
                                                                 ParentId IN (SELECT Id
                                                                              FROM GRD_Borehole__c)
                                                           LIMIT 1000];
            transient List<Id> wellsBeingFollowed = new List<Id>();
            for(EntitySubscription follow : followingOnChatter)
            {
                wellsBeingFollowed.add(follow.parentId);
            }
            query += ' WHERE Id IN: wellsBeingFollowed ';
            hasWHERE = true;
        }
        
        if(searchString != null && searchString != '')
        {
            transient String wildCardedSearchString = '%' + searchString + '%';
            wildCardedSearchString = wildCardedSearchString.replace('*','%');
            
            if(hasWHERE) query += ' AND ';
            else         query += ' WHERE ';
            query += ' (Name LIKE: wildCardedSearchString OR Well_License_No__c LIKE: wildCardedSearchString OR Well__r.Surface_Location__r.Name LIKE: wildCardedSearchString OR Target_Formation__c LIKE: wildCardedSearchString OR Well_Class__c LIKE: wildCardedSearchString OR UWI_Location__r.Name LIKE: wildCardedSearchString OR UWI_Location__r.Operating_Field_AMU__r.Operating_District__r.Name LIKE: wildCardedSearchString OR UWI_Location__r.Operating_Field_AMU__r.Name LIKE: wildCardedSearchString OR Well_State__c LIKE: wildCardedSearchString OR Development_Year__c LIKE: wildCardedSearchString ) ';
            hasWHERE = true;
        }
        
        if(hasWHERE) query += ' AND ';
        else         query += ' WHERE ';
        
        // What schemas (record types) do we show?
        String recordTypes = WCPRoles.schemaName2RoleName(operatingDistrict);
        query += ' RecordType.Name =: recordTypes ';
        
        if(sortField == 'Surface Location')
        {
            query += 'ORDER BY Well__r.Surface_Location__r.Name ' + order;
        }
        else if(sortField == 'Target Formation')
        {
            query += 'ORDER BY Target_Formation__c ' + order;
        }
        else if(sortField == 'Well Class')
        {
            query += 'ORDER BY Well_Class__c ' + order;
        }
        else if(sortField == 'License Number')
        {
            query += 'ORDER BY Well_License_No__c ' + order;
        }
        else if(sortField == 'District')
        {
            query += 'ORDER BY UWI_Location__r.Operating_Field_AMU__r.Operating_District__r.Name ' + order;
        } 
        else if(sortField == 'AMU')
        {
            query += 'ORDER BY UWI_Location__r.Operating_Field_AMU__r.Name ' + order;
        } 
        else if(sortField == 'Well State')
        {
            query += 'ORDER BY Well_State__c ' + order;
        }    
        else if(sortField == 'Development Year')
        {
            query += 'ORDER BY Development_Year__c ' + order;
        }                              
        else if(sortField == 'Name')
        {
            query += 'ORDER BY Name ' + order;
        }
        else // initially we're sorting by Scheduled_Future_Date__c, which will show wells with a scheduled date somtime in the future at the top of the list
        {
            query += 'ORDER BY Scheduled_Future_Date__c DESC,Name ASC ';
        }
        
        query += ' LIMIT 10000';
        
system.debug('mikep query '+query);
        
////        boreholes = ;
        
        mySetController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        mySetController.setPageSize(wellsPerPage);
        mySetController.first();
        return null;
    }
    
    // Searches for wells. Note fields to search by are hardcoded.
    @RemoteAction
    public static List<GRD_Borehole__c> findWells(String searchString) {
        transient  List<List<SObject>> searchList = [FIND :searchString
                                          IN ALL FIELDS
                                          RETURNING GRD_Borehole__c (Id,
                                                                 RTD_Initiated__c,
                                                                 Name,
                                                                 Geologist__c,
                                                                 Well__r.Surface_Location__c,
                                                                 Well__r.Surface_Location__r.Operating_Field_AMU__r.Name,
                                                                 Well__r.Surface_Location__r.Name,
                                                                 Final_UWI__c
                                                        ORDER BY RTD_Initiated__c)];
        List<GRD_Borehole__c> wells = searchList[0];
        wells.addall([SELECT Id,
                             Name,
                             RTD_Initiated__c,
                             Geologist__c,
                             Well__r.Surface_Location__c,
                             Well__r.Surface_Location__r.Operating_Field_AMU__r.Name,
                             Well__r.Surface_Location__r.Name,
                             Well_Class__c,
                             Well_Orientation__c
                      FROM GRD_Borehole__c
                      ]);
        return wells;
    }
    
    public PageReference searchWells() {
        
        if(searchString == null || searchString == '')
            return null;
        
        transient  List<List<SObject>> searchList = [FIND :searchString
                                          IN ALL FIELDS
                                          RETURNING GRD_Borehole__c (Id,
                                                                 RTD_Initiated__c,
                                                                 Name,
                                                                 Geologist__c,
                                                                 Well__r.Surface_Location__r.Operating_Field_AMU__r.Name,
                                                                 Well__r.Surface_Location__r.Name,
                                                                 Final_UWI__c
                                                        ORDER BY RTD_Initiated__c)];
        List<sObject> wellList    = searchList[0];
        
        transient String query = 'SELECT Id,Name,Scheduled_Future_Date__c,RTD_Initiated__c,License_Status__c,Geologist__c,Well__r.Surface_Location__r.Operating_Field_AMU__r.Name,UWI_Location__r.Operating_District__c,Well__r.Surface_Location__r.Name,Well_Class__c,Well_Orientation__c,UWI_Location__r.Coordinate__Latitude__s,UWI_Location__r.Coordinate__Longitude__s,UWI_Location__r.Status__c,Well_License_No__c,Final_UWI__c,Drilling_Project__r.AFE_Number__c,Budget_Year_Drilling__c,Budget_Year_Tie_In__c,Budget_Year_Completion__c,Budget_Year_Facility_Equipment__c,Target_Formation__c,Spud_Date__c,Budget_Years_For_Sorting__c,UWI_Location__r.Operating_Field_AMU__r.Operating_District__r.Name,UWI_Location__r.Operating_Field_AMU__r.Name,Well_State__c,Development_Year__c FROM GRD_Borehole__c WHERE Id IN: wellList ORDER BY Name ASC';
        
////        boreholes = ;
        
        mySetController = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        
        mySetController.setPageSize(wellsPerPage);
        mySetController.first();
        
        return null;
    }
    
    //================================
    //    GRDDashboard VF page
    //================================
    
    public GRD_Borehole__c well {get;set;}
    
    public String spotfireURL {get;set;}

    public WellExtensionController(ApexPages.StandardController controller) {
        
        // Get user's role
        Id roleId = UserInfo.getUserRoleId();
        // Convert that into a schema name(s), this'll identify what record types we're going to show Production tab for
        schemas = WCPRoles.roleId2schemaNames(roleId);
        
        recordTypesToShow = new Set<Id>();
        if(schemas != null && schemas.size() > 0)
        {
            // Fetch all record type names and ids
            Map<Id, RecordType> recordTypes = new Map<Id,RecordType>([SELECT Id,
                                                                             Name,
                                                                             DeveloperName
                                                                      FROM RecordType
                                                                      WHERE DeveloperName =: schemas AND
                                                                            sObjectType = 'GRD_Borehole__c']);
            
            recordTypesToShow = recordTypes.keySet();
        
        }
        
        GRD_Borehole__c borehole = (GRD_Borehole__c)controller.getRecord();
        
        showProductionTab = False;
        if(recordTypesToShow.contains(borehole.RecordTypeId))
            showProductionTab = True;
        
        List<GRD_Borehole__c> bhs = [SELECT Id, LicensedUWI__c, Production_Status__c FROM GRD_Borehole__c WHERE Id =: borehole.Id];

        if(bhs[0].LicensedUWI__c != null)
        {
            // Convert the borehole record type id into a schema name
            List<RecordType> boreholeRecordType = new List<RecordType>([SELECT Id,
                                                                               DeveloperName
                                                                        FROM RecordType
                                                                        WHERE Id =: borehole.RecordTypeId AND
                                                                              sObjectType = 'GRD_Borehole__c']);
            
            spotfireURL = '';
            
            // Sanity check
            if(boreholeRecordType.size() == 0)
            {
                return;
            }
            
            // Convert the schema name into a Spotfire URL prefix
            String spotfireURLPrefix = WCPRoles.schemaName2spotfirePrefix(boreholeRecordType[0].DeveloperName);
            
            // Sanity check
            if(spotfireURLPrefix == null)
            {
                return;
            }
            
            AURASpotfireURLs__c spotfireURLs = AURASpotfireURLs__c.getOrgDefaults();
            
            // Sanity check. If no Spotfire URLs are defined
            if(spotfireURLs.get(spotfireURLPrefix+'ProductionSpotfireSession__c') == null || spotfireURLs.get(spotfireURLPrefix+'ProductionSpotfireSession__c') == '' || spotfireURLs.get(spotfireURLPrefix+'DrillingSpotfireSession__c') == null || spotfireURLs.get(spotfireURLPrefix+'DrillingSpotfireSession__c') == '')
            {
                return;
            }
            
            if(bhs[0].Production_Status__c == 'Active' || bhs[0].Production_Status__c == 'Complete')
            {
                // Production
                try                { spotfireURL = ((String)(spotfireURLs.get(spotfireURLPrefix+'ProductionSpotfireSession__c'))).replace('$UWI',bhs[0].LicensedUWI__c); }
                catch(Exception e) { spotfireURL = ''; }
            }
            else
            {
                // Drilling
                try                { spotfireURL = ((String)(spotfireURLs.get(spotfireURLPrefix+'DrillingSpotfireSession__c'))).replace('$UWI',bhs[0].LicensedUWI__c); }
                catch(Exception e) { spotfireURL = ''; }
            }
        }
        else
        {
            spotfireURL = '';
        }
    }
    
    public void fetchWellFromDatabase()
    {
        return;
    }
    
    //Added by Gangha for link to well inventory page
    public PageReference navigateToWellInventory()
    {
        PageReference wellInventoryPage = new PageReference('/apex/WellInventory');
        wellInventoryPage.setRedirect(true);
        wellInventoryPage.getParameters().put('id',ApexPages.currentPage().getParameters().get('id'));
        return wellInventoryPage;
    }
    //End of change Gangha
}