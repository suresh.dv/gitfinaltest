@isTest
public class PlantTestData {
	
	public static List<Plant__c> createPlants(Id operatingDistrictId, Integer numberOfPlants) {
		
		List<Plant__c> recList = new List<Plant__c>();
		
		for(Integer i = 0; i < numberOfPlants; i++) {
			Plant__c plant = new Plant__c();
			plant.Name = 'Plant Name ' + i;
			plant.Functional_Location__c = 'Plant Funtional Location ' + i;
			plant.Operating_District__c = operatingDistrictId;
			
			recList.add(plant);	
		}
		
		insert recList;
		return recList;
	}
}