/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Utility class for HOG Vent Gas purposes
Test Class:		HOG_VentGasUtilitiesTest
				HOG_VentGasAlertTriggerTest
				HOG_VentGasTaskCtrlTest
History:        jschn 04.12.2018 - Created.
				jschn 04.19.2018 - Added trigger status validation methods for HOG Vent Gas Alert 
				jschn 04.23.2018 - Reworked methods to work with HOG_Vent_Gas_Alert_Task__c instead Task
**************************************************************************************************/
public with sharing class HOG_VentGas_Utilities {

/**

VENT GAS TASK STUFF

*/
	public static String URL_PARAM_RETURN_URL 	= 'retURL';
	public static String URL_PARAM_SAVE_URL 	= 'saveURL';
	public static String URL_PARAM_TASK			= 'id';
	public static String URL_PARAM_ALERT		= 'alertId';

	public static String TASK_STATUS_NOT_STARTED 	= 'Not Started';
	public static String TASK_STATUS_IN_PROGRESS 	= 'In Progress';
	public static String TASK_STATUS_COMPLETED 		= 'Completed';
	public static String TASK_STATUS_CANCELLED 		= 'Cancelled';

	public static String ASSIGNEE_TYPE_ROUTE_OPERATOR 		= 'Route Operator';
	public static String ASSIGNEE_TYPE_OPERATIONS_ENGINEER 	= 'Operations Engineer';

	/**
	GETTERS
	*/
	/**
	* Get return url param if present
	*/
	public static String getReturnUrlParameter() {
		return ApexPages.currentPage().getParameters().get(URL_PARAM_RETURN_URL);
	}
	
	/**
	* Get save url param if present
	*/
	public static String getSaveUrlParameter() {
		return ApexPages.currentPage().getParameters().get(URL_PARAM_SAVE_URL);
	}
	
	/**
	* Get Vent Gas Task parameter(Id) if present
	*/
	public static String getVentGasTaskParameter() {
		return ApexPages.currentPage().getParameters().get(URL_PARAM_TASK);
	}

	/**
	* Get Vent Gas Alert param (ID) if present
	*/
	public static String getVentGasAlertParameter() {
		return ApexPages.currentPage().getParameters().get(URL_PARAM_ALERT);
	}

	/**
	RETRIEVE DATA
	*/
	/**
	* Retrieve task by Id.
	*/
	public static List<HOG_Vent_Gas_Alert_Task__c> retrieveTask(String id) {
		if(String.isNotBlank(id))
			return [SELECT Id, Assignee1__c, Assignee2__c, Priority__c, Remind__c
						, Status__c, Comments__c, Assignee_Type__c, Subject__c
						, Vent_Gas_Alert__c, Vent_Gas_Alert__r.Production_Engineer__c
						, Vent_Gas_Alert__r.Type__c, Vent_Gas_Alert__r.Priority__c
						, Vent_Gas_Alert__r.Well_Location__c, Vent_Gas_Alert__r.Status__c
					FROM HOG_Vent_Gas_Alert_Task__c 
					WHERE Id =: id];
		return null;
	}

	/**
	* Retrieve tasks based on What Id.
	*/
	public static List<HOG_Vent_Gas_Alert_Task__c> retrieveTasksByAlertId(Set<Id> ids) {
		return [SELECT Id, Vent_Gas_Alert__c, Status__c
				FROM HOG_Vent_Gas_Alert_Task__c
				WHERE Vent_Gas_Alert__c in :ids];
	}
	
	/**
	HELPER METHODS
	*/
	/**
	* Generates List of tasks for alerts compiled into Map, where key for map is Alert Id.
	*/
	private static Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> getAlertsTasks(List<HOG_Vent_Gas_Alert__c> alerts) {
		Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> taskMap = new Map<Id, List<HOG_Vent_Gas_Alert_Task__c>>();
		if(alerts != null && alerts.size() > 0) {
			Set<Id> alertIds = (new Map<Id, HOG_Vent_Gas_Alert__c>(alerts)).keySet();
			for(HOG_Vent_Gas_Alert_Task__c t : retrieveTasksByAlertId(alertIds))
				addTaskToMap(t, taskMap, t.Vent_Gas_Alert__c);
		}
		return taskMap;
	}

	/**
	* Add task to Map based on key.
	*/
	private static void addTaskToMap(HOG_Vent_Gas_Alert_Task__c t, Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> taskMap, Id key) {
		if(!taskMap.containsKey(key))
			taskMap.put(key, new List<HOG_Vent_Gas_Alert_Task__c>());
		taskMap.get(key).add(t);
	}

	/**
	TRIGGER METHODS
	*/
	public static void ventGasTaskTriggerBeforeUpdate(List<HOG_Vent_Gas_Alert_Task__c> taskList,
													Map<Id, HOG_Vent_Gas_Alert_Task__c> taskOldMap) {
		//TODO validate owner
		validateVentGasTaskUpdateOwner(taskList, taskOldMap);
	}

	public static void ventGasTaskTriggerBeforeInsert(List<HOG_Vent_Gas_Alert_Task__c> taskList) {
		validateVentGasTaskInsertOwner(taskList);
	}

	/**
	VALIDAITONS
 	*/
 	private static void validateVentGasTaskInsertOwner(List<HOG_Vent_Gas_Alert_Task__c> taskList) {
 		if(!getIsAdmin()
		&& !getIsHOGAdmin()) {
 			Set<Id> alertIds = getSetOfAlertsIds(taskList);
 			Map<Id, HOG_Vent_Gas_Alert__c> alertsMap = retrieveAlertsMap(alertIds);
	 		for(HOG_Vent_Gas_Alert_Task__c task : taskList) {
	 			validateTaskParentOwner(task, alertsMap.get(task.Vent_Gas_Alert__c));
	 		}
	 	}
 	}

 	private static void validateVentGasTaskUpdateOwner(List<HOG_Vent_Gas_Alert_Task__c> taskList,
													Map<Id, HOG_Vent_Gas_Alert_Task__c> taskOldMap) {
 		if(!getIsAdmin()
		&& !getIsHOGAdmin()) {
			Set<Id> alertIds = getSetOfAlertsIds(taskList);
 			Map<Id, HOG_Vent_Gas_Alert__c> alertsMap = retrieveAlertsMap(alertIds);
	 		for(HOG_Vent_Gas_Alert_Task__c task : taskList) {
	 			validateTaskOwner(task, taskOldMap.get(task.Id), alertsMap.get(task.Vent_Gas_Alert__c));
	 		}
	 	}
 	}

 	private static void validateTaskOwner(HOG_Vent_Gas_Alert_Task__c task, 
 											HOG_Vent_Gas_Alert_Task__c oldTask,
 										 	HOG_Vent_Gas_Alert__c parentAlert) {
		String userId = UserInfo.getUserId();
 		if(!userId.equals(task.Assignee1__c)
		&& !userId.equals(task.Assignee2__c)
		&& !userId.equals(oldTask.Assignee1__c)
		&& !userId.equals(oldTask.Assignee2__c)
		&& !userId.equals(parentAlert.Production_Engineer__c)) {
 			task.addError('Only assignees can edit tasks.');
 		}
 	}

 	private static void validateTaskParentOwner(HOG_Vent_Gas_Alert_Task__c task, HOG_Vent_Gas_Alert__c parentAlert) {
 		String userId = UserInfo.getUserId();
 		if(!userId.equals(parentAlert.Production_Engineer__c)) {
 			task.addError('Only Production Engineer assigned to related HOG Vent Gas Alert can create subtask.');
 		}
 	}

 	private static Set<Id> getSetOfAlertsIds(List<HOG_Vent_Gas_Alert_Task__c> tasks) {
 		Set<Id> ids = new Set<Id>();
 		for(HOG_Vent_Gas_Alert_Task__c task : tasks) {
 			ids.add(task.Vent_Gas_Alert__c);
 		}
 		return ids;
 	}

/**

HOG VENTGAS ALERT STUFF

*/
	public static String ALERT_STATUS_NOT_STARTED 	= 'Not Started';
	public static String ALERT_STATUS_IN_PROGRESS 	= 'In Progress';
	public static String ALERT_STATUS_COMPLETED 	= 'Completed';
	public static String ALERT_STATUS_ACKNOWLEDGE 	= 'Acknowledge';

	public static String ALERT_TYPE_EXPIRED_TEST 	= 'Expired GOR Test';
	public static String ALERT_TYPE_HIGH_VENT 		= 'Predicted High Vent Rate';
	public static String ALERT_TYPE_LOW_VARIANCE 	= 'Large Low Variance in Predicted Vent Rate vs.Test Vent Rate';
	public static String ALERT_TYPE_HIGH_VARIANCE 	= 'Large High Variance in Predicted Vent Rate vs.Test Vent Rate';

	public static String ALERT_PRIORITY_URGENT 	= 'Urgent';
	public static String ALERT_PRIORITY_HIGH 	= 'High';
	public static String ALERT_PRIORITY_MEDIUM 	= 'Medium';
	public static String ALERT_PRIORITY_LOW 	= 'Low';

	/**
	VALIDATIONS
	*/
	/**
	* Validation -> true -> if alert status is Not Started.
	*/
	public static Boolean isAlertNotStarted(HOG_Vent_Gas_Alert__c alert) {
		return ALERT_STATUS_NOT_STARTED.equals(alert.Status__c);
	}

	/**
	RETRIEVE DATA
	*/
	/**
	* Retrieve alert record by Id.
	*/
	public static List<HOG_Vent_Gas_Alert__c> retrieveAlert(String id) {
		return [SELECT Id, Status__c, Priority__c, Production_Engineer__c, Type__c
					, Well_Location__c, Well_Location__r.Route__c, Well_Location__r.Name
				FROM HOG_Vent_Gas_Alert__c 
				WHERE Id =: id];
	}

	/**
	* Retrieve Map of alerts by set of Ids.
	*/
	public static Map<Id, HOG_Vent_Gas_Alert__c> retrieveAlertsMap(Set<Id> ids) {
		return new Map<Id, HOG_Vent_Gas_Alert__c>([SELECT Id, Status__c, Production_Engineer__c
													FROM HOG_Vent_Gas_Alert__c
													WHERE Id in : ids]);
	}

	/**
	TRIGGER METHODS
	*/
	/**
	* Before update trigger method for Vent Gas.
	*/
	public static void ventGasTriggerBeforeUpdate(List<HOG_Vent_Gas_Alert__c> newAlerts, Map<Id, HOG_Vent_Gas_Alert__c> oldAlertMap) {
		Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> taskMap = getAlertsTasks(newAlerts);
		validateAlertStatus(newAlerts, taskMap);
		validateAlertChildTasks(newAlerts, taskMap);
		validateAlertEditPrivilages(newAlerts, oldAlertMap);
		updateEndDate(newAlerts, oldAlertMap);
	}

	/**
	HELPER METHODS
	*/
	/**
	* Update end date for alerts which went to Final stage (Completed, Acknowledge statuses)
	*/
	private static void updateEndDate(List<HOG_Vent_Gas_Alert__c> newAlerts, Map<Id, HOG_Vent_Gas_Alert__c> oldAlertMap) {
		for (HOG_Vent_Gas_Alert__c alert : newAlerts)
			if(alert.Status__c != oldAlertMap.get(alert.Id).Status__c
				&& (ALERT_STATUS_COMPLETED.equals(alert.Status__c)
					|| ALERT_STATUS_ACKNOWLEDGE.equals(alert.Status__c)))
				alert.End_Date__c = Datetime.now();
	}

	/**
	* Validate if user has privilages to edit record.
	* Only System admin, Hog Admin and assignee(Production Engineer) are able to edit alert.
	*/
	private static void validateAlertEditPrivilages(List<HOG_Vent_Gas_Alert__c> alerts, Map<Id, HOG_Vent_Gas_Alert__c> oldAlertMap) {
		Boolean isAdmin = getIsAdmin();
		Boolean isHOGAdmin = getIsHOGAdmin();
		Id usrId = UserInfo.getUserId();
		for (HOG_Vent_Gas_Alert__c alert : alerts)
			if(!usrId.equals(alert.Production_Engineer__c)
				&& !usrId.equals(oldAlertMap.get(alert.Id).Production_Engineer__c)
				&& !isAdmin
				&& !isHOGAdmin) alert.Production_Engineer__c.addError('Only Production Engineer can edit this Vent Gas Alert.');
	}

	/**
	* Validate statuses on all alerts. 
	* There can't be Completed status on Alert when there are no subtasks on it.
	* There can't be Acknowledge status on Alert when there are subtasks on it.
	*/
	private static void validateAlertStatus(List<HOG_Vent_Gas_Alert__c> alerts, Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> taskMap) {
		for(HOG_Vent_Gas_Alert__c alert : alerts) {
			if(ALERT_STATUS_ACKNOWLEDGE.equals(alert.Status__c)
				&& taskMap.containsKey(alert.Id)
				&& taskMap.get(alert.Id).size() > 0) 
				alert.Status__c.addError('You cannot set Status to ' + alert.Status__c + ' when there are related tasks.');
			else if (ALERT_STATUS_COMPLETED.equals(alert.Status__c)
					&& (!taskMap.containsKey(alert.Id)
						|| taskMap.get(alert.Id).size() == 0)) 
				alert.Status__c.addError('You cannot set Status to ' + alert.Status__c + ' when there are no related tasks.');
		}
	}

	/**
	* Validate if there is any noncompleted tasks for Alert when completing.
	*/
	private static void validateAlertChildTasks(List<HOG_Vent_Gas_Alert__c> alerts, Map<Id, List<HOG_Vent_Gas_Alert_Task__c>> taskMap) {
		for(HOG_Vent_Gas_Alert__c alert : alerts)
			if(taskMap.containsKey(alert.Id)
				&& ALERT_STATUS_COMPLETED.equals(alert.Status__c)) {
				Boolean valid = true;
				for(HOG_Vent_Gas_Alert_Task__c t : taskMap.get(alert.Id))
					if(!TASK_STATUS_COMPLETED.equals(t.Status__c)
						&& !TASK_STATUS_CANCELLED.equals(t.Status__c)) {
						valid = false;
						break;
					}
				if(!valid) alert.Status__c.addError('You cannot set Status to ' + alert.Status__c + ' when there are non Completed related tasks.');
			}
	}

/**

LOCATION STUFF

*/
	/**
	RETRIEVE METHODS
	*/
	/**
	* Retieve route operator based on route Id.
	*/
	public static List<Operator_On_Call__c> getRouteOperator(String routeId) {
		return [SELECT Operator__c
				FROM Operator_On_Call__c
				WHERE Operator_Route__c =: routeId
				ORDER BY CreatedDate DESC LIMIT 1];
	}

	/**
	HELPER METHODS
	*/
	/**
	* Get route operator user based on route Id.
	*/
	public static Id getRouteOperatorUserId(String routeId) {
		List<Operator_On_Call__c> operators = getRouteOperator(routeId);
		if(operators != null && operators.size() > 0) return operators.get(0).Operator__c;
		return null;
	}

/**

GENERAL 

*/

	public static final String ADMIN_PROFILE_NAME 	= 'System Administrator';
	public static final String HOG_ADMIN_PS_NAME 	= 'HOG_Administrator';

	/**
	HELPER METHODS
	*/
	/**
	* Add error msg into page.
	*/
	public static void addErrorMsg(String msg) {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
	}

	/**
	* Get First record or null from list.
	*/
	public static SObject getFirstOrNull(List<SObject> objects) {
		 if(objects != null && objects.size() > 0) return objects.get(0);
		 return null;
	}

	/**
	VALIDATIONS
	*/
	/**
	* Get true if running user is Admin(Profile)
	*/
	public static Boolean getIsAdmin() {
		String userId = UserInfo.getUserId();
		User usr = [SELECT Profile.Name FROM User WHERE Id =: userId].get(0);
		return ADMIN_PROFILE_NAME.equals(usr.Profile.Name);
	}

	/**
	* Get true if running user has HOG Administrator Permission set assigned.
	*/
	public static Boolean getIsHOGAdmin() {
		try{
			String userId = UserInfo.getUserId();
			PermissionSet hogAdminPSId = [SELECT Id FROM PermissionSet WHERE Name =: HOG_ADMIN_PS_NAME].get(0);
			Integer assignments = [SELECT Count() FROM PermissionSetAssignment WHERE AssigneeId =: userId AND PermissionSetId =: hogAdminPSId.Id];
			return assignments == 1;
		} catch (Exception ex) {
			System.debug(ex);
		}
		return false;
	}

}