/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_BOLListControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_BOLs;
            Test.setCurrentPageReference(pageRef);


            //lets create some test terminal
            USCP_Terminal__c terminal =   USCP_TestData.createTerminal('trm1', 'addr1', 'city1', 'state1', 'opis1', true);
            System.AssertNotEquals(terminal.Id, Null);
           
            //create test product
            Product2 product = USCP_TestData.createProduct('product1',true);
/*            
            //associate this product with second terminal            
            USCP_TestData.createTerminalProductAssociation(terminal.id, product.id, true);
            //create another test product
            product = USCP_TestData.createProduct('product2',true);
            //associate this product with second terminal            
            USCP_TestData.createTerminalProductAssociation(terminal.id, product.id, true);
*/
        
            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            insert account;
            System.AssertNotEquals(account.Id, Null);



            Date movementdate =  Date.today();
            
            product = USCP_TestData.createProduct('product1',true);


            USCP_Invoice__c invoice = USCP_TestData.createInvoice(account.Id, 'INV100',  null,  movementdate , movementdate , movementdate , 1000.0,  true);
            USCP_TestData.createInvoice(account.Id, 'INV200', null, movementdate , movementdate , movementdate , 2000.0,  true);

            USCP_TestData.createBOLAccTransaction(account.Id, 'BOL1', invoice.id,  'Carrier 1', terminal.id,   movementdate , product.id ,  true);
            USCP_TestData.createBOLAccTransaction(account.Id, 'BOL2', invoice.id,  'Carrier 1', terminal.id,   movementdate , product.id ,  true);


            //test account search method for autocomplete
            System.AssertEquals(1,USCP_BOLListController.searchAccount('100').size());

            //test terminal search method for autocomplete
            System.AssertEquals(1,USCP_BOLListController.searchTerminal('trm1').size());

            //test product search method for autocomplete
            System.AssertEquals(1,USCP_BOLListController.searchProduct('product1').size());

            //test carriersearch method for autocomplete
            System.AssertEquals(1,USCP_BOLListController.searchCarrier('Carrier 1').size());


            //now lets create controller
            USCP_BOLListController controller = new USCP_BOLListController();
            //we should have 0 records           
            System.AssertEquals(0, controller.getBOLRecords().size());             
            //we should have 0 records            
            System.AssertEquals(0, controller.getAllBOLRecords().size());              


            //set search parameters for the controller
            controller.AccountID= account.id;            
            controller.BOLNumber = 'BOL1';
            controller.InvoiceNumber = 'INV100';            
            controller.DateRangeType = '1';
            controller.dateFrom = Date.Today()-1;
            controller.dateTo = Date.Today(); 
            controller.Location = '1';
            controller.Carrier = '1'; 
            controller.Product = 'product1';             
            System.AssertNotEquals(null,controller.dateFromStr);      
            System.AssertNotEquals(null,controller.dateToStr);            

            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getBOLRecords().size());    

            controller.ClearFilter();  
            //we should have 0 record now
            System.AssertEquals(0, controller.getBOLRecords().size());    
/*
            controller.Product = 'product1';     
            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getBOLRecords().size());    
*/
            System.AssertNotEquals(null, controller.SaveToExcel()); 
        }
    }

}