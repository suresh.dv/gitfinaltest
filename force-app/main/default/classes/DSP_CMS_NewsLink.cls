global virtual with sharing class DSP_CMS_NewsLink extends DSP_CMS_LinkController
{
    global DSP_CMS_NewsLink(cms.GenerateContent cc)
    {
        super(cc);
    }
    
    global DSP_CMS_NewsLink()
    {
        super();
    }
    
    global override String getHTML()
    {
        return newsLinkHTML();
    }
}