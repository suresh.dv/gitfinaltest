/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_ALVControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_AllocationViewer;
            Test.setCurrentPageReference(pageRef);

            //setup custom settings for DTN call
            USCP_Settings__c newUserVal =  new USCP_Settings__c();
            //newUserVal.setupOwnerId = runningUser.id;
            newUserVal.DTN_WS_UserName__c = 'HUSWS';
            newUserVal.DTN_WS_Password__c = 'D5a36aF407d024w7Ex';
            newUserVal.DTN_WS_EndPoint__c = 'https://catdtnrfsecurityservice.dtnenergy.com/Service.svc/CreateToken2';
            newUserVal.DTN_ALV_PortalID__c = 'hus';
            newUserVal.DTN_ALV_ComponentID__c = 'ALV';
            newUserVal.DTN_ALV_UserName__c = '200XAVUSER';
            newUserVal.DTN_ALV_Password__c = 'AVUSER';                        
            insert newUserVal;            




            USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
            String WS_UserName = mc.DTN_WS_UserName__c;
            System.AssertEquals('HUSWS', WS_UserName);             

            Test.startTest();
            // Set mock callout class 
            Test.setMock(HttpCalloutMock.class, new USCP_DTN_MockHttpResponseGenerator());
            //Create Controller
            USCP_ALVController controller = new USCP_ALVController();
            controller.SoldTo ='121355';
            controller.Refresh();


            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            String recordtypeid = [select id from recordtype where developername ='USCP_Account_Record' limit 1 ].id;
            account.recordtypeid = recordtypeid ;
            
            insert account;
            System.AssertNotEquals(account.Id, Null);            

            //test account search method for autocomplete
            System.AssertEquals(1,USCP_ALVController.searchAccount('100').size());

            Test.stopTest();
            
            String  securityToken = controller.SecurityToken;
            System.AssertNotEquals(null, securityToken);            

        }
    }

}