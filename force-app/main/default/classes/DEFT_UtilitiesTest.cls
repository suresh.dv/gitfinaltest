@isTest
private class DEFT_UtilitiesTest {
	
	@isTest static void TestTriggerBeforeInsertEOJ() {
		User deftUser = DEFT_TestData.createDeftUser();
		User productionCoordinator = DEFT_TestData.createProductionCoordinator();
		User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
		User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

		User runningUser = [Select Id, Name From User Where Id =: UserInfo.getUserId()];
		System.runAs(runningUser) {
			//Setup
			Route__c route = RouteTestData.createRoute('Test Route');
			route.Operator_1_User__c = deftUser.Id;
			route.Operator_2_User__c = deftUser.Id;
			route.Operator_3_User__c = deftUser.Id;
			route.Operator_4_User__c = deftUser.Id;
			route.Operator_5_User__c = deftUser.Id;
			route.Operator_6_User__c = deftUser.Id;
			insert route;

			//Create Service Rig Program
			HOG_Service_Rig_Program__c serviceRigProgram = DEFT_TestData.createServiceRigProgram(deftUser, productionCoordinator, serviceRigCoordinator);
			serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
			update serviceRigProgram;

			//Create Approval Request for the SRP
			Approval.ProcessSubmitRequest srpApprovalRequest = new Approval.ProcessSubmitRequest();
			srpApprovalRequest.setComments('SRP Approval Request');
			srpApprovalRequest.setObjectId(serviceRigProgram.Id);

			//Submit the record to specific process and skip the criteria evaluation
			srpApprovalRequest.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
			srpApprovalRequest.setSkipEntryCriteria(true);

			//Submit the approval request
			Approval.ProcessResult result = Approval.process(srpApprovalRequest);

			//Verify the result
			System.assert(result.isSuccess());
			System.assertEquals(
	            'Pending', result.getInstanceStatus(), 
	            'Instance Status'+result.getInstanceStatus());

	        // Approve the submitted request
	        // First, get the ID of the newly created item
	        List<Id> newWorkItemIds = result.getNewWorkitemIds();
	        
	        // Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest techApproval = 
	            new Approval.ProcessWorkitemRequest();
	        techApproval.setComments('Technical Approving request.');
	        techApproval.setAction('Approve');
	        techApproval.setNextApproverIds(new Id[] {runningUser.Id});
	        
	        // Use the ID from the newly created item to specify the item to be worked
	        techApproval.setWorkitemId(newWorkItemIds.get(0));
	        
	        // Submit the request for approval
	        Approval.ProcessResult result2 =  Approval.process(techApproval);
	        
	        // Verify the results
	        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
	        
	        //Financial Approval
	        newWorkItemIds = result2.getNewWorkitemIds();
	        Approval.ProcessWorkitemRequest finApproval = new Approval.ProcessWorkitemRequest();
	        finApproval.setComments('Financial Approving Request');
	        finApproval.setAction('Approve');
	        finApproval.setWorkitemId(newWorkItemIds.get(0));

	        //Submit financial approval request
	        Approval.ProcessResult result3 = Approval.process(finApproval);

	        // Verify the results
	        System.assert(result3.isSuccess(), 'Result Status:'+result3.isSuccess());

	        System.assertEquals(
	            'Approved', result3.getInstanceStatus(), 
	            'Instance Status '+result3.getInstanceStatus());
		
			HOG_EOJ__c eoj = DEFT_TestData.createEOJReport(serviceRigProgram);

			//Test Email Fields
			eoj = [Select Id, Name, Production_Engineer_Email__c, Field_Operator1_Email__c, Field_Operator2_Email__c,
					Field_Operator3_Email__c, Field_Operator4_Email__c, Field_Operator5_Email__c, Field_Operator6_Email__c,
					Field_Senior_Email__c, Production_Coordinator_Email__c,Operations_Coordinator_Email__c, 
					Service_Rig_Coordinator_Email__c, Service_Rig_Planner_Email__c
				   From HOG_EOJ__c
				   Where Id =: eoj.Id];

			System.debug('deft test eoj: '+eoj);
			System.debug('deft test eoj.Field_Operator5_Email__c: '+eoj.Field_Operator5_Email__c);
			System.debug('deft test eoj.Field_Operator6_Email__c: '+eoj.Field_Operator6_Email__c);
			System.debug('deft test deftUser.Email: '+deftUser.Email);
			System.assert(eoj.Production_Engineer_Email__c.equalsIgnoreCase(deftUser.Email));
			System.assert(eoj.Field_Operator1_Email__c.equalsIgnoreCase(deftUser.Email));
			System.assert(eoj.Field_Operator2_Email__c.equalsIgnoreCase(deftUser.Email));
			System.assert(eoj.Field_Operator3_Email__c.equalsIgnoreCase(deftUser.Email));
			System.assert(eoj.Field_Operator4_Email__c.equalsIgnoreCase(deftUser.Email));
			//System.assert(eoj.Field_Operator5_Email__c.equalsIgnoreCase(deftUser.Email));
			//System.assert(eoj.Field_Operator6_Email__c.equalsIgnoreCase(deftUser.Email));
			System.assert(eoj.Field_Senior_Email__c.equalsIgnoreCase(deftUser.Email));
			System.assert(eoj.Production_Coordinator_Email__c.equalsIgnoreCase(productionCoordinator.Email));
			System.assert(eoj.Operations_Coordinator_Email__c.equalsIgnoreCase(deftUser.Email));
			//System.assert(eoj.Service_Rig_Coordinator_Email__c.equalsIgnoreCase(serviceRigCoordinator.Email));
			System.assert(eoj.Service_Rig_Planner_Email__c.equalsIgnoreCase(serviceRigPlanner.Email));
		}
	}

	@isTest static void TestRigCompaniesPicklists() {
		User runningUser = [Select Id, Name From User Where Id =: UserInfo.getUserId()];
		System.runAs(runningUser) {
			//Setup
			Account acc = DEFT_TestData.createTestAccount();
        	Vendor_Unit_Company__c vuc = DEFT_TestData.createVUC(acc.Id,'Rig Company');
        	HOG_Rig__c rig = DEFT_TestData.createRig(vuc.Id);

	        List<SelectOption> RigCompanies = new List<SelectOption>(DEFT_Utilities.getRigCompanies());
	        List<SelectOption> Rigs = new List<SelectOption>(DEFT_Utilities.getRigs(vuc.Id));

	        System.assertEquals(RigCompanies.size(),2);
	        System.assertEquals(Rigs.size(),2);
		}
	}

}