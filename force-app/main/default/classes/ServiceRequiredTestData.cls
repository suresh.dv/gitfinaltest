/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Service_Required__c
-------------------------------------------------------------------------------------------------*/
@isTest                
public class ServiceRequiredTestData
{
    public static HOG_Service_Required__c createServiceRequired
    (
    	String name,
    	Boolean supervised, 
    	Boolean validatePressureTest,
    	Boolean validateStartAndStopTime
    	
   	)
    {                
        HOG_Service_Required__c results = new HOG_Service_Required__c
            (
                Name = name,
                Supervised__c = supervised,
                Validate_Pressure_Test__c = validatePressureTest,
                Validate_Start_and_Stop_Time__c = validateStartAndStopTime
            );
            
        return results;
    }
}