public class USCP_InterfaceListController{
    
    public String InterfaceName {get; set;}  
    public Date InterfaceDate {get; set;}   
    
    public Map<string,InterfaceWrapper> InterfaceMap {get; set;}
    public List<InterfaceWrapper> InterfaceWrapperList {get;set;}

    public USCP_InterfaceListController()
    {
        InterfaceMap = new Map<String,InterfaceWrapper>();
        InterfaceWrapperList = new List<InterfaceWrapper>();
        
        Map<String, USCP_Interfaces__c> interfaceSettings = USCP_Interfaces__c.getAll();
        List<String> interfaceNames = new List<String>();
        interfaceNames.addAll(interfaceSettings.keySet());
        interfaceNames.sort();        
        
        
        //for(USCP_Interfaces__c intConfig: USCP_Interfaces__c.getall().values())
        for(String interfacename: interfaceNames)
        {
            USCP_Interfaces__c intConfig =  interfaceSettings.get(interfacename);
            InterfaceWrapper intW = new InterfaceWrapper();
            intW.InterfaceConfig = intConfig;
            intW.dateplaceholder = new Contact();
            InterfaceMap.put(intConfig.Interface_Name__c,intW);  
            InterfaceWrapperList.add(intW);     
        }
     
    }

    public PageReference InvokeInterface()
    {
        String strInterfaceName = ApexPages.currentPage().getParameters().get('interfacename');
        String parameters = '';
        //USCP_Utils.logError(strInterfaceName + ' ' +  acc1.Account_Setup_Date__c);

        InterfaceWrapper intWraper = InterfaceMap.get(strInterfaceName);
        if(intWraper==null)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Unknown interface: ' + strInterfaceName)); 
           return null;
        }

        if(intWraper.dateplaceholder.birthdate==null)
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please selecte date')); 
           return null;
        }  
              
        InterfaceDate  = intWraper.dateplaceholder.birthdate;
        Integer noOfDays;    
        string calloutURL = '';
            
        if(intWraper.InterfaceConfig.NumDays_Parameter__c) // we need to check how many days back we can go
        {
            noOfDays = InterfaceDate.daysBetween(Date.today());
            if(noOfDays > intWraper.InterfaceConfig.Max_Days__c )
            {
                Integer numMaxDays = intWraper.InterfaceConfig.Max_Days__c.IntValue();
                USCP_Utils.logError(strInterfaceName + ' - you can only go back ' + numMaxDays  + ' days to run this interface.');    
                return null;            
            }
           calloutURL  =  intWraper.InterfaceConfig.Invoke_URL__c.replace('{0}','' + noOfDays);
           parameters  = ' number of days: ' + noOfDays;
        }
        if(intWraper.InterfaceConfig.Date_Parameter__c) //Date parameter
        {
           DateTime dt =  InterfaceDate;
           if(InterfaceDate>Date.Today())
           {
                USCP_Utils.logError(strInterfaceName + ' - you can only run this interface for past dates.');    
                return null;  
           }
           String InterfaceDateStr =  dt.format('yyyy-MM-dd');
           calloutURL  =  intWraper.InterfaceConfig.Invoke_URL__c.replace('{0}', InterfaceDateStr );
           parameters  = ' Date: ' + InterfaceDateStr;           
        }
        
        
        if(MakeCallOut(calloutURL))
        {
           String message = strInterfaceName + ' interface successfully started. ' + parameters  ;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, message));
        }
        return null;
    }   


    private boolean MakeCallOut(string url)
    {
            try
            {
                USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
                String WS_CertificateName = mc.HEI_WS_CertificateName__c;              
                String endpointUrl= url;
                System.debug(endpointUrl);
                
                // Instantiate a new http object
                Http h = new Http();
                // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
                HttpRequest req = new HttpRequest();
                
                System.debug(WS_CertificateName);
                req.setEndpoint(endpointUrl);
                if(WS_CertificateName != null) req.setClientCertificateName(WS_CertificateName);
                req.setTimeout(60000);
                System.debug('req.getBody()>>>>>>'+req.getBody());
                req.setMethod('GET');
                
                HttpResponse res;
                
                if (Test.isRunningTest()) { 
                    //return true ;
                    res = new HttpResponse();
                    res.SetBody('Great success!!!!');
                    res.SetStatus('Ok');
                    res.SetStatusCode(200);
                }
                else
                {
                    // Send the request, and return a response
                    res = h.send(req);
                }
                System.debug('res is>>>>>>'+res);
                System.debug('res.getBody()>>>>>>'+res.tostring());
                    
                
                if(res.getStatusCode()>299) 
                    {
                      System.debug('ERROR: '+res.getStatusCode()+': '+res.getStatus());
                      System.debug(res.getBody());
                      USCP_Utils.logError('Web service call to invoke ' + url + ' endpoint failed with status: ' + res.getStatusCode()+' '+res.getStatus() );
                      return false;
                    } 
                else 
                    {                
                        System.debug('res.getBody()>>>>>>'+res.getBody());
                        return true;
                    }
                    
        }
        catch (Exception e) 
        {  
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+ e));        
            USCP_Utils.logError(e);
            return false;
        }
    }
 

public class InterfaceWrapper
{
    public USCP_Interfaces__c InterfaceConfig {get; set;}
    public Contact dateplaceholder {get;set;}
    public Date InterfaceDate {get;set;}
}

}