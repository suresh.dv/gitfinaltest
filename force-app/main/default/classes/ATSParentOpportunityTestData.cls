public class ATSParentOpportunityTestData {
    
    public static List<ATS_Parent_Opportunity__c> createTender(Integer numberOfRecords) {
        
        List<ATS_Parent_Opportunity__c> tenderList = new List<ATS_Parent_Opportunity__c>();
        User tempUser;
        if(Test.isRunningTest()) {
            System.runAs(new User(Id = UserInfo.getUserId())) {
				tempUser = UserTestData.createTestUser();
        		insert tempUser;                
            }
        }
        
        for(Integer i = 0; i < numberOfRecords; i++) {
            ATS_Parent_Opportunity__c tender = new ATS_Parent_Opportunity__c();
            tender.Tender__c= 'Tender Name ' + i;
            tender.Bid_Due_Date_Time__c= Date.today();
            tender.Freight_Due_Date_Time__c = DateTime.now();
            tender.Marketer__c = tempUser.Id;
            tender.Destination_City_Province__c = 'Test Destination';
            tenderList.add(tender);
        }
        
        insert tenderList;
        return tenderList; 
    }
}