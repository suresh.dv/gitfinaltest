public class NCR_CARViewPageControllerX{
    String recordId;
    ID contactId;
    ID AccID;
    ID userID;
  
  
    public NCR_WorkFlowEngine workflowEngine {get; private set;} 
    public String businessUnitName {get;set;}
    public String ProjectName {get;set;}

  
    private final ApexPages.StandardController stdcontroller;
    private final NCR_CAR__c ncr_record;
    
    public string currentBU { get; set; }
    public string currentProj { get; set; }   
    public List<NCR_CAR_NCR__c> relatedNCR {get; private set;}
    
    //used to get a hold of the attachment record selected for deletion
    public String selectedAttachmentId { get; set; }
    public List<Document> documentList {get; private set;}     
    public Document documentFile {get; set;}    
    
     
    //public Map<String, Schema.FieldSet> fieldSetMap {get; private set;}
    public Map<String, string> fieldSetLabelsMap {get; private set;}

    public PageReference NCR_Action1()
    {
        return null;
    }
    public PageReference NCR_Action2()
    {
        return null;
    }    
    public PageReference NCR_Action3()
    {
        return null;
    }           
    public PageReference NCR_Save()
    {                    
        return null;
    }       
    
    public PageReference Save()
    {
        this.ncr_record.Business_Unit__c = currentBU;
        this.ncr_record.Project__c = currentProj;  
        
        SaveAttachments();
                     
        return workflowEngine.NCR_Save();
    } 
    
    public PageReference NCR_CAR_Pdf(){
        List<NCR_CAR_NCR__c> nCR = new List<NCR_CAR_NCR__c>();
        relatedNCR = populateRelatedNCR(ApexPages.currentPage().getParameters().get('Id'), nCR);
        System.debug('xxx ' + relatedNCR.size());    
        return Page.NCR_CAR_PDF_Page;
    }           

       
    public NCR_CARViewPageControllerX(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
        userID =   UserInfo.getUserid();
        
       List<string> fields = new List<String>{'Name','State__c','Project__r.Name','Business_Unit__r.Name'};
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    

        //add fields from fieldset
        List<String> fieldNames = new List<String>();

        fieldSetLabelsMap = new Map<string, String>();
        
        fieldSetLabelsMap.put('CAR_Details', 'Corrective Action Details');               
        
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.NCR_CAR__c.FieldSets.getMap();
        for (String fieldSetName : fieldSetMap.keySet()){
            
            //System.debug('field name is ' + fieldSetName );
            
            fieldSetLabelsMap.put(fieldSetName, SObjectType.NCR_CAR__c.FieldSets.getMap().get(fieldSetName).getLabel()) ;
            
            for(Schema.FieldSetMember f : SObjectType.NCR_CAR__c.FieldSets.getMap().get(fieldSetName).getFields()){
                        fieldNames.add(f.getFieldPath());
                      }
                      //stdController.addFields(fieldNames);            
        }
         

        if(!Test.isRunningTest()) {
            stdController.addFields(fieldNames);   
        }

        this.stdcontroller = stdController;
        //LoadFieldSetFields();
        
        this.ncr_record = (NCR_CAR__c)stdController.getRecord();  
        
        //TODO we need to find record type name based on record ID
        workflowEngine = new NCR_WorkFlowEngine(stdController );
        
        documentList = new List<Document>() ; 
        documentFile = new Document();        
        
        //this.ProjectName = ncr_record.Project__r.Name;
        //this.BusinessUnitNAme = this.ncr_record.Business_Unit__r.Name;  
        
        this.currentProj = this.ncr_record.Project__c;
        this.currentBU = this.ncr_record.Business_Unit__r.Id;                        
        
    }
    
    /*populate business unit picklist*/
    public List<SelectOption> getBusinessUnits() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(Business_Unit__c b:[SELECT id, name from Business_Unit__c where RecordType.name = 'SAP Profit Center Level 4']){
            options.add(new SelectOption(b.id,b.name));
        }
        
        return options;
    }
    
    /*populate project picklist based on business unit*/
    public List<SelectOption> getProjects() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(Milestone1_Project__c p:[SELECT id, name from Milestone1_Project__c where RecordType.name = 'SAP Project' and Business_Unit__c = :currentBU]){
            options.add(new SelectOption(p.id,p.name));
        }
        
        return options;
    } 
    
    /*get the related NCR list*/
    private List<NCR_CAR_NCR__c> populateRelatedNCR(Id car, List<NCR_CAR_NCR__c> relatedNCR) {
        for(NCR_CAR_NCR__c c : [SELECT CAR__c, NCR_Case__c, NCR_Case__r.name, NCR_Case__r.Short_Description_of_Non_Conformance__c from NCR_CAR_NCR__c where CAR__r.id =:car ]){
                
            relatedNCR.add(c);   
        }
        
        return relatedNCR;
    }
    
    private void SaveAttachments()
    {
        
        List<Attachment> attachmentList = new List<Attachment>();
        
        documentList = [select id, Name, Body from Document where id in :documentList];
        for(Document doc: documentList)
        {
        
            Attachment newAtt = new Attachment();
             
            newAtt.ParentId = ncr_record.id;
            newAtt.OwnerId = UserInfo.getUserId();            
            newAtt.Body= doc.body;                        
            newAtt.Name= doc.Name;              
            
            attachmentList.add(newAtt); 
        }
        insert attachmentList;
       
        delete documentList;
    }
    
    public PageReference removeFile()
    {
        // if for any reason we are missing the reference
        if (selectedAttachmentId == null)
            return null;

        List<Document> selectedDocument = [Select Id, Name From Document Where Id = :selectedAttachmentId ];
         
        if (selectedDocument.size() > 0)
        {
                try
                {
                    delete selectedDocument ;
                    
                    
                    documentList = [select id, name from Document where id in :documentList];
                    /*                    
                    Integer i = 0;
                    for(Document doc: documentList)
                    {
                        if(doc.id == selectedAttachmentId)
                        {
                            documentList.remove(i);
                        }
                        i++;
                    }
                    */
                } 
                catch (DMLException e) 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting file'));
                    return null;
                }
                finally 
                {
                    //maintenanceAttachments = getMaintenanceAttachments();
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File deleted successfully')); 
                }
        }
            
        return null;
    }    
    public PageReference uploadFile()  
    {  
    
        try 
        {    
        if(documentFile.Name == null || documentFile.Body == null)
        {
            return null;
        }
    
    /*
        Document doc = new Document();
        doc.folderid = UserInfo.getUserId();
        doc.name = document.Name;
        doc.body = document.body;
        insert doc;
      */
        documentFile.folderid = UserInfo.getUserId(); 
        insert documentFile;
        documentList.add(documentFile);                    

        System.debug('\n*****************************************\n'
            + 'METHOD: NCR_CreatePageControllerX.uploadFile'
            + '\NCR ID: ' + this.ncr_record.id
            + '\ndocument: ' + documentFile
            + '\ndocumentBody: ' + documentFile.Body
            + '\ndocumentName: ' + documentFile.Name
            + '\n************************************************\n');    

            //insert document;
            //attachmentList.add(document);
        } 
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        }
        finally 
        {
            //document.body = null; // clears the viewstate
            //document = new Attachment();
            
            
            documentFile.body = null;
            documentFile = new Document();
        }    

        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File uploaded successfully')); 
        
       
  
                     
        return null; 
    }              

}