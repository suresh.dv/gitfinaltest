/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Related Class:  HOG_SObjectFactory
				HOG_VentGas_FieldDefaults
				HOG_TestDataFactory
Description:    Class for creating test data for purpose of testing classes for HOG Vent Gas applications
History:        jschn 04.26.2018 - Created.
**************************************************************************************************/
@isTest
public class HOG_VentGas_TestData {

/*
LOCATION
 */
 	/**
 	 * Creates Location record with default values.
 	 * In progress it will create route, Business Department, Operating District and AMU.
 	 * @return Location record
 	 */
	public static Location__c createLocation() {
		Route__c route = createRoute(true);
		Business_Department__c department = createBusinessDepartment(true);
		Operating_District__c district = createOperatingDistrict(department.Id, true);
		Field__c amu = createAMU(district.Id, true);

		Location__c location = createLocation('Beach House', route.Id, amu.Id);
		insert location;
		return location;
	}

 	/**
 	 * Creates Location record with default values.
 	 * @param  routeId  Id of route on which location sits
 	 * @param  fieldId  Id of field on which location sits
 	 * @param  doInsert Indicates if returned record is stored inIndicates if returned record is stored in DB
 	 * @return          Location record
 	 */
	public static Location__c createLocation(Id routeId, Id fieldId, Boolean doInsert) {
		Location__c location = (Location__c) HOG_SObjectFactory.createSObject(new Location__c());
		location.Route__c = routeId;
		location.Operating_Field_AMU__c = fieldId;
		if(doInsert) insert location;
		return location;
	}

 	/**
 	 * Creates Location record with default values.
 	 * @param  name     String to override Name of location
 	 * @param  routeId  Id of route on which location sits
 	 * @param  fieldId  Id of field on which location sits
 	 * @param  doInsert Indicates if returned record is stored in DB
 	 * @return          Location record
 	 */
	public static Location__c createLocation(String name, Id routeId, Id fieldId, Boolean doInsert) {
		Location__c location = createLocation(routeId, fieldId, false);
		location.Name = name;
		if(doInsert) insert location;
		return location;
	}

/*
ROUTE
 */
 	/**
 	 * Creates Route record with default values.
 	 * @param  doInsert Indicates if returned record is stored in DB
 	 * @return          Route record
 	 */
	public static Route__c createRoute(Boolean doInsert) {
		return (Route__c) HOG_SObjectFactory.createSObject(new Route__c(), doInsert);
	}

/*
BUSINESS DEPARTMENT
 */
 	/**
 	 * Creates Buesiness Department record with default values.
 	 * @param  doInsert Indicates if returned record is stored in DB
 	 * @return          Business Department record
 	 */
	public static Business_Department__c createBusinessDepartment(Boolean doInsert) {
		return (Business_Department__c) HOG_SObjectFactory.createSObject(new Business_Department__c(), doInsert);
	}

/*
OPERATING DISTRICT
 */
 	/**
 	 * Creates Operating District record with default values.
 	 * @param  departmentId Id of Business Department on which Operating district sits.
 	 * @param  doInsert     Indicates if returned record is stored in DB
 	 * @return              Operating District record
 	 */
	public static Operating_District__c createOperatingDistrict(Id departmentId, Boolean doInsert) {
		Operating_District__c district = (Operating_District__c) HOG_SObjectFactory.createSObject(new Operating_District__c());
		district.Business_Department__c = departmentId;
		if(doInsert) insert district;
		return district;
	}

/*
AMU
 */
 	/**
 	 * Creates AMU record with default values.
 	 * @param  districtId Id of Operating District on which AMU sits
 	 * @param  doInsert   Indicates if returned record is stored in DB
 	 * @return            AMU record
 	 */
	public static Field__c createAMU(Id districtId, Boolean doInsert) {
		Field__c amu = (Field__c) HOG_SObjectFactory.createSObject(new Field__c());
		amu.Operating_District__c = districtId;
		if(doInsert) insert amu;
		return amu;
	}

/*
OPERATOR ON CALL
 */
 	/**
 	 * Creates Operator on call record with default values.
 	 * @param  routeId    Id of Route for which we want to assign operator
 	 * @param  operatorId Id of Operator for whom we want to assign route
 	 * @return            Operator on call record
 	 */
	public static Operator_On_Call__c createOperatorOnCall(String routeId, String operatorId) {
		return HOG_TestDataFactory.createOperatorOnCall(routeId, operatorId);
	}

/*
ALERT
 */
 	/**
 	 * Creates HOG Vent Gas Alert record with default values.
 	 * @return HOG Vent Gas Alert record
 	 */
 	public static HOG_Vent_Gas_Alert__c createAlert() {
 		return (HOG_Vent_Gas_Alert__c) HOG_SObjectFactory.createSObject(new HOG_Vent_Gas_Alert__c(), 
 																		HOG_VentGas_FieldDefaults.CLASS_NAME, 
 																		false);
 	}

 	/**
 	 * Creates HOG Vent Gas Alert record with default values.
 	 * @param  locationId           Id of location on which alert will be created
 	 * @param  productionEngineerId Id of user for which will be Alert created
 	 * @param  doInsert             Indicates if returned record is stored in DB
 	 * @return                      HOG Vent Gas Alert record
 	 */
 	public static HOG_Vent_Gas_Alert__c createAlert(Id locationId, Id productionEngineerId, Boolean doInsert) {
 		HOG_Vent_Gas_Alert__c alert = createAlert();
 		alert.Well_Location__c = locationId;
 		alert.Production_Engineer__c = productionEngineerId;
 		if(doInsert) insert alert;
 		return alert;
 	}

 	/**
 	 * Creates HOG Vent Gas Alert record with default values.
 	 * @param  status               Status of alert
 	 * @param  locationId           Id of location on which alert will be created
 	 * @param  productionEngineerId Id of user for which will be Alert created
 	 * @param  doInsert             Indicates if returned record is stored in DB
 	 * @return                      HOG Vent Gas Alert record
 	 */
 	public static HOG_Vent_Gas_Alert__c createAlert(String status, Id locationId, Id productionEngineerId, Boolean doInsert) {
 		HOG_Vent_Gas_Alert__c alert = createAlert(locationId, productionEngineerId, false);
 		alert.Status__c = status;
 		if(doInsert) insert alert;
 		return alert;
 	}

 	/**
 	 * Creates HOG Vent Gas Alert record with default values.
 	 * @param  name                 Name to override default name
 	 * @param  status               Status of alert
 	 * @param  locationId           Id of location on which alert will be created
 	 * @param  productionEngineerId Id of user for which will be Alert created
 	 * @param  doInsert             Indicates if returned record is stored in DB
 	 * @return                      HOG Vent Gas Alert record
 	 */
 	public static HOG_Vent_Gas_Alert__c createAlert(String name, String status, Id locationId, Id productionEngineerId, Boolean doInsert) {
 		HOG_Vent_Gas_Alert__c alert = createAlert(status, locationId, productionEngineerId, false);
 		alert.Name = name;
 		if(doInsert) insert alert;
 		return alert;
 	}

/*
TASK
 */
 	/**
 	 * Creates HOG Vent Gas Alert Task record with default values.
 	 * @return HOG Vent Gas Alert Task record
 	 */
 	public static HOG_Vent_Gas_Alert_Task__c createTask() {
 		return (HOG_Vent_Gas_Alert_Task__c) HOG_SObjectFactory.createSObject(new HOG_Vent_Gas_Alert_Task__c(), 
 																			HOG_VentGas_FieldDefaults.CLASS_NAME, 
 																			false);
 	}

 	/**
 	 * Creates HOG Vent Gas Alert Task record with default values.
 	 * @param  alert       HOG Vent Gas Alert record for which we want to create task
 	 * @param  assignee1Id Id of user to which will be task assigned
 	 * @param  doInsert    Indicates if returned record is stored in DB
 	 * @return             HOG Vent Gas Alert Task record
 	 */
 	public static HOG_Vent_Gas_Alert_Task__c createTask(HOG_Vent_Gas_Alert__c alert, Id assignee1Id, Boolean doInsert) {
 		HOG_Vent_Gas_Alert_Task__c task = createTask();
 		task.Vent_Gas_Alert__c = alert.Id;
 		task.Priority__c = alert.Priority__c;
 		task.Assignee1__c = assignee1Id;
 		if(doInsert) insert task;
 		return task;
 	}

 	/**
 	 * Creates HOG Vent Gas Alert Task record with default values.
 	 * @param  alert        HOG Vent Gas Alert record for which we want to create task
 	 * @param  assignee1Id  Id of user to which will be task assigned
 	 * @param  assignee2Id  Id of second user to which will be task assigned
 	 * @param  assigneeType Assignee type
 	 * @param  doInsert     Indicates if returned record is stored in DB
 	 * @return              HOG Vent Gas Alert Task record
 	 */
 	public static HOG_Vent_Gas_Alert_Task__c createTask(HOG_Vent_Gas_Alert__c alert, 
 														Id assignee1Id, 
 														Id assignee2Id, 
 														String assigneeType, 
 														Boolean doInsert) {
 		HOG_Vent_Gas_Alert_Task__c task = createTask(alert, assignee1Id, false);
 		task.Assignee2__c = assignee2Id;
 		task.Assignee_Type__c = assigneeType;
 		if(doInsert) insert task;
 		return task;
 	}

/*
USER
 */
 	/**
 	 * [createUser description]
 	 * @return [description]
 	 */
	public static User createUser() {
		User user = (User) HOG_SObjectFactory.createSObject(new User());
        Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

        Double random = Math.Random();

        user.Email = 'tony' +  random + '@stark.com.test';
        user.Alias = 'IronMan' ;
        user.LastName = 'Stark';
        user.ProfileId = p.Id;
        user.UserName = 'tony' + random + '@stark.com.unittest';

        insert user;
        return user;
	}

 	/**
 	 * [createUser description]
 	 * @param  firstName [description]
 	 * @param  lastName  [description]
 	 * @param  nick      [description]
 	 * @return           [description]
 	 */
	public static User createUser(String firstName, String lastName, String nick) {
		User user = (User) HOG_SObjectFactory.createSObject(new User());
        Profile p = [Select Id from Profile where Name='Standard HOG - General User'];

        Double random = Math.Random();

        user.Email = firstName + '@' + lastName + '.com.test';
        user.Alias = nick;
        user.LastName = lastName;
        user.ProfileId = p.Id;
        user.UserName = firstName + '@' + lastName + '.com.unittest';

        insert user;
        return user;
	}

 	/**
 	 * [createAdmin description]
 	 * @return [description]
 	 */
	public static User createAdmin() {
		User user = (User) HOG_SObjectFactory.createSObject(new User());
        Profile p = [Select Id from Profile where Name='System Administrator'];

        Double random = Math.Random();

        user.Email = 'nick.fury' +  random + '@shield.com';
        user.Alias = 'eyepatch' ;
        user.LastName = 'Furry';
        user.ProfileId = p.Id;
        user.UserName = 'nick.fury' + random + '@shield.com.avengers';

        insert user;
        return user;
	}

 	/**
 	 * [assignPermissionSet description]
 	 * @param  user                  [description]
 	 * @param  userPermissionSetName [description]
 	 * @return                       [description]
 	 */
	public static User assignPermissionSet(User user, String userPermissionSetName){
        return HOG_TestDataFactory.assignPermissionSet(user, userPermissionSetName);
     }

 	/**
 	 * [assignProfile description]
 	 * @param  user        [description]
 	 * @param  profileName [description]
 	 * @return             [description]
 	 */
    public static User assignProfile (User user, String profileName){
    	return HOG_TestDataFactory.assignProfile(user, profileName);
    }

/**************************************
				DEPRECATED
OLD METHODS - NEXT TIME DON"T USE THEM.
				DEPRECATED
**************************************/

	public static Route__c createRoute(String name) {
        Route__c route = new Route__c(Name = name,
						                Route_Number__c = name,
						                Fluid_Management__c = true);            
        return route;
	}

	 public static Location__c createLocation(String name, Id routeId, Id fieldId){                
        Location__c location = new Location__c(Name = name,
							                Route__c = routeId, 
							                Fluid_Location_Ind__c = true,
							                Operating_Field_AMU__c = fieldId);            
        return location;
    }

	public static HOG_Vent_Gas_Alert__c createAlert(String name, 
													String description,
													String locationId, 
													String productionEngineerId, 
													String priority,
													String status,
													String type) {
		HOG_Vent_Gas_Alert__c alert = new HOG_Vent_Gas_Alert__c(Name = name,
																Description__c = description,
																Start_Date__c = Datetime.now(),
																Production_Engineer__c = productionEngineerId,
																Status__c = status,
																Type__c = type,
																Priority__c = priority,
																Well_Location__c = locationId);
		insert alert;
		return alert;
	}

	public static HOG_Vent_Gas_Alert_Task__c createTask(String alertId, 
														String assignee1Id, 
														String assignee2Id,
														String comments,
														String priority,
														Boolean remind,
														String status, 
														String assigneeType) {
		HOG_Vent_Gas_Alert_Task__c t = new HOG_Vent_Gas_Alert_Task__c(Vent_Gas_Alert__c = alertId,
																	Assignee1__c = assignee1Id,
																	Assignee2__c = assignee2Id,
																	Comments__c = comments,
																	Priority__c = priority,
																	Remind__c = remind,
																	Status__c = status,
																	Assignee_Type__c = assigneeType);
		insert t;
		return t;
	}

}