/**
 * Agile_Initiative_Overview_Controller
 * Test: Agile_Initiative_Overview_ControllerTest
 *
 * Created: 2018/04/16
 * Author: MBrimus
 */
public without sharing class Agile_Initiative_Overview_Controller {
	public AA_Initiative__c record;
	public List<Agile_Initiative_Stage__c> stageList = new List<Agile_Initiative_Stage__c>();
	public Boolean isAAUser {get; private set;}

	public Agile_Initiative_Overview_Controller(ApexPages.StandardController stdController) {
        record = (AA_Initiative__c) stdController.getRecord();
        isAAUser = checkIfUserIsAAUser();
    }

	public List<Agile_Initiative_Stage__c> getStageList(){
	 	stageList = [SELECT Name, Start_Date__c, End_Date__c, Notes__c, Phase__c, Status__c, Chevron__c
		 	FROM Agile_Initiative_Stage__c 
		 	WHERE Agile_Initiative__c =: record.Id];
	    
	    return stageList;

	}

	public void saveEdits() {
	    update stageList;
	}

	public boolean checkIfUserIsAAUser(){
		List<PermissionSetAssignment> psa = [SELECT PermissionSetId 
						FROM PermissionSetAssignment 
						WHERE AssigneeId= :UserInfo.getUserId() 
						AND PermissionSet.Label = 'Agile Accelerator Initiatives'];

		return psa.isEmpty() ? false :  true;
	}
}