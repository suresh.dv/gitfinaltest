/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Service_Time__c
-------------------------------------------------------------------------------------------------*/
@isTest                
public class ServiceTimeTestData
{
    public static HOG_Service_Time__c createServiceTime
    (
		Id workOrderId,
		String serviceDay,
		DateTime onLocation,
		DateTime offLocation
   	)
    {                
        HOG_Service_Time__c results = new HOG_Service_Time__c
            (
                Work_Order__c = workOrderId,
		        Day__c = serviceDay,
		        Rig_On_Location__c = onLocation,
		        Rig_Off_Location__c = offLocation
            );
            
        return results;
    }
}