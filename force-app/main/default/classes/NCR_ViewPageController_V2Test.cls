@isTest(SeeAllData=true)
private class NCR_ViewPageController_V2Test{
    static testMethod void testNCR_ViewPageControllerX_V2(){
        //Create NCR user
        User ncrNormalUser = NCRUserTestData.createNCRAdminUser();
                            
        System.runAs(ncrNormalUser){
            //Create NCR state
            NCR_State__c state1 = NCR_CAR_TestData.createNCRState('State1', 'QA Lead Input to NCR and Assignment', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State1', 'Case', 'Project', true);     
                 
            NCR_State__c state2 = NCR_CAR_TestData.createNCRState('State2', 'Develop & Submit Disposition Plan', 'NCR_Case_Detail,Location,Non_conformance_Details,Acceptance,Disposition_Plan', 
                'State2', 'Case', 'Project', true);
                
            //Create NCR action
            NCR_Action__c action1 = NCR_CAR_TestData.createNCRAction('action1', 'action1', true);        
            
            //Create NCR State action
            NCR_StateAction__c stateAction1 = NCR_CAR_TestData.createNCRStateAction(state1, action1, null, 'AssignNCR', 
                    state2, '', 'Current User', true); 
                    
            System.AssertNotEquals(ncrNormalUser.Id, null);
            System.AssertNotEquals(state1.Id, null);
            System.AssertNotEquals(state2.Id, null);
            System.AssertNotEquals(action1.Id, null);
            System.AssertNotEquals(stateAction1.Id, null);  
            
            PageReference pageRef = Page.NCR_ViewPage;
            Test.setCurrentPageReference(pageRef);
            
            Account account = AccountTestData.createAccount('NCR Vendor Account', null);
            RecordType recordType = [Select Id From RecordType  Where SobjectType='Account' and Name = 'SAP Vendor'];
            account.RecordTypeId = recordType.Id;
            insert account;
            
            System.AssertNotEquals(account.Id, null);
        
            NCR_Case__c  ncrCase = NCR_CAR_TestData.createProjectNCRCase('Business Unit', 'Project Name', account.Id, 'Discipline', 'Problem Type', 'Supplier Name', 'Severity',
                    'Short Description of Non-Conformance', 'Detailed Description of Nonconformance', 'Controlling Document', 'Immediate Actions Taken',
                    'Proposed Disposition', 'Proposed Disposition Plan Detail', null, null, null, 'State1', true);  
            
            System.AssertNotEquals(ncrCase.Id, null);
            
            
            ApexPages.StandardController standardController = new ApexPages.standardController(ncrCase);
            System.AssertNotEquals((sObject)standardController.getRecord(), null);
            
            NCR_ViewPageControllerX_V2 controller = new NCR_ViewPageControllerX_V2(standardController); 
            
            
            System.AssertEquals(controller.NCR_Action1(), null);       
            System.AssertEquals(controller.NCR_Action2(), null);
            System.AssertEquals(controller.NCR_Action3(), null);   
            //System.AssertEquals(controller.NCR_Save(), null);
            System.AssertNotEquals(controller.NCR_AdminEdit(), null); 
            
            System.AssertNotEquals(controller.NCR_NotifyQALead(), null); 
            System.AssertNotEquals(controller.NCR_Pdf(), null);         
            System.AssertNotEquals(controller.getBusinessUnits(), null);         
            System.AssertNotEquals(controller.getProjects(), null);  
            
            System.AssertNotEquals(controller.NCR_Exit(), null); 
            
            Document d = new Document();
            d.name = 'Document Name';
            d.body = Blob.valueOf('Unit Test Document Body');
            
            controller.documentFile = d;
            controller.uploadFile();
            
            controller.selectedAttachmentId = d.id;
            System.AssertNotEquals(controller.selectedAttachmentId, Null);
            
            controller.removeFile();
            
            controller.documentFile = d;
            controller.uploadFile();            
            controller.selectedAttachmentId = d.id;    
            
            controller.NCR_CancelEdit();                                                                                  
        }
                
                                                
    }
}