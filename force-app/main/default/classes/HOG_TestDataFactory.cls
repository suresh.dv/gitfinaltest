/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Related Class:  HOG_SObjectFactory
Description:    FLOC data factory class for Testing purposes. 
History:        jschn 05.14.2018 - Created.
                jschn 06.13.2018 - added createHogSettings
                mbrimus 06.18.2018  - added new AMU method - takes in floc value
                                    - added business unit factory methods
                                    - added new location method - takes in floc and floc category
                                    - added new Well Event factory methods
**************************************************************************************************/
@isTest
public class HOG_TestDataFactory {

    //TODO Well event (missing also feidlDefaults -> HOG_SObjectFactory line: 335)
    //
    private static final String CLASS_NAME = 'HOG_TestDataFactory';
    
    private static final String objectType = 'Dispatch_Desk__c';
    private static final String developerName = 'Fluid_Dispatch';
    private static final RecordType dispatchRecordType {
        get {
            if(dispatchRecordType == null) {
                try {
                    dispatchRecordType = [SELECT Id 
                                        FROM RecordType 
                                        WHERE SobjectType = :objectType 
                                            AND DeveloperName = :developerName];
                } catch(Exception ex) {
                    System.debug(CLASS_NAME + ' -> ERROR: ' + ex);
                }
            }
            return dispatchRecordType;
        }
        private set;
    }
    
    /*
HOG SETTINGS
     */
    /**
     * Create HOG Settings with default values
     * @param  doInsert Indicates if returned record is stored in DB
     * @return          HOG_Settings__c
     */
    public static HOG_Settings__c createHOGSettings(Boolean doInsert) {
        return (HOG_Settings__c) HOG_SObjectFactory.createSObject(new HOG_Settings__c(), doInsert);
    }

    /*
Business Unit 
    */
   /**
     * Creates Business Unit record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Business_Unit__c
     */
    public static Business_Unit__c createBusinessUnit(Boolean doInsert) {
        Business_Unit__c businessUnit = (Business_Unit__c) HOG_SObjectFactory.createSObject(new Business_Unit__c(), doInsert);
        return businessUnit;
    }

    /**
     * Creates Business Unit record with default values.
     * @param  name     String for overriding default Name value
     * @param  floc     String for changing FLOC value
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Business_Unit__c
     */
    public static Business_Unit__c createBusinessUnit(String name, String floc, Boolean doInsert) {
        Business_Unit__c businessUnit = createBusinessUnit(false);
        businessUnit.Name = name;
        businessUnit.Functional_Location__c = floc;
        if(doInsert) insert businessUnit;
        return businessUnit;
    }

    /*
Business Department 
    */
    /**
     * Creates Business Department record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Business_Department__c
     */
    public static Business_Department__c createBusinessDepartment(Boolean doInsert) {
        Business_Department__c department = (Business_Department__c) HOG_SObjectFactory.createSObject(new Business_Department__c(), doInsert);
        return department;
    }

    /**
     * Creates Business Department record with default values.
     * @param  name     String for overriding default Name value
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Business_Department__c
     */
    public static Business_Department__c createBusinessDepartment(String name, Boolean doInsert) {
        Business_Department__c department = createBusinessDepartment(false);
        department.Name = name;
        if(doInsert) insert department;
        return department;
    }
    

    /*
Operating District
    */
    /**
     * Creates Operating District record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Operating_District__c
     */
    public static Operating_District__c createOperatingDistrict(Boolean doInsert) {
        Business_Department__c department = createBusinessDepartment(true);
        Operating_District__c district = createOperatingDistrict(department.Id, doInsert);
        return district;
    }

    /**
     * Creates Operating District record with default values.
     * @param  name     String for overriding default Name value
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Operating_District__c
     */
    public static Operating_District__c createOperatingDistrict(String name, Boolean doInsert) {
        Business_Department__c department = createBusinessDepartment(true);
        Operating_District__c district = createOperatingDistrict(name, department.Id, doInsert);
        return district;
    }

    /**
     * Creates Operating District record with default values.
     * @param  departmentName String for overriding default Name value on Business Department record created within method
     * @param  districtName   String for overriding default Name value on Operating District record created within method
     * @param  doInsert       indicates if returned record is stored in DB
     * @return                Operating_District__c
     */
    public static Operating_District__c createOperatingDistrict(String departmentName, String districtName, Boolean doInsert) {
        Business_Department__c department = createBusinessDepartment(departmentName, true);
        Operating_District__c district = createOperatingDistrict(districtName, department.Id, doInsert);
        return district;
    }

    /**
     * Creates Operating District record with default values.
     * @param  businessDepartmentId Id for assigning related Business Department record.
     * @param  doInsert             indicates if returned record is stored in DB
     * @return                      Operating_District__c
     */
    public static Operating_District__c createOperatingDistrict(Id businessDepartmentId, Boolean doInsert) {
        Operating_District__c district = (Operating_District__c) HOG_SObjectFactory.createSObject(new Operating_District__c());
        district.Business_Department__c = businessDepartmentId;
        if(doInsert) insert district;
        return district;
    }

    /**
     * Creates Operating District record with default values.
     * @param  name                 String for overriding default Name value
     * @param  businessDepartmentId Id for assigning related Business Dperatment record.
     * @param  doInsert             indicates if returned record is stored in DB
     * @return                      Operating_District__c
     */
    public static Operating_District__c createOperatingDistrict(String name, Id businessDepartmentId, Boolean doInsert) {
        Operating_District__c district = createOperatingDistrict(businessDepartmentId, false);
        district.Name = name;
        if(doInsert) insert district;
        return district;
    }

    /**
     * Creates Operating District record with default values.
     * @param  name                 String for overriding default Name value
     * @param  businessUnit         Id of business Unit   
     * @param  floc                 floc string
     * @param  doInsert             indicates if returned record is stored in DB
     * @return                      Operating_District__c
     */
    public static Operating_District__c createOperatingDistrict(String name, Id businessUnit, String floc, Boolean doInsert) {
        Business_Department__c department = createBusinessDepartment('TestDep', true);
        Operating_District__c district = createOperatingDistrict(department.Id, false);
        district.Name = name;
        district.Functional_Location__c = floc;
        district.Business_Unit__c = businessUnit;
        if(doInsert) insert district;
        return district;
    }


    /*
AMU
    */
   /**
     * Creates AMU(Field) record with default values.
     * @param  districtId Id for assigning related Operating District record.
     * @param  doInsert   indicates if returned record is stored in DB
     * @return            Field__c
     */
    public static Field__c createAMU(Id districtId, Boolean doInsert) {
        Field__c amu = (Field__c) HOG_SObjectFactory.createSObject(new Field__c());
        amu.Operating_District__c = districtId;
        if(doInsert) insert amu;
        return amu;      
    }

   
    /**
     * Creates AMU(Field) record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Field__c
     */
    public static Field__c createAMU(Boolean doInsert) {
        Operating_District__c district = createOperatingDistrict(true);
        Field__c amu = createAMU(district.Id, doInsert);
        return amu;     
    }

    /**
     * Creates AMU(Field) record with default values.
     * @param  name     String for overriding default Name value
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Field__c
     */
    public static Field__c createAMU(String name, Boolean doInsert) {
        Operating_District__c district = createOperatingDistrict(true);
        Field__c amu = createAMU(district.Id, false);
        amu.Name = name;
        if(doInsert) insert amu;
        return amu;     
    }
    /**
     * Creates AMU(Field) record with default values.
     * @param  districtName String for overriding default Name value on Operating District record created within method
     * @param  amuName      String for overriding default Name value on Field record created within method
     * @param  doInsert     indicates if returned record is stored in DB
     * @return              Field__c
     */
    public static Field__c createAMU(String districtName, String amuName, Boolean doInsert) {
        Operating_District__c district = createOperatingDistrict(districtName, true);
        Field__c amu = createAMU(amuName, district.Id, doInsert);
        return amu;     
    }

    /**
     * Creates AMU(Field) record with default values.
     * @param  businessDepartmentName String for overriding default Name value on Business Department record created within method
     * @param  districtName           String for overriding default Name value on Operating District record created within method
     * @param  amuName                String for overriding default Name value on Field record created within method
     * @param  doInsert indicates if returned record is stored in DB
     * @return                        Field__c
     */
    public static Field__c createAMU(String businessDepartmentName, String districtName, String amuName, Boolean doInsert) {
        Operating_District__c district = createOperatingDistrict(businessDepartmentName, districtName, true);
        Field__c amu = createAMU(amuName, district.Id, doInsert);
        return amu;     
    }

    /**
     * Creates AMU(Field) record with default values.
     * @param  name       String for overriding default Name value
     * @param  districtId Id for assigning related Operating District record.
     * @param  doInsert   indicates if returned record is stored in DB
     * @return            Field__c
     */
    public static Field__c createAMU(String name, Id districtId, Boolean doInsert) {
        Field__c amu = createAMU(districtId, false);
        amu.Name = name;
        if(doInsert) insert amu;
        return amu;      
    }

    /**
     * Creates AMU(Field) record with default values.
     * @param  plannerGroup Planner_Group__c
     * @param  recordType   RecordType
     * @param  districtId   Id for assigning related Operating District record.
     * @param  doInsert     Planner_Group__c
     * @return              Field__c
     */
    public static Field__c createAMU(String plannerGroup, Id districtId, Id recordType, Boolean doInsert) {
        Field__c amu = createAMU(districtId, false);
        amu.Planner_Group__c = plannerGroup;
        amu.RecordTypeId = recordType;
        if(doInsert) insert amu;
        return amu;      
    }

    /**
     * Creates AMU(Field) record with default values.
     * @param  plannerGroup Planner_Group__c
     * @param  recordType   RecordType
     * @param  districtId   Id for assigning related Operating District record.
     * @param  floc         Floc value of AMU
     * @param  doInsert     Planner_Group__c
     * @return              Field__c
     */
    public static Field__c createAMU(String plannerGroup, Id districtId, Id recordType, String floc, Boolean doInsert) {
        Field__c amu = createAMU(plannerGroup, districtId, recordType, false);
        amu.Functional_Location__c = floc;
        if(doInsert) insert amu;
        return amu;      
    }

    /*
ROUTE
    */
    /**
     * Creates Route record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Route__c
     */
    public static Route__c createRoute(Boolean doInsert) {
        Route__c route = (Route__c) HOG_SObjectFactory.createSObject(new Route__c());
        if(doInsert) insert route;
        return route;
    }

    /**
     * Creates Route record with default values.
     * @param  routeName String for overriding default Name value on Route record created within method
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Route__c
     */
    public static Route__c createRoute(String routeName, Boolean doInsert) {
        Route__c route = createRoute(false);
        route.Name = routeName;
        route.Route_Number__c = routeName; 
        if(doInsert) insert route;
        return route;
    }

    /**
     * Creates Route record with default values.
     * @param  routeName            String for overriding default Name value on Route record created within method
     * @param  fluidManagementRoute Boolean for overriding default Fluid Managementflag on Route record
     * @param  doInsert             indicates if returned record is stored in DB
     * @return                      Route__c
     */
    public static Route__c createRoute(String routeName, Boolean fluidManagementRoute, Boolean doInsert) {
        Route__c route = createRoute(routeName, false);
        route.Fluid_Management__c = fluidManagementRoute;
        if(doInsert) insert route;
        return route;
    }

    /**
     * Creates Operator on call record with default values.
     * @param  routeId    Id for assigning related Route record.
     * @param  operatorId Id for assigning related Operator(User) record.
     * @return            Operator_On_Call__c
     */
    public static Operator_On_Call__c createOperatorOnCall(String routeId,
                                                            String operatorId) {
        Operator_On_Call__c ooc = new Operator_On_Call__c(Operator_Route__c = routeId,
                                                        Operator__c = operatorId);
        insert ooc;
        return ooc;
    }

    /*
DISPATCH DESK
    */
   /**
    * Create Dispatch Desk record with default values.
    * It fails if correct record type is not found.
    * @param  doInsert indicates if returned record is stored in DB
    * @return          Dispatch_Desk__c
    */
    public static Dispatch_Desk__c createDispatchDesk(Boolean doInsert) {
        Dispatch_Desk__c dispatchDesk = (Dispatch_Desk__c) HOG_SObjectFactory.createSObject(new Dispatch_Desk__c());
        if(dispatchRecordType != null) {
            dispatchDesk.RecordTypeId = dispatchRecordType.Id;
            if(doInsert) insert dispatchDesk;
        }
        return dispatchDesk;
    }

    /*
LOCATION
    */
   
    /**
     * Creates Location record with default values.
     * @param  routeId  Id for assigning related Route record.
     * @param  fieldId  Id for assigning related Field record.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Location__c
     */
    public static Location__c createLocation(Id routeId, Id fieldId, Boolean doInsert) {
        Location__c location = (Location__c) HOG_SObjectFactory.createSObject(new Location__c());
        location.Route__c = routeId;
        location.Operating_Field_AMU__c = fieldId;
        if(doInsert) insert location;
        return location;
    }

    
    /**
     * Creates Location record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Location__c
     */
    public static Location__c createLocation(Boolean doInsert) {
        Route__c route = createRoute(true);
        Field__c amu = createAMU(true);
        Location__c location = createLocation(route.Id, amu.Id, doInsert);
        return location;
    }

    /**
     * Creates Location record with default values.
     * @param  routeName Id for assigning related Route record.
     * @param  fieldId   Id for assigning related Field record.
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Location__c
     */
    public static Location__c createLocation(String routeName, Id fieldId, Boolean doInsert) {
        Route__c route = createRoute(routeName, true);
        Location__c location = createLocation(route.Id, fieldId, doInsert);
        return location;
    }

    /**
     * Creates Location record with default values.
     * @param  routeId   Id for assigning related Route record.
     * @param  fieldName String for overriding default Name value on Field record created within method
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Location__c
     */
    public static Location__c createLocation(Id routeId, String fieldName, Boolean doInsert) {
        Field__c amu = createAMU(fieldName, true);
        Location__c location = createLocation(routeId, amu.Id, doInsert);
        return location;
    }

    /**
     * Creates Location record with default values.
     * @param  name     String for overriding default Name value
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Location__c
     */
    public static Location__c createLocation(String name, Boolean doInsert) {
        Route__c route = createRoute(true);
        Field__c amu = createAMU(true);
        Location__c location = createLocation(name, route.Id, amu.Id, doInsert);
        return location;
    }

    /**
     * Creates Location record with default values.
     * @param  name     String for overriding default Name value
     * @param  routeId  Id for assigning related Route record.
     * @param  fieldId  Id for assigning related Field record.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Location__c
     */
    public static Location__c createLocation(String name, Id routeId, Id fieldId, Boolean doInsert) {
        Location__c location = createLocation(routeId, fieldId, false);
        location.Name = name;
        location.Location_Name_for_Fluid__c = name;
        if(doInsert) insert location;
        return location;
    }

    /**
     * Creates Location record with default values.
     * @param  name     String for overriding default Name value
     * @param  routeId  Id for assigning related Route record.
     * @param  fieldId  Id for assigning related Field record.
     * @param  floc     floc String
     * @param  flocCategory  floc cateogory - should be 4 but can be null for testing
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Location__c
     */
    public static Location__c createLocation(String name, Id fieldId, String floc, Integer flocCategory, Boolean doInsert) {
        Location__c location = createLocation('testRoute', fieldId, false);
        location.Name = name;
        location.Functional_Location_Description_SAP__c = floc;
        location.Functional_Location_Category__c = flocCategory;
        if(doInsert) insert location;
        return location;
    }

    /*
FACILITY
    */
    /**
     * Creates Facility record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Facility__c
     */
    public static Facility__c createFacility(Boolean doInsert) {
        Route__c route = createRoute(true);
        Field__c amu = createAMU(true);
        Facility__c facility = createFacility(route.Id, amu.Id, doInsert);
        return facility;
    }

    /**
     * Creates Facility record with default values.
     * @param  routeName String for overriding default Name value on Route created within method
     * @param  fieldId   Id for assigning related Field record.
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Facility__c
     */
    public static Facility__c createFacility(String routeName, Id fieldId, Boolean doInsert) {
        Route__c route = createRoute(routeName, true);
        Facility__c facility = createFacility(route.Id, fieldId, doInsert);
        return facility;
    }

    /**
     * Creates Facility record with default values.
     * @param  routeId   Id for assigning related Route record.
     * @param  fieldName String for overriding default Name value on Field created within method
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Facility__c
     */
    public static Facility__c createFacility(Id routeId, String fieldName, Boolean doInsert) {
        Field__c amu = createAMU(fieldName, true);
        Facility__c facility = createFacility(routeId, amu.Id, doInsert);
        return facility;
    }

    /**
     * Creates Facility record with default values.
     * @param  routeId  Id for assigning related Route record.
     * @param  fieldId  Id for assigning related Field record.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Facility__c
     */
    public static Facility__c createFacility(Id routeId, Id fieldId, Boolean doInsert) {
        Facility__c facility = (Facility__c) HOG_SObjectFactory.createSObject(new Facility__c());
        facility.Operating_Field_AMU__c = fieldId;
        facility.Plant_Section__c = routeId;
        if(doInsert) insert facility;
        return facility;
    }

    /**
     * Creates Facility record with default values.
     * @param  name     String for overriding default Name value
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Facility__c
     */
    public static Facility__c createFacility(String name, Boolean doInsert) {
        Route__c route = createRoute(true);
        Field__c amu = createAMU(true);
        Facility__c facility = createFacility(name, route.Id, amu.Id, doInsert);
        return facility;
    }

    /**
     * Creates Facility record with default values.
     * @param  name     String for overriding default Name value
     * @param  routeId  Id for assigning related Route record.
     * @param  fieldId  Id for assigning related Field record.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Facility__c
     */
    public static Facility__c createFacility(String name, Id routeId, Id fieldId, Boolean doInsert) {
        Facility__c facility = createFacility(routeId, fieldId, false);
        facility.Name = name;
        facility.Facility_Name_for_Fluid__c = name;
        if(doInsert) insert facility;
        return facility;
    }

   /*
WELL EVENT
    */
    private static Well_Event__c createWellEvent() {
        return (Well_Event__c) HOG_SObjectFactory.createSObject(new Well_Event__c());
    } 

    /**
     * Creates Facility record with default values.
     * @param  locationId parent location Id - required
     * @param  floc       floc value
     * @param  flocCategory flocCategory value - can be null for testing
     * @param  doInsert   indicates if returned record is stored in DB
     * @return            Well_Event__c
     */
    public static Well_Event__c createWellEvent(Id locationId, String floc, Integer flocCategory, Boolean doInsert) {
        Well_Event__c event = createWellEvent();
        event.Well_ID__c = locationId;
        event.Functional_Location_Description_SAP__c = floc;
        event.Functional_Location_Category__c = flocCategory;
        if(doInsert) insert event;
        return event;
    }  

    /*
EQUIPMENT
    */
    public static Equipment__c createEquipment() {
        return (Equipment__c) HOG_SObjectFactory.createSObject(new Equipment__c());
    }

    public static Equipment__c createEquipment(Location__c loc, Boolean doInsert) {
        Equipment__c equipment = createEquipment();
        equipment.Location__c = loc.Id;
        if(doInsert) insert equipment;
        return equipment;
    }

    public static Equipment__c createEquipment(Facility__c fac, Boolean doInsert) {
        Equipment__c equipment = createEquipment();
        equipment.Facility__c = fac.Id;
        if(doInsert) insert equipment;
        return equipment;
    }

    /**
     * Creates Tank Equipment record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Equipment_Tank__c
     */
    public static Equipment_Tank__c createEquipmentTank(Location__c loc, Boolean doInsert) {
        Equipment__c eq = createEquipment(loc, true);
        Equipment_Tank__c eqTank = (Equipment_Tank__c) HOG_SObjectFactory.createSObject(new Equipment_Tank__c());
        eqTank.Equipment__c = eq.Id;
        if(doInsert) insert eqTank;
        return eqTank;
    }

    /**
     * Creates Tank Equipment record with default values.
     * @param  doInsert indicates if returned record is stored in DB
     * @return          Equipment_Tank__c
     */
    public static Equipment_Tank__c createEquipmentTank(Facility__c fac, Boolean doInsert) {
        Equipment__c eq = createEquipment(fac, true);
        Equipment_Tank__c eqTank = (Equipment_Tank__c) HOG_SObjectFactory.createSObject(new Equipment_Tank__c());
        eqTank.Equipment__c = eq.Id;
        if(doInsert) insert eqTank;
        return eqTank;
    }

    /**
     * Creates Tank Equipment record with default values.
     * @param  tankLevel Integer for overriding default Tank Level value
     * @param  lowLevel  Integer for overriding default Low Level value
     * @param  tankSize  Integer for overriding default Tank Size value
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Equipment_Tank__c
     */
    public static Equipment_Tank__c createEquipmentTank(Facility__c fac, Integer tankLevel, Integer lowLevel, Integer tankSize, Boolean doInsert) {
        Equipment_Tank__c eqTank = createEquipmentTank(fac, false);
        eqTank.SCADA_Tank_Level__c = tankLevel;
        eqTank.Low_Level__c = lowLevel;
        eqTank.Tank_Size_m3__c = tankSize;
        if(doInsert) insert eqTank;
        return eqTank;
    }

    /**
     * Creates Tank Equipment record with default values.
     * @param  tankLevel Integer for overriding default Tank Level value
     * @param  lowLevel  Integer for overriding default Low Level value
     * @param  tankSize  Integer for overriding default Tank Size value
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Equipment_Tank__c
     */
    public static Equipment_Tank__c createEquipmentTank(Location__c loc, Integer tankLevel, Integer lowLevel, Integer tankSize, Boolean doInsert) {
        Equipment_Tank__c eqTank = createEquipmentTank(loc, false);
        eqTank.SCADA_Tank_Level__c = tankLevel;
        eqTank.Low_Level__c = lowLevel;
        eqTank.Tank_Size_m3__c = tankSize;
        if(doInsert) insert eqTank;
        return eqTank;
    }

    /**
     * Creates Tank Equipment record with default values.
     * @param  tankLevel Integer for overriding default Tank Level value
     * @param  lowLevel  Integer for overriding default Low Level value
     * @param  tankSize  Integer for overriding default Tank Size value
     * @param  tankLabel String for overriding default Tank Label value
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Equipment_Tank__c
     */
    public static Equipment_Tank__c createEquipmentTank(Location__c loc, Integer tankLevel, Integer lowLevel, Integer tankSize, String tankLabel, Boolean doInsert) {
        Equipment_Tank__c eqTank = createEquipmentTank(loc, tankLevel, lowLevel, tankSize, false);
        eqTank.Tank_Label__c = tankLabel;
        if(doInsert) insert eqTank;
        return eqTank;
    }

    /**
     * Creates Tank Equipment record with default values.
     * @param  tankLevel Integer for overriding default Tank Level value
     * @param  lowLevel  Integer for overriding default Low Level value
     * @param  tankSize  Integer for overriding default Tank Size value
     * @param  tankLabel String for overriding default Tank Label value
     * @param  doInsert  indicates if returned record is stored in DB
     * @return           Equipment_Tank__c
     */
    public static Equipment_Tank__c createEquipmentTank(Facility__c fac, Integer tankLevel, Integer lowLevel, Integer tankSize, String tankLabel, Boolean doInsert) {
        Equipment_Tank__c eqTank = createEquipmentTank(fac, tankLevel, lowLevel, tankSize, false);
        eqTank.Tank_Label__c = tankLabel;
        if(doInsert) insert eqTank;
        return eqTank;
    }


    /*
USER
    */
    /**
     * Creates User record with default values.
     * @param  firstName String for overriding default First Name value
     * @param  lastName  String for overriding default Last Name value
     * @param  nick      String for overriding default Nick value
     * @return           User
     */
    public static User createUser(String firstName, String lastName, String nick) {
        User user = (User) HOG_SObjectFactory.createSObject(new User());

        user.Email = firstName + '@' + lastName + '.com.test';
        user.Alias = nick;
        user.LastName = lastName;
        user.UserName = firstName + '@' + lastName + '.com.unittest';

        insert user;
        return user;
    }

    /**
     * Creates User with Admin profile record with default values.
     * @return User
     */
    public static User createAdmin() {
        User user = (User) HOG_SObjectFactory.createSObject(new User());
        Profile p = [Select Id from Profile where Name='System Administrator'];

        user.Email = 'tony.stark@shield.com.avengers';
        user.Alias = 'IronMan';
        user.LastName = 'stark';
        user.ProfileId = p.Id;
        user.UserName = 'tony.stark@shield.com.avengers';

        insert user;
        return user;
    }

    /**
     * Creates PermissionSetAssignment record for user with desired permission set.
     * @param  user                  User for assignment
     * @param  userPermissionSetName PermissionSet Name for User to assign
     * @return                       User
     */
    public static User assignPermissionSet(User user, String userPermissionSetName){
         PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name =: userPermissionSetName];

         PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
         permissionSetAssignment.PermissionSetId = permissionSet.Id;
         permissionSetAssignment.AssigneeId = user.Id;

         insert permissionSetAssignment;

         return user;
     }

    /**
     * Assign desired profile to user.
     * @param  user        User for assignment
     * @param  profileName Profile name for User to assign
     * @return             User
     */
    public static User assignProfile (User user, String profileName){
        Profile profile = [SELECT Id FROM Profile WHERE Name =: profileName];
        user.ProfileId = profile.Id;
        update user;
        return user;
    }

}