/****
*Class       -----     RaM_UtilityHolder
*Author      -----     Accenture
*Date        -----     23/1/2018
*Description -----     This class will hold the utility functions that will log error details if it accour.
***/
public class RaM_UtilityHolder{
    // declare the variables
   static List<Error_Log__c> lstErrorLog = new List<Error_Log__c>();
   static Error_Log__c objErrorLog;
    
    /*
    * method name - errorMethod
    * arguments   - String objectName, String className, String methodName, String sSeverity, String sErrorMsg
    * return type - void
    * description - This is the generic method that can create ERROR records in the Error Log
    *               when an error has occurred while making any DML operation.
    */
    public static void errorMethod(String objectName, String className, String methodName, String sSeverity, String sErrorMsg){
        // clear the list
        lstErrorLog.clear();
        // ++ if the issue is of type Error
        if(null != sErrorMsg && sErrorMsg != ''){
            objErrorLog = new Error_Log__c();
            // map the values to the field and put that in the list that will be inserted later
            objErrorLog.Object_Name__c = objectName;
            objErrorLog.Apex_Class_Trigger__c = className;
            objErrorLog.Method_Name__c = methodName;
            objErrorLog.Description__c = sErrorMsg.length()>32768 ? sErrorMsg.substring(4,32768): sErrorMsg;
            objErrorLog.Severity__c = sSeverity;
            objErrorLog.type__c = 'Error';
            lstErrorLog.add(objErrorLog);
                
        }
        // check if the size of lstErrorLog is greater than zero. This will happen
        // if a record has failed while causing the DML operation.
        if(null != lstErrorLog && lstErrorLog.size()>0){
            insert lstErrorLog;
        }
    }
    
    /*
    * method name - errorMethod
    * arguments   - String objectName, String className, String methodName, String sSeverity, Exception e
    * return type - String sMessage
    * description - This is the generic method that can create EXCEPTION records in the Error Log
    *               when an esxception has occurred while making any operation.
    */    
  /*  public static String errorMethod(String objectName, String className, String methodName, String sSeverity, Exception ex){
       // Map<String, Error_Messages_Setting__c> errorMessageSettings = Error_Messages_Setting__c.getAll();
        // clear the list
        lstErrorLog.clear();
        // get the message for the occured Exception
        //String sMessage = errorMessageSettings.get(ex.getTypeName()).value__c;
        String sMessage=ex.getMessage();//.substringAfter(':');
        // ++ if the issue is of type Exception
        if(null != ex){
            objErrorLog = new Error_Log__c();
            // map the values to the field and put that in the list that will be inserted later
            objErrorLog.Object_Name__c = objectName;
            objErrorLog.Apex_Class_Trigger__c = className;
            objErrorLog.Method_Name__c = methodName;
            objErrorLog.Description__c = String.valueOf(ex);
            objErrorLog.Severity__c = sSeverity;
            objErrorLog.type__c = 'Exception';
            lstErrorLog.add(objErrorLog);
        }
        // check if the size of lstErrorLog is greater than zero. This will happen
        // if a record has failed while causing the DML operation.
        if(null != lstErrorLog && lstErrorLog.size()>0){
            insert lstErrorLog;
        }
        return sMessage;
    }*/

}