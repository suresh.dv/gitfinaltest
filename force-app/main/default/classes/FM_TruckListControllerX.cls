/*-------------------------------------------------------------------
Author      : Gangha Kaliyan
Company     : Husky
Description : Truck List page for view, edit, assign trips, submit, print PDF functionality
Test Class  : FM_TruckList_Test
Date        : 3-Feb-2015
OBSOLETE CLASS, NOT USED AFTER TRUCK LIST REMOVAL, @isTest to skip unit tests
----------------------------------------------------------------------*/
@isTest
public class FM_TruckListControllerX{
    private Map<Id, TruckTripWrapper> mapWrappers;
    public List<TruckTripWrapper> lWrapperList {get; set;}

    public FM_Truck_List__c truckList {get;set;}
    public FM_Truck_List__c oLoadData {get; private set;}
    public FM_Truck_List_Comments__c oDispatchComments {get; private set;}

    //used for Truck List pdf page
    public List<FM_Truck_Trip__c> truckTripList {get;set;}

    public String truckListId {get;set;}
    public String accessLevel {get;set;}
    public Boolean readOnlyMode {get; private set;}

    ApexPages.StandardController controller;
    public String sDeskSelection {get; set;}
    public String sDispatcherSelected {get; set;}

    private String sGroupingFinal;
    private String sGroupingSelection;

    public FM_TruckListControllerX(ApexPages.StandardController stdController)
    {
        truckListId = ApexPages.currentPage().getParameters().get('id');
        controller = stdController;

        truckList = (FM_Truck_List__c)controller.getRecord();
        this.sDeskSelection = 'PrefillValueRequired';
//        readOnlyMode = (truckList.Truck_List_Status__c != FM_Utilities.TRUCKLIST_STATUS_BOOKED) ||
//            (truckList.Run_Sheet_Date__c < System.today());

        //getUserAccessLevel();

        readOnlyMode = (truckList.Truck_List_Status__c != FM_Utilities.TRUCKLIST_STATUS_BOOKED) ||
            (truckList.Run_Sheet_Date__c != System.today() && (!FM_Utilities.canEditTruckList(truckList.Run_Sheet_Date__c) || truckList.Shift__c != 'Day'));

        retrieveTruckTrips();
        UpdateControllerRecord();
        UpdateDispatchComments();

        if (truckList.Truck_List_Status__c == FM_Utilities.TRUCKLIST_STATUS_CARRYOVER ||
            truckList.Truck_List_Status__c == FM_Utilities.TRUCKLIST_STATUS_CANCELLED ||
            truckList.Truck_List_Status__c == FM_Utilities.TRUCKLIST_STATUS_DISPATCHED)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, truckList.Truck_List_Status__c +
                ' truck lists are not editable.'));
            return;
        }

//        if (truckList.Run_Sheet_Date__c < System.today())
//        if (readOnlyMode)
        // if the run sheet date is not today, it might still be editable if the run sheet date is yesterday AND the shift is Day.
        // But if the run sheet date is yesterday AND the shift is not Day, then it's considered past truck lists.
        if ((truckList.Run_Sheet_Date__c != System.today() && (!FM_Utilities.canEditTruckList(truckList.Run_Sheet_Date__c) || truckList.Shift__c != 'Day')))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
                'Past truck lists are not editable.'));
            return;
        }

        if (truckList.Truck_List_Status__c == FM_Utilities.TRUCKLIST_STATUS_PENDING)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                'There are missing information in this Truck List.  Please first use Dispatcher View to enter the missing information.'));
            return;
        }
    }

    public PageReference UpdateControllerRecord()
    {
        List<FM_Truck_List__c> lTruckList = [SELECT Final_Grouping__c, Phone_Lookup__c FROM FM_Truck_List__c WHERE Id = :this.truckList.Id LIMIT 1];
        FM_Truck_List__c oOneList = (lTruckList.size() == 1) ? lTruckList[0] : null;

        this.sGroupingFinal = (oOneList != null) ? oOneList.Final_Grouping__c : '';

        if(this.sDeskSelection == 'PrefillValueRequired')
        {
            this.sDeskSelection = (oOneList == null) ? '' : oOneList.Phone_Lookup__c;
        }

        List<Dispatch_Desk__c> lDispatchData = [SELECT Grouping__c FROM Dispatch_Desk__c WHERE Id = :this.sDeskSelection LIMIT 1];
        Dispatch_Desk__c oOneDesk = (lDispatchData.size() == 1) ? lDispatchData[0] : null;

        this.sGroupingSelection =(oOneDesk != null) ? oOneDesk.Grouping__c : '';

        UpdateDispatchComments();
        return null;
    }

    public Boolean canDispatch
    {
        // KK 2015-07-07 Defect #111 only allow Dispatch under certain conditions.  Do not allow dispatch again after it's been dispatched.
        // a truck list can be dispatched if all these conditions below are true
        // 1) the status is Booked
        // 2) either a) the run sheet date is today, OR
        //           b) the run sheet date is yesterday AND it's within the grace period AND the shift is Day.
        // 3) there is at least one truck trips for this truck list
        get
        {
            canDispatch = (truckList.Truck_List_Status__c == FM_Utilities.TRUCKLIST_STATUS_BOOKED);
            canDispatch = canDispatch && (truckList.Run_Sheet_Date__c == System.today() ||
                (truckList.Run_Sheet_Date__c == System.today() - 1 && FM_Utilities.canEditTruckList(truckList.Run_Sheet_Date__c) && truckList.Shift__c == 'Day'));
            canDispatch = canDispatch && (truckTripList.size() > 0);
            return canDispatch;
        }
        private set;
    }

    public String getBaseURL()
    {
        return URL.getSalesforceBaseUrl().toExternalForm();
    }

    public void retrieveTruckTrips()
    {
        this.mapWrappers = new Map<Id, TruckTripWrapper>();
        this.lWrapperList = new List<TruckTripWrapper>();
        TruckTripWrapper oOneWrapper;

        //if there is a truck list proceed further - otherwise do nothing
        if(truckList != null)
        {
            //get truck trips for this truck list
            // Act_Tank_Level__c,
            truckTripList = [SELECT Id, Name, Carrier__c, Unit__c, Shift__c, Facility_Lookup__c, Facility__c, Truck_List_Lookup__c, Well__c,
                                Product__c, Standing_Comments__c, Truck_Trip_Status__c, Axle__c, Unit_Configuration__c, Priority__c, Order__c,
                                Run_Sheet_Date__c, Run_Sheet_Lookup__r.Well__c, Run_Sheet_Lookup__r.Well__r.Name, Facility_Type__c,
                                Run_Sheet_Lookup__r.Tank_Low_Level__c, Run_Sheet_Lookup__r.Act_Flow_Rate__c, Destination_Id__c,
                                Run_Sheet_Lookup__r.Act_Tank_Level__c, Run_Sheet_Lookup__r.Flowline_Volume__c, Location_Lookup__c,
                                Run_Sheet_Lookup__r.Route__c, Run_Sheet_Lookup__r.Sour__c, Run_Sheet_Lookup__r.Tank__c,
                                Run_Sheet_Lookup__r.Well__r.Surface_Location__c, Load_Type__c, Load_Type_Formula__c, Truck_List_Lookup__r.Phone_Lookup__c, Truck_List_Lookup__r.Phone_Lookup__r.Grouping__c, Truck_List_Lookup__r.Dispatched_By__r.Id,
                                Run_Sheet_Lookup__r.Well__r.Route__r.Dispatch_Desk__c, Run_Sheet_Lookup__r.Well__r.Route__r.Dispatch_Desk__r.Id,
                                Run_Sheet_Lookup__r.RM_County__c, //W-000407  19.5.2016
                                Run_Sheet_Lookup__r.Equipment_Tank__r.SCADA_Tank_Level__c, Run_Sheet_Lookup__r.Equipment_Tank__r.Latest_Tank_Reading_Date__c
                                FROM FM_Truck_Trip__c
                                WHERE Truck_List_Lookup__c =: truckList.Id
                                ORDER BY Order__c];

            this.sGroupingSelection = (truckTripList.size() == 0) ? '' : truckTripList[0].Truck_List_Lookup__r.Phone_Lookup__r.Grouping__c;
            this.sDispatcherSelected = (truckTripList.size() == 0) ? '' : truckTripList[0].Truck_List_Lookup__r.Dispatched_By__r.Id;

            // * ADDED BY GM 2015-08-11 FOR ALM #128
            List<FM_Truck_List__c> lLoadData = [SELECT Id FROM FM_Truck_List__c WHERE Id = :truckList.Id]; //Oil_Loads__c, Total_Loads__c, Water_Loads__c
            oLoadData = (lLoadData.size() != 0) ? lLoadData[0] : null;

            // ADD TO WRAPPER LIST

            for(FM_Truck_Trip__c oOneTripRecord : truckTripList)
            {
                oOneWrapper = new TruckTripWrapper(oOneTripRecord);

                this.lWrapperList.add(oOneWrapper);
                this.mapWrappers.put(oOneTripRecord.Id, oOneWrapper);
            }
        }
        if (truckTripList.size() == 0)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'This truck list has no loads and has therefore' +
                ' been deleted.  Please go back to "Truck List" tab.'));
            return;

        }
    }

    private void UpdateDispatchComments()
    {
        List<FM_Truck_List_Comments__c> truckListCommentsList;
        String sGroupingToUse = (truckList.Truck_List_Status__c == FM_Utilities.TRUCKLIST_STATUS_DISPATCHED) ? this.sGroupingFinal : this.sGroupingSelection;

        truckListCommentsList = (this.sGroupingFinal != '' && this.sGroupingFinal != null) ? [SELECT Id, Dispatch_Comments__c FROM FM_Truck_List_Comments__c WHERE Run_Sheet_Date__c <= :this.truckList.Run_Sheet_Date__c AND Dispatch_Desk__c = :sGroupingToUse AND Dispatch_Desk__c != '' AND Dispatch_Desk__c != null ORDER BY CreatedDate DESC, Dispatch_Desk__c DESC NULLS LAST LIMIT 1] : [SELECT Id, Dispatch_Comments__c FROM FM_Truck_List_Comments__c WHERE Run_Sheet_Date__c <= :this.truckList.Run_Sheet_Date__c AND Dispatch_Desk__c = null ORDER BY CreatedDate DESC, Dispatch_Desk__c DESC NULLS LAST LIMIT 1];

        // There should only one comment per day.
        this.oDispatchComments = (truckListCommentsList.size() > 0) ? truckListCommentsList[0] : null;
    }

    public PageReference AssignOrder()
    {
        if (SaveTruckList())
        {
            Integer orderIndex = 1;
            List<FM_Truck_Trip__c> orderTruckTripList = [SELECT Order__c FROM FM_Truck_Trip__c
                                                            Where Truck_List_Lookup__c =: truckList.Id
                                                            ORDER BY Priority__c];

            for (FM_Truck_Trip__c truckTrip : orderTruckTripList)
            {
                truckTrip.Order__c = orderIndex++;
            }
            try
            {
                Database.update(orderTruckTripList);
            }
            catch (DMLException e)
            {
                FM_Utilities.logError(e);
            }
            retrieveTruckTrips();
        }
        return null;
    }

    private Boolean isDispatchPhoneSelected()
    {
        if (truckList.Phone_Lookup__c == null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                'Please select a value for Phone.'));
            return false;
        }
        return true;
    }

    public PageReference SubmitTruckList()
    {
        System.debug('SubmitTruckList:' );
        // * ADDED BY GM 2015-07-27 CHECK TO SEE IF TRUCK LIST CAN BE MODIFIED FIRST * //
        List<FM_Truck_List__c> lTruckList = [SELECT Id FROM FM_Truck_List__c WHERE Id = :truckListId AND Truck_List_Status__c <> 'Dispatched' LIMIT 1];
        Boolean bValid = true;

        for(FM_Truck_Trip__c oOneTrip : truckTripList)
        {
            System.debug('oOneTrip.Location_Lookup__c: ' + oOneTrip.Location_Lookup__c + ' Well__c: ' + oOneTrip.Well__c);
            if(oOneTrip.Order__c == null)
            {
                bValid = false;
            }
        }

        if(bValid == false)
        {
            truckList.addError('Please fill out the order number for each Truck Trip.');
            return null;
        }

        if(lTruckList.size() != 1)
        {
            return PageRedirect('/apex/FM_TruckListEdit?id=' + truckListId, true);
        }

        String unitEmail;
        if (!isDispatchPhoneSelected())
        {
            return null;
        }


        //Boolean truckListSaved = SaveTruckList();
        //if (!truckListSaved){
        //    return;
        //}

        unitEmail = truckList.Unit__r.Unit_Email__c;
        if (unitEmail == null || unitEmail == '')
        {
            unitEmail = [SELECT Unit_Email__c FROM Carrier_Unit__c WHERE Id = :truckList.Unit__c].Unit_Email__c;
            if (unitEmail == null || unitEmail == '')
            {
                //ApexPages.Message emailErrorMsg = new ApexPages.Message(ApexPages.Severity.ERROR,
                //  'The Unit does not have an assigned email address.  Please contact your Administrator.');
                //ApexPages.addMessage(emailErrorMsg);
                truckList.addError('The Unit does not have an assigned email address.  Please contact your Administrator.');
                return null;
            }
        }

        // If the code can execute right here, that means email was not a problem.
        // Set the status of the truck trip and all the truck lists for the truck trip to Dispatched
        truckList.Truck_List_Status__c = FM_Utilities.TRUCKLIST_STATUS_DISPATCHED;
        for (FM_Truck_Trip__c truckTrip : truckTripList)
        {
            truckTrip.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED;
            System.debug('Location Lookup:: '+truckTrip.Location_Lookup__c + ' Facility Lookup:: ' +truckTrip.Facility_Lookup__c + ' Well__c:: ' +truckTrip.Well__c); 
        }

        truckList.Dispatched_By__c = (this.sDispatcherSelected == null || this.sDispatcherSelected == '') ? UserInfo.getUserId() : this.sDispatcherSelected;
        truckList.Dispatched_On__c = System.now();
        truckList.Final_Grouping__c = this.sGroupingSelection;
        truckList.Phone_Lookup__c = this.sDeskSelection;

        try
        {
            Database.update(truckList);
            Database.update(truckTripList);
        }
        catch (DMLException e)
        {
            FM_Utilities.logError(e);
        }

        // Re-evaluate whether it is now read-only or not.  It should be because the status should now be dispatched
        if (!Test.isRunningTest())
        {
            controller.reset();
        }
system.debug(truckList.Truck_List_Status__c + ' and ' + truckList.Name);
//        readOnlyMode = (truckList.Truck_List_Status__c != FM_Utilities.TRUCKLIST_STATUS_BOOKED) ||
//            (truckList.Run_Sheet_Date__c < System.today());
        readOnlyMode = (truckList.Truck_List_Status__c != FM_Utilities.TRUCKLIST_STATUS_BOOKED) ||
            (truckList.Run_Sheet_Date__c != System.today() && (!FM_Utilities.canEditTruckList(truckList.Run_Sheet_Date__c) || truckList.Shift__c != 'Day'));

        //refresh truck list and comments data
        retrieveTruckTrips();

        //set email body and pdf variables
        Blob pdfBlob;
        String emailBody;

        //reserve email capacity - can be upto 4 addresses
        Messaging.reserveSingleEmailCapacity(4);

        //html email body
        PageReference truckListEmail = Page.FM_TruckListEmail;
        truckListEmail.getParameters().put('id', truckListId);

        //pdf email attachment
        PageReference truckListEmailPDF = Page.FM_TruckListEmailPDF;
        truckListEmailPDF.getParameters().put('id', truckListId);

        // Methods defined as TestMethod do not support getContent call, so we need to mock it up.
        if(Test.IsRunningTest())
        {
            pdfBlob = Blob.valueOf('Truck List Unit Test');
            emailBody = 'Truck List Unit Test';
        }
        else
        {
            pdfBlob = truckListEmailPDF.getContent();
            emailBody = truckListEmail.getContent().toString();
        }

        //set pdf attachment
        Messaging.EmailFileAttachment pdfAttc = new Messaging.EmailFileAttachment();
        //set the date format for run sheet date
//        String runSheetDate = truckList.Run_Sheet_Date__c.month() + '/' + truckList.Run_Sheet_Date__c.day() + '/' + truckList.Run_Sheet_Date__c.year();

        // KK 2015-07-15 Cannot use Name field directly.  For Day shift, we would like to increment the Run Sheet Date by one day
        // because that represents the haul date of the loads.
        //FM_Truck_List__c submittedTruckList = [SELECT Name, Truck_List_Status__c FROM FM_Truck_List__c WHERE Id = :truckList.Id];
        FM_Truck_List__c submittedTruckList = [SELECT Name, Truck_List_Status__c, Unit__r.Name, Shift__c, Run_Sheet_Date__c
                                                FROM FM_Truck_List__c WHERE Id = :truckList.Id];

        String dispatchTruckListName = submittedTruckList.Truck_List_Status__c + ' ' + submittedTruckList.Unit__r.Name + ' ' +
            submittedTruckList.Shift__c + ' ';
        dispatchTruckListName += FM_Utilities.getHaulDate(submittedTruckList.Shift__c, null, submittedTruckList.Run_Sheet_Date__c);

        // KK 2015-07-15 set the name of the pdf file based on the dispatchTruckListName.
        // KK 2015-07-07 Defect #106 - rename the pdf to follow the truck list name.  For now keep starting with "TruckList_"
        //string pdfFileName = 'TruckList_' + truckList.Shift__c + '_' + runSheetDate+'.pdf';
        //string pdfFileName = 'TruckList_' + submittedTruckList.Name + '.pdf';
        String pdfFileName = 'TruckList_' + dispatchTruckListName + '.pdf';
        pdfAttc.setFileName(pdfFileName);
        pdfAttc.setBody(pdfBlob);

        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        List<String> toAddresses = new list<string> ();

        //add email addresses if they are not null

        if(unitEmail != null)
        {
            toAddresses.add(unitEmail);
        }

        // KK 2015-07-15 instead of using the truck list name, use the dispatchTruckListName - because that will increment
        // the Run Sheet Date by 1 for a Day shift.
        // KK 2015-07-07 Defect #106 - rename the Subject to follow the truck list name
        //String subject = truckList.Unit__r.Name + ' Truck List ' + truckList.Shift__c + ' ' + runSheetDate;
//        String subject = 'Truck List ' + submittedTruckList.Name;
        String subject = 'Truck List ' + dispatchTruckListName;
        email.setSubject(subject);
        email.setToAddresses( toAddresses );
        email.setHtmlBody(emailBody);
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{pdfAttc});

        //send email
        try
        {
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        }
        catch(Exception e)
        {
            ApexPages.Message emailErrorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to Send Email to Truck Unit. Please try again. Please contact your Administrator if the error persists');
            ApexPages.addMessage(emailErrorMsg);
            system.debug('Stack Trace : '+e.getStackTraceString());
            system.debug('error message : '+e.getMessage());

            if(e.getTypeName() == 'EmailException' || e.getTypeName() == 'DMLException')
            {
                system.debug('dml message '+e.getDmlMessage(0));
                system.debug('dml status code '+e.getDmlStatusCode(0));
                system.debug('dml type '+e.getDmlType(0));
            }
            return null;
        }

        // Take out attaching the attachment to Uni
        //if(truckList.Unit__c != null)
        //{
        //    //add a PDF attachment in Truck Unit Record
        //    Attachment truckListAttachment = new Attachment();
        //    truckListAttachment.Name = truckList.Unit__r.Name + ' Truck List ' + truckList.Shift__c + ' ' + runSheetDate+'.pdf';
        //    truckListAttachment.Body = pdfBlob;
        //    truckListAttachment.ParentId = truckList.Unit__c;
        //    insert truckListAttachment;
        //}

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,
            'This Truck List has been dispatched and is no longer editable.'));

        return null;
    }

    public String DispatchedOnString
    {
        get
        {
            if (truckList.Dispatched_On__c != null)
            {
                return truckList.Dispatched_On__c.format();
            }
            return '';
        }
        private set;
    }

    public String DispatchedByString
    {
        get
        {
            if (DispatchedByString == null || DispatchedByString == '')
            {
                List<User> dispatcherInfo = [SELECT Name FROM User WHERE Id = :truckList.Dispatched_By__c];
                if (dispatcherInfo.size() == 1)
                {
                    DispatchedByString = dispatcherInfo[0].Name;
                }
            }
            return DispatchedByString;
        }
        private set;
    }

    public void printPDF()
    {
        //save();
    }

    public PageReference save()
    {
        // * ADDED BY GM 2015-07-27 CHECK TO SEE IF TRUCK LIST CAN BE MODIFIED FIRST * //
        List<FM_Truck_List__c> lTruckList = [SELECT Id FROM FM_Truck_List__c WHERE Id = :truckListId AND Truck_List_Status__c <> 'Dispatched' LIMIT 1];

        if(lTruckList.size() == 1 && SaveTruckList())
        {
            //refresh truck list and comments data
            retrieveTruckTrips();
            return null;
        }
        else
        {
            return PageRedirect('/apex/FM_TruckListEdit?id=' + truckListId, true);
        }
    }

    private PageReference PageRedirect(String sNewLocation, Boolean bRedirect)
    {
        PageReference oPage = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + sNewLocation);
        oPage.setRedirect(bRedirect);

        return oPage;
    }

   private Boolean SaveTruckList()
   {
        SavePoint savePoint = Database.SetSavePoint();
        TruckTripWrapper oOneWrapper;

        try
        {
            SortTruckTrips();
            truckList.Phone_Lookup__c = this.sDeskSelection;
            truckList.Dispatched_By__c = (this.sDispatcherSelected != '') ? this.sDispatcherSelected : null;

            for(Integer iCount = 0; iCount < truckTripList.size(); iCount++)
            {
                oOneWrapper = this.mapWrappers.get(truckTripList[iCount].Id);

                if(oOneWrapper.aTruckTrip.Well__c != null)
                {
                    truckTripList[iCount].Location_Lookup__c = oOneWrapper.aTruckTrip.Well__c;
                }
                else if(oOneWrapper.sFacilityString.contains('###_WELL_###'))
                {
                    truckTripList[iCount].Location_Lookup__c = oOneWrapper.sFacilityString.remove('###_WELL_###');
                    truckTripList[iCount].Facility_Type__c = 'Location';

                    truckTripList[iCount].Facility_Lookup__c = null;
                    //oOneWrapper.sFacilityString = '###_WELL_###' + oOneWrapper.sFacilityString;
                }
                else
                {
                    truckTripList[iCount].Facility_Lookup__c = oOneWrapper.sFacilityString;
                    truckTripList[iCount].Facility_Type__c = 'Facility';

                    truckTripList[iCount].Location_Lookup__c = null;
                }
            }

            Database.Update(truckList);
            Database.Update(truckTripList);

            return true;
        }
        catch (DMLException e)
        {
            // trigger already is displaying an error message, so nothing to catch here.
            system.debug('SaveTruckList error message is ' + e.getMessage());
            Database.rollback(savePoint);
            return false;
        }
   }

    private void SortTruckTrips()
    {
        Map<String, Id> mapSort = new Map<String, Id>();
        Map<Id, Integer> mapSortOrder = new Map<Id, Integer>();
        List<String> lSort = new List<String>();

        String sIndex = '';
        String sOneOrder = '';
        Integer iOneOrder = 0;
        Integer iStartSort = 1;

        for(FM_Truck_Trip__c oOneTrip : truckTripList)
        {
            iOneOrder = (oOneTrip.Order__c == null) ? 99 : Integer.valueOf(oOneTrip.Order__c);
            sOneOrder = (iOneOrder < 10) ? '0' + String.valueOf(iOneOrder) : String.valueOf(iOneOrder);

            sIndex = sOneOrder + ' ' + oOneTrip.Priority__c + ' ' + oOneTrip.Id;
            mapSort.put(sIndex, oOneTrip.Id);
            lSort.add(sIndex);
        }

        lSort.sort();

        for(String sOneSort : lSort)
        {
            mapSortOrder.put(mapSort.get(sOneSort), iStartSort++);
        }

        for(Integer iCount = 0; iCount < truckTripList.size(); iCount++)
        {
            truckTripList[iCount].Order__c = mapSortOrder.get(truckTripList[iCount].Id);
        }
    }

    public List<SelectOption> unitSelectOptions
    {
        get
        {
            if(unitSelectOptions == null)
            {
                unitSelectOptions = new List<SelectOption>();
                SelectOption oOneOption;

                if(truckList.Carrier__c != null)
                {
                    List<Carrier_Unit__c> unitNameList = FM_Utilities.carrierUnitMap.get(truckList.Carrier__c);

                    for(Carrier_Unit__c unitName : unitNameList)
                    {
                        oOneOption = new SelectOption(unitName.Id, unitName.Name);

                        if(unitName.Enabled__c != true)
                        {
                            oOneOption.setDisabled(true);
                        }

                        if(unitName.Enabled__c == true || truckList.Unit__c == unitName.Id)
                        {
                            unitSelectOptions.add(oOneOption);
                        }
                    }
                }
            }

            return unitSelectOptions;
        }

        set;
    }

    public List<SelectOption> shiftSelectOptionList
    {
        get
        {
            if (shiftSelectOptionList == null)
            {
                shiftSelectOptionList = FM_Utilities.GenerateSelectOptions(FM_Truck_Trip__c.Shift__c.getDescribe());
            }
            return shiftSelectOptionList;
        }
        private set;
    }

    public List<SelectOption> facilitySelectOptionList
    {
        get
        {
            if (facilitySelectOptionList == null)
            {
                facilitySelectOptionList = FM_Utilities.facilityPickList;
            }
            return facilitySelectOptionList;
        }
        private set;
    }

    public List<SelectOption> prioritySelectOptionList
    {
        get
        {
            if (prioritySelectOptionList == null)
            {
                prioritySelectOptionList = FM_Utilities.GenerateSelectOptions(FM_Truck_Trip__c.Priority__c.getDescribe());
            }
            return prioritySelectOptionList;
        }
        private set;
    }

    public List<SelectOption> dispatchDeskSelectOptionList
    {
        get
        {
            if (dispatchDeskSelectOptionList == null)
            {
                dispatchDeskSelectOptionList = new List<SelectOption>();
                for (Dispatch_Desk__c dispatchDesk : [SELECT Name, Phone__c FROM Dispatch_Desk__c
                                                    WHERE RecordType.DeveloperName = 'Fluid_Dispatch' ORDER BY Name])
                {
                    dispatchDeskSelectOptionList.add(new SelectOption(dispatchDesk.Id, dispatchDesk.Name + ' - ' + String.ValueOf(dispatchDesk.Phone__c)));
                }
            }
            return dispatchDeskSelectOptionList;
        }
        private set;
    }

    public List<SelectOption> lDispatchUsersList
    {
        get
        {
            if(lDispatchUsersList == null)
            {
                List<PermissionSetAssignment> lUserList = [SELECT Assignee.Id, Assignee.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = :FM_Utilities.FLUID_PERMISSION_SET_DISPATCH_STANDARD AND Assignee.IsActive = true AND Assignee.Id != :UserInfo.getUserId() ORDER BY Assignee.Name];
                lDispatchUsersList = new List<SelectOption>();

                for(PermissionSetAssignment oOneUser : lUserList)
                {
                    lDispatchUsersList.add(new SelectOption(oOneUser.Assignee.Id, oOneUser.Assignee.Name));
                }
            }

            return lDispatchUsersList;
        }

        private set;
    }

    public List<SelectOption> lFacilityList
    {
        get
        {
            if(lFacilityList == null)
            {
                List<SelectOption> lFullFacilityList = new List<SelectOption>();
                List<Facility__c> lFacilities;
                List<Location__c> lWells;

                SelectOption oOneOption;
                String sFacilityName;
                String sWellName;

                /////////////////////////////////////////
                // ** SEARCH FOR REGULAR FACILITIES ** //
                /////////////////////////////////////////

                lFacilities = [SELECT Id, Name, Facility_Name_For_Fluid__c FROM Facility__c WHERE Fluid_Facility_Ind__c = true ORDER BY Facility_Name_For_Fluid__c, Name];

                for(Facility__c oOneFacility : lFacilities)
                {
                    sFacilityName = (oOneFacility.Facility_Name_For_Fluid__c == '' || oOneFacility.Facility_Name_For_Fluid__c == null) ? oOneFacility.Name : oOneFacility.Facility_Name_For_Fluid__c;
                    oOneOption = new SelectOption(oOneFacility.Id, sFacilityName);

                    lFullFacilityList.add(oOneOption);
                }

                ///////////////////////////////////////////////////////////////////////
                // ** SEARCH FOR "OTHER" FACILITIES (NON STANDARD WELL LOCATIONS) ** //
                ///////////////////////////////////////////////////////////////////////

                lWells = [SELECT Id, Name, Location_Name_For_Fluid__c FROM Location__c WHERE Fluid_Location_Ind__c = true ORDER BY Location_Name_For_Fluid__c, Name];

                for(Location__c oOneLocation : lWells)
                {
                    sWellName = (oOneLocation.Location_Name_For_Fluid__c == '' || oOneLocation.Location_Name_For_Fluid__c == null) ? oOneLocation.Name : oOneLocation.Location_Name_For_Fluid__c;
                    oOneOption = new SelectOption(oOneLocation.Id + '###_WELL_###', sWellName);

                    lFullFacilityList.add(oOneOption);
                }

                // Sort AND Assign Final List
                lFacilityList = FM_Utilities.SortSelectOptions(lFullFacilityList, FM_Utilities.SortSelectOptionBy.Label);
            }

            return lFacilityList;
        }

        private set;
    }

    public class TruckTripWrapper
    {
        public FM_Truck_Trip__c aTruckTrip {get; set;}
        public String sFacilityString {get; set;}

        public TruckTripWrapper(FM_Truck_Trip__c oOneTrip)
        {
            this.aTruckTrip = oOneTrip;
            this.sFacilityString = (oOneTrip.Facility_Type__c == 'Facility') ? oOneTrip.Facility_Lookup__c : oOneTrip.Location_Lookup__c;
            this.sFacilityString += (oOneTrip.Facility_Type__c == 'Location') ? '###_WELL_###' : '';
        }
    }
}