public with sharing class ScaffoldTestData {

    public static Scaffold__c createScaffold() {
        Account acct = new Account(name='HUSKY ENERGY');
        acct.ParentId = null;
        insert acct;

        Contact ct = new Contact(AccountId=acct.Id, lastname='Scaffold', firstname='Scaffold');
        insert ct;

        Scaffold__c scaffold = new Scaffold__c();
        scaffold.Name = 'Scaffold Test';
        scaffold.Date_Up__c = Date.today();
        scaffold.Work_Order__c = 'Work Order Test';
        scaffold.Description__c = 'Scaffold Description Test';
        scaffold.Trade__c = 'Fabrication';
        scaffold.Scaffold_Contact__c = ct.id;
        
        insert scaffold;
        return scaffold;
    }
}