@isTest(SeeAllData=true)
public class TriggerTestSuite{
	
	static Map<String, Schema.Recordtypeinfo> shiftRTMap = Shift_Configuration__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> taRTMap = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> teamRTMap = Team__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> roleRTMap = Team_Role__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> doiRTMap = DOI__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> eventRTMap = Operational_Event__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> areaRTMap = Process_Area__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    
    static Id maintenanceTART = taRTMap.get('Maintenance').getRecordTypeId();
    static Id maintenanceShiftRT = shiftRTMap.get('Maintenance').getRecordTypeId();
    static Id maintenanceRoleRT = roleRTMap.get('Maintenance').getRecordTypeId();
    static Id maintenanceTeamRT = teamRTMap.get('Maintenance').getRecordTypeId();
    static Id maintenanceDOIRT = doiRTMap.get('Maintenance').getRecordTypeId();
    static Id maintenanceEventRT = eventRTMap.get('Maintenance').getRecordTypeId();
    static Id maintenanceAreaRT = areaRTMap.get('Maintenance').getRecordTypeId();
    
    static Business_Unit__c businessUnit;
    static Business_Department__c businessDepartment;
    static Operating_District__c operatingDistrict;
    static Plant__c plant;
    static Unit__c unit;
    
	private static void createSampleData()
    {
         businessUnit = TestUtilities.createBusinessUnit('Sunrise Field');
        insert businessUnit;
            
         businessDepartment = new Business_Department__c(Name = 'Test Business Department');            
        insert businessDepartment;

         operatingDistrict = TestUtilities.createOperatingDistrict(businessUnit.Id, businessDepartment.Id, 'Sunrise');
        insert operatingDistrict;
        
         plant = TestUtilities.createPlant('test-plant');
         insert plant;
         unit = TestUtilities.createUnit('Test Unit', plant.Id);
         insert unit;        
    }
    
    @isTest static void testUpdateShiftConfiguration() {
        List<Shift_Configuration__c> shiftConfigs = new List<Shift_Configuration__c>();
        
        final String JOB_NAME = 'Team Allocation Lifecycle Job';
        
        TestUtilities.disableAllShiftConfiguration();
        
        Integer beforeCount = [SELECT count() FROM Scheduled_Job__c WHERE Name = :JOB_NAME];
        
        createSampleData();
                          
        // Load data 
        for (Integer i = 0; i < 10; i++) {
            String startZeros = (i + 1) > 9 ? '0' : '';
            String endZeros = (i + 2) > 9 ? '0' : '';
            
            Shift_Configuration__c current = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, (startZeros + (i+1) + '00'), (endZeros + (i+2) + '00'), maintenanceShiftRT);
            shiftConfigs.add(current);
        }
        
        // Because these are enabled, this call will also exercise TAllocationLifecycleJobScheduler.start()
        insert(shiftConfigs);
        
        Integer afterCount = [SELECT count() FROM Scheduled_Job__c WHERE Name = :JOB_NAME];
        
        // Test disabling of job as well.
        TAllocationLifecycleJobScheduler.stop();
        
        System.assert(afterCount == beforeCount);
    }
    
    
    @isTest static void testUpdateTeamAllocationName() {
        List<Team_Allocation__c> tas = new List<Team_Allocation__c>();
        
        createSampleData();
        
        TestUtilities.disableAllShiftConfiguration();
                
        Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', maintenanceShiftRT);
        insert dayShift;
        
        Boolean hasActive = false;
        for(Integer i = 0; i < 2; i++) {
            String status = TeamAllocationUtilities.INACTIVE_STATUS_NAME;
            if (!hasActive) {
                status = TeamAllocationUtilities.ACTIVE_STATUS_NAME;
                hasActive = true;
            }
            
            Team_Allocation__c current = TestUtilities.createTeamAllocation(operatingDistrict.Id, Date.today(), dayShift.Id, status, maintenanceTART); 
            tas.add(current);
        }
        
        insert tas;
        
        Test.startTest();
        
        Integer blankNameCount = [SELECT count() From Team_Allocation__c WHERE Team_Allocation_Name__c = NULL];
        System.assert(blankNameCount <= 0);
        
        Test.stopTest();
    }
    
    
    @isTest static void testUpdateShiftOperator() {
        try {
            
            createSampleData();
            
            TestUtilities.disableAllShiftConfiguration();
                        
            Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', maintenanceShiftRT);
            insert dayShift;
            
            Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, Date.today(), dayShift.Id, TeamAllocationUtilities.INACTIVE_STATUS_NAME, maintenanceTART);
            insert ta;

            Account testAccount = TestUtilities.createTestAccount();
            insert testAccount;
            
            Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
            insert testOperator;
            
            
            Team_Role__c operatorRole = TestUtilities.createTeamRole('Operator', false, -1, maintenanceRoleRT);
            insert operatorRole;
            
            Shift_Operator__c inactiveShiftOp = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, ta.Id, plant.Id, operatorRole.Id);
            try {               
                insert inactiveShiftOp;
               
                 //System.assert(false, 'In-active shift operator added errornously. \'testUpdateShiftOperator()\'.');
            } catch (DmlException dex) {
                if (dex.getMessage().contains('Cannot create new shift operator for a team allocation that is not active')) {
                    System.assert(true,
                        '\n*****************************\n'
                        + 'Expected error caught while adding in-active shift operator.'
                        + '\n \'testUpdateShiftOperator()\'.'
                        + dex.getMessage()
                        + '\n*****************************\n');
                } else {
                    System.assert(false, 
                        '\n*****************************\n'
                        + 'DML exception occured while saving in-active shift operator.' 
                        + '\n \'testUpdateShiftOperator()\'.'
                        + dEx.getMessage() 
                        + '\n*****************************\n');
                }
            }
        } catch (Exception ex) {
            System.assert(false, 'Error setting up \'testUpdateShiftOperator\' dependies.' + ex.getMessage());
        }
    }
    
    
    @isTest static void testUpdateOperationalEvent() {
        try {
            
            createSampleData();
            
            TestUtilities.disableAllShiftConfiguration();
                        
            Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', maintenanceShiftRT);
            insert dayShift;
           
            Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, Date.today(), dayShift.Id, TeamAllocationUtilities.INACTIVE_STATUS_NAME, maintenanceTART);
            insert ta;
            
            Account testAccount = TestUtilities.createTestAccount();
            insert testAccount;
            
            Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
            insert testOperator;

            Process_Area__c area = TestUtilities.createProcessArea('Test Area', maintenanceAreaRT);
            insert area;  
            
            Operational_Event__c inactiveOpEvent = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, ta.Id, unit.Id, area.Id, plant.Id, maintenanceEventRT);
            try {               
                insert inactiveOpEvent;
                
                //System.assert(false, 'In-active operational event added errornously. \'testUpdateOperationalEvent()\'.');
            } catch (DmlException dex) {
                if (dex.getMessage().contains('Cannot create new operational event for a team allocation that is not active')) {
                    System.assert(true,
                        '\n*****************************\n'
                        + 'Expected error caught while adding in-active operational event.'
                        + '\n \'testUpdateOperationalEvent()\'.' 
                        + dex.getMessage()
                        + '\n*****************************\n');
                } else {
                    System.assert(false, 
                        '\n*****************************\n'
                        + 'DML exception occured while saving in-active operational event.' 
                        + '\n \'testUpdateOperationalEvent()\'.' 
                        + dEx.getMessage() 
                        + '\n*****************************\n');
                }
            }
            
                
        } catch (Exception ex) {
            System.assert(false, 'Error setting up \'testUpdateOperationalEvent\' dependies.' + ex.getMessage());
        }
    }
    
     
    // Test Updating of carseal status
    static testMethod void testCarsealStatusChange() {
        Carseal__c testSeal = new Carseal__c();
        insert testSeal;
        Carseal_Status_Code__c code = new Carseal_Status_Code__c(Name = 'CSC');
        insert code;
         Carseal_Status_Code__c code2 = new Carseal_Status_Code__c(Name = 'CSO');
        insert code2;
        Carseal_Status__c testStatus = 
          new Carseal_Status__c(Carseal__c = testSeal.Id, 
                                Status_Lookup__c=code.Id); 
        testStatus.Date__c=Date.today();
        insert testStatus;
        //System.assertEquals(testStatus.Date__c, testSeal.Current_Status_Updated_On__c);
        testStatus.Status_Lookup__c = code2.Id;
        update testStatus;
        Carseal__c carSealCreated = 
          [SELECT (SELECT Status_Lookup__c FROM Carseal_Statuses__r)  
                  FROM Carseal__c WHERE Id = :testSeal.Id];
        //System.assertEquals(2, carSealCreated.Carseal_Statuses__r.size());
    }
    
    static testMethod void testValidateShiftOperator()
    {
        Date testDate = Date.today();
        String formattedTestDate = DateUtilities.toDateTime(testDate).format(DateUtilities.REQUEST_DATE_FORMAT);
        
        createSampleData();
        
        TestUtilities.disableAllShiftConfiguration();
            
        Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', maintenanceShiftRT);
        insert dayShift;
        
        Team__c team = TestUtilities.createTeam('Testing Team', operatingDistrict.Id, maintenanceTeamRT);
        insert team;
            
        //create a Role that allow maximum 1 ShiftOperator in this Role for each Team or Team Allocation    
        Team_Role__c role = TestUtilities.createTeamRole('Field Team Leader', true, 1, maintenanceRoleRT);
        insert role;
         
        Account testAccount = TestUtilities.createTestAccount();
        insert testAccount;

        Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
        insert testOperator;
                        
     
        //Test creating number of ShifResource for a Team exceed threshold    
        //insert successfully the first Shift Operator     
        Shift_Operator__c shiftOpOfTeam = TestUtilities.createShiftOperator(team.Id, testOperator.Id,plant.Id, role.Id);
        insert shiftOpOfTeam;
        
        try
        {
            Shift_Operator__c violatedShiftOpOfTeam = TestUtilities.createShiftOperator(team.Id, testOperator.Id,plant.Id, role.Id);
            insert violatedShiftOpOfTeam;
        }
        catch (DmlException dex) 
        {
            if (dex.getMessage().contains('There can only be ' + role.Max_Occurence__c + ' ' + role.Name)) {
                System.assert(true,
                    '\n*****************************\n'
                    + 'Expected error caught while the number of ShiftResource exceed threshold.'
                    + '\n \'testValidateShiftOperator()\'.' 
                    + dex.getMessage()
                    + '\n*****************************\n');
            } 
        }
        //test creating number of ShiftResource for a TeamAllocation exceed threshold
        Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, maintenanceTART);
        insert ta;
        
        Shift_Operator__c shiftOpOfTA = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, ta.Id, plant.Id, role.Id);
        insert shiftOpOfTA;  
        
        try
        {
            Shift_Operator__c violatedShiftOpOfTA = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, ta.Id, plant.Id, role.Id);
            insert violatedShiftOpOfTA;
        }
        catch (DmlException dex) 
        {
            if (dex.getMessage().contains('There can only be ' + role.Max_Occurence__c + ' ' + role.Name)) {
                System.assert(true,
                    '\n*****************************\n'
                    + 'Expected error caught while the number of ShiftResource exceed threshold.'
                    + '\n \'testValidateShiftOperator()\'.' 
                    + dex.getMessage()
                    + '\n*****************************\n');
            } 
        }  
    }
    static testMethod void testFindShiftResourceRole()
    {
    	createSampleData();
            
        TestUtilities.disableAllShiftConfiguration();
                        
        Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', maintenanceShiftRT);
        insert dayShift;
           
        Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, Date.today(), dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, maintenanceTART);
        insert ta;
            
        Account testAccount = TestUtilities.createTestAccount();
        insert testAccount;
            
        Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
        insert testOperator;

        Process_Area__c area = TestUtilities.createProcessArea('Test Area', maintenanceAreaRT);
        insert area;  
        
        Team_Role__c role = TestUtilities.createTeamRole('Chief Steam Engineer', true, 1, maintenanceRoleRT);
        insert role;
        
        Shift_Operator__c shiftOpOfTeam = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, ta.Id, plant.Id, role.Id);
        insert shiftOpOfTeam;   
         
        Operational_Event__c opEvent = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, ta.Id, unit.Id, area.Id, plant.Id, maintenanceEventRT);
        insert opEvent;
        
        Contact newOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
        insert newOperator;
        
        opEvent.Operator_Contact__c = newOperator.Id;
        update opEvent;
                
        Team_Role__c role1 = TestUtilities.createTeamRole('Operator', false, -1, maintenanceRoleRT);
        insert role1;
        
        shiftOpOfTeam.Role__c = role1.id;
        update shiftOpOfTeam;
        delete shiftOpOfTeam;
    }
    static testMethod void testMaintenanceEditInactiveTA()
    {
        createSampleData();
        TestUtilities.disableAllShiftConfiguration();
                        
        Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', maintenanceShiftRT);
        insert dayShift;
           
        Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, Date.today(), dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, maintenanceTART);
        insert ta;
            
        Account testAccount = TestUtilities.createTestAccount();
        insert testAccount;
            
        Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
        insert testOperator;

        Process_Area__c area = TestUtilities.createProcessArea('Test Area', maintenanceAreaRT);
        insert area;  
            
        Operational_Event__c opEvent = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, ta.Id, unit.Id, area.Id, plant.Id, maintenanceEventRT);
        insert opEvent;
        
        //make TA inactive
        ta.Status__c = TeamAllocationUtilities.INACTIVE_STATUS_NAME;
        update ta;
        
        Team__c team = TestUtilities.createTeam('maintenance team', operatingDistrict.Id, maintenanceTeamRT);
        insert team;
        
        ta.Team__c = team.Id;
        update ta;
        System.assertEquals(team.Id, ta.Team__c);
        
        Team_Role__c operatorRole = TestUtilities.createTeamRole('Operator', false, -1, maintenanceRoleRT);
        insert operatorRole;
        
        Shift_Operator__c inactiveShiftOp = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, ta.Id, plant.Id, operatorRole.Id);
        insert inactiveShiftOp;    
    }
}