@isTest
private class FM_LoadRequestExportControllerXTest {
    

	private static testMethod void testexportLoadRequestsCsv() {
	    
	    CreateTestData();

        User oUser = GetTestUser();
        String sHeaderData;

        //////////////////
        //* START TEST *//
        //////////////////

        Test.startTest();

        System.runAs(oUser)
        {
            
            PageReference pageRef = Page.FM_LoadRequestExportCsv;
            pageRef.getParameters().put('isCsv', 'true');
            Test.setCurrentPage(pageRef);

            //Create Controller
            FM_LoadRequestExportControllerX lrExportCsv = new FM_LoadRequestExportControllerX();
            lrExportCsv.exportLoadRequests();
            
            //Get Header Data -> Verify it is Set
            sHeaderData = lrExportCsv.xlsHeader;
            System.assert(sHeaderData != '' && sHeaderData != null);

            // Get Excel Results -> Verify the Export size matches the number LR records
            System.assertEquals(lrExportCsv.loadRequestWrapperList.size(), 2);


        }

        /////////////////
        //* STOP TEST *//
        /////////////////

        Test.stopTest();

	}
	
		private static testMethod void testexportLoadRequestsXls() {
	    
	    CreateTestData();

        User oUser = GetTestUser();
        String sHeaderData;

        //////////////////
        //* START TEST *//
        //////////////////

        Test.startTest();

        System.runAs(oUser)
        {
            
            PageReference pageRef = Page.FM_LoadRequestExportCsv;
            pageRef.getParameters().put('isXls', 'true');
            Test.setCurrentPage(pageRef);

            //Create Controller
            FM_LoadRequestExportControllerX lrExportXls = new FM_LoadRequestExportControllerX();
            lrExportXls.exportLoadRequests();
            
            //Get Header Data -> Verify it is Set
            sHeaderData = lrExportXls.xlsHeader;
            System.assert(sHeaderData != '' && sHeaderData != null);

            //Get Excel Results -> Verify the Export size matches the number LR records
            System.assertEquals(lrExportXls.loadRequestWrapperList.size(), 2);


        }

        /////////////////
        //* STOP TEST *//
        /////////////////

        Test.stopTest();

	}
	
	private static void CreateTestData()
    {
        //-- Setup floc data                                
        Business_Unit__c businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

        Business_Department__c businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

        Operating_District__c operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        Id fieldRecordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        Field__c field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, fieldRecordTypeId);
        insert field;

        Route__c route = RouteTestData.createRoute('999');
        route.Fluid_Management__c = true;
        insert route;                  

        Location__c location = LocationTestData.createLocation('Test Location', route.Id, field.Id);
        location.Functional_Location_Category__c = 4;
        location.Fluid_Location_Ind__c = true;
        insert location;

        Facility__c facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        facility.Fluid_Facility_Ind_Origin__c = true;
        facility.Fluid_Facility_Ind__c = true;
        insert facility;

        List<Profile> lProfiles = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User' LIMIT 1];  //this will be changed to Dispatcher? role maybe

        //Test USER
        User oUser = new User();
        oUser.FirstName = 'Mr';
        oUser.LastName = 'Exporter';
        oUser.Email = oUser.FirstName.toLowerCase() + '.' + oUser.LastName.toLowerCase() + '@testingexport.com';
        oUser.UserName = oUser.Email;
        oUser.Alias = 'mrexport';
        oUser.TimeZoneSidKey = 'America/New_York';
        oUser.LocaleSidKey = 'en_US';
        oUser.EmailEncodingKey = 'ISO-8859-1'; 
        oUser.LanguageLocaleKey='en_US';
        oUser.ProfileId = lProfiles[0].Id;
        oUser.IsActive = true;
        insert oUser;

        System.runAs(oUser)
        {
            // Permission Set Assignment - Sand Lead
            List<PermissionSet> lPermissions = [SELECT Id, Name FROM PermissionSet WHERE Name = 'HOG_Data_Loader' LIMIT 1];  //this will be changed to specific PS who will be doing Export
            PermissionSet oPermission = (lPermissions.size() == 1) ? lPermissions[0] : null;

            if(oPermission != null)
            {
                PermissionSetAssignment oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
                insert oPermissionAssign;
            }
        }

        // Test Account
        Account oAccount = new Account();
        oAccount.Name = 'Husky Account';
        insert oAccount;
        
        //Test Carrier
        Carrier__c oCarrier = new Carrier__c();
        oCarrier.Carrier__c = oAccount.Id;
        insert oCarrier;

        //Test Load Requests
        FM_Load_Request__c oLoadReq1 = new FM_Load_Request__c();
        oLoadReq1.Carrier__c = oCarrier.Id;
        oLoadReq1.Load_Type__c = 'Standard Load';
        oLoadReq1.Product__c = 'W';
        oLoadReq1.Shift__c = 'Day';
        oLoadReq1.Standing_Comments__c = 'Load Request "1"'; //test double quotes encapsulation
        oLoadReq1.Source_Location__c = location.Id;
        oLoadReq1.Tank__c = 'Tank 1';
        insert oLoadReq1;
        
        FM_Load_Request__c oLoadReq2 = new FM_Load_Request__c();
        oLoadReq2.Carrier__c = oCarrier.Id;
        oLoadReq2.Load_Type__c = 'Skim';
        oLoadReq2.Product__c = 'O';
        oLoadReq2.Shift__c = 'Night';
        oLoadReq2.Standing_Comments__c = 'Load Request 2';
        oLoadReq2.Source_Facility__c = facility.Id;
        oLoadReq2.Tank__c = 'Tank 1';
        insert oLoadReq2;
        
    
    }
    
    private static User GetTestUser()
    {
        return [SELECT Id FROM User WHERE Alias = 'mrexport' AND Email = 'mr.exporter@testingexport.com' LIMIT 1];
    }

}