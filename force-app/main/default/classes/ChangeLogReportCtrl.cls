public with sharing class ChangeLogReportCtrl {

    public String selectedProgram {get;set;}
    public String selectedProject {get;set;}
    public String selectedArea {get;set;}
    public List<Change_Request__c> result {get; private set;}
    public List<CRWrapper > resultWrapper {get; set;}
    public List<Change_Request__c> requests {get; private set;}
    public Map<String,Decimal> tic {get; private set;}
    public String selectedContractor { get; set; }
    public String selectedPONumber { get; set; }
    public String SortFieldSave;
    private boolean showExport = false;
    public boolean showExportHeader {get;set;}
    public String originalPOValue {get;set;}
    public String currentPOValue {get;set;}
    public String totalPOExecuted {get;set;}
    public String totalPOPending {get;set;}       
    public String contractNumber {get;set;}
    public String CONumber {get;set;}
    public String selectedChangeStatus {get;set;}
    public Decimal approved {get;set;}
    public Decimal approvedHold {get;set;}
    public Decimal pending {get;set;}   
    public Decimal totalProject {get;set;}     
    public List<Impact_Assessor__c> i {get;set;}
    public List<Change_Request__c> resultUpdate {get;set;}    
    public String approvedString {get;set;}
    public String approvedHoldString {get;set;}
    public String pendingString {get;set;}
    public String totalProjectString {get;set;} 
    public String addDecimal {get;set;}    
    public List<Boolean> locked {get;set;}
    public Decimal origPOVal {get;set;}
    public Decimal curPOVal {get;set;}
    public Decimal totalPORevExec {get;set;}
    public Decimal totalPORevPend {get;set;} 
    public Boolean bolPrintableView {get;set;}      
    public void init() 
    {
        String strPrintableView = ApexPages.currentPage().getParameters().get('print');
        bolPrintableView = (strPrintableView == '1');
    }
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    
    public ChangeLogReportCtrl() {
        addDecimal = '0';    
        if (ApexPages.currentPage().getParameters().get('program') != null)
             selectedProgram = ApexPages.currentPage().getParameters().get('program');
        if (ApexPages.currentPage().getParameters().get('Group') != null)
             selectedProject = ApexPages.currentPage().getParameters().get('Group');
        if (ApexPages.currentPage().getParameters().get('area') != null)
             selectedArea = ApexPages.currentPage().getParameters().get('area');
        if (ApexPages.currentPage().getParameters().get('contractor') != null)
             selectedContractor = ApexPages.currentPage().getParameters().get('contractor');
        if (ApexPages.currentPage().getParameters().get('poNumber') != null)
             selectedPONumber = ApexPages.currentPage().getParameters().get('poNumber');  
         //resultWrapper = new List<CRWrapper>();
         requests = new List<Change_Request__c>();  
         showExportHeader = false;       
    }
    
    public ChangeLogReportCtrl getController() {
        return this; 
    }    
    
    public List<SelectOption> ProgramOptions {
        get
        {
            if (ProgramOptions == null)
            {
                ProgramOptions = new List<SelectOption>();
                ProgramOptions.add(new SelectOption('', '-- All --'));
                List<Project__c> programs = [select Id, Name from Project__c order by Name];
                for(Project__c p : programs)
                   ProgramOptions.add(new SelectOption(p.Id, p.Name));
            }
            return ProgramOptions;  
        }
        set;
    }  
    
    public List<SelectOption> ProjectOptions {
        get{
            System.debug('Inside get of ProjectOptions ');
            if (ProjectOptions == null)
            {
                System.debug('Inside get of ProjectOptions and ProjectOptions is null? ');    
                ProjectOptions = new List<SelectOption>();
                ProjectOptions.add(new SelectOption('', '-- All --'));
                System.debug('selectedProgram is '+selectedProgram ); 
                if (selectedProgram == '')
                {
                    System.debug('selectedProgram is blank ? ');    
                    return ProjectOptions;
                }
                List<Project_Area__c> projects = [select Id, Name from Project_Area__c order by Name];
                System.debug('Query Projects ? ');    
                for(Project_Area__c p : projects)
                   ProjectOptions.add(new SelectOption(p.Id, p.Name));
            }
            return ProjectOptions;  
        }
        set;
    }
    
    public List<SelectOption> AreaOptions {
        get
        {
            if (AreaOptions == null)
            {
                AreaOptions = new List<SelectOption>();
                AreaOptions.add(new SelectOption('', '-- All --'));
                if (selectedProject == '') return AreaOptions;
                
                List<CR_Impacted_Functions__c> areas = [select Id, Name from CR_Impacted_Functions__c order by Name];
                for(CR_Impacted_Functions__c a : areas)
                   AreaOptions.add(new SelectOption(a.Id, a.Name));
            }
            return AreaOptions;
        }
        set; 
    }  
    
    public List<SelectOption> ContractorOptions {
        get
        {
            if( ContractorOptions == null)
            {
                ContractorOptions = new List<SelectOption>();
                ContractorOptions.add(new SelectOption('', '-- All --'));
                if (selectedContractor == '') return ContractorOptions;
                
                List<Change_Request__c> requests = [select Id, Name, Contractor__c from Change_Request__c where Contractor__c != null order by CreatedDate ];
                Set<String> uniqueCont = new Set<String>();
                for(Change_Request__c a : requests)
                    uniqueCont.add( a.Contractor__c );
                System.debug('REQ '+requests);
                for( String a : uniqueCont )
                    ContractorOptions.add(new SelectOption( a, a ));
            }
            return ContractorOptions;
        }
        set; 
    }
    
    public List<SelectOption> PONumberOptions {
        get
        {
            if( PONumberOptions == null )
            {
                PONumberOptions = new List<SelectOption>();
                PONumberOptions.add(new SelectOption('', '-- All --'));
                if(selectedPONumber == '') return PONumberOptions;
                
                List<Change_Request__c> requests = [select Id, Name, Document_Number__c from Change_Request__c where Document_Number__c != null order by CreatedDate];
                Set<String> uniquePO = new Set<String>();
                for(Change_Request__c a : requests)
                    uniquePO.add( a.Document_Number__c );
                
                for( String a : uniquePO )
                    PONumberOptions.add(new SelectOption( a, a ));

                //for(Change_Request__c a : requests)
                   //PONumberOptions.add(new SelectOption(a.Document_Number__c, a.Document_Number__c));
            }
            return PONumberOptions;
        }
        set; 
    }
    /*public List<SelectOption> ChangeStatusOptions{
        get
        {
            if( ChangeStatusOptions == null)
            {
                ChangeStatusOptions = new List<SelectOption>();
                ChangeStatusOptions.add(new SelectOption('', '-- Any --'));
                if (selectedChangeStatus == '') return ChangeStatusOptions ;                
                Schema.DescribeFieldResult fieldResult = Change_Request__c.Change_Status__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple)
                {
                   ChangeStatusOptions .add(new SelectOption( f.getLabel(), f.getValue() ));
                   //options.add(new SelectOption(f.getLabel(), f.getValue()));
                }  
                System.debug('Picklist entries '+ChangeStatusOptions );   
                
                //for( String a : uniqueCont )
                    //ContractorOptions.add(new SelectOption( a, a ));
            }
            return ChangeStatusOptions ;
        }
        set; 
    }*/
    
    public void RunReport() {
        //showExportHeader =false;
        resultWrapper = new List<CRWrapper>();
        String query = 'SELECT Name, Title__c, CreatedDate, Initiator__c, Initiator__r.Name, Change_Status__c, Document_Number__c, Contractor__c, PCN_Number__c, Change_Type__c, Contract_Number__c, CO_Number__c, CO_Status__c, CO_Status_Date__c, CO_Comments__c, ' +
                    '(select CreatedDate from Histories where Field = \'Change_Status__c\' order by CreatedDate desc), Scope_Type__c, Justification__c,Project_Area__r.Name,Project_Area__r.Project_AFE__c, ' +
                    '(select TIC_Cost__c, Cost_Impact__c, Change_Impact_Hours__c, Requested_By__c from Impact_Assessors__r), Budget__c, Project_AFE__c, Project_Manager_Approval_Date__c ' +
                    'FROM Change_Request__c ' +
                    'WHERE Change_Status__c != null ';
                    
        if (selectedProject != '' && selectedProject != null)
           query += ' and Project_Area__r.Id = :selectedProject ';
           
        if (selectedContractor != '' && selectedContractor != null)
           query += ' and Contractor__c = :selectedContractor ';
           
        if (selectedPONumber != '' && selectedPONumber != null)
           query += ' and Document_Number__c = :selectedPONumber ';
       
        if (contractNumber != '' && contractNumber != null)
           query += ' and Contract_Number__c = :contractNumber ';
           
        if (CONumber != '' && CONumber != null)
           query += ' and CO_Number__c = :CONumber ';
           
        /*if (originalPOValue != '' && originalPOValue != null)
        { 
           Decimal originalPOValueDecimal = Decimal.valueOf(originalPOValue);
           query += ' and Budget__c = :originalPOValueDecimal ';           
        }
        if (selectedChangeStatus != '' && selectedChangeStatus != null)
           query += ' and Change_Status__c = :selectedChangeStatus';*/
                      
        query += ' limit 10000';
        System.debug('Query is '+query);
        result = Database.query(query);
        for(Change_Request__c changeReq : result)
        {
            CRWrapper c = new CRWrapper(changeReq);
            resultWrapper.add(c);
        }
        System.debug('result is '+result);
        /*Decimal origPOVal = 0;
        Decimal curPOVal = 0;
        Decimal totalPORevExec = 0;
        Decimal totalPORevPend = 0;*/
        initializeToZero();
        
        /*for(Change_Request__c changeReq : result)
        {
            setValues(changeReq);
        }*/
        for(CRWrapper wrapperItem : resultWrapper)
        {
            setValues(wrapperItem.changeRequest);
        }
        
        setDecimals();
        
        //currentPOValue = (origPOVal + COVal).format();
        i = new List<Impact_Assessor__c>();
        /*for(Change_Request__c c : result)
        {
            i.addAll(c.Impact_Assessors__r);
        }*/
        for(CRWrapper wrapperItem : resultWrapper)
        {
            i.addAll(wrapperItem.changeRequest.Impact_Assessors__r);
        }
        
        approved = 0;
        approvedHold = 0;
        pending = 0;
        /*for(Change_Request__c changeReq : result)
        {
            if( changeReq.Change_Status__c == 'Approved')
            {
                for(Impact_Assessor__c impactAssessorObj : changeReq.Impact_Assessors__r)
                {
                    approved = approved + impactAssessorObj.Cost_Impact__c;
                }
            }
            else if(changeReq.Change_Status__c == 'Approved - Hold')
            {
                for(Impact_Assessor__c impactAssessorObj : changeReq.Impact_Assessors__r)
                {
                    approvedHold = approvedHold + impactAssessorObj.Cost_Impact__c;
                }
            }
            else
            {
                for(Impact_Assessor__c impactAssessorObj : changeReq.Impact_Assessors__r)
                {
                    pending = pending + impactAssessorObj.Cost_Impact__c;
                }
            }
        }*/
        
        for(CRWrapper wrapperItem: resultWrapper)
        {
            if( wrapperItem.changeRequest.Change_Status__c == 'Approved')
            {
                for(Impact_Assessor__c impactAssessorObj : wrapperItem.changeRequest.Impact_Assessors__r)
                {
                    approved = approved + impactAssessorObj.Cost_Impact__c;
                }
            }
            else if(wrapperItem.changeRequest.Change_Status__c == 'Approved - Hold')
            {
                for(Impact_Assessor__c impactAssessorObj : wrapperItem.changeRequest.Impact_Assessors__r)
                {
                    approvedHold = approvedHold + impactAssessorObj.Cost_Impact__c;
                }
            }
            else
            {
                for(Impact_Assessor__c impactAssessorObj : wrapperItem.changeRequest.Impact_Assessors__r)
                {
                    pending = pending + impactAssessorObj.Cost_Impact__c;
                }
            }
        }
        
        totalProject = approved + approvedHold +pending;        
        approvedString = approved.format();
        if(!(approvedString.contains('.')))
        approvedString=approvedString+'.00';
        
        approvedHoldString = approvedHold.format();
        if(!(approvedHoldString.contains('.' )))
        approvedHoldString=approvedHoldString+'.00';
        
        pendingString = pending.format();
        if(!(pendingString.contains('.')))
        pendingString=pendingString+'.00';
        totalProjectString = totalProject.format();
        if(!(totalProjectString.contains('.')))
        totalProjectString =totalProjectString +'.00';
        System.debug('====>>'+currentPOValue );
        if(String.isBlank(selectedPONumber))
        {
          originalPOValue = null;
          currentPOValue = null;
        }
        showExport = true;  
        //if(showExport && resultWrapper.size()>0)
            showExportHeader =true;
        ApexPages.getMessages().clear();               
    }
    public void initializeToZero()
    {
        origPOVal = 0;
        curPOVal = 0;
        totalPORevExec = 0;
        totalPORevPend = 0;
    }
    public void setValues(Change_Request__c changeReq)
    {
        if(origPOVal == 0 && !String.isBlank(selectedPONumber))
            {
                if(changeReq.Budget__c!=null)
                    origPOVal = changeReq.Budget__c;
                else
                    origPOVal = 0.00;  
                curPOVal = origPOVal;      
            }
            if(changeReq.CO_Status__c == 'Executed')
            {
                for(Impact_Assessor__c i : changeReq.Impact_Assessors__r)
                {                    
                    if(!String.isBlank(selectedPONumber))
                    {
                        if(i!=null && i.Cost_Impact__c!=null && curPOVal!=null)
                            curPOVal = curPOVal + i.Cost_Impact__c;
                    }
                    totalPORevExec  = totalPORevExec + i.Cost_Impact__c;
                    
                }
            }
            else if(changeReq.CO_Status__c == 'Pending')
            {
                for(Impact_Assessor__c i : changeReq.Impact_Assessors__r)
                {
                    totalPORevPend = totalPORevPend + i.Cost_Impact__c;
                }
            }
    }
    public void setDecimals()
    {
        
        if(origPOVal!=null && origPOVal!=0)
            originalPOValue = origPOVal.format();
        if(origPOVal==0.0 || origPOVal==null)
            originalPOValue = '0.00';
        if(originalPOValue.substringAfter('.').length()==0)
            originalPOValue = originalPOValue+'.00';
        else if(originalPOValue.substringAfter('.').length()==1)
            originalPOValue = originalPOValue+'0';
        if(curPOVal!=null && curPOVal!=0)
            currentPOValue = curPOVal.format();
        if(curPOVal == 0.0)
            currentPOValue = '0.00'; 
         if(currentPOValue.substringAfter('.').length()==0)
            currentPOValue  = currentPOValue +'.00';
        else if(currentPOValue .substringAfter('.').length()==1)
            currentPOValue = currentPOValue +'0';   
                   
        if(totalPORevExec== 0.0)
            totalPOExecuted = '0.00';
        if(totalPORevPend== 0.0)
            totalPOPending = '0.00';
        if(totalPORevExec!=0 && totalPORevExec!=null)
        {
            totalPOExecuted = totalPORevExec.format();
            if(!(totalPOExecuted.contains('.')))
            totalPOExecuted =totalPOExecuted +'.00';
        }
        if(totalPORevPend!=0 && totalPORevPend!=null)
        {
            totalPOPending = totalPORevPend.format();
            if(!(totalPOPending.contains('.')))
            totalPOPending=totalPOPending+'.00';
        }
    }
    
    public PageReference exportDetail()
    {
        if( !showExport )
            runReport();
        
        //requests = new List<Change_Request__c>();
        SYSTEM.DEBUG('Before : Req is '+requests +' and size '+ requests.size());
        /*for( Change_Request__c cr : result )
        {
            Change_Request__c crNew = cr.clone( true, true );
            crNew.PCN_Number__c = crNew.PCN_Number__c.replaceAll( '-', '' );
            
            requests.add( crNew );
            SYSTEM.DEBUG('After: Req is '+requests+' and size '+requests.size());            
        }*/
        /*for( CRWrapper cr : resultWrapper )
        {
            if(cr.selectedCR)
            {
                Change_Request__c crNew = cr.changeRequest.clone( true, true );
                crNew.PCN_Number__c = crNew.PCN_Number__c.replaceAll( '-', '' );
                
                requests.add( crNew );
                SYSTEM.DEBUG('===>Selected are: After: Req is '+requests+' and size '+requests.size());            
            }
        }*/
        
        for(Change_Request__c cc: requests)
            {
                System.debug(' The Imp Ass are : '+cc.Impact_Assessors__r);
            }
        String url = '/apex/ChangeLogReportExport'; 
               
        PageReference detailPage = new PageReference( url ); 
        return detailPage;
    }
    public void preExport(Boolean exportable)
    {
        ApexPages.getMessages().clear();               
        if(!showExport)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please run report and select one or more Change Requests to export');
            ApexPages.addMessage(myMsg);
            //return null;
        }
        
        for( CRWrapper cr : resultWrapper )
        {
            if(cr.selectedCR)
            {
                exportable=true; 
                Change_Request__c crNew = cr.changeRequest.clone( true, true );
                crNew.PCN_Number__c = crNew.PCN_Number__c.replaceAll( '-', '' );
                
                requests.add( crNew );
                SYSTEM.DEBUG('===>Selected are: After: Req is '+requests+' and size '+requests.size());            
            }
        }
    }
    public PageReference exportHeader()
    {
        Boolean exportable = false;        
        //preExport(exportable );        
        ApexPages.getMessages().clear();               
        if(!showExport)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please run report and select one or more Change Requests to export');
            ApexPages.addMessage(myMsg);
            //return null;
        }
        
        for( CRWrapper cr : resultWrapper )
        {
            if(cr.selectedCR) 
            {
                exportable=true; 
                Change_Request__c crNew = cr.changeRequest.clone( true, true );
                crNew.PCN_Number__c = crNew.PCN_Number__c.replaceAll( '-', '' );
                
                requests.add( crNew );
                SYSTEM.DEBUG('===>Selected are: After: Req is '+requests+' and size '+requests.size());            
            }
        }
        for(Change_Request__c cc: requests)
            {
                System.debug(' The Imp Ass are : '+cc.Impact_Assessors__r);
            }
        String url = '/apex/ChangeLogReport_ExportHeader'; 
               
        PageReference detailPage = new PageReference( url ); 
        if( exportable && requests.size()>0)
        {
            detailPage = new PageReference( url ); 
            //return detailPage;
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select one or more Change Requests to export');
            ApexPages.addMessage(myMsg);
            detailPage =null;            
        }
        
        return detailPage;
    }
    public PageReference exportDetails()
    {
        Boolean exportable = false;        
        //preExport(exportable );        
        ApexPages.getMessages().clear();               
        if(!showExport)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please run report and select one or more Change Requests to export');
            ApexPages.addMessage(myMsg);
            //return null;
        }
        
        for( CRWrapper cr : resultWrapper )
        {
            if(cr.selectedCR) 
            {
                exportable=true; 
                Change_Request__c crNew = cr.changeRequest.clone( true, true );
                crNew.PCN_Number__c = crNew.PCN_Number__c.replaceAll( '-', '' );
                
                requests.add( crNew );
                SYSTEM.DEBUG('===>Selected are: After: Req is '+requests+' and size '+requests.size());            
            }
        }        
        //preExport(exportable );        
        for(Change_Request__c cc: requests)
            {
                System.debug(' The Imp Ass are : '+cc.Impact_Assessors__r);
            }
        String url = '/apex/ChangeLogReport_ExportDetails'; 
               
        PageReference detailPage = new PageReference( url ); 
        if( exportable && requests.size()>0)
        {
            detailPage = new PageReference( url ); 
            //return detailPage;
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select one or more Change Requests to export');
            ApexPages.addMessage(myMsg);
            detailPage =null;            
        }
        
        return detailPage;
    }
    
    public String SortField {
        get { 
            if (SortField == null) 
            {
                SortField = 'CreatedDate'; 
            } 
            return SortField;  
        }
        set; 
    } 
    
    public String SortDirection {
        get { if (SortDirection == null) {  SortDirection = 'desc'; } return SortDirection;  }
        set;
    }     
    
    public void SortToggle() {
        SortDirection = SortDirection.equals('asc') ? 'desc NULLS LAST' : 'asc';
        // reset alpha filter and sort sequence when sorted field is changed
        if (SortFieldSave != SortField) {
            SortDirection = 'asc';
        
            SortFieldSave = SortField;
        }
        // run the query again
  
        RunReport();
    }
    public void savecR()
    {
        System.debug('Entering savecR method');
        resultUpdate = new List<Change_Request__c>();
        locked = new List<Boolean>();
        for(Change_Request__c c: result)
        {
            if(c.Change_Status__c=='Approved')
            {
                Boolean b = Approval.isLocked(c);
                locked.add(b);
                if(b)
                    Approval.unlock(c);
                resultUpdate.add(c);
            }            
        }
        update resultUpdate;
        initializeToZero();
        for(Change_Request__c changeReq : resultUpdate)
        {
            setValues(changeReq);
        }
        setDecimals();
       //update resultUpdate;
       System.debug('ExitingsavecR method');
    } 
    public class CRWrapper             
    {    
        public Change_Request__c changeRequest {get;set;}
        public Boolean selectedCR {get;set;}
        public CRWrapper()
        {
            changeRequest = null;
            selectedCR = false;
        }
        CRWrapper(Change_Request__c changeRequestRecord)
        {
            changeRequest = changeRequestRecord;
            selectedCR = false;
        }
    }
    
}