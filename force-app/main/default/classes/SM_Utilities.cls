public class SM_Utilities
{
    // ************************************************************************ //
    // ** THIS CLASS CONTAINS UTILITY FUNCTIONS AND DATA FOR SAND MANAGEMENT ** //
    // ************************************************************************ //

    //////////////////////////
    // * DEFINE CONSTANTS * //
    //////////////////////////

    private static final String QUERY_LOCATION_LIST = 'SELECT Id, Name, Cost_Center__c, Senior__c, Well_Orientation__c, Current_Well_Status__c, Status__c, Unit_Configuration__c, Hazardous_H2S__c, Surface_Location__c, Operating_Field_AMU__c, Operating_Field_AMU__r.Operating_District__c, Operating_Field_AMU__r.Operating_District__r.Business_Unit__c, Route__r.Field_Senior__r.FirstName, Route__r.Field_Senior__r.LastName, Route__r.Field_Senior__r.Id, (SELECT Id, Name from Sand_Run_Sheets__r WHERE Date__c = TODAY LIMIT 1), PVR_AVGVOL_5D_SAND__c, PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_WATER__c, PVR_AVGVOL_30D_SAND__c FROM Location__c WHERE Functional_Location_Category__c in :FUNCTIONAL_LOCATION_CATEGORY AND Status__c not in :WELL_EXCLUDED_STATUSES AND Route__c = \'!!ROUTENUM!!\' AND Well_Type__c in :WELL_TYPE ORDER BY Status__c, Name LIMIT 1000';

    public static final String PROFIlE_SYSTEM_ADMIN = 'System Administrator';
    public static final String PROFILE_HOG_ADMIN = 'Standard HOG - Administrator';
    public static final String PERMISSION_SET_SAND_LEAD = 'HOG_Sand_Management_Lead';
    public static final String NOT_SELECTED = 'na';
    public static final String LOCATION_INDEX_ALL_WELLS = 'all_wells';
    public static final String LOCATION_INDEX_SELECTED_WELLS = 'selected_wells';
    public static final String LOCATION_INDEX_LOCATION_LIST = 'location_list';

    public static final String SANDSTING_STATUS_BOOKED = 'Booked';
    public static final String SANDSTING_STATUS_CANCELLED = 'Cancelled';
    public static final String SANDSTING_STATUS_COMPLETE = 'Complete';
    public static final String SANDSTING_STATUS_IN_PROGRESS = 'In Progress';
    public static final String SANDSTING_STATUS_NEW = 'New';
    public static final String SANDSTING_STATUS_NO_LOAD = 'No Load';
    public static final String SANDSTING_STATUS_ON_HOLD = 'On-hold';
    public static final String SANDSTING_STATUS_CLOSED = 'Closed';

    public static final String[] WELL_EXCLUDED_STATUSES = new String[] {'ABAN', 'RTD', 'CTCP'};
    public static final String[] WELL_TYPE = new String[]{'OIL', 'OIL-HVY'};
    public static final Integer[] FUNCTIONAL_LOCATION_CATEGORY = new Integer[]{4};
    public static final Integer NO_RESULT = 0;
    public static final Integer SINGLE_RESULT = 1;
    public static final Integer MAX_TANK_NUMBER = 3;

    public static Boolean bExecuteTriggerCode = true;

    ////////////////////////////////////////////////////////////
    // * BUILD LOCATION MAPS BASED ON ROUTE NUMBER PROVIDED * //
    ////////////////////////////////////////////////////////////

    public static Map<String, List<SelectOption>> BuildLocationMaps(List<String> lSelectedWell, Set<String> setWells, String sRouteNumber)
    {
        List<Location__c> lLocations = (sRouteNumber != NOT_SELECTED) ? GetLocationList(sRouteNumber) : new List<Location__c>();
        List<SelectOption> lLocationOptions = new List<SelectOption>();
        List<SelectOption> lSelectedWells = new List<SelectOption>();
        List<SelectOption> lAllWells = new List<SelectOption>();

        Map<String, List<SelectOption>> mapFinal = new Map<String, List<SelectOption>>();
        Map<Id, Location__c> mapLocations = GetLocationMap(sRouteNumber);

        setWells = (setWells == null) ? new Set<String>() : setWells;
        Location__c oOneLocation;
        SelectOption oOneOption;

        String sOptionId;
        String sOptionName;

        for(String sOneWellId : lSelectedWell)
        {
            oOneLocation = mapLocations.get(sOneWellId);

            if(oOneLocation != null)
            {
                sOptionId = oOneLocation.Id;
                sOptionName = oOneLocation.Name;

                oOneOption = new SelectOption(sOptionId, sOptionName);
                lSelectedWells.add(oOneOption);

                sOptionName = (oOneLocation.Sand_Run_Sheets__r.size() > NO_RESULT) ? String.format('{0}~{1} ✓', new String[]{'Favorite', oOneLocation.Name}) : String.format('{0}~{1}', new String[]{'Favorite', oOneLocation.Name});
                oOneOption.setLabel(sOptionName);

                lLocationOptions.add(oOneOption);
            }
        }

        for(Location__c oSingleLocation : lLocations)
        {
            if(!setWells.contains(oSingleLocation.Id))
            {
                sOptionId = oSingleLocation.Id;
                sOptionName = oSingleLocation.Name;

                oOneOption = new SelectOption(sOptionId, sOptionName);
                lAllWells.add(oOneOption);

                sOptionName = (oSingleLocation.Sand_Run_Sheets__r.size() > NO_RESULT) ? String.format('{0}~{1} ✓', new String[]{oSingleLocation.Status__c, oSingleLocation.Name}) : String.format('{0}~{1}', new String[]{oSingleLocation.Status__c, oSingleLocation.Name});
                oOneOption.setLabel(sOptionName);

                lLocationOptions.add(oOneOption);
            }
        }

        // BUILD FULL MAP
        mapFinal.put(LOCATION_INDEX_ALL_WELLS, lAllWells);
        mapFinal.put(LOCATION_INDEX_SELECTED_WELLS, lSelectedWells);
        mapFinal.put(LOCATION_INDEX_LOCATION_LIST, lLocationOptions);

        return mapFinal;
    }

    //////////////////////////////////////////////////////////////
    // * RETURN QUERY LIST RESULT FOR ALL LOCATIONS PER ROUTE * //
    //////////////////////////////////////////////////////////////

    public static List<Location__c> GetLocationList(String sRouteNumber)
    {
        String sQuery = QUERY_LOCATION_LIST.replace('!!ROUTENUM!!', sRouteNumber);
        return (sRouteNumber != NOT_SELECTED) ? Database.query(sQuery) : new List<Location__c>();
    }

    /////////////////////////////////
    // * RETURN MAP OF LOCATIONS * //
    /////////////////////////////////

    public static Map<Id, Location__c> GetLocationMap(String sRouteNumber)
    {
        return (sRouteNumber != NOT_SELECTED) ? new Map<Id, Location__c>(GetLocationList(sRouteNumber)) : new Map<Id, Location__c>();
    }

    ///////////////////////////////////////////////////////////
    // * BUILD PICKLIST FOR SAND CHILD OBJECT STATUS FIELD * //
    ///////////////////////////////////////////////////////////

    public static List<SelectOption> GetSandDetailStatusPickList()
    {
        List<SelectOption> lOptions = new List<SelectOption>();
        Schema.DescribeFieldResult oFields = SM_Run_Sheet_Detail__c.Status__c.getDescribe();

        List<Schema.PicklistEntry> oPickList = oFields.getPicklistValues();
        SelectOption oOneOption;

        for(Schema.PicklistEntry oOneEntry : oPickList)
        {
            oOneOption = new SelectOption(oOneEntry.getLabel(), oOneEntry.getValue());

            // Users cannot select NEW / BOOKED / COMPLETE -> disable those entries
            if(oOneEntry.getValue().toLowerCase() == SANDSTING_STATUS_NEW.toLowerCase()
                /*|| oOneEntry.getValue().toLowerCase() == SANDSTING_STATUS_BOOKED.toLowerCase()*/)
            {
                oOneOption.setDisabled(true);
            }

            lOptions.add(oOneOption);
        }

        return lOptions;
    }

    ///////////////////////////////////////////////
    // * TRIGGER HANDLER FOR SAND CHILD OBJECT * //
    ///////////////////////////////////////////////

    public static void TriggerBeforeUpdateSandDetail(List<SM_Run_Sheet_Detail__c> lNewList, Map<Id, SM_Run_Sheet_Detail__c> mapOldList)
    {
        SM_Run_Sheet_Detail__c oOldItem;

        String sOneStatus;
        Boolean bStatusChanged;

        system.debug(lNewList);
        system.debug('TriggerBeforeUpdateSandDetail executing...');
        for(SM_Run_Sheet_Detail__c oOneItem : lNewList)
        {
            if(oOneItem.Trigger_Bypass__c == false)
            {
                oOldItem = mapOldList.get(oOneItem.Id);

                system.debug('Id: ' + oOneItem.Id + ' Old Status: ' +oOldItem.Status__c + ' New Status: ' +oOneItem.Status__c);

                if( oOneItem.Status__c != SANDSTING_STATUS_ON_HOLD &&
                    oOneItem.Status__c != SANDSTING_STATUS_CANCELLED)
                {
                    //else if(oOneItem.Vendor_Count__c > 0 && oOldItem.Status__c == SANDSTING_STATUS_NEW)
                    //{
                    //  oOneItem.Status__c = SANDSTING_STATUS_BOOKED;
                    //}
                    /*
                    if((oOneItem.Status__c == SANDSTING_STATUS_BOOKED || oOneItem.Status__c == SANDSTING_STATUS_IN_PROGRESS || oOneItem.Status__c == SANDSTING_STATUS_COMPLETE || oOneItem.Status__c == SANDSTING_STATUS_CLOSED || oOneItem.Status__c == SANDSTING_STATUS_NO_LOAD) &&
                        (oOneItem.Primary_Disposal_Facility__c == null ||
                            oOneItem.Vendor_Date_given_out__c == null ||
                            oOneItem.Primary_Vendor__c ==null))
                    */
                    //mz: 20.6.2016 change as Picklist Manager is beeing to be used for primary disposal external and primary vendor
                    if((oOneItem.Status__c == SANDSTING_STATUS_BOOKED || oOneItem.Status__c == SANDSTING_STATUS_IN_PROGRESS || oOneItem.Status__c == SANDSTING_STATUS_COMPLETE || oOneItem.Status__c == SANDSTING_STATUS_CLOSED || oOneItem.Status__c == SANDSTING_STATUS_NO_LOAD) &&
                        ((oOneItem.Primary_Disposal_Facility__c == null && oOneItem.Primary_Disposal_Facility_Ext__c == null && oOneItem.Disposal_Destination__c == null) ||
                          oOneItem.Vendor_Date_given_out__c == null ||
                         (oOneItem.Primary_Vendor_PM__c == null && oOneItem.Primary_Vendor__c == null)))
                    {
                        oOneItem.AddError('Status cannot be set to \'' +  oOneItem.Status__c + '\' if Primary Vendor, Primary Disposal and Vendor Date given out are not provided.');

                    }


                    else if(oOneItem.Primary_Vendor_PM__c !=null
                        && (oOneItem.Primary_Disposal_Facility__c !=null || oOneItem.Primary_Disposal_Facility_Ext__c != null)
                        && oOneItem.Vendor_Date_given_out__c != null
                        && oOneItem.Status__c == SANDSTING_STATUS_NEW && oOldItem.Status__c == SANDSTING_STATUS_NEW)
                    {
                        oOneItem.Status__c = SANDSTING_STATUS_BOOKED;
                        system.debug('Changing status to Booked');
                    }

                }
            }
        }
    }

    /////////////////////////////////////////////
    // * WRAPPER CLASS FOR SAND CHILD OBJECT * //
    /////////////////////////////////////////////

    public class SM_Tank_Wrapper
    {
        public SM_Run_Sheet_Detail__c oTankData;
        public Boolean bSelected;
    }

    ///////////////////////////////////////////////////////////
    // * CHECK USERS PERMISSION SET -> DETERMINE IF EXISTS * //
    ///////////////////////////////////////////////////////////

    public static Boolean VerifyPermissions(String sUserId, String sPermissionSet)
    {
        List<PermissionSetAssignment> lPermissions = [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :sUserId AND PermissionSet.Name = :sPermissionSet];
        return (lPermissions.size() == SINGLE_RESULT) ? true : false;
    }

    ///////////////////////////////////////
    // * CHECK IF USER MATCHES PROFILE * //
    ///////////////////////////////////////

    public static Boolean VerifyProfile(String sProfileName)
    {
        Profile lprofile = [SELECT Id FROM Profile Where Name =: sProfileName LIMIT 1];
        return UserInfo.getProfileId() == lprofile.Id;
    }

    /////////////////////////
    // * CUSTOM REDIRECT * //
    /////////////////////////

    public static PageReference SandRedirect(String sNewLocation, Boolean bSetRedirect)
    {
        PageReference oPage = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + sNewLocation);
        oPage.setRedirect(bSetRedirect);

        return oPage;
    }
}